"use strict";
exports.__esModule = true;
exports.config = void 0;
var dotenv_1 = require("dotenv");
dotenv_1["default"].config();
exports.config = {
    serviceName: process.env.SERVICE_NAME || '',
    port: Number(process.env.PORT) || 5000,
    loggerLevel: 'debug',
    db: {
        database: process.env.DATABASE || ''
    }
};
//# sourceMappingURL=index.js.map
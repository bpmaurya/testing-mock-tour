"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = void 0;
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
exports.config = {
    serviceName: process.env.SERVICE_NAME || '',
    port: Number(process.env.PORT) || 5000,
    loggerLevel: 'debug',
    jwt_secret: process.env.JWT_SECRET || '',
    JWT_MAX_AGE: process.env.JWT_MAX_AGE || '',
    ORIGIN_ACCESS: process.env.ORIGIN_ACCESS || '*',
    db: {
        database: process.env.DATABASE || '',
    }
};
//# sourceMappingURL=index.js.map
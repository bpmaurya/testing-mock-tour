"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WingController = void 0;
const constant_1 = require("../../utils/constant");
const logger_1 = require("../../utils/logger");
const wing_validation_1 = require("../validations/wing.validation");
const wing_model_1 = __importDefault(require("../models/wing.model"));
class WingController {
    constructor() {
        this.createWing = async (req, res, next) => {
            try {
                const wingValidation = await wing_validation_1.CreateWingValidation.validateAsync(req.body);
                if (!wingValidation) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
                else {
                    try {
                        const foundWing = await wing_model_1.default.findOne({ name: req.body.name, location: req.body.location, floor: req.body.floor });
                        if (foundWing) {
                            return res.status(400).send({
                                message: `Wing ${req.body.name} not available! Already exist!`
                            });
                        }
                        else {
                            const wing = new wing_model_1.default({
                                name: req.body.name,
                                location: req.body.location,
                                floor: req.body.floor
                            });
                            await wing.save();
                            return res.status(200).send({ message: constant_1.Messages.WING_CREATE_SUCCESS });
                        }
                    }
                    catch (error) {
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
            }
            catch (err) {
                logger_1.logger.info(err.message);
                logger_1.logger.info(err);
                return res.status(400).send({ message: err.message });
            }
        };
        this.deleteWing = async (req, res, next) => {
            try {
                const idValidation = await wing_validation_1.WingIdValidation.validateAsync(req.body.id);
                if (!idValidation) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
                else {
                    try {
                        const foundWing = await wing_model_1.default.findOne({ _id: req.body.id });
                        if (!foundWing) {
                            return res.status(400).send({
                                message: `Wing not exist!!`
                            });
                        }
                        else {
                            const deleteWing = await wing_model_1.default.deleteOne({ _id: idValidation });
                            if (deleteWing)
                                return res.status(200).send({ message: constant_1.Messages.WING_DELETE_SUCCESS });
                            else {
                                return next(res.status(400).json({
                                    message: constant_1.Messages.INVALID_DETAIL
                                }));
                            }
                        }
                    }
                    catch (error) {
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
            }
            catch (err) {
                console.log(err.message);
                console.log(err);
                return res.status(400).send({ message: err.message });
            }
        };
        this.getAllWing = async (req, res, next) => {
            try {
                const allWing = await wing_model_1.default.find({ location: (req.body.location || { $exists: true }), floor: (req.body.floor || { $exists: true }) }).populate(['floor', 'location']);
                if (allWing)
                    return res.status(200).send({ "allWing": allWing });
            }
            catch (error) {
                return res.status(400).send(error);
            }
        };
        this.getAllWingForLocationAndFloor = async (req, res, next) => {
            try {
                const allWing = await wing_model_1.default.find({ location: (req.params.location || { $exists: true }), floor: (req.params.floor || { $exists: true }) }).populate(['floor', 'location']);
                if (allWing)
                    return res.status(200).send({ "allWing": allWing });
            }
            catch (error) {
                return res.status(400).send(error);
            }
        };
    }
    async updateWing(req, res, next) {
        try {
            logger_1.logger.info(JSON.stringify(req.body));
            const wingValidation = await wing_validation_1.UpdateWingValidation.validateAsync(req.body);
            if (!wingValidation) {
                return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
            }
            else {
                try {
                    const isWingValid = await wing_model_1.default.findOne({
                        _id: wingValidation.id
                    });
                    if (!isWingValid) {
                        return res.status(400).send({
                            message: constant_1.Messages.INVALID_DETAIL
                        });
                    }
                    else {
                        const updateWing = await wing_model_1.default.findOneAndUpdate({
                            _id: isWingValid.id
                        }, {
                            $set: {
                                name: req.body.name,
                                location: req.body.location,
                                floor: req.body.floor,
                                status: req.body.status
                            }
                        });
                        if (updateWing)
                            return res.status(200).send({ message: constant_1.Messages.WING_UPDATE_SUCCESS, });
                        else {
                            return next(res.status(400).json({
                                message: constant_1.Messages.INVALID_DETAIL
                            }));
                        }
                    }
                }
                catch (error) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
            }
        }
        catch (err) {
            console.log(err.message);
            console.log(err);
            return res.status(400).send({ message: err.message });
        }
    }
}
exports.WingController = WingController;
//# sourceMappingURL=wing.controller.js.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RequestController = void 0;
const constant_1 = require("../../utils/constant");
const logger_1 = require("../../utils/logger");
const notification_model_1 = __importDefault(require("../models/notification.model"));
const request_model_1 = __importDefault(require("../models/request.model"));
const seats_model_1 = __importDefault(require("../models/seats.model"));
const request_validation_1 = require("../validations/request.validation");
class RequestController {
    constructor() {
        this.createRequest = async (req, res, next) => {
            var io = req.app.get('socketIO');
            try {
                const requestValidation = await request_validation_1.CreateRequestValidation.validateAsync(req.body);
                if (!requestValidation) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
                else {
                    try {
                        const foundDeskRequest = await request_model_1.default.find({ user: req.body.user, seat: req.body.seat });
                        if (foundDeskRequest.length !== 0) {
                            return res.status(400).send({
                                message: `This request not available! Already exist!`
                            });
                        }
                        else {
                            const deskRequest = new request_model_1.default({
                                user: req.body.user,
                                reportingManager: req.body.reportingManager,
                                project: req.body.project,
                                seat: req.body.seat,
                                description: req.body.description,
                                priority: req.body.priority,
                                status: req.body.status
                            });
                            await deskRequest.save();
                            const bookSeat = await seats_model_1.default.findOneAndUpdate({
                                _id: req.body.seat
                            }, {
                                $set: {
                                    bookedBy: req.body.user,
                                    isSelectable: false
                                }
                            });
                            if (bookSeat) {
                                io.emit('update-message', { seat: req.body.seat, selectable: false });
                                return res.status(200).send({ message: constant_1.Messages.DESK_REQUEST_CREATE_SUCCESS });
                            }
                            else {
                                return next(res.status(400).json({
                                    message: constant_1.Messages.INVALID_DETAIL
                                }));
                            }
                        }
                    }
                    catch (error) {
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
            }
            catch (err) {
                logger_1.logger.info(err.message);
                logger_1.logger.info(err);
                return res.status(400).send({ message: err.message });
            }
        };
        this.deleteDeskRequest = async (req, res, next) => {
            try {
                const idValidation = await request_validation_1.RequestIdValidation.validateAsync(req.body.id);
                if (!idValidation) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
                else {
                    try {
                        const foundWing = await request_model_1.default.findOne({ _id: req.body.id });
                        if (!foundWing) {
                            return res.status(400).send({
                                message: `Request not exist!!`
                            });
                        }
                        else {
                            const deleteRequest = await request_model_1.default.deleteOne({ _id: idValidation });
                            if (deleteRequest)
                                return res.status(200).send({ message: constant_1.Messages.DESK_REQUEST_DELETE_SUCCESS });
                            else {
                                return next(res.status(400).json({
                                    message: constant_1.Messages.INVALID_DETAIL
                                }));
                            }
                        }
                    }
                    catch (error) {
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
            }
            catch (err) {
                console.log(err.message);
                console.log(err);
                return res.status(400).send({ message: err.message });
            }
        };
        this.getAllDeskRequests = async (req, res, next) => {
            try {
                const allRequests = await request_model_1.default.find({}).populate([
                    {
                        path: 'user',
                        model: 'User',
                        select: '-password'
                    }, {
                        path: 'seat',
                        model: 'Seat',
                        populate: [{
                                path: 'location',
                                model: 'Location'
                            }, {
                                path: 'floor',
                                model: "Floor"
                            }, {
                                path: 'wing',
                                model: "Wing"
                            }]
                    }, {
                        path: 'project',
                        model: 'Project'
                    }, {
                        path: 'reportingManager',
                        model: 'User',
                        select: '-password'
                    }
                ]);
                if (allRequests)
                    return res.status(200).send({ "allRequest": allRequests });
            }
            catch (error) {
                return res.status(400).send(error);
            }
        };
        this.getAllDeskRequestsForUser = async (req, res, next) => {
            try {
                const allRequests = await request_model_1.default.find({ user: req.params.user }).populate([
                    {
                        path: 'user',
                        model: 'User',
                        select: '-password'
                    }, {
                        path: 'seat',
                        model: 'Seat',
                        populate: [{
                                path: 'location',
                                model: 'Location'
                            }, {
                                path: 'floor',
                                model: "Floor"
                            }, {
                                path: 'wing',
                                model: "Wing"
                            }]
                    }, {
                        path: 'project',
                        model: 'Project'
                    }, {
                        path: 'reportingManager',
                        model: 'User',
                        select: '-password'
                    }
                ]);
                if (allRequests)
                    return res.status(200).send({ "allRequest": allRequests });
            }
            catch (error) {
                return res.status(400).send(error);
            }
        };
    }
    async updateDeskRequest(req, res, next) {
        var io = req.app.get('socketIO');
        try {
            logger_1.logger.info(JSON.stringify(req.body));
            const reqValidation = await request_validation_1.UpdateRequestValidation.validateAsync(req.body);
            if (!reqValidation) {
                return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
            }
            else {
                try {
                    const isRequestValid = await request_model_1.default.findOne({
                        _id: reqValidation._id
                    });
                    if (!isRequestValid) {
                        return res.status(400).send({
                            message: constant_1.Messages.INVALID_DETAIL
                        });
                    }
                    else {
                        const updateRequest = await request_model_1.default.findOneAndUpdate({
                            _id: isRequestValid.id
                        }, {
                            $set: {
                                status: req.body.status
                            }
                        });
                        if (updateRequest) {
                            if (req.body.status === 'reject') {
                                const update = await seats_model_1.default.findOneAndUpdate({
                                    _id: req.body.seat
                                }, {
                                    $unset: {
                                        bookedBy: "",
                                        isSelectable: ''
                                    }
                                });
                                const findNotification = await notification_model_1.default.find({ user: req.body.user, request: isRequestValid.id });
                                if (findNotification.length === 0) {
                                    const notificationCreate = await notification_model_1.default.create({
                                        user: req.body.user,
                                        request: isRequestValid.id,
                                        isRead: false,
                                    }).then(e => e.populate([{
                                            path: 'user',
                                            model: 'User',
                                            select: '-password',
                                            populate: [{
                                                    path: 'project',
                                                    model: 'Project'
                                                }]
                                        }, {
                                            path: 'request',
                                            model: 'Request',
                                            populate: {
                                                path: 'seat',
                                                model: 'Seat',
                                                populate: [{
                                                        path: 'location',
                                                        model: 'Location'
                                                    }, {
                                                        path: 'floor',
                                                        model: "Floor"
                                                    }, {
                                                        path: 'wing',
                                                        model: "Wing"
                                                    }]
                                            }
                                        }]));
                                    io.emit('message', notificationCreate);
                                    return res.status(200).send({ message: constant_1.Messages.REQUEST_UPDATE_SUCCESS, });
                                }
                                if (update) {
                                    return res.status(200).send({ message: constant_1.Messages.REQUEST_UPDATE_SUCCESS, });
                                }
                            }
                            else if (req.body.status === 'approved') {
                                const update = await seats_model_1.default.findOneAndUpdate({
                                    _id: req.body.seat
                                }, {
                                    $set: {
                                        bookedBy: req.body.user,
                                        isSelectable: false
                                    }
                                });
                                const findNotification = await notification_model_1.default.find({ user: req.body.user, request: isRequestValid.id });
                                if (findNotification.length === 0) {
                                    const notificationCreate = await notification_model_1.default.create({
                                        user: req.body.user,
                                        request: isRequestValid.id,
                                        isRead: false,
                                    }).then(e => e.populate([{
                                            path: 'user',
                                            model: 'User',
                                            select: '-password',
                                            populate: [{
                                                    path: 'project',
                                                    model: 'Project'
                                                }]
                                        }, {
                                            path: 'request',
                                            model: 'Request',
                                            populate: {
                                                path: 'seat',
                                                model: 'Seat',
                                                populate: [{
                                                        path: 'location',
                                                        model: 'Location'
                                                    }, {
                                                        path: 'floor',
                                                        model: "Floor"
                                                    }, {
                                                        path: 'wing',
                                                        model: "Wing"
                                                    }]
                                            }
                                        }]));
                                    io.emit('message', notificationCreate);
                                    return res.status(200).send({ message: constant_1.Messages.REQUEST_UPDATE_SUCCESS, });
                                }
                                if (update) {
                                    return res.status(200).send({ message: constant_1.Messages.REQUEST_UPDATE_SUCCESS, });
                                }
                            }
                            else
                                return res.status(200).send({ message: constant_1.Messages.REQUEST_UPDATE_SUCCESS, });
                        }
                        else {
                            return next(res.status(400).json({
                                message: constant_1.Messages.INVALID_DETAIL
                            }));
                        }
                    }
                }
                catch (error) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
            }
        }
        catch (err) {
            console.log(err.message);
            console.log(err);
            return res.status(400).send({ message: err.message });
        }
    }
}
exports.RequestController = RequestController;
//# sourceMappingURL=request.controller.js.map
"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const bcrypt_nodejs_1 = __importDefault(require("bcrypt-nodejs"));
const jwt = __importStar(require("jsonwebtoken"));
const config_1 = require("../../config");
const logger_1 = require("../../utils/logger");
const user_model_1 = __importDefault(require("../models/user.model"));
const user_validation_1 = require("../validations/user.validation");
const constant_1 = require("../../utils/constant");
class UserController {
    async registerUser(req, res, next) {
        try {
            const userValidation = await user_validation_1.UserRegistrationValidation.validateAsync(req.body);
            if (!userValidation) {
                return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
            }
            else {
                try {
                    const foundUser = await user_model_1.default.findOne({ email: req.body.email });
                    if (foundUser) {
                        return res.status(400).send({
                            message: `Username ${req.body.email} not available! Already exist!`
                        });
                    }
                    else {
                        const hashedPassword = bcrypt_nodejs_1.default.hashSync(req.body.password, bcrypt_nodejs_1.default.genSaltSync(10));
                        const user = new user_model_1.default({
                            email: req.body.email,
                            firstName: req.body.firstName,
                            middleName: req.body.middleName,
                            lastName: req.body.lastName,
                            pastExperience: req.body.pastExperience,
                            domain: req.body.domain,
                            department: req.body.department,
                            tempPassword: hashedPassword,
                            role: req.body.role,
                            position: req.body.position,
                            project: req.body.project,
                            phone: req.body.phone,
                        });
                        await user.save();
                        return res.status(200).send({ message: constant_1.Messages.USER_REGISTER_SUCCESS });
                    }
                }
                catch (error) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
            }
        }
        catch (err) {
            logger_1.logger.info(err.message);
            logger_1.logger.info(err);
            return res.status(400).send({ message: err.message });
        }
    }
    async updateUserPassword(req, res, next) {
        try {
            const userValidation = await user_validation_1.UserIdValidation.validateAsync(req.params.user_id);
            if (!userValidation) {
                return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
            }
            else {
                try {
                    const isUsernameValid = await user_model_1.default.findOne({
                        _id: userValidation
                    });
                    if (!isUsernameValid) {
                        return res.status(400).send({
                            message: `Username ${req.params.user_id} not valid!`
                        });
                    }
                    else {
                        const user = await user_model_1.default.findOne(({ _id: isUsernameValid._id }));
                        if (user) {
                            const currentPassword = await bcrypt_nodejs_1.default.compareSync(req.body.currentPass, user.tempPassword);
                            if (!currentPassword) {
                                return res.status(400).send({
                                    message: `Current Password not valid!`
                                });
                            }
                            else {
                                const updateUser = await user_model_1.default.updateOne({
                                    _id: isUsernameValid._id
                                }, {
                                    $set: {
                                        password: bcrypt_nodejs_1.default.hashSync(req.body.password, bcrypt_nodejs_1.default.genSaltSync(10))
                                    }
                                });
                                if (updateUser) {
                                    await user_model_1.default.updateOne({
                                        _id: isUsernameValid._id
                                    }, {
                                        $unset: {
                                            tempPassword: ''
                                        }
                                    });
                                    return res.status(200).send({ message: constant_1.Messages.USER_UPDATE_SUCCESS });
                                }
                                else {
                                    return next(res.status(400).json({
                                        message: constant_1.Messages.INVALID_DETAIL
                                    }));
                                }
                            }
                        }
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
                catch (error) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
            }
        }
        catch (err) {
            logger_1.logger.info(err.message);
            logger_1.logger.info(err);
            return res.status(400).send({ message: err.message });
        }
    }
    async updateUser(req, res, next) {
        try {
            const userValidation = await user_validation_1.UserUpdateValidation.validateAsync(req.body);
            if (!userValidation) {
                return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
            }
            else {
                try {
                    const isUsernameValid = await user_model_1.default.findOne({
                        email: userValidation.email
                    });
                    if (!isUsernameValid) {
                        return res.status(400).send({
                            message: `Username ${req.body.email} not valid!`
                        });
                    }
                    else {
                        const updateUser = await user_model_1.default.updateOne({
                            _id: isUsernameValid._id
                        }, {
                            $set: {
                                email: req.body.email,
                                firstName: req.body.firstName,
                                middleName: req.body.middleName,
                                lastName: req.body.lastName,
                                pastExperience: req.body.pastExperience,
                                domain: req.body.domain,
                                department: req.body.department,
                                role: req.body.role,
                                position: req.body.position,
                                project: req.body.project,
                                phone: req.body.phone,
                            }
                        });
                        if (updateUser)
                            return res.status(200).send({ message: constant_1.Messages.USER_UPDATE_SUCCESS });
                        else {
                            return next(res.status(400).json({
                                message: constant_1.Messages.INVALID_DETAIL
                            }));
                        }
                    }
                }
                catch (error) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
            }
        }
        catch (err) {
            logger_1.logger.info(err.message);
            logger_1.logger.info(err);
            return res.status(400).send({ message: err.message });
        }
    }
    async updateProfilePictureUser(req, res, next) {
        try {
            let buffer = req.file?.buffer;
            const userValidation = await user_validation_1.UserIdValidation.validateAsync(req.params.user_id);
            if (!userValidation) {
                return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
            }
            else {
                try {
                    const isUsernameValid = await user_model_1.default.findOne({
                        _id: userValidation
                    });
                    if (!isUsernameValid || !buffer) {
                        return res.status(400).send({
                            message: `Username ${req.params.user_id} not valid!`
                        });
                    }
                    else {
                        const updateUser = await user_model_1.default.updateOne({
                            _id: isUsernameValid._id
                        }, {
                            $set: {
                                avatar: buffer
                            }
                        });
                        if (updateUser)
                            return res.status(200).send({ message: constant_1.Messages.USER_UPDATE_SUCCESS });
                        else {
                            return next(res.status(400).json({
                                message: constant_1.Messages.INVALID_DETAIL
                            }));
                        }
                    }
                }
                catch (error) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
            }
        }
        catch (err) {
            logger_1.logger.info(err.message);
            logger_1.logger.info(err);
            return res.status(400).send({ message: err.message });
        }
    }
    async authenticateUser(req, res, next) {
        logger_1.logger.info(req.body.email);
        const foundUser = await user_model_1.default.findOne({ email: req.body.email });
        if (!foundUser)
            return res.status(400).send({ message: "This user does not exist" });
        try {
            if (foundUser.tempPassword) {
                const matchTempPass = await bcrypt_nodejs_1.default.compareSync(req.body.password, foundUser.tempPassword);
                if (!matchTempPass)
                    return res.status(400).send({ message: "Incorrect Password!!" });
                const token = await jwt.sign({ _id: foundUser._id }, config_1.config.jwt_secret);
                let user = await user_model_1.default.findOne({ email: req.body.email }).select(["-password", "-tempPassword"]).populate([{
                        path: 'position',
                        model: 'Designation'
                    }, { path: 'project', model: 'Project' },
                    { path: 'department', model: 'Department' },
                    { path: 'avatar' }
                ]);
                return res.status(200).header("auth-token", token).send({
                    "accessToken": token,
                    "refreshToken": token,
                    userId: foundUser._id,
                    role: foundUser.role,
                    name: `${foundUser.firstName} ${foundUser.middleName} ${foundUser.lastName}`,
                    email: foundUser.email,
                    userObject: user,
                    "jwt": token,
                    "_id": foundUser._id
                });
            }
            else {
                const isMatch = await bcrypt_nodejs_1.default.compareSync(req.body.password, foundUser.password);
                if (!isMatch)
                    return res.status(400).send({ message: "Incorrect Password!!" });
                let user = await user_model_1.default.findOne({ email: req.body.email }).select(["-password", "-tempPassword"]).populate([{
                        path: 'position',
                        model: 'Designation'
                    }, { path: 'project', model: 'Project' },
                    { path: 'department', model: 'Department' },
                    { path: 'avatar' }
                ]);
                const token = await jwt.sign({ _id: foundUser._id }, config_1.config.jwt_secret, {
                    expiresIn: config_1.config.JWT_MAX_AGE,
                });
                return res.status(200).header("auth-token", token).send({
                    "accessToken": token,
                    "refreshToken": token,
                    userId: foundUser._id,
                    role: foundUser.role,
                    name: `${foundUser.firstName} ${foundUser.middleName} ${foundUser.lastName}`,
                    email: foundUser.email,
                    userObject: user
                });
            }
        }
        catch (error) {
            return res.status(400).send(error);
        }
    }
}
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map
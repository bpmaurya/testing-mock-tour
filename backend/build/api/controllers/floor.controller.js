"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FloorController = void 0;
const floor_validation_1 = require("../validations/floor.validation");
const constant_1 = require("../../utils/constant");
const floor_model_1 = __importDefault(require("../models/floor.model"));
const logger_1 = require("../../utils/logger");
class FloorController {
    constructor() {
        this.createFloor = async (req, res, next) => {
            try {
                const floorValidation = await floor_validation_1.CreateFloorValidation.validateAsync(req.body);
                if (!floorValidation) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
                else {
                    try {
                        const foundLocation = await floor_model_1.default.findOne({ name: req.body.name, location: req.body.location });
                        if (foundLocation) {
                            return res.status(400).send({
                                message: `Floor ${req.body.name} not available! Already exist!`
                            });
                        }
                        else {
                            const floor = new floor_model_1.default({
                                name: req.body.name,
                                location: req.body.location
                            });
                            await floor.save();
                            return res.status(200).send({ message: constant_1.Messages.FLOOR_CREATE_SUCCESS });
                        }
                    }
                    catch (error) {
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
            }
            catch (err) {
                logger_1.logger.info(err.message);
                logger_1.logger.info(err);
                return res.status(400).send({ message: err.message });
            }
        };
        this.deleteFloor = async (req, res, next) => {
            try {
                const idValidation = await floor_validation_1.FloorIdValidation.validateAsync(req.body.id);
                if (!idValidation) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
                else {
                    try {
                        const foundLocation = await floor_model_1.default.findOne({ _id: req.body.id });
                        if (!foundLocation) {
                            return res.status(400).send({
                                message: `Floor not exist!!`
                            });
                        }
                        else {
                            const deleteFloor = await floor_model_1.default.deleteOne({ _id: idValidation });
                            if (deleteFloor)
                                return res.status(200).send({ message: constant_1.Messages.FLOOR_DELETE_SUCCESS });
                            else {
                                return next(res.status(400).json({
                                    message: constant_1.Messages.INVALID_DETAIL
                                }));
                            }
                        }
                    }
                    catch (error) {
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
            }
            catch (err) {
                console.log(err.message);
                console.log(err);
                return res.status(400).send({ message: err.message });
            }
        };
        this.getAllFloor = async (req, res, next) => {
            try {
                const allFloor = await floor_model_1.default.find({}).populate('location');
                if (allFloor)
                    return res.status(200).send({ "allFloor": allFloor });
            }
            catch (error) {
                return res.status(400).send(error);
            }
        };
        this.getAllFloorWithLocation = async (req, res, next) => {
            try {
                const allFloor = await floor_model_1.default.find({ location: (req.params.location || { $exists: true }) }).populate('location');
                if (allFloor)
                    return res.status(200).send({ "allFloor": allFloor });
            }
            catch (error) {
                return res.status(400).send(error);
            }
        };
    }
    async updateFloor(req, res, next) {
        try {
            logger_1.logger.info(JSON.stringify(req.body));
            const floorValidation = await floor_validation_1.UpdateFloorValidation.validateAsync(req.body);
            if (!floorValidation) {
                return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
            }
            else {
                try {
                    const isFloorValid = await floor_model_1.default.findOne({
                        _id: floorValidation.id
                    });
                    if (!isFloorValid) {
                        return res.status(400).send({
                            message: constant_1.Messages.INVALID_DETAIL
                        });
                    }
                    else {
                        const updateFloor = await floor_model_1.default.findOneAndUpdate({
                            _id: isFloorValid.id
                        }, {
                            $set: {
                                name: req.body.name,
                                location: req.body.location,
                                status: req.body.status
                            }
                        });
                        if (updateFloor)
                            return res.status(200).send({ message: constant_1.Messages.FLOOR_UPDATE_SUCCESS, });
                        else {
                            return next(res.status(400).json({
                                message: constant_1.Messages.INVALID_DETAIL
                            }));
                        }
                    }
                }
                catch (error) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
            }
        }
        catch (err) {
            console.log(err.message);
            console.log(err);
            return res.status(400).send({ message: err.message });
        }
    }
}
exports.FloorController = FloorController;
//# sourceMappingURL=floor.controller.js.map
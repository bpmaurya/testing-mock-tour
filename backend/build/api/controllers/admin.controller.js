"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminController = void 0;
const bcrypt_nodejs_1 = __importDefault(require("bcrypt-nodejs"));
const jwt = __importStar(require("jsonwebtoken"));
const config_1 = require("../../config");
const user_model_1 = __importDefault(require("../models/user.model"));
const location_model_1 = __importDefault(require("../models/location.model"));
const logger_1 = require("../../utils/logger");
const location_validation_1 = require("../validations/location.validation");
const user_validation_1 = require("../validations/user.validation");
const constant_1 = require("../../utils/constant");
class AdminController {
    constructor() {
        this.getAllUser = async (req, res, next) => {
            try {
                const allUser = await user_model_1.default.find({}, { showCredentials: false }).select(["-password", "-tempPassword"]).populate([{
                        path: 'position',
                        model: 'Designation'
                    }, { path: 'project', model: 'Project' },
                    { path: 'department', model: 'Department' }
                ]);
                if (allUser.length !== 0)
                    return res.status(200).send({ "alluser": allUser });
            }
            catch (error) {
                return res.status(400).send(error);
            }
        };
        this.getAllLocation = async (req, res, next) => {
            try {
                const allLocation = await location_model_1.default.find({});
                if (allLocation)
                    return res.status(200).send({ "allLocation": allLocation });
            }
            catch (error) {
                return res.status(400).send(error);
            }
        };
        this.createLocation = async (req, res, next) => {
            try {
                const locationValidation = await location_validation_1.CreateLocationValidation.validateAsync(req.body);
                if (!locationValidation) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
                else {
                    try {
                        const foundLocation = await location_model_1.default.findOne({ name: req.body.name });
                        if (foundLocation) {
                            return res.status(400).send({
                                message: `Location ${req.body.name} not available! Already exist!`
                            });
                        }
                        else {
                            const location = new location_model_1.default({
                                name: req.body.name,
                                status: req.body.status
                            });
                            await location.save();
                            return res.status(200).send({ message: constant_1.Messages.LOCATION_CREATE_SUCCESS });
                        }
                    }
                    catch (error) {
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
            }
            catch (err) {
                logger_1.logger.info(err.message);
                logger_1.logger.info(err);
                return res.status(400).send({ message: err.message });
            }
        };
        this.deleteUser = async (req, res, next) => {
            try {
                const idValidation = await user_validation_1.UserIdValidation.validateAsync(req.body.id);
                if (!idValidation) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
                else {
                    try {
                        const foundUser = await user_model_1.default.findOne({ _id: req.body.id });
                        if (!foundUser) {
                            return res.status(400).send({
                                message: `User not exist!`
                            });
                        }
                        else {
                            const deleteUser = await user_model_1.default.deleteOne({ _id: idValidation });
                            if (deleteUser)
                                return res.status(200).send({ message: constant_1.Messages.USER_DELETE_SUCCESS });
                            else {
                                return next(res.status(400).json({
                                    message: constant_1.Messages.INVALID_DETAIL
                                }));
                            }
                        }
                    }
                    catch (error) {
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
            }
            catch (err) {
                logger_1.logger.info(err.message);
                logger_1.logger.info(err);
                return res.status(400).send({ message: err.message });
            }
        };
        this.deleteLocation = async (req, res, next) => {
            try {
                const idValidation = await user_validation_1.LocationIdValidation.validateAsync(req.body.id);
                if (!idValidation) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
                else {
                    try {
                        const foundLocation = await location_model_1.default.findOne({ _id: req.body.id });
                        if (!foundLocation) {
                            return res.status(400).send({
                                message: `Location not exist!!`
                            });
                        }
                        else {
                            const deleteLocation = await location_model_1.default.deleteOne({ _id: idValidation });
                            if (deleteLocation)
                                return res.status(200).send({ message: constant_1.Messages.LOCATION_DELETE_SUCCESS });
                            else {
                                return next(res.status(400).json({
                                    message: constant_1.Messages.INVALID_DETAIL
                                }));
                            }
                        }
                    }
                    catch (error) {
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
            }
            catch (err) {
                console.log(err.message);
                console.log(err);
                return res.status(400).send({ message: err.message });
            }
        };
    }
    async registerAdmin(req, res) {
        try {
            const hashedPassword = bcrypt_nodejs_1.default.hashSync(req.body.password, bcrypt_nodejs_1.default.genSaltSync(10));
            await user_model_1.default.create({
                username: req.body.username,
                password: hashedPassword,
            });
            const token = jwt.sign({ username: req.body.username, scope: req.body.scope }, config_1.config.jwt_secret);
            res.status(200).send({ token: token });
        }
        catch (error) {
            res.send({ error: constant_1.Messages.INVALID_DETAIL });
        }
    }
    async updateLocation(req, res, next) {
        try {
            logger_1.logger.info(JSON.stringify(req.body));
            const locationValidation = await location_validation_1.UpdateLocationValidation.validateAsync(req.body);
            if (!locationValidation) {
                return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
            }
            else {
                try {
                    const isLocationValid = await location_model_1.default.findOne({
                        _id: locationValidation.id
                    });
                    if (!isLocationValid) {
                        return res.status(400).send({
                            message: constant_1.Messages.INVALID_DETAIL
                        });
                    }
                    else {
                        const updateLocation = await location_model_1.default.findOneAndUpdate({
                            _id: isLocationValid.id
                        }, {
                            $set: {
                                name: req.body.name,
                                status: req.body.status
                            }
                        });
                        if (updateLocation)
                            return res.status(200).send({ message: constant_1.Messages.LOCATION_UPDATE_SUCCESS, });
                        else {
                            return next(res.status(400).json({
                                message: constant_1.Messages.INVALID_DETAIL
                            }));
                        }
                    }
                }
                catch (error) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
            }
        }
        catch (err) {
            console.log(err.message);
            console.log(err);
            return res.status(400).send({ message: err.message });
        }
    }
}
exports.AdminController = AdminController;
//# sourceMappingURL=admin.controller.js.map
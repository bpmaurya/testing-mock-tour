"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificationController = void 0;
const notification_model_1 = __importDefault(require("../models/notification.model"));
const logger_1 = require("../../utils/logger");
class NotificationController {
    constructor() {
        this.createNotification = async (req, res, next) => {
            try {
                const notificationCreate = await notification_model_1.default.create({
                    user: req.body.user,
                    request: req.body.request,
                    isRead: req.body.isRead,
                });
                await notificationCreate.save();
                return res.status(200).send({ message: 'Notification Created!' });
            }
            catch (err) {
                logger_1.logger.info(err.message);
                logger_1.logger.info(err);
                return res.status(400).send({ message: err.message });
            }
        };
        this.getAllNotification = async (req, res, next) => {
            try {
                const allNotification = await notification_model_1.default.find({ user: req.params.user_id }).populate([
                    {
                        path: 'user',
                        model: 'User',
                        select: '-password',
                        populate: [{
                                path: 'project',
                                model: 'Project'
                            }]
                    }, {
                        path: 'request',
                        model: 'Request',
                        populate: {
                            path: 'seat',
                            model: 'Seat',
                            populate: [{
                                    path: 'location',
                                    model: 'Location'
                                }, {
                                    path: 'floor',
                                    model: "Floor"
                                }, {
                                    path: 'wing',
                                    model: "Wing"
                                }]
                        }
                    }
                ]);
                if (allNotification)
                    return res.status(200).send({ "allNotification": allNotification });
            }
            catch (error) {
                return res.status(400).send(error);
            }
        };
    }
}
exports.NotificationController = NotificationController;
//# sourceMappingURL=notification.controller.js.map
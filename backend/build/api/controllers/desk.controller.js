"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeskController = void 0;
const seat_validation_1 = require("../validations/seat.validation");
const constant_1 = require("../../utils/constant");
const seats_model_1 = __importDefault(require("../models/seats.model"));
const logger_1 = require("../../utils/logger");
class DeskController {
    constructor() {
        this.createDesk = async (req, res, next) => {
            try {
                const deskValidation = await seat_validation_1.CreateSeatValidation.validateAsync(req.body);
                if (!deskValidation) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
                else {
                    try {
                        const foundDesk = await seats_model_1.default.find({ location: req.body.location, floor: req.body.floor, wing: req.body.wing, row: req.body.row });
                        if (foundDesk.length !== 0) {
                            return res.status(400).send({
                                message: `Row ${req.body.row} not available! Already exist!`
                            });
                        }
                        else {
                            let deskCollection = [];
                            for (let i = req.body.start; i <= req.body.end; i++) {
                                deskCollection.push({
                                    name: `${req.body.row}-${i}`,
                                    row: req.body.row,
                                    location: req.body.location,
                                    floor: req.body.floor,
                                    wing: req.body.wing,
                                    status: req.body.status,
                                });
                            }
                            deskCollection.map(async (e) => {
                                const desk = new seats_model_1.default(e);
                                await desk.save();
                            });
                            return res.status(200).send({ message: constant_1.Messages.DESK_CREATE_SUCCESS });
                        }
                    }
                    catch (error) {
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
            }
            catch (err) {
                logger_1.logger.info(err.message);
                logger_1.logger.info(err);
                return res.status(400).send({ message: err.message });
            }
        };
        this.deleteDesk = async (req, res, next) => {
            try {
                const seatValidation = await seat_validation_1.DeleteSeatValidation.validateAsync(req.body);
                if (!seatValidation) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
                else {
                    try {
                        const foundDesk = await seats_model_1.default.findOne({
                            location: seatValidation.location,
                            floor: seatValidation.floor,
                            wing: seatValidation.wing,
                            row: seatValidation.row
                        });
                        if (!foundDesk) {
                            return res.status(400).send({
                                message: `Desk not exist!!`
                            });
                        }
                        else {
                            const deleteDesk = await seats_model_1.default.deleteMany({
                                location: seatValidation.location,
                                floor: seatValidation.floor,
                                wing: seatValidation.wing,
                                row: seatValidation.row
                            });
                            if (deleteDesk)
                                return res.status(200).send({ message: constant_1.Messages.DESK_DELETE_SUCCESS });
                            else {
                                return next(res.status(400).json({
                                    message: constant_1.Messages.INVALID_DETAIL
                                }));
                            }
                        }
                    }
                    catch (error) {
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
            }
            catch (err) {
                console.log(err.message);
                console.log(err);
                return res.status(400).send({ message: err.message });
            }
        };
        this.getAllDesks = async (req, res, next) => {
            try {
                const allSeat = await seats_model_1.default.find({ location: req.params.location, floor: req.params.floor, wing: req.params.wing }).populate([{
                        path: 'floor',
                        model: 'Floor'
                    }, { path: 'location', model: 'Location' },
                    { path: 'wing', model: 'Wing' }, {
                        path: 'bookedBy', model: 'User', select: "-password", populate: [{
                                path: 'department',
                                model: 'Department'
                            }, { path: 'position', model: "Designation" }, { path: 'project', model: "Project" }]
                    }]);
                if (allSeat)
                    return res.status(200).send({ "allSeat": allSeat });
            }
            catch (error) {
                return res.status(400).send(error);
            }
        };
        this.updateDeskOrder = async (req, res, next) => {
            try {
                const providedDesk = req.body.data;
                if (!providedDesk) {
                    return res.status(400).send({
                        message: `data not provided!`
                    });
                }
                else {
                    let data = req.body.data;
                    const update = data.map(async (e) => {
                        return e.seats.map(async (f) => {
                            await seats_model_1.default.findOneAndUpdate({
                                _id: f._id
                            }, {
                                $set: {
                                    orderIndex: e.orderIndex
                                }
                            });
                        });
                    });
                    if (update)
                        return res.status(200).send({ message: constant_1.Messages.DESK_UPDATE_SUCCESS, });
                    else {
                        return next(res.status(400).json({
                            message: constant_1.Messages.INVALID_DETAIL
                        }));
                    }
                }
            }
            catch (error) {
                return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
            }
        };
    }
    async updateDesk(req, res, next) {
        var io = req.app.get('socketIO');
        try {
            logger_1.logger.info(JSON.stringify(req.body));
            const deskValidation = await seat_validation_1.UpdateSeatValidation.validateAsync(req.body);
            if (!deskValidation) {
                return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
            }
            else {
                try {
                    const isSeatValid = await seats_model_1.default.findOne({
                        _id: deskValidation.id
                    });
                    if (!isSeatValid) {
                        return res.status(400).send({
                            message: constant_1.Messages.INVALID_DETAIL
                        });
                    }
                    else {
                        const updateSeat = await seats_model_1.default.findOneAndUpdate({
                            _id: isSeatValid.id
                        }, {
                            $set: {
                                name: req.body.name,
                                row: req.body.row,
                                location: req.body.location,
                                floor: req.body.floor,
                                wing: req.body.wing,
                                status: req.body.status,
                                isSelectable: req.body.isSelectable
                            }
                        });
                        if (updateSeat) {
                            io.emit('update-message', { seat: isSeatValid.id, selectable: req.body.isSelectable });
                            return res.status(200).send({ message: constant_1.Messages.DESK_UPDATE_SUCCESS, });
                        }
                        else {
                            return next(res.status(400).json({
                                message: constant_1.Messages.INVALID_DETAIL
                            }));
                        }
                    }
                }
                catch (error) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
            }
        }
        catch (err) {
            console.log(err.message);
            console.log(err);
            return res.status(400).send({ message: err.message });
        }
    }
    async createBookDeskRequest(req, res, next) {
        try {
            logger_1.logger.info(JSON.stringify(req.body));
            const deskValidation = await seat_validation_1.CreateBookSeatValidation.validateAsync(req.body);
            if (!deskValidation) {
                return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
            }
            else {
                try {
                    const isSeatValid = await seats_model_1.default.findOne({
                        _id: deskValidation.id
                    });
                    if (!isSeatValid) {
                        return res.status(400).send({
                            message: constant_1.Messages.INVALID_DETAIL
                        });
                    }
                    else {
                        const bookSeat = await seats_model_1.default.findOneAndUpdate({
                            _id: isSeatValid.id
                        }, {
                            $set: {
                                name: req.body.name,
                                row: req.body.row,
                                location: req.body.location,
                                floor: req.body.floor,
                                wing: req.body.wing,
                                status: req.body.status,
                                bookedBy: req.body.bookedBy,
                                isSelectable: false
                            }
                        });
                        if (bookSeat)
                            return res.status(200).send({ message: constant_1.Messages.DESK_BOOK_REQUEST_SUCCESS, });
                        else {
                            return next(res.status(400).json({
                                message: constant_1.Messages.INVALID_DETAIL
                            }));
                        }
                    }
                }
                catch (error) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
            }
        }
        catch (err) {
            console.log(err.message);
            console.log(err);
            return res.status(400).send({ message: err.message });
        }
    }
}
exports.DeskController = DeskController;
//# sourceMappingURL=desk.controller.js.map
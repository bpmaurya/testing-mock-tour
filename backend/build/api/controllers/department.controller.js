"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DepartmentController = void 0;
const constant_1 = require("../../utils/constant");
const logger_1 = require("../../utils/logger");
const department_model_1 = __importDefault(require("../models/department.model"));
const department_validation_1 = require("../validations/department.validation");
class DepartmentController {
    constructor() {
        this.getAllDepartments = async (req, res, next) => {
            try {
                const allDepartments = await department_model_1.default.find({});
                if (allDepartments)
                    return res.status(200).send({ "allDepartment": allDepartments });
            }
            catch (error) {
                return res.status(400).send(error);
            }
        };
        this.createDepartment = async (req, res, next) => {
            try {
                const departmentValidation = await department_validation_1.CreateDepartmentValidation.validateAsync(req.body);
                if (!departmentValidation) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
                else {
                    try {
                        const foundDepartment = await department_model_1.default.findOne({ name: req.body.name });
                        if (foundDepartment) {
                            return res.status(400).send({
                                message: `Department ${req.body.name} not available! Already exist!`
                            });
                        }
                        else {
                            const department = new department_model_1.default({
                                name: req.body.name,
                                status: req.body.status
                            });
                            await department.save();
                            return res.status(200).send({ message: constant_1.Messages.DEPARTMENT_CREATE_SUCCESS });
                        }
                    }
                    catch (error) {
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
            }
            catch (err) {
                logger_1.logger.info(err.message);
                logger_1.logger.info(err);
                return res.status(400).send({ message: err.message });
            }
        };
        this.deleteDepartment = async (req, res, next) => {
            try {
                const idValidation = await department_validation_1.DepartmentIdValidation.validateAsync(req.body.id);
                if (!idValidation) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
                else {
                    try {
                        const foundDepartment = await department_model_1.default.findOne({ _id: req.body.id });
                        if (!foundDepartment) {
                            return res.status(400).send({
                                message: `Department not exist!!`
                            });
                        }
                        else {
                            const deleteDepartment = await department_model_1.default.deleteOne({ _id: idValidation });
                            if (deleteDepartment)
                                return res.status(200).send({ message: constant_1.Messages.DEPARTMENT_DELETE_SUCCESS });
                            else {
                                return next(res.status(400).json({
                                    message: constant_1.Messages.INVALID_DETAIL
                                }));
                            }
                        }
                    }
                    catch (error) {
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
            }
            catch (err) {
                console.log(err.message);
                console.log(err);
                return res.status(400).send({ message: err.message });
            }
        };
    }
    async updateDepartment(req, res, next) {
        try {
            logger_1.logger.info(JSON.stringify(req.body));
            const departmentValidation = await department_validation_1.UpdateDepartmentValidation.validateAsync(req.body);
            if (!departmentValidation) {
                return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
            }
            else {
                try {
                    const isDepartmentValid = await department_model_1.default.findOne({
                        _id: departmentValidation.id
                    });
                    if (!isDepartmentValid) {
                        return res.status(400).send({
                            message: constant_1.Messages.INVALID_DETAIL
                        });
                    }
                    else {
                        const updateDepartment = await department_model_1.default.findOneAndUpdate({
                            _id: isDepartmentValid.id
                        }, {
                            $set: {
                                name: req.body.name,
                                status: req.body.status
                            }
                        });
                        if (updateDepartment)
                            return res.status(200).send({ message: constant_1.Messages.DEPARTMENT_UPDATE_SUCCESS, });
                        else {
                            return next(res.status(400).json({
                                message: constant_1.Messages.INVALID_DETAIL
                            }));
                        }
                    }
                }
                catch (error) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
            }
        }
        catch (err) {
            console.log(err.message);
            console.log(err);
            return res.status(400).send({ message: err.message });
        }
    }
}
exports.DepartmentController = DepartmentController;
//# sourceMappingURL=department.controller.js.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProjectController = void 0;
const project_models_1 = __importDefault(require("../models/project.models"));
const project_validation_1 = require("../validations/project.validation");
const constant_1 = require("../../utils/constant");
const logger_1 = require("../../utils/logger");
class ProjectController {
    constructor() {
        this.getAllProjects = async (req, res, next) => {
            try {
                const allProjects = await project_models_1.default.find({});
                if (allProjects)
                    return res.status(200).send({ "allProject": allProjects });
            }
            catch (error) {
                return res.status(400).send(error);
            }
        };
        this.createProject = async (req, res, next) => {
            try {
                const projectValidation = await project_validation_1.CreateProjectValidation.validateAsync(req.body);
                if (!projectValidation) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
                else {
                    try {
                        const foundProject = await project_models_1.default.findOne({ name: req.body.name });
                        if (foundProject) {
                            return res.status(400).send({
                                message: `Project ${req.body.name} not available! Already exist!`
                            });
                        }
                        else {
                            const project = new project_models_1.default({
                                name: req.body.name,
                                status: req.body.status
                            });
                            await project.save();
                            return res.status(200).send({ message: constant_1.Messages.PROJECT_CREATE_SUCCESS });
                        }
                    }
                    catch (error) {
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
            }
            catch (err) {
                logger_1.logger.info(err.message);
                logger_1.logger.info(err);
                return res.status(400).send({ message: err.message });
            }
        };
        this.deleteProject = async (req, res, next) => {
            try {
                const idValidation = await project_validation_1.ProjectIdValidation.validateAsync(req.body.id);
                if (!idValidation) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
                else {
                    try {
                        const foundProject = await project_models_1.default.findOne({ _id: req.body.id });
                        if (!foundProject) {
                            return res.status(400).send({
                                message: `Project not exist!!`
                            });
                        }
                        else {
                            const deleteProject = await project_models_1.default.deleteOne({ _id: idValidation });
                            if (deleteProject)
                                return res.status(200).send({ message: constant_1.Messages.PROJECT_DELETE_SUCCESS });
                            else {
                                return next(res.status(400).json({
                                    message: constant_1.Messages.INVALID_DETAIL
                                }));
                            }
                        }
                    }
                    catch (error) {
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
            }
            catch (err) {
                console.log(err.message);
                console.log(err);
                return res.status(400).send({ message: err.message });
            }
        };
    }
    async updateProject(req, res, next) {
        try {
            logger_1.logger.info(JSON.stringify(req.body));
            const projectValidation = await project_validation_1.UpdateProjectValidation.validateAsync(req.body);
            if (!projectValidation) {
                return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
            }
            else {
                try {
                    const isProjectValid = await project_models_1.default.findOne({
                        _id: projectValidation.id
                    });
                    if (!isProjectValid) {
                        return res.status(400).send({
                            message: constant_1.Messages.INVALID_DETAIL
                        });
                    }
                    else {
                        const updateProject = await project_models_1.default.findOneAndUpdate({
                            _id: isProjectValid.id
                        }, {
                            $set: {
                                name: req.body.name,
                                status: req.body.status
                            }
                        });
                        if (updateProject)
                            return res.status(200).send({ message: constant_1.Messages.PROJECT_UPDATE_SUCCESS, });
                        else {
                            return next(res.status(400).json({
                                message: constant_1.Messages.INVALID_DETAIL
                            }));
                        }
                    }
                }
                catch (error) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
            }
        }
        catch (err) {
            console.log(err.message);
            console.log(err);
            return res.status(400).send({ message: err.message });
        }
    }
}
exports.ProjectController = ProjectController;
//# sourceMappingURL=project.controller.js.map
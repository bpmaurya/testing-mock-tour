"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.socketIoServer = void 0;
const socket_io_1 = require("socket.io");
exports.socketIoServer = {
    getIo: (server) => {
        const io = new socket_io_1.Server(server, {
            cors: {
                origin: "http://localhost:3000",
                methods: ["GET", "POST"],
                credentials: false
            }
        });
        io.on('connection', (socket) => {
            console.log('User connected');
            socket.on('notification', () => {
                console.log('got notification!');
            });
            socket.on("message", (data) => {
                io.emit(`notification`, data);
            });
            socket.on('disconnect', () => {
                console.log('User disconnected!');
            });
        });
        return io;
    }
};
//# sourceMappingURL=socket.controllet.js.map
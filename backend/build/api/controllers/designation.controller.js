"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DesignationController = void 0;
const constant_1 = require("../../utils/constant");
const logger_1 = require("../../utils/logger");
const designation_model_1 = __importDefault(require("../models/designation.model"));
const designation_validation_1 = require("../validations/designation.validation");
class DesignationController {
    constructor() {
        this.getAllDesignation = async (req, res, next) => {
            try {
                const allDesignation = await designation_model_1.default.find({});
                if (allDesignation)
                    return res.status(200).send({ "allDesignation": allDesignation });
            }
            catch (error) {
                return res.status(400).send(error);
            }
        };
        this.createDesignation = async (req, res, next) => {
            try {
                const designationValidation = await designation_validation_1.CreateDesignationValidation.validateAsync(req.body);
                if (!designationValidation) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
                else {
                    try {
                        const foundDesignation = await designation_model_1.default.findOne({ name: req.body.name });
                        if (foundDesignation) {
                            return res.status(400).send({
                                message: `Designation ${req.body.name} not available! Already exist!`
                            });
                        }
                        else {
                            const designation = new designation_model_1.default({
                                name: req.body.name,
                                status: req.body.status
                            });
                            await designation.save();
                            return res.status(200).send({ message: constant_1.Messages.DESIGNATION_CREATE_SUCCESS });
                        }
                    }
                    catch (error) {
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
            }
            catch (err) {
                logger_1.logger.info(err.message);
                logger_1.logger.info(err);
                return res.status(400).send({ message: err.message });
            }
        };
        this.deleteDesignation = async (req, res, next) => {
            try {
                const idValidation = await designation_validation_1.DesignationIdValidation.validateAsync(req.body.id);
                if (!idValidation) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
                else {
                    try {
                        const foundDesignation = await designation_model_1.default.findOne({ _id: req.body.id });
                        if (!foundDesignation) {
                            return res.status(400).send({
                                message: `Designation not exist!!`
                            });
                        }
                        else {
                            const deleteDesignation = await designation_model_1.default.deleteOne({ _id: idValidation });
                            if (deleteDesignation)
                                return res.status(200).send({ message: constant_1.Messages.DESIGNATION_DELETE_SUCCESS });
                            else {
                                return next(res.status(400).json({
                                    message: constant_1.Messages.INVALID_DETAIL
                                }));
                            }
                        }
                    }
                    catch (error) {
                        return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                    }
                }
            }
            catch (err) {
                console.log(err.message);
                console.log(err);
                return res.status(400).send({ message: err.message });
            }
        };
    }
    async updateDesignation(req, res, next) {
        try {
            logger_1.logger.info(JSON.stringify(req.body));
            const designationValidation = await designation_validation_1.UpdateDesignationValidation.validateAsync(req.body);
            if (!designationValidation) {
                return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
            }
            else {
                try {
                    const isDesignationValid = await designation_model_1.default.findOne({
                        _id: designationValidation.id
                    });
                    if (!isDesignationValid) {
                        return res.status(400).send({
                            message: constant_1.Messages.INVALID_DETAIL
                        });
                    }
                    else {
                        const updateDesignation = await designation_model_1.default.findOneAndUpdate({
                            _id: isDesignationValid.id
                        }, {
                            $set: {
                                name: req.body.name,
                                status: req.body.status
                            }
                        });
                        if (updateDesignation)
                            return res.status(200).send({ message: constant_1.Messages.DESIGNATION_UPDATE_SUCCESS, });
                        else {
                            return next(res.status(400).json({
                                message: constant_1.Messages.INVALID_DETAIL
                            }));
                        }
                    }
                }
                catch (error) {
                    return res.status(400).send({ message: constant_1.Messages.INVALID_DETAIL });
                }
            }
        }
        catch (err) {
            console.log(err.message);
            console.log(err);
            return res.status(400).send({ message: err.message });
        }
    }
}
exports.DesignationController = DesignationController;
//# sourceMappingURL=designation.controller.js.map
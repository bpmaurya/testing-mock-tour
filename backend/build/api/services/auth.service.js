"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.requireAuth = void 0;
const jwt = require("jsonwebtoken");
const config_1 = require("../../config");
const logger_1 = require("../../utils/logger");
const requireAuth = (req, res, next) => {
    const token = req.body.token || req.query.token || req.headers["x-access-token"];
    if (token) {
        jwt.verify(token, config_1.config.jwt_secret, (err) => {
            if (err) {
                logger_1.logger.info(err.message);
                return res.status(400).send(err);
            }
            else {
                next();
            }
        });
    }
    else {
        return res.status(400).send({ "message": "token not found!" });
    }
};
exports.requireAuth = requireAuth;
//# sourceMappingURL=auth.service.js.map
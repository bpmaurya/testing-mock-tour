"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminRoutes = void 0;
const express_1 = require("express");
const admin_controller_1 = require("../controllers/admin.controller");
const floor_controller_1 = require("../controllers/floor.controller");
const wing_controller_1 = require("../controllers/wing.controller");
const desk_controller_1 = require("../controllers/desk.controller");
const request_controller_1 = require("../controllers/request.controller");
const designation_controller_1 = require("../controllers/designation.controller");
const department_controller_1 = require("../controllers/department.controller");
const project_controller_1 = require("../controllers/project.controller");
const auth_service_1 = require("../services/auth.service");
const notification_controller_1 = require("../controllers/notification.controller");
class AdminRoutes {
    constructor() {
        this.adminController = new admin_controller_1.AdminController();
        this.floorController = new floor_controller_1.FloorController();
        this.wingController = new wing_controller_1.WingController();
        this.deskController = new desk_controller_1.DeskController();
        this.requestController = new request_controller_1.RequestController();
        this.designationController = new designation_controller_1.DesignationController();
        this.departmentController = new department_controller_1.DepartmentController();
        this.projectController = new project_controller_1.ProjectController();
        this.notificationController = new notification_controller_1.NotificationController();
        this.router = (0, express_1.Router)();
        this.routes();
    }
    routes() {
        this.router.post("/register", this.adminController.registerAdmin);
        this.router.get('/all-user', auth_service_1.requireAuth, this.adminController.getAllUser);
        this.router.get('/all-location', auth_service_1.requireAuth, this.adminController.getAllLocation);
        this.router.post('/create-location', auth_service_1.requireAuth, this.adminController.createLocation);
        this.router.post('/delete-user', auth_service_1.requireAuth, this.adminController.deleteUser);
        this.router.post('/delete-location', auth_service_1.requireAuth, this.adminController.deleteLocation);
        this.router.post('/location-update', auth_service_1.requireAuth, this.adminController.updateLocation);
        this.router.post('/create-floor', auth_service_1.requireAuth, this.floorController.createFloor);
        this.router.post('/update-floor', auth_service_1.requireAuth, this.floorController.updateFloor);
        this.router.post('/delete-floor', auth_service_1.requireAuth, this.floorController.deleteFloor);
        this.router.get('/all-floor/:location', auth_service_1.requireAuth, this.floorController.getAllFloorWithLocation);
        this.router.get('/all-floor', auth_service_1.requireAuth, this.floorController.getAllFloor);
        this.router.get('/all-wings', auth_service_1.requireAuth, this.wingController.getAllWing);
        this.router.get('/all-wings/:location/:floor', auth_service_1.requireAuth, this.wingController.getAllWingForLocationAndFloor);
        this.router.post('/update-wing', auth_service_1.requireAuth, this.wingController.updateWing);
        this.router.post('/delete-wing', auth_service_1.requireAuth, this.wingController.deleteWing);
        this.router.post('/create-wing', auth_service_1.requireAuth, this.wingController.createWing);
        this.router.post('/create-desk', auth_service_1.requireAuth, this.deskController.createDesk);
        this.router.get('/all-desks/:location/:floor/:wing', auth_service_1.requireAuth, this.deskController.getAllDesks);
        this.router.post('/delete-desk', auth_service_1.requireAuth, this.deskController.deleteDesk);
        this.router.post('/update-desk', auth_service_1.requireAuth, this.deskController.updateDesk);
        this.router.post('/create-request', auth_service_1.requireAuth, this.requestController.createRequest);
        this.router.post('/update-request', auth_service_1.requireAuth, this.requestController.updateDeskRequest);
        this.router.post('/desk-book-request', auth_service_1.requireAuth, this.deskController.createBookDeskRequest);
        this.router.get('/all-deskrequest', auth_service_1.requireAuth, this.requestController.getAllDeskRequests);
        this.router.get('/all-desk-request/:user', auth_service_1.requireAuth, this.requestController.getAllDeskRequestsForUser);
        this.router.get('/all-department', auth_service_1.requireAuth, this.departmentController.getAllDepartments);
        this.router.post('/create-department', auth_service_1.requireAuth, this.departmentController.createDepartment);
        this.router.post('/delete-department', auth_service_1.requireAuth, this.departmentController.deleteDepartment);
        this.router.post('/update-department', auth_service_1.requireAuth, this.departmentController.updateDepartment);
        this.router.get('/all-projects', auth_service_1.requireAuth, this.projectController.getAllProjects);
        this.router.post('/create-projects', auth_service_1.requireAuth, this.projectController.createProject);
        this.router.post('/update-project', auth_service_1.requireAuth, this.projectController.updateProject);
        this.router.post('/delete-project', auth_service_1.requireAuth, this.projectController.deleteProject);
        this.router.get('/all-designation', auth_service_1.requireAuth, this.designationController.getAllDesignation);
        this.router.post('/create-designation', auth_service_1.requireAuth, this.designationController.createDesignation);
        this.router.post('/delete-designation', auth_service_1.requireAuth, this.designationController.deleteDesignation);
        this.router.post('/update-designation', auth_service_1.requireAuth, this.designationController.updateDesignation);
        this.router.post('/update-desk-index', auth_service_1.requireAuth, this.deskController.updateDeskOrder);
        this.router.get('/get-all-notification/:user_id', auth_service_1.requireAuth, this.notificationController.getAllNotification);
    }
}
exports.AdminRoutes = AdminRoutes;
//# sourceMappingURL=admin.route.js.map
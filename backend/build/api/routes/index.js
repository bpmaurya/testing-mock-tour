"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_route_1 = require("./user.route");
const admin_route_1 = require("./admin.route");
const router = (0, express_1.Router)();
router.use('/user', new user_route_1.UserRoutes().router);
router.use('/admin', new admin_route_1.AdminRoutes().router);
exports.default = router;
//# sourceMappingURL=index.js.map
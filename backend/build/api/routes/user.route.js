"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRoutes = void 0;
const express_1 = require("express");
const user_controller_1 = require("../controllers/user.controller");
const auth_service_1 = require("../services/auth.service");
const upload_1 = __importDefault(require("../controllers/upload"));
class UserRoutes {
    constructor() {
        this.userController = new user_controller_1.UserController();
        this.router = (0, express_1.Router)();
        this.routes();
    }
    routes() {
        this.router.post("/register", this.userController.registerUser);
        this.router.post("/login", this.userController.authenticateUser);
        this.router.post("/update", auth_service_1.requireAuth, this.userController.updateUser);
        this.router.post('/update-user/:user_id/profile', upload_1.default.single('profile'), this.userController.updateProfilePictureUser);
        this.router.post('/update-password/:user_id', this.userController.updateUserPassword);
    }
}
exports.UserRoutes = UserRoutes;
//# sourceMappingURL=user.route.js.map
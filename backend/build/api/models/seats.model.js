"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
const SeatSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: true
    },
    row: {
        type: String,
        required: true
    },
    location: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'Location',
    },
    floor: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'Floor'
    },
    wing: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'Wing'
    },
    bookedBy: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'User',
        required: false,
    },
    bookedStatus: {
        type: String,
        required: false
    },
    status: {
        type: Boolean,
        required: false
    },
    isSelectable: {
        type: Boolean,
        required: false,
        default: true
    },
    orderIndex: {
        type: Number,
        required: false,
    }
}, { timestamps: true });
const OfficeSeat = mongoose_1.default.model("Seat", SeatSchema);
exports.default = OfficeSeat;
//# sourceMappingURL=seats.model.js.map
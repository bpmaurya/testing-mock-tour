"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationIdValidation = exports.UserUpdateValidation = exports.UserIdValidation = exports.UserLoginValidation = exports.UserRegistrationValidation = void 0;
const joi_1 = __importDefault(require("joi"));
exports.UserRegistrationValidation = joi_1.default.object({
    firstName: joi_1.default.string().min(3).max(50).required(),
    middleName: joi_1.default.string().allow(null, ''),
    lastName: joi_1.default.string().min(3).max(50).required(),
    email: joi_1.default.string().email().min(6).max(255).required(),
    domain: joi_1.default.string().min(3).max(50).required(),
    department: joi_1.default.string().alphanum().required(),
    password: joi_1.default.string().min(6).max(50).required(),
    role: joi_1.default.string().required(),
    position: joi_1.default.string().alphanum().required(),
    phone: joi_1.default.number().required(),
    pastExperience: joi_1.default.string().required(),
    project: joi_1.default.string().alphanum().allow(null, '')
});
exports.UserLoginValidation = joi_1.default.object({
    email: joi_1.default.string().email().min(6).max(255).required(),
});
exports.UserIdValidation = joi_1.default.string().alphanum().required();
exports.UserUpdateValidation = joi_1.default.object({
    firstName: joi_1.default.string().min(3).max(50).required(),
    middleName: joi_1.default.string().allow(null, ''),
    lastName: joi_1.default.string().min(3).max(50).required(),
    email: joi_1.default.string().email().min(6).max(255).required(),
    domain: joi_1.default.string().min(3).max(50).required(),
    department: joi_1.default.string().alphanum().required(),
    role: joi_1.default.string().required(),
    position: joi_1.default.string().alphanum().required(),
    phone: joi_1.default.number().required(),
    pastExperience: joi_1.default.string().required(),
    project: joi_1.default.string().alphanum().allow(null, '')
});
exports.LocationIdValidation = joi_1.default.string().alphanum().required();
//# sourceMappingURL=user.validation.js.map
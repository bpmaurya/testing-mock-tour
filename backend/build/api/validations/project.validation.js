"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProjectIdValidation = exports.UpdateProjectValidation = exports.CreateProjectValidation = void 0;
const joi_1 = __importDefault(require("joi"));
exports.CreateProjectValidation = joi_1.default.object({
    name: joi_1.default.string().min(3).max(50).required(),
});
exports.UpdateProjectValidation = joi_1.default.object({
    id: joi_1.default.string().alphanum().required(),
    name: joi_1.default.string().min(3).max(50).required(),
    status: joi_1.default.string().allow(null, '')
});
exports.ProjectIdValidation = joi_1.default.string().alphanum().required();
//# sourceMappingURL=project.validation.js.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FloorIdValidation = exports.UpdateFloorValidation = exports.CreateFloorValidation = void 0;
const joi_1 = __importDefault(require("joi"));
exports.CreateFloorValidation = joi_1.default.object({
    name: joi_1.default.string().min(3).max(50).required(),
    location: joi_1.default.string().alphanum().required(),
    status: joi_1.default.string().allow(null, '')
});
exports.UpdateFloorValidation = joi_1.default.object({
    id: joi_1.default.string().alphanum().required(),
    name: joi_1.default.string().min(3).max(50).required(),
    location: joi_1.default.string().alphanum().required(),
    status: joi_1.default.string().allow(null, '')
});
exports.FloorIdValidation = joi_1.default.string().alphanum().required();
//# sourceMappingURL=floor.validation.js.map
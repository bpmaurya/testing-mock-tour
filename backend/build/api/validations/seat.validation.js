"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SeatIdValidation = exports.CreateBookSeatValidation = exports.UpdateSeatValidation = exports.DeleteSeatValidation = exports.CreateSeatValidation = void 0;
const joi_1 = __importDefault(require("joi"));
exports.CreateSeatValidation = joi_1.default.object({
    row: joi_1.default.string().min(1).max(50).required(),
    start: joi_1.default.number().min(1).max(50).required(),
    end: joi_1.default.number().min(1).max(15).required(),
    location: joi_1.default.string().alphanum().required(),
    floor: joi_1.default.string().alphanum().required(),
    wing: joi_1.default.string().alphanum().required(),
    status: joi_1.default.string().allow(null, ''),
    orderIndex: joi_1.default.number().allow(null)
});
exports.DeleteSeatValidation = joi_1.default.object({
    row: joi_1.default.string().min(1).max(50).required(),
    location: joi_1.default.string().alphanum().required(),
    floor: joi_1.default.string().alphanum().required(),
    wing: joi_1.default.string().alphanum().required(),
    status: joi_1.default.string().allow(null, '')
});
exports.UpdateSeatValidation = joi_1.default.object({
    id: joi_1.default.string().alphanum().required(),
    name: joi_1.default.string().required(),
    row: joi_1.default.string().min(1).max(50).required(),
    location: joi_1.default.string().alphanum().required(),
    floor: joi_1.default.string().alphanum().required(),
    wing: joi_1.default.string().alphanum().required(),
    status: joi_1.default.string().allow(null, ''),
    isSelectable: joi_1.default.boolean().allow(null, '')
});
exports.CreateBookSeatValidation = joi_1.default.object({
    id: joi_1.default.string().alphanum().required(),
    name: joi_1.default.string().required(),
    row: joi_1.default.string().min(1).max(50).required(),
    location: joi_1.default.string().alphanum().required(),
    floor: joi_1.default.string().alphanum().required(),
    wing: joi_1.default.string().alphanum().required(),
    status: joi_1.default.string().allow(null, ''),
    bookedBy: joi_1.default.string().alphanum().required(),
    isSelectable: joi_1.default.boolean().allow(null, '')
});
exports.SeatIdValidation = joi_1.default.string().alphanum().required();
//# sourceMappingURL=seat.validation.js.map
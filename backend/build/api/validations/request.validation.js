"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RequestIdValidation = exports.DeleteRequestValidation = exports.UpdateRequestValidation = exports.CreateRequestValidation = void 0;
const joi_1 = __importDefault(require("joi"));
exports.CreateRequestValidation = joi_1.default.object({
    user: joi_1.default.string().alphanum().required(),
    reportingManager: joi_1.default.string().alphanum().required(),
    project: joi_1.default.string().alphanum().required(),
    seat: joi_1.default.string().alphanum().required(),
    description: joi_1.default.string().allow(null, ''),
    priority: joi_1.default.string().allow(null, ''),
    status: joi_1.default.string().allow(null, '')
});
exports.UpdateRequestValidation = joi_1.default.object({
    _id: joi_1.default.string().alphanum().required(),
    user: joi_1.default.string().alphanum().required(),
    reportingManager: joi_1.default.string().alphanum().required(),
    project: joi_1.default.string().alphanum().required(),
    seat: joi_1.default.string().alphanum().required(),
    description: joi_1.default.string().allow(null, ''),
    priority: joi_1.default.string().allow(null, ''),
    status: joi_1.default.string().required()
});
exports.DeleteRequestValidation = joi_1.default.object({
    user: joi_1.default.string().alphanum().required(),
    seat: joi_1.default.string().alphanum().required(),
});
exports.RequestIdValidation = joi_1.default.string().alphanum().required();
//# sourceMappingURL=request.validation.js.map
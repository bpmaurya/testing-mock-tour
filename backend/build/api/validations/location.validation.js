"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateLocationValidation = exports.CreateLocationValidation = void 0;
const joi_1 = __importDefault(require("joi"));
exports.CreateLocationValidation = joi_1.default.object({
    name: joi_1.default.string().min(3).max(50).required(),
});
exports.UpdateLocationValidation = joi_1.default.object({
    id: joi_1.default.string().alphanum().required(),
    name: joi_1.default.string().min(3).max(50).required(),
    status: joi_1.default.string().allow(null, '')
});
//# sourceMappingURL=location.validation.js.map
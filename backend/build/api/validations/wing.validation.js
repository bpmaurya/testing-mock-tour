"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WingIdValidation = exports.UpdateWingValidation = exports.CreateWingValidation = void 0;
const joi_1 = __importDefault(require("joi"));
exports.CreateWingValidation = joi_1.default.object({
    name: joi_1.default.string().min(3).max(50).required(),
    location: joi_1.default.string().alphanum().required(),
    floor: joi_1.default.string().alphanum().required(),
    status: joi_1.default.string().allow(null, '')
});
exports.UpdateWingValidation = joi_1.default.object({
    id: joi_1.default.string().alphanum().required(),
    name: joi_1.default.string().min(3).max(50).required(),
    location: joi_1.default.string().alphanum().required(),
    floor: joi_1.default.string().alphanum().required(),
    status: joi_1.default.string().allow(null, '')
});
exports.WingIdValidation = joi_1.default.string().alphanum().required();
//# sourceMappingURL=wing.validation.js.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const logger_1 = require("./logger");
mongoose_1.default.set("strictQuery", false);
exports.default = (database) => {
    const connect = () => {
        mongoose_1.default
            .connect(database)
            .then(() => logger_1.logger.info(`Database connection successful.....`))
            .catch((error) => {
            logger_1.logger.info("Unable to connect to the db: " + error.message);
            return process.exit(1);
        });
    };
    connect();
    mongoose_1.default.connection.on("disconnected", () => {
        logger_1.logger.info(`Database disconnected`);
    });
    process.on("SIGINT", async () => {
        await mongoose_1.default.connection.close();
        process.exit(0);
    });
};
//# sourceMappingURL=db.js.map
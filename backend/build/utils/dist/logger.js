"use strict";
exports.__esModule = true;
exports.logger = void 0;
var winston_1 = require("winston");
var config_1 = require("../config");
var combine = winston_1.format.combine, timestamp = winston_1.format.timestamp, printf = winston_1.format.printf;
var loggerFormat = printf(function (info) {
    return info.timestamp + " | " + info.level + ": " + info.message;
});
exports.logger = winston_1.createLogger({
    level: config_1.config.loggerLevel,
    format: combine(winston_1.format.colorize(), timestamp(), loggerFormat),
    transports: [new winston_1.transports.Console()]
});
//# sourceMappingURL=logger.js.map
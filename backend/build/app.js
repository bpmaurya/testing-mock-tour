"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Application = void 0;
const express_1 = __importDefault(require("express"));
const helmet_1 = __importDefault(require("helmet"));
const body_parser_1 = __importDefault(require("body-parser"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
const db_1 = __importDefault(require("./utils/db"));
const config_1 = require("./config");
const logger_1 = require("./utils/logger");
const routes_1 = __importDefault(require("./api/routes"));
const http_1 = __importDefault(require("http"));
const socket_controller_1 = require("./api/controllers/socket.controller");
const swagger_ui_express_1 = __importDefault(require("swagger-ui-express"));
const swagger_output_json_1 = __importDefault(require("./swagger_output.json"));
class Application {
    constructor() {
        this.corsOptions = {
            origin: ['http://localhost:3000', "http://192.168.2.219:8100"],
            optionsSuccessStatus: 200,
        };
        this.app = (0, express_1.default)();
        this.httpServer = http_1.default.createServer(this.app);
        this.middlewares();
        this.routes();
        this.dbConnect();
    }
    dbConnect() {
        const dbConnectionString = config_1.config.db.database;
        (0, db_1.default)(dbConnectionString);
    }
    middlewares() {
        this.app.use(express_1.default.urlencoded({ extended: false }));
        this.app.use(express_1.default.json());
        this.app.use((0, helmet_1.default)({
            crossOriginResourcePolicy: { policy: "cross-origin" },
            contentSecurityPolicy: false,
        }));
        this.app.use(body_parser_1.default.json());
        this.app.use(body_parser_1.default.urlencoded({ extended: true }));
        this.app.use((0, morgan_1.default)("dev"));
        this.app.use((0, cors_1.default)(this.corsOptions));
        let io = socket_controller_1.socketIoServer.getIo(this.httpServer);
        this.app.set('socketIO', io);
    }
    routes() {
        this.app.use("/api", routes_1.default);
        this.app.use('/api-docs', swagger_ui_express_1.default.serve, swagger_ui_express_1.default.setup(swagger_output_json_1.default));
        const router = express_1.default.Router();
        this.app.use(router);
        this.app.use((error, res, next) => {
            try {
                const status = error.status || 500;
                const message = error.message ||
                    "There was an error while processing your request, please try again";
                return res.status(status).send({
                    status,
                    message,
                });
            }
            catch (error) {
                next(error);
            }
        });
    }
    start() {
        const port = config_1.config.port;
        this.httpServer.listen(port, async () => {
            logger_1.logger.info(`🤖 Server -Seat-Sync- is running on port ${port}, path http://localhost:${port}`);
        });
    }
}
exports.Application = Application;
async function main() {
    const app = new Application();
    app.start();
}
main();
//# sourceMappingURL=app.js.map
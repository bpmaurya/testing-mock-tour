import { Router } from "express";
import { UserController } from "../controllers/user.controller";
import { requireAuth } from "../services/auth.service";
import upload from "../controllers/upload";

export class UserRoutes {

	router: Router;
	public userController: UserController = new UserController();

	constructor() {
		this.router = Router();
		this.routes();
	}
	routes() {
		this.router.post("/register", this.userController.registerUser);
		this.router.post("/login", this.userController.authenticateUser);
		this.router.post("/update", requireAuth, this.userController.updateUser);
		this.router.post('/update-user/:user_id/profile', upload.single('profile'), this.userController.updateProfilePictureUser)
		this.router.post('/update-password/:user_id', this.userController.updateUserPassword)
	}
}
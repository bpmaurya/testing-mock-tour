import { Router } from "express";
import { AdminController } from "../controllers/admin.controller";
import { FloorController } from "../controllers/floor.controller";
import { WingController } from "../controllers/wing.controller";
import { DeskController } from "../controllers/desk.controller";
import { RequestController } from "../controllers/request.controller";
import { DesignationController } from "../controllers/designation.controller";
import { DepartmentController } from "../controllers/department.controller";
import { ProjectController } from "../controllers/project.controller";
import { requireAuth } from "../services/auth.service";
import { NotificationController } from "../controllers/notification.controller";

export class AdminRoutes {

	router: Router;
	public adminController: AdminController = new AdminController();
	public floorController: FloorController = new FloorController();
	public wingController: WingController = new WingController();
	public deskController: DeskController = new DeskController();
	public requestController: RequestController = new RequestController();
	public designationController: DesignationController = new DesignationController();
	public departmentController: DepartmentController = new DepartmentController();
	public projectController: ProjectController = new ProjectController()
	public notificationController: NotificationController = new NotificationController()

	constructor() {
		this.router = Router();
		this.routes();
	}
	routes() {
		this.router.post("/register", this.adminController.registerAdmin);
		this.router.get('/all-user', requireAuth, this.adminController.getAllUser)

		//location route
		this.router.get('/all-location', requireAuth, this.adminController.getAllLocation)
		this.router.post('/create-location', requireAuth, this.adminController.createLocation)
		this.router.post('/delete-user', requireAuth, this.adminController.deleteUser)
		this.router.post('/delete-location', requireAuth, this.adminController.deleteLocation)
		this.router.post('/location-update', requireAuth, this.adminController.updateLocation)

		//floor route
		this.router.post('/create-floor', requireAuth, this.floorController.createFloor)
		this.router.post('/update-floor', requireAuth, this.floorController.updateFloor)
		this.router.post('/delete-floor', requireAuth, this.floorController.deleteFloor)
		this.router.get('/all-floor/:location', requireAuth, this.floorController.getAllFloorWithLocation)
		this.router.get('/all-floor', requireAuth, this.floorController.getAllFloor)

		//wing route
		this.router.get('/all-wings', requireAuth, this.wingController.getAllWing)
		this.router.get('/all-wings/:location/:floor', requireAuth, this.wingController.getAllWingForLocationAndFloor)
		this.router.post('/update-wing', requireAuth, this.wingController.updateWing)
		this.router.post('/delete-wing', requireAuth, this.wingController.deleteWing)
		this.router.post('/create-wing', requireAuth, this.wingController.createWing)

		//desk route
		this.router.post('/create-desk', requireAuth, this.deskController.createDesk)
		this.router.get('/all-desks/:location/:floor/:wing', requireAuth, this.deskController.getAllDesks)
		this.router.post('/delete-desk', requireAuth, this.deskController.deleteDesk)
		this.router.post('/update-desk', requireAuth, this.deskController.updateDesk)

		//request route
		this.router.post('/create-request', requireAuth, this.requestController.createRequest)
		this.router.post('/update-request', requireAuth, this.requestController.updateDeskRequest)
		this.router.post('/desk-book-request', requireAuth, this.deskController.createBookDeskRequest)
		this.router.get('/all-deskrequest', requireAuth, this.requestController.getAllDeskRequests)
		this.router.get('/all-desk-request/:user', requireAuth, this.requestController.getAllDeskRequestsForUser)

		//department route
		this.router.get('/all-department', requireAuth, this.departmentController.getAllDepartments)
		this.router.post('/create-department', requireAuth, this.departmentController.createDepartment)
		this.router.post('/delete-department', requireAuth, this.departmentController.deleteDepartment)
		this.router.post('/update-department', requireAuth, this.departmentController.updateDepartment)

		//project route
		this.router.get('/all-projects', requireAuth, this.projectController.getAllProjects)
		this.router.post('/create-projects', requireAuth, this.projectController.createProject)
		this.router.post('/update-project', requireAuth, this.projectController.updateProject)
		this.router.post('/delete-project', requireAuth, this.projectController.deleteProject)

		//designation route
		this.router.get('/all-designation', requireAuth, this.designationController.getAllDesignation)
		this.router.post('/create-designation', requireAuth, this.designationController.createDesignation)
		this.router.post('/delete-designation', requireAuth, this.designationController.deleteDesignation)
		this.router.post('/update-designation', requireAuth, this.designationController.updateDesignation)

		//maintain desk order
		this.router.post('/update-desk-index', requireAuth, this.deskController.updateDeskOrder)

		//get all notification 
		this.router.get('/get-all-notification/:user_id', requireAuth, this.notificationController.getAllNotification)
	}
}
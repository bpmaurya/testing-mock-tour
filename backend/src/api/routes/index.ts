import { Router } from "express";
import { UserRoutes } from "./user.route";
import { AdminRoutes } from "./admin.route";

const router = Router();
router.use('/user', new UserRoutes().router)
router.use('/admin', new AdminRoutes().router)
export default router;

import Joi from 'joi'

export const CreateFloorValidation = Joi.object({
	name: Joi.string().min(3).max(50).required(),
	location: Joi.string().alphanum().required(),
	status: Joi.string().allow(null, '')
});

export const UpdateFloorValidation = Joi.object({
	id: Joi.string().alphanum().required(),
	name: Joi.string().min(3).max(50).required(),
	location: Joi.string().alphanum().required(),
	status: Joi.string().allow(null, '')
});
export const FloorIdValidation = Joi.string().alphanum().required()
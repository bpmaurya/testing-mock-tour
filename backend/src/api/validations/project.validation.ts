import Joi from 'joi';

export const CreateProjectValidation = Joi.object({
	name: Joi.string().min(3).max(50).required(),
});

export const UpdateProjectValidation = Joi.object({
	id: Joi.string().alphanum().required(),
	name: Joi.string().min(3).max(50).required(),
	status: Joi.string().allow(null, '')
});
export const ProjectIdValidation = Joi.string().alphanum().required()
import Joi from 'joi';

export const CreateLocationValidation = Joi.object({
	name: Joi.string().min(3).max(50).required(),
});

export const UpdateLocationValidation = Joi.object({
	id: Joi.string().alphanum().required(),
	name: Joi.string().min(3).max(50).required(),
	status: Joi.string().allow(null, '')
});
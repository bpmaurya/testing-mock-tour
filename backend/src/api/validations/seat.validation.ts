import Joi from 'joi'

export const CreateSeatValidation = Joi.object({
	row: Joi.string().min(1).max(50).required(),
	start: Joi.number().min(1).max(50).required(),
	end: Joi.number().min(1).max(15).required(),
	location: Joi.string().alphanum().required(),
	floor: Joi.string().alphanum().required(),
	wing: Joi.string().alphanum().required(),
	status: Joi.string().allow(null, ''),
	orderIndex: Joi.number().allow(null)
});

export const DeleteSeatValidation = Joi.object({
	row: Joi.string().min(1).max(50).required(),
	location: Joi.string().alphanum().required(),
	floor: Joi.string().alphanum().required(),
	wing: Joi.string().alphanum().required(),
	status: Joi.string().allow(null, '')
});

export const UpdateSeatValidation = Joi.object({
	id: Joi.string().alphanum().required(),
	name: Joi.string().required(),
	row: Joi.string().min(1).max(50).required(),
	location: Joi.string().alphanum().required(),
	floor: Joi.string().alphanum().required(),
	wing: Joi.string().alphanum().required(),
	status: Joi.string().allow(null, ''),
	isSelectable: Joi.boolean().allow(null, '')
});

export const CreateBookSeatValidation = Joi.object({
	id: Joi.string().alphanum().required(),
	name: Joi.string().required(),
	row: Joi.string().min(1).max(50).required(),
	location: Joi.string().alphanum().required(),
	floor: Joi.string().alphanum().required(),
	wing: Joi.string().alphanum().required(),
	status: Joi.string().allow(null, ''),
	bookedBy: Joi.string().alphanum().required(),
	isSelectable: Joi.boolean().allow(null, '')
});
export const SeatIdValidation = Joi.string().alphanum().required()
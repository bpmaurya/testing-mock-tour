import Joi from 'joi';

export const UserRegistrationValidation = Joi.object({
	firstName: Joi.string().min(3).max(50).required(),
	middleName: Joi.string().allow(null, ''),
	lastName: Joi.string().min(3).max(50).required(),
	email: Joi.string().email().min(6).max(255).required(),
	domain: Joi.string().min(3).max(50).required(),
	department: Joi.string().alphanum().required(),
	password: Joi.string().min(6).max(50).required(),
	role: Joi.string().required(),
	position: Joi.string().alphanum().required(),
	phone: Joi.number().required(),
	pastExperience: Joi.string().required(),
	project: Joi.string().alphanum().allow(null, '')
});

export const UserLoginValidation = Joi.object({
	email: Joi.string().email().min(6).max(255).required(),
})

export const UserIdValidation = Joi.string().alphanum().required();

export const UserUpdateValidation = Joi.object({
	firstName: Joi.string().min(3).max(50).required(),
	middleName: Joi.string().allow(null, ''),
	lastName: Joi.string().min(3).max(50).required(),
	email: Joi.string().email().min(6).max(255).required(),
	domain: Joi.string().min(3).max(50).required(),
	department: Joi.string().alphanum().required(),
	role: Joi.string().required(),
	position: Joi.string().alphanum().required(),
	phone: Joi.number().required(),
	pastExperience: Joi.string().required(),
	project: Joi.string().alphanum().allow(null, '')
})

export const LocationIdValidation = Joi.string().alphanum().required()
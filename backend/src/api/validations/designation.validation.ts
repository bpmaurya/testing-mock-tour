import Joi from 'joi';

export const CreateDesignationValidation = Joi.object({
	name: Joi.string().min(3).max(50).required(),
});

export const UpdateDesignationValidation = Joi.object({
	id: Joi.string().alphanum().required(),
	name: Joi.string().min(3).max(50).required(),
	status: Joi.string().allow(null, '')
});

export const DesignationIdValidation = Joi.string().alphanum().required()
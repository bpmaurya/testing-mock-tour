import Joi from 'joi';

export const CreateDepartmentValidation = Joi.object({
	name: Joi.string().min(3).max(50).required(),
});

export const UpdateDepartmentValidation = Joi.object({
	id: Joi.string().alphanum().required(),
	name: Joi.string().min(3).max(50).required(),
	status: Joi.string().allow(null, '')
});

export const DepartmentIdValidation = Joi.string().alphanum().required()
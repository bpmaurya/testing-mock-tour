import Joi from 'joi'

export const CreateWingValidation = Joi.object({
	name: Joi.string().min(3).max(50).required(),
	location: Joi.string().alphanum().required(),
	floor: Joi.string().alphanum().required(),
	status: Joi.string().allow(null, '')
});

export const UpdateWingValidation = Joi.object({
	id: Joi.string().alphanum().required(),
	name: Joi.string().min(3).max(50).required(),
	location: Joi.string().alphanum().required(),
	floor: Joi.string().alphanum().required(),
	status: Joi.string().allow(null, '')
});
export const WingIdValidation = Joi.string().alphanum().required()
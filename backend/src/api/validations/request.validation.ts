import Joi from 'joi'

export const CreateRequestValidation = Joi.object({
	user: Joi.string().alphanum().required(),
	reportingManager: Joi.string().alphanum().required(),
	project: Joi.string().alphanum().required(),
	seat: Joi.string().alphanum().required(),
	description: Joi.string().allow(null, ''),
	priority: Joi.string().allow(null, ''),
	status: Joi.string().allow(null, '')
});

export const UpdateRequestValidation = Joi.object({
	_id: Joi.string().alphanum().required(),
	user: Joi.string().alphanum().required(),
	reportingManager: Joi.string().alphanum().required(),
	project: Joi.string().alphanum().required(),
	seat: Joi.string().alphanum().required(),
	description: Joi.string().allow(null, ''),
	priority: Joi.string().allow(null, ''),
	status: Joi.string().required()
});



export const DeleteRequestValidation = Joi.object({
	user: Joi.string().alphanum().required(),
	seat: Joi.string().alphanum().required(),
});

export const RequestIdValidation = Joi.string().alphanum().required()
import mongoose, { Schema } from "mongoose";
import { FloorType } from "../../types";

const FloorSchema: Schema = new Schema(
	{
		name: {
			type: String,
			unique: false
		},
		location: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Location',
		},
		status: {
			type: Boolean,
			required: false
		}
	},
	{ timestamps: true }
);


const Floor = mongoose.model<FloorType>("Floor", FloorSchema);
export default Floor;
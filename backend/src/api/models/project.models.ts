import mongoose, { Schema } from "mongoose";
import { IProject } from "../../types";

const ProjectSchema: Schema = new Schema(
	{
		name: {
			type: String,
			required: true,
			unique: false,
		},
		status: {
			type: Boolean,
			required: false
		},
	},
	{ timestamps: true }
);


const Project = mongoose.model<IProject>("Project", ProjectSchema);
export default Project;
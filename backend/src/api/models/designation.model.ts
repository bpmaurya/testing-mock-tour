import mongoose, { Schema } from "mongoose";
import { IDesignation } from "../../types";

const DesignationSchema: Schema = new Schema(
	{
		name: {
			type: String,
			required: true,
			unique: false,
		},
		status: {
			type: Boolean,
			required: false
		},
	},
	{ timestamps: true }
);


const Designation = mongoose.model<IDesignation>("Designation", DesignationSchema);
export default Designation;
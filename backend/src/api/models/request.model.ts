import mongoose, { Schema } from "mongoose";
import { RequestType } from "../../types";

const RequestSchema: Schema = new Schema(
	{
		user: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'User',
		},
		seat: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Seat'
		},
		reportingManager: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'User'
		},
		project: {
			type: mongoose.Schema.Types.ObjectId,
			ref: "Project"
		},
		description: {
			type: String,
			required: false
		},
		priority: {
			type: String,
			required: false
		},
		status: {
			type: String,
			default: 'pending',
			required: false
		}
	},
	{ timestamps: true }
);


const DeskRequest = mongoose.model<RequestType>("Request", RequestSchema);
export default DeskRequest;
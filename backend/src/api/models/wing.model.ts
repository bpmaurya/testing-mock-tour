import mongoose, { Schema } from "mongoose";
import { WingType } from "../../types";

const WingSchema: Schema = new Schema(
	{
		name: {
			type: String,
			unique: false
		},
		location: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Location',
		},
		floor: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Floor',
		},
		status: {
			type: Boolean,
			required: false
		}
	},
	{ timestamps: true }
);


const Wing = mongoose.model<WingType>("Wing", WingSchema);
export default Wing;
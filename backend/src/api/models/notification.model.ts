import mongoose, { Schema } from "mongoose";
import { INotification } from "../../types";

const NotificationSchema: Schema = new Schema(
	{
		user: {
			type: mongoose.Schema.ObjectId,
			ref: 'User'
		},
		request: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Request',
		},
		isRead: {
			type: Boolean,
			required: false
		},
	},
	{ timestamps: true }
);


const Notification = mongoose.model<INotification>("Notification", NotificationSchema);
export default Notification;
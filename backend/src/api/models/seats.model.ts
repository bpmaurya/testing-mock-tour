import mongoose, { Schema } from "mongoose";
import { SeatType } from "../../types";

const SeatSchema: Schema = new Schema(
	{
		name: {
			type: String,
			required: true
		},
		row: {
			type: String,
			required: true
		},
		location: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Location',
		},
		floor: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Floor'
		},
		wing: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Wing'
		},
		bookedBy: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'User',
			required: false,
		},
		bookedStatus: {
			type: String,
			required: false
		},
		status: {
			type: Boolean,
			required: false
		},
		isSelectable: {
			type: Boolean,
			required: false,
			default: true
		},
		orderIndex: {
			type: Number,
			required: false,
		}
	},
	{ timestamps: true }
);


const OfficeSeat = mongoose.model<SeatType>("Seat", SeatSchema);
export default OfficeSeat;
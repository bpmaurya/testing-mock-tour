import mongoose, { Schema } from "mongoose";
import { ILocation } from "../../types";

const LocationSchema: Schema = new Schema(
	{
		name: {
			type: String,
			required: true,
			unique: false,
		},
		status: {
			type: Boolean,
			required: false
		},
	},
	{ timestamps: true }
);


const OfficeLocation = mongoose.model<ILocation>("Location", LocationSchema);
export default OfficeLocation;
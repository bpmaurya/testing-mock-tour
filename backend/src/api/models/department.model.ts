import mongoose, { Schema } from "mongoose";
import { IDepartment } from "../../types";

const DepartmentSchema: Schema = new Schema(
	{
		name: {
			type: String,
			required: true,
			unique: false,
		},
		status: {
			type: Boolean,
			required: false
		},
	},
	{ timestamps: true }
);


const Department = mongoose.model<IDepartment>("Department", DepartmentSchema);
export default Department;
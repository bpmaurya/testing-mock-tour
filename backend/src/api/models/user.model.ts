import mongoose, { Schema } from "mongoose";
import { IUser } from "../../types/user.types";

const UserSchema: Schema = new Schema(
	{
		email: {
			type: String,
			required: true,
			unique: true,
			lowercase: true,
			min: 6,
			max: 255,
			// match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
		},
		firstName: {
			type: String,
			required: true,
		},
		middleName: {
			type: String,
			required: false,
		},
		lastName: {
			type: String,
			required: true
		},
		pastExperience: {
			type: String,
			required: true
		},
		department: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Department',
		},
		domain: {
			type: String,
		},
		password: {
			type: String,
			required: false,
		},
		tempPassword: {
			type: String,
			required: false
		},
		role: {
			type: String,
			required: false
		},
		position: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Designation',
		},
		project: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Project',
		},
		phone: {
			type: Number,
			required: true,
		},
		avatar: {
			type: Buffer,
			required: false
		},
	},
	{ timestamps: true }
);


const User = mongoose.model<IUser>("User", UserSchema);
export default User;
import { NextFunction, Response, Request } from "express";
import { CreateBookSeatValidation, CreateSeatValidation, DeleteSeatValidation, UpdateSeatValidation } from "../validations/seat.validation";
import { Messages } from "../../utils/constant";
import OfficeSeat from "../models/seats.model";
import { logger } from "../../utils/logger";
import { SeatType } from "../../types";

export class DeskController {
	public createDesk = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const deskValidation = await CreateSeatValidation.validateAsync(req.body)

			if (!deskValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {
				try {
					const foundDesk = await OfficeSeat.find({ location: req.body.location, floor: req.body.floor, wing: req.body.wing, row: req.body.row });
					if (foundDesk.length !== 0) {
						return res.status(400).send({
							message: `Row ${req.body.row} not available! Already exist!`
						})
					} else {
						let deskCollection = []
						for (let i = req.body.start; i <= req.body.end; i++) {
							deskCollection.push({
								name: `${req.body.row}-${i}`,
								row: req.body.row,
								location: req.body.location,
								floor: req.body.floor,
								wing: req.body.wing,
								status: req.body.status,
							});
						}
						deskCollection.map(async e => {
							const desk = new OfficeSeat(e)
							await desk.save();
						})
						return res.status(200).send({ message: Messages.DESK_CREATE_SUCCESS });

					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			logger.info(err.message)
			logger.info(err)
			return res.status(400).send({ message: err.message });
		}
	};

	public deleteDesk = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const seatValidation = await DeleteSeatValidation.validateAsync(req.body)

			if (!seatValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const foundDesk = await OfficeSeat.findOne({
						location: seatValidation.location,
						floor: seatValidation.floor,
						wing: seatValidation.wing,
						row: seatValidation.row
					});
					if (!foundDesk) {
						return res.status(400).send({
							message: `Desk not exist!!`
						})
					}
					else {
						const deleteDesk = await OfficeSeat.deleteMany({
							location: seatValidation.location,
							floor: seatValidation.floor,
							wing: seatValidation.wing,
							row: seatValidation.row
						})
						if (deleteDesk)
							return res.status(200).send({ message: Messages.DESK_DELETE_SUCCESS });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			console.log(err.message)
			console.log(err)
			return res.status(400).send({ message: err.message });
		}
	}


	public async updateDesk(req: Request, res: Response, next: NextFunction) {
		var io = req.app.get('socketIO');
		try {
			logger.info(JSON.stringify(req.body))
			const deskValidation = await UpdateSeatValidation.validateAsync(req.body)

			if (!deskValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {
				try {
					const isSeatValid = await OfficeSeat.findOne({
						_id: deskValidation.id
					});
					if (!isSeatValid) {
						return res.status(400).send({
							message: Messages.INVALID_DETAIL
						})
					}
					else {
						const updateSeat = await OfficeSeat.findOneAndUpdate(
							{
								_id: isSeatValid.id
							},
							{
								$set: {
									name: req.body.name,
									row: req.body.row,
									location: req.body.location,
									floor: req.body.floor,
									wing: req.body.wing,
									status: req.body.status,
									isSelectable: req.body.isSelectable
								}
							}
						);
						if (updateSeat) {
							io.emit('update-message', { seat: isSeatValid.id, selectable: req.body.isSelectable });
							return res.status(200).send({ message: Messages.DESK_UPDATE_SUCCESS, });
						}
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			console.log(err.message)
			console.log(err)
			return res.status(400).send({ message: err.message });
		}
	}


	public getAllDesks = async (req: Request, res: Response, next: NextFunction) => {

		try {
			const allSeat = await OfficeSeat.find({ location: req.params.location, floor: req.params.floor, wing: req.params.wing }).populate([{
				path: 'floor',
				model: 'Floor'
			}, { path: 'location', model: 'Location' },
			{ path: 'wing', model: 'Wing' }, {
				path: 'bookedBy', model: 'User', select: "-password", populate: [{
					path: 'department',
					model: 'Department'
				}, { path: 'position', model: "Designation" }, { path: 'project', model: "Project" }]
			}])
			if (allSeat)
				return res.status(200).send({ "allSeat": allSeat });
		} catch (error) {
			return res.status(400).send(error);
		}
	};


	public async createBookDeskRequest(req: Request, res: Response, next: NextFunction) {
		try {
			logger.info(JSON.stringify(req.body))
			const deskValidation = await CreateBookSeatValidation.validateAsync(req.body)

			if (!deskValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {
				try {
					const isSeatValid = await OfficeSeat.findOne({
						_id: deskValidation.id
					});
					if (!isSeatValid) {
						return res.status(400).send({
							message: Messages.INVALID_DETAIL
						})
					}
					else {
						const bookSeat = await OfficeSeat.findOneAndUpdate(
							{
								_id: isSeatValid.id
							},
							{
								$set: {
									name: req.body.name,
									row: req.body.row,
									location: req.body.location,
									floor: req.body.floor,
									wing: req.body.wing,
									status: req.body.status,
									bookedBy: req.body.bookedBy,
									isSelectable: false
								}
							}
						);
						if (bookSeat)
							return res.status(200).send({ message: Messages.DESK_BOOK_REQUEST_SUCCESS, });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			console.log(err.message)
			console.log(err)
			return res.status(400).send({ message: err.message });
		}
	}



	public updateDeskOrder = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const providedDesk = req.body.data;
			if (!providedDesk) {
				return res.status(400).send({
					message: `data not provided!`
				})
			} else {
				let data = req.body.data
				const update = data.map(async (e: any) => {
					return e.seats.map(async (f: any) => {
						await OfficeSeat.findOneAndUpdate(
							{
								_id: f._id
							},
							{
								$set: {
									orderIndex: e.orderIndex
								}
							}
						);

					})
				})

				if (update)
					return res.status(200).send({ message: Messages.DESK_UPDATE_SUCCESS, });
				else {
					return next(
						res.status(400).json({
							message: Messages.INVALID_DETAIL
						})
					);
				}

			}
		} catch (error) {
			return res.status(400).send({ message: Messages.INVALID_DETAIL });
		}
	}


}
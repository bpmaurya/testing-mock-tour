import bcrypt from "bcrypt-nodejs";
import { NextFunction, Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { config } from "../../config";
import User from "../models/user.model";
import OfficeLocation from "../models/location.model";
import { logger } from "../../utils/logger";
import { CreateLocationValidation, UpdateLocationValidation } from "../validations/location.validation";
import { LocationIdValidation, UserIdValidation } from "../validations/user.validation";
import { Messages } from "../../utils/constant";


export class AdminController {

	public async registerAdmin(req: Request, res: Response): Promise<void> {
		try {
			const hashedPassword = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
			await User.create({
				username: req.body.username,
				password: hashedPassword,
			});

			const token = jwt.sign({ username: req.body.username, scope: req.body.scope }, config.jwt_secret);
			res.status(200).send({ token: token });
		} catch (error) {
			res.send({ error: Messages.INVALID_DETAIL });
		}
	}

	public getAllUser = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const allUser = await User.find({}, { showCredentials: false }).select(["-password", "-tempPassword"]).populate([{
				path: 'position',
				model: 'Designation'
			}, { path: 'project', model: 'Project' },
			{ path: 'department', model: 'Department' }
			])
			if (allUser.length !== 0)
				return res.status(200).send({ "alluser": allUser });
		} catch (error) {
			return res.status(400).send(error);
		}
	};

	public getAllLocation = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const allLocation = await OfficeLocation.find({})
			if (allLocation)
				return res.status(200).send({ "allLocation": allLocation });
		} catch (error) {
			return res.status(400).send(error);
		}
	};

	public createLocation = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const locationValidation = await CreateLocationValidation.validateAsync(req.body)

			if (!locationValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {
				try {
					const foundLocation = await OfficeLocation.findOne({ name: req.body.name });
					if (foundLocation) {
						return res.status(400).send({
							message: `Location ${req.body.name} not available! Already exist!`
						})
					}
					else {
						const location = new OfficeLocation({
							name: req.body.name,
							status: req.body.status
						});
						await location.save()
						return res.status(200).send({ message: Messages.LOCATION_CREATE_SUCCESS });
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			logger.info(err.message)
			logger.info(err)
			return res.status(400).send({ message: err.message });
		}
	};

	public deleteUser = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const idValidation = await UserIdValidation.validateAsync(req.body.id)

			if (!idValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const foundUser = await User.findOne({ _id: req.body.id });
					if (!foundUser) {
						return res.status(400).send({
							message: `User not exist!`
						})
					}
					else {
						const deleteUser = await User.deleteOne({ _id: idValidation })
						if (deleteUser)
							return res.status(200).send({ message: Messages.USER_DELETE_SUCCESS });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			logger.info(err.message)
			logger.info(err)
			return res.status(400).send({ message: err.message });
		}
	}

	public deleteLocation = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const idValidation = await LocationIdValidation.validateAsync(req.body.id)

			if (!idValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const foundLocation = await OfficeLocation.findOne({ _id: req.body.id });
					if (!foundLocation) {
						return res.status(400).send({
							message: `Location not exist!!`
						})
					}
					else {
						const deleteLocation = await OfficeLocation.deleteOne({ _id: idValidation })
						if (deleteLocation)
							return res.status(200).send({ message: Messages.LOCATION_DELETE_SUCCESS });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			console.log(err.message)
			console.log(err)
			return res.status(400).send({ message: err.message });
		}
	}


	public async updateLocation(req: Request, res: Response, next: NextFunction) {
		try {
			logger.info(JSON.stringify(req.body))
			const locationValidation = await UpdateLocationValidation.validateAsync(req.body)

			if (!locationValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const isLocationValid = await OfficeLocation.findOne({
						_id: locationValidation.id
					});
					if (!isLocationValid) {
						return res.status(400).send({
							message: Messages.INVALID_DETAIL
						})
					}
					else {
						const updateLocation = await OfficeLocation.findOneAndUpdate(
							{
								_id: isLocationValid.id
							},
							{
								$set: {
									name: req.body.name,
									status: req.body.status
								}
							}
						);
						if (updateLocation)
							return res.status(200).send({ message: Messages.LOCATION_UPDATE_SUCCESS, });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			console.log(err.message)
			console.log(err)
			return res.status(400).send({ message: err.message });
		}
	}


}
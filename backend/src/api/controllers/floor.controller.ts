import { NextFunction, Request, Response } from "express";
import { CreateFloorValidation, FloorIdValidation, UpdateFloorValidation } from "../validations/floor.validation";
import { Messages } from "../../utils/constant";
import Floor from "../models/floor.model";
import { logger } from "../../utils/logger";

export class FloorController {

	public createFloor = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const floorValidation = await CreateFloorValidation.validateAsync(req.body)

			if (!floorValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {
				try {
					const foundLocation = await Floor.findOne({ name: req.body.name, location: req.body.location });
					if (foundLocation) {
						return res.status(400).send({
							message: `Floor ${req.body.name} not available! Already exist!`
						})
					}
					else {
						const floor = new Floor({
							name: req.body.name,
							location: req.body.location
						});
						await floor.save()
						return res.status(200).send({ message: Messages.FLOOR_CREATE_SUCCESS });
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			logger.info(err.message)
			logger.info(err)
			return res.status(400).send({ message: err.message });
		}
	};

	public async updateFloor(req: Request, res: Response, next: NextFunction) {
		try {
			logger.info(JSON.stringify(req.body))
			const floorValidation = await UpdateFloorValidation.validateAsync(req.body)

			if (!floorValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const isFloorValid = await Floor.findOne({
						_id: floorValidation.id
					});
					if (!isFloorValid) {
						return res.status(400).send({
							message: Messages.INVALID_DETAIL
						})
					}
					else {
						const updateFloor = await Floor.findOneAndUpdate(
							{
								_id: isFloorValid.id
							},
							{
								$set: {
									name: req.body.name,
									location: req.body.location,
									status: req.body.status
								}
							}
						);
						if (updateFloor)
							return res.status(200).send({ message: Messages.FLOOR_UPDATE_SUCCESS, });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			console.log(err.message)
			console.log(err)
			return res.status(400).send({ message: err.message });
		}
	}


	public deleteFloor = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const idValidation = await FloorIdValidation.validateAsync(req.body.id)

			if (!idValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const foundLocation = await Floor.findOne({ _id: req.body.id });
					if (!foundLocation) {
						return res.status(400).send({
							message: `Floor not exist!!`
						})
					}
					else {
						const deleteFloor = await Floor.deleteOne({ _id: idValidation })
						if (deleteFloor)
							return res.status(200).send({ message: Messages.FLOOR_DELETE_SUCCESS });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			console.log(err.message)
			console.log(err)
			return res.status(400).send({ message: err.message });
		}
	}


	public getAllFloor = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const allFloor = await Floor.find({}).populate('location')
			if (allFloor)
				return res.status(200).send({ "allFloor": allFloor });
		} catch (error) {
			return res.status(400).send(error);
		}
	};

	public getAllFloorWithLocation = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const allFloor = await Floor.find({ location: (req.params.location || { $exists: true }) }).populate('location')
			if (allFloor)
				return res.status(200).send({ "allFloor": allFloor });
		} catch (error) {
			return res.status(400).send(error);
		}
	};
}
import { NextFunction, Request, Response } from "express";
import { Messages } from "../../utils/constant";
import { logger } from "../../utils/logger";
import { CreateWingValidation, UpdateWingValidation, WingIdValidation } from "../validations/wing.validation";
import Wing from "../models/wing.model";

export class WingController {

	public createWing = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const wingValidation = await CreateWingValidation.validateAsync(req.body)

			if (!wingValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {
				try {
					const foundWing = await Wing.findOne({ name: req.body.name, location: req.body.location, floor: req.body.floor });
					if (foundWing) {
						return res.status(400).send({
							message: `Wing ${req.body.name} not available! Already exist!`
						})
					}
					else {
						const wing = new Wing({
							name: req.body.name,
							location: req.body.location,
							floor: req.body.floor
						});
						await wing.save()
						return res.status(200).send({ message: Messages.WING_CREATE_SUCCESS });
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			logger.info(err.message)
			logger.info(err)
			return res.status(400).send({ message: err.message });
		}
	};

	public async updateWing(req: Request, res: Response, next: NextFunction) {
		try {
			logger.info(JSON.stringify(req.body))
			const wingValidation = await UpdateWingValidation.validateAsync(req.body)

			if (!wingValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const isWingValid = await Wing.findOne({
						_id: wingValidation.id
					});
					if (!isWingValid) {
						return res.status(400).send({
							message: Messages.INVALID_DETAIL
						})
					}
					else {
						const updateWing = await Wing.findOneAndUpdate(
							{
								_id: isWingValid.id
							},
							{
								$set: {
									name: req.body.name,
									location: req.body.location,
									floor: req.body.floor,
									status: req.body.status
								}
							}
						);
						if (updateWing)
							return res.status(200).send({ message: Messages.WING_UPDATE_SUCCESS, });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			console.log(err.message)
			console.log(err)
			return res.status(400).send({ message: err.message });
		}
	}


	public deleteWing = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const idValidation = await WingIdValidation.validateAsync(req.body.id)

			if (!idValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const foundWing = await Wing.findOne({ _id: req.body.id });
					if (!foundWing) {
						return res.status(400).send({
							message: `Wing not exist!!`
						})
					}
					else {
						const deleteWing = await Wing.deleteOne({ _id: idValidation })
						if (deleteWing)
							return res.status(200).send({ message: Messages.WING_DELETE_SUCCESS });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			console.log(err.message)
			console.log(err)
			return res.status(400).send({ message: err.message });
		}
	}


	public getAllWing = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const allWing = await Wing.find({ location: (req.body.location || { $exists: true }), floor: (req.body.floor || { $exists: true }) }).populate(['floor', 'location'])
			if (allWing)
				return res.status(200).send({ "allWing": allWing });
		} catch (error) {
			return res.status(400).send(error);
		}
	};

	public getAllWingForLocationAndFloor = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const allWing = await Wing.find({ location: (req.params.location || { $exists: true }), floor: (req.params.floor || { $exists: true }) }).populate(['floor', 'location'])
			if (allWing)
				return res.status(200).send({ "allWing": allWing });
		} catch (error) {
			return res.status(400).send(error);
		}
	};
}
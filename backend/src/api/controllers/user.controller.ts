import bcrypt from "bcrypt-nodejs";
import { NextFunction, Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { config } from "../../config";
import { logger } from "../../utils/logger";
import User from "../models/user.model";
import { UserIdValidation, UserRegistrationValidation, UserUpdateValidation } from "../validations/user.validation";
import { Messages } from "../../utils/constant";


export class UserController {

	public async registerUser(req: Request, res: Response, next: NextFunction) {
		try {
			const userValidation = await UserRegistrationValidation.validateAsync(req.body)

			if (!userValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const foundUser = await User.findOne({ email: req.body.email });
					if (foundUser) {
						return res.status(400).send({
							message: `Username ${req.body.email} not available! Already exist!`
						})
					}
					else {
						const hashedPassword = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
						const user = new User({
							email: req.body.email,
							firstName: req.body.firstName,
							middleName: req.body.middleName,
							lastName: req.body.lastName,
							pastExperience: req.body.pastExperience,
							domain: req.body.domain,
							department: req.body.department,
							tempPassword: hashedPassword,
							role: req.body.role,
							position: req.body.position,
							project: req.body.project,
							phone: req.body.phone,
						});
						await user.save()
						return res.status(200).send({ message: Messages.USER_REGISTER_SUCCESS });
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			logger.info(err.message)
			logger.info(err)
			return res.status(400).send({ message: err.message });
		}
	}

	public async updateUserPassword(req: Request, res: Response, next: NextFunction) {
		try {
			const userValidation = await UserIdValidation.validateAsync(req.params.user_id)
			if (!userValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {
				try {
					const isUsernameValid = await User.findOne({
						_id: userValidation
					});
					if (!isUsernameValid) {
						return res.status(400).send({
							message: `Username ${req.params.user_id} not valid!`
						})
					}
					else {
						const user = await User.findOne(({ _id: isUsernameValid._id }))
						if (user) {
							const currentPassword = await bcrypt.compareSync(req.body.currentPass, user.tempPassword)
							if (!currentPassword) {
								return res.status(400).send({
									message: `Current Password not valid!`
								})
							}
							else {
								const updateUser = await User.updateOne(
									{
										_id: isUsernameValid._id
									},
									{
										$set: {
											password: bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10))
										}
									},
								);
								if (updateUser) {
									await User.updateOne(
										{
											_id: isUsernameValid._id
										},
										{
											$unset: {
												tempPassword: ''
											}
										},
									);

									return res.status(200).send({ message: Messages.USER_UPDATE_SUCCESS });
								}
								else {
									return next(
										res.status(400).json({
											message: Messages.INVALID_DETAIL
										})
									);
								}
							}
						}
						return res.status(400).send({ message: Messages.INVALID_DETAIL });
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			logger.info(err.message)
			logger.info(err)
			return res.status(400).send({ message: err.message });
		}
	}

	public async updateUser(req: Request, res: Response, next: NextFunction) {
		try {
			const userValidation = await UserUpdateValidation.validateAsync(req.body)

			if (!userValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const isUsernameValid = await User.findOne({
						email: userValidation.email
					});
					if (!isUsernameValid) {
						return res.status(400).send({
							message: `Username ${req.body.email} not valid!`
						})
					}
					else {
						const updateUser = await User.updateOne(
							{
								_id: isUsernameValid._id
							},
							{
								$set: {
									email: req.body.email,
									firstName: req.body.firstName,
									middleName: req.body.middleName,
									lastName: req.body.lastName,
									pastExperience: req.body.pastExperience,
									domain: req.body.domain,
									department: req.body.department,
									role: req.body.role,
									position: req.body.position,
									project: req.body.project,
									phone: req.body.phone,
								}
							}
						);
						if (updateUser)
							return res.status(200).send({ message: Messages.USER_UPDATE_SUCCESS });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			logger.info(err.message)
			logger.info(err)
			return res.status(400).send({ message: err.message });
		}
	}

	public async updateProfilePictureUser(req: Request, res: Response, next: NextFunction) {
		try {
			let buffer = req.file?.buffer;
			const userValidation = await UserIdValidation.validateAsync(req.params.user_id)

			if (!userValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const isUsernameValid = await User.findOne({
						_id: userValidation
					});
					if (!isUsernameValid || !buffer) {
						return res.status(400).send({
							message: `Username ${req.params.user_id} not valid!`
						})
					}
					else {
						const updateUser = await User.updateOne(
							{
								_id: isUsernameValid._id
							},
							{
								$set: {
									avatar: buffer
								}
							}
						);
						if (updateUser)
							return res.status(200).send({ message: Messages.USER_UPDATE_SUCCESS });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			logger.info(err.message)
			logger.info(err)
			return res.status(400).send({ message: err.message });
		}
	}

	public async authenticateUser(req: Request, res: Response, next: NextFunction) {
		logger.info(req.body.email)
		const foundUser = await User.findOne({ email: req.body.email });
		if (!foundUser) return res.status(400).send({ message: "This user does not exist" });
		try {
			if (foundUser.tempPassword) {
				const matchTempPass = await bcrypt.compareSync(req.body.password, foundUser.tempPassword);
				if (!matchTempPass) return res.status(400).send({ message: "Incorrect Password!!" });
				const token = await jwt.sign({ _id: foundUser._id }, config.jwt_secret);
				let user = await User.findOne({ email: req.body.email }).select(["-password", "-tempPassword"]).populate([{
					path: 'position',
					model: 'Designation'
				}, { path: 'project', model: 'Project' },
				{ path: 'department', model: 'Department' },
				{ path: 'avatar' }
				]);

				return res.status(200).header("auth-token", token).send({
					"accessToken": token,
					"refreshToken": token,
					userId: foundUser._id,
					role: foundUser.role,
					name: `${foundUser.firstName} ${foundUser.middleName} ${foundUser.lastName}`,
					email: foundUser.email,
					userObject: user,
					"jwt": token,
					"_id": foundUser._id
				});

			}
			else {
				const isMatch = await bcrypt.compareSync(req.body.password, foundUser.password);
				if (!isMatch) return res.status(400).send({ message: "Incorrect Password!!" });
				let user = await User.findOne({ email: req.body.email }).select(["-password", "-tempPassword"]).populate([{
					path: 'position',
					model: 'Designation'
				}, { path: 'project', model: 'Project' },
				{ path: 'department', model: 'Department' },
				{ path: 'avatar' }
				]);
				const token = await jwt.sign({ _id: foundUser._id }, config.jwt_secret, {
					expiresIn: config.JWT_MAX_AGE,
				});

				return res.status(200).header("auth-token", token).send({
					"accessToken": token,
					"refreshToken": token,
					userId: foundUser._id,
					role: foundUser.role,
					name: `${foundUser.firstName} ${foundUser.middleName} ${foundUser.lastName}`,
					email: foundUser.email,
					userObject: user
				});
			}
		} catch (error) {
			return res.status(400).send(error);
		}
	}
}
import { Request, Response, NextFunction } from "express";
import { Messages } from "../../utils/constant";
import { logger } from "../../utils/logger";
import Department from "../models/department.model";
import { CreateDepartmentValidation, DepartmentIdValidation, UpdateDepartmentValidation } from "../validations/department.validation";

export class DepartmentController {
	public getAllDepartments = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const allDepartments = await Department.find({})
			if (allDepartments)
				return res.status(200).send({ "allDepartment": allDepartments });
		} catch (error) {
			return res.status(400).send(error);
		}
	};

	public createDepartment = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const departmentValidation = await CreateDepartmentValidation.validateAsync(req.body)

			if (!departmentValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {
				try {
					const foundDepartment = await Department.findOne({ name: req.body.name });
					if (foundDepartment) {
						return res.status(400).send({
							message: `Department ${req.body.name} not available! Already exist!`
						})
					}
					else {
						const department = new Department({
							name: req.body.name,
							status: req.body.status
						});
						await department.save()
						return res.status(200).send({ message: Messages.DEPARTMENT_CREATE_SUCCESS });
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			logger.info(err.message)
			logger.info(err)
			return res.status(400).send({ message: err.message });
		}
	};

	public deleteDepartment = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const idValidation = await DepartmentIdValidation.validateAsync(req.body.id)

			if (!idValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const foundDepartment = await Department.findOne({ _id: req.body.id });
					if (!foundDepartment) {
						return res.status(400).send({
							message: `Department not exist!!`
						})
					}
					else {
						const deleteDepartment = await Department.deleteOne({ _id: idValidation })
						if (deleteDepartment)
							return res.status(200).send({ message: Messages.DEPARTMENT_DELETE_SUCCESS });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			console.log(err.message)
			console.log(err)
			return res.status(400).send({ message: err.message });
		}
	}


	public async updateDepartment(req: Request, res: Response, next: NextFunction) {
		try {
			logger.info(JSON.stringify(req.body))
			const departmentValidation = await UpdateDepartmentValidation.validateAsync(req.body)

			if (!departmentValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const isDepartmentValid = await Department.findOne({
						_id: departmentValidation.id
					});
					if (!isDepartmentValid) {
						return res.status(400).send({
							message: Messages.INVALID_DETAIL
						})
					}
					else {
						const updateDepartment = await Department.findOneAndUpdate(
							{
								_id: isDepartmentValid.id
							},
							{
								$set: {
									name: req.body.name,
									status: req.body.status
								}
							}
						);
						if (updateDepartment)
							return res.status(200).send({ message: Messages.DEPARTMENT_UPDATE_SUCCESS, });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			console.log(err.message)
			console.log(err)
			return res.status(400).send({ message: err.message });
		}
	}
}
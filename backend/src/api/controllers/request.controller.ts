import { Messages } from "../../utils/constant";
import { logger } from "../../utils/logger";
import Notification from "../models/notification.model";
import DeskRequest from "../models/request.model";
import OfficeSeat from "../models/seats.model";
import { CreateRequestValidation, RequestIdValidation, UpdateRequestValidation } from "../validations/request.validation";
import { Request, Response, NextFunction } from "express";


export class RequestController {
	public createRequest = async (req: Request, res: Response, next: NextFunction) => {
		var io = req.app.get('socketIO');
		try {
			const requestValidation = await CreateRequestValidation.validateAsync(req.body)

			if (!requestValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {
				try {
					const foundDeskRequest = await DeskRequest.find({ user: req.body.user, seat: req.body.seat });
					if (foundDeskRequest.length !== 0) {
						return res.status(400).send({
							message: `This request not available! Already exist!`
						})
					} else {
						const deskRequest = new DeskRequest({
							user: req.body.user,
							reportingManager: req.body.reportingManager,
							project: req.body.project,
							seat: req.body.seat,
							description: req.body.description,
							priority: req.body.priority,
							status: req.body.status
						})
						await deskRequest.save();
						const bookSeat = await OfficeSeat.findOneAndUpdate(
							{
								_id: req.body.seat
							},
							{
								$set: {
									bookedBy: req.body.user,
									isSelectable: false
								}
							}
						);

						if (bookSeat) { io.emit('update-message', { seat: req.body.seat, selectable: false }); return res.status(200).send({ message: Messages.DESK_REQUEST_CREATE_SUCCESS }); }
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}

					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			logger.info(err.message)
			logger.info(err)
			return res.status(400).send({ message: err.message });
		}
	};

	public deleteDeskRequest = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const idValidation = await RequestIdValidation.validateAsync(req.body.id)

			if (!idValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const foundWing = await DeskRequest.findOne({ _id: req.body.id });
					if (!foundWing) {
						return res.status(400).send({
							message: `Request not exist!!`
						})
					}
					else {
						const deleteRequest = await DeskRequest.deleteOne({ _id: idValidation })
						if (deleteRequest)
							return res.status(200).send({ message: Messages.DESK_REQUEST_DELETE_SUCCESS });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			console.log(err.message)
			console.log(err)
			return res.status(400).send({ message: err.message });
		}
	}

	public async updateDeskRequest(req: Request, res: Response, next: NextFunction) {
		var io = req.app.get('socketIO');
		try {
			logger.info(JSON.stringify(req.body))
			const reqValidation = await UpdateRequestValidation.validateAsync(req.body)

			if (!reqValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {
				try {
					const isRequestValid = await DeskRequest.findOne({
						_id: reqValidation._id
					});
					if (!isRequestValid) {
						return res.status(400).send({
							message: Messages.INVALID_DETAIL
						})
					}
					else {
						const updateRequest = await DeskRequest.findOneAndUpdate(
							{
								_id: isRequestValid.id
							},
							{
								$set: {
									status: req.body.status
								}
							}
						);
						if (updateRequest) {
							if (req.body.status === 'reject') {
								const update = await OfficeSeat.findOneAndUpdate(
									{
										_id: req.body.seat
									},
									{
										$unset: {
											bookedBy: "",
											isSelectable: ''
										}
									},

								);
								const findNotification = await Notification.find({ user: req.body.user, request: isRequestValid.id })
								if (findNotification.length === 0) {
									const notificationCreate = await Notification.create({
										user: req.body.user,
										request: isRequestValid.id,
										isRead: false,
									}).then(e =>
										e.populate([{
											path: 'user',
											model: 'User',
											select: '-password',
											populate: [{
												path: 'project',
												model: 'Project'
											}]
										}, {
											path: 'request',
											model: 'Request',
											populate: {
												path: 'seat',
												model: 'Seat',
												populate: [{
													path: 'location',
													model: 'Location'
												}, {
													path: 'floor',
													model: "Floor"
												}, {
													path: 'wing',
													model: "Wing"
												}]
											}
										}])
									)
									io.emit('message', notificationCreate);
									return res.status(200).send({ message: Messages.REQUEST_UPDATE_SUCCESS, })
								}

								if (update) {
									return res.status(200).send({ message: Messages.REQUEST_UPDATE_SUCCESS, })
								}
							}
							else if (req.body.status === 'approved') {
								const update = await OfficeSeat.findOneAndUpdate(
									{
										_id: req.body.seat
									},
									{
										$set: {
											bookedBy: req.body.user,
											isSelectable: false
										}
									}
								);

								const findNotification = await Notification.find({ user: req.body.user, request: isRequestValid.id })
								if (findNotification.length === 0) {
									const notificationCreate = await Notification.create({
										user: req.body.user,
										request: isRequestValid.id,
										isRead: false,
									}).then(e =>
										e.populate([{
											path: 'user',
											model: 'User',
											select: '-password',
											populate: [{
												path: 'project',
												model: 'Project'
											}]
										}, {
											path: 'request',
											model: 'Request',
											populate: {
												path: 'seat',
												model: 'Seat',
												populate: [{
													path: 'location',
													model: 'Location'
												}, {
													path: 'floor',
													model: "Floor"
												}, {
													path: 'wing',
													model: "Wing"
												}]
											}
										}])
									)
									io.emit('message', notificationCreate);
									return res.status(200).send({ message: Messages.REQUEST_UPDATE_SUCCESS, })
								}

								if (update) {
									return res.status(200).send({ message: Messages.REQUEST_UPDATE_SUCCESS, })
								}

							}
							else return res.status(200).send({ message: Messages.REQUEST_UPDATE_SUCCESS, });
						}
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			console.log(err.message)
			console.log(err)
			return res.status(400).send({ message: err.message });
		}
	}

	public getAllDeskRequests = async (req: Request, res: Response, next: NextFunction) => {

		try {
			const allRequests = await DeskRequest.find({}).populate([
				{
					path: 'user',
					model: 'User',
					select: '-password'
				}, {
					path: 'seat',
					model: 'Seat',
					populate: [{
						path: 'location',
						model: 'Location'
					}, {
						path: 'floor',
						model: "Floor"
					}, {
						path: 'wing',
						model: "Wing"
					}]
				}, {
					path: 'project',
					model: 'Project'
				}, {
					path: 'reportingManager',
					model: 'User',
					select: '-password'
				}])
			if (allRequests)
				return res.status(200).send({ "allRequest": allRequests });
		} catch (error) {
			return res.status(400).send(error);
		}
	};

	public getAllDeskRequestsForUser = async (req: Request, res: Response, next: NextFunction) => {

		try {
			const allRequests = await DeskRequest.find({ user: req.params.user }).populate([
				{
					path: 'user',
					model: 'User',
					select: '-password'
				}, {
					path: 'seat',
					model: 'Seat',
					populate: [{
						path: 'location',
						model: 'Location'
					}, {
						path: 'floor',
						model: "Floor"
					}, {
						path: 'wing',
						model: "Wing"
					}]
				}, {
					path: 'project',
					model: 'Project'
				}, {
					path: 'reportingManager',
					model: 'User',
					select: '-password'
				}])
			if (allRequests)
				return res.status(200).send({ "allRequest": allRequests });
		} catch (error) {
			return res.status(400).send(error);
		}
	};


}
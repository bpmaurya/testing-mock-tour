import { Request, Response, NextFunction } from "express";
import { Messages } from "../../utils/constant";
import { logger } from "../../utils/logger";
import Designation from "../models/designation.model";
import { CreateDesignationValidation, DesignationIdValidation, UpdateDesignationValidation } from "../validations/designation.validation";

export class DesignationController {
	public getAllDesignation = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const allDesignation = await Designation.find({})
			if (allDesignation)
				return res.status(200).send({ "allDesignation": allDesignation });
		} catch (error) {
			return res.status(400).send(error);
		}
	};

	public createDesignation = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const designationValidation = await CreateDesignationValidation.validateAsync(req.body)

			if (!designationValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {
				try {
					const foundDesignation = await Designation.findOne({ name: req.body.name });
					if (foundDesignation) {
						return res.status(400).send({
							message: `Designation ${req.body.name} not available! Already exist!`
						})
					}
					else {
						const designation = new Designation({
							name: req.body.name,
							status: req.body.status
						});
						await designation.save()
						return res.status(200).send({ message: Messages.DESIGNATION_CREATE_SUCCESS });
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			logger.info(err.message)
			logger.info(err)
			return res.status(400).send({ message: err.message });
		}
	};

	public deleteDesignation = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const idValidation = await DesignationIdValidation.validateAsync(req.body.id)

			if (!idValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const foundDesignation = await Designation.findOne({ _id: req.body.id });
					if (!foundDesignation) {
						return res.status(400).send({
							message: `Designation not exist!!`
						})
					}
					else {
						const deleteDesignation = await Designation.deleteOne({ _id: idValidation })
						if (deleteDesignation)
							return res.status(200).send({ message: Messages.DESIGNATION_DELETE_SUCCESS });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			console.log(err.message)
			console.log(err)
			return res.status(400).send({ message: err.message });
		}
	}


	public async updateDesignation(req: Request, res: Response, next: NextFunction) {
		try {
			logger.info(JSON.stringify(req.body))
			const designationValidation = await UpdateDesignationValidation.validateAsync(req.body)

			if (!designationValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const isDesignationValid = await Designation.findOne({
						_id: designationValidation.id
					});
					if (!isDesignationValid) {
						return res.status(400).send({
							message: Messages.INVALID_DETAIL
						})
					}
					else {
						const updateDesignation = await Designation.findOneAndUpdate(
							{
								_id: isDesignationValid.id
							},
							{
								$set: {
									name: req.body.name,
									status: req.body.status
								}
							}
						);
						if (updateDesignation)
							return res.status(200).send({ message: Messages.DESIGNATION_UPDATE_SUCCESS, });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			console.log(err.message)
			console.log(err)
			return res.status(400).send({ message: err.message });
		}
	}
}
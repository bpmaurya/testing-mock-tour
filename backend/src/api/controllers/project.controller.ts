import { Request, Response, NextFunction } from "express";
import Project from "../models/project.models";
import { CreateProjectValidation, ProjectIdValidation, UpdateProjectValidation } from "../validations/project.validation";
import { Messages } from "../../utils/constant";
import { logger } from "../../utils/logger";

export class ProjectController {
	public getAllProjects = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const allProjects = await Project.find({})
			if (allProjects)
				return res.status(200).send({ "allProject": allProjects });
		} catch (error) {
			return res.status(400).send(error);
		}
	};

	public createProject = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const projectValidation = await CreateProjectValidation.validateAsync(req.body)

			if (!projectValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {
				try {
					const foundProject = await Project.findOne({ name: req.body.name });
					if (foundProject) {
						return res.status(400).send({
							message: `Project ${req.body.name} not available! Already exist!`
						})
					}
					else {
						const project = new Project({
							name: req.body.name,
							status: req.body.status
						});
						await project.save()
						return res.status(200).send({ message: Messages.PROJECT_CREATE_SUCCESS });
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			logger.info(err.message)
			logger.info(err)
			return res.status(400).send({ message: err.message });
		}
	};

	public deleteProject = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const idValidation = await ProjectIdValidation.validateAsync(req.body.id)

			if (!idValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const foundProject = await Project.findOne({ _id: req.body.id });
					if (!foundProject) {
						return res.status(400).send({
							message: `Project not exist!!`
						})
					}
					else {
						const deleteProject = await Project.deleteOne({ _id: idValidation })
						if (deleteProject)
							return res.status(200).send({ message: Messages.PROJECT_DELETE_SUCCESS });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			console.log(err.message)
			console.log(err)
			return res.status(400).send({ message: err.message });
		}
	}


	public async updateProject(req: Request, res: Response, next: NextFunction) {
		try {
			logger.info(JSON.stringify(req.body))
			const projectValidation = await UpdateProjectValidation.validateAsync(req.body)

			if (!projectValidation) {
				return res.status(400).send({ message: Messages.INVALID_DETAIL });
			}
			else {

				try {
					const isProjectValid = await Project.findOne({
						_id: projectValidation.id
					});
					if (!isProjectValid) {
						return res.status(400).send({
							message: Messages.INVALID_DETAIL
						})
					}
					else {
						const updateProject = await Project.findOneAndUpdate(
							{
								_id: isProjectValid.id
							},
							{
								$set: {
									name: req.body.name,
									status: req.body.status
								}
							}
						);
						if (updateProject)
							return res.status(200).send({ message: Messages.PROJECT_UPDATE_SUCCESS, });
						else {
							return next(
								res.status(400).json({
									message: Messages.INVALID_DETAIL
								})
							);
						}
					}
				} catch (error) {
					return res.status(400).send({ message: Messages.INVALID_DETAIL });
				}
			}
		}
		catch (err: any) {
			console.log(err.message)
			console.log(err)
			return res.status(400).send({ message: err.message });
		}
	}
}
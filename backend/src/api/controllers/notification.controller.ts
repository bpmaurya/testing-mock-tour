import { NextFunction, Request, Response } from "express";
import Notification from "../models/notification.model";
import { logger } from "../../utils/logger";


export class NotificationController {
	public createNotification = async (req: Request, res: Response, next: NextFunction) => {
		try {
			const notificationCreate = await Notification.create({
				user: req.body.user,
				request: req.body.request,
				isRead: req.body.isRead,
			})
			await notificationCreate.save()
			return res.status(200).send({ message: 'Notification Created!' });
		} catch (err: any) {
			logger.info(err.message)
			logger.info(err)
			return res.status(400).send({ message: err.message });
		}
	}

	public getAllNotification = async (req: Request, res: Response, next: NextFunction) => {

		try {
			const allNotification = await Notification.find({ user: req.params.user_id }).populate([
				{
					path: 'user',
					model: 'User',
					select: '-password',
					populate: [{
						path: 'project',
						model: 'Project'
					}]
				}, {
					path: 'request',
					model: 'Request',
					populate: {
						path: 'seat',
						model: 'Seat',
						populate: [{
							path: 'location',
							model: 'Location'
						}, {
							path: 'floor',
							model: "Floor"
						}, {
							path: 'wing',
							model: "Wing"
						}]
					}
				}])
			if (allNotification)
				return res.status(200).send({ "allNotification": allNotification });
		} catch (error) {
			return res.status(400).send(error);
		}
	}
}
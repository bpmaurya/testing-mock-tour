import { Server } from "socket.io";

export const socketIoServer = {
	getIo: (server: any) => {
		const io = new Server(server, {
			cors: {
				origin: ['http://localhost:3000', "http://192.168.2.219:8100"],
				methods: ["GET", "POST"],
				credentials: false
			}
		});
		io.on('connection', (socket) => {
			console.log('User connected');

			socket.on('notification', () => {
				console.log('got notification!');
			})
			socket.on("message", (data: any) => {
				io.emit(`notification`, data);
			});

			socket.on('disconnect', () => {
				console.log('User disconnected!');
			});
		});
		return io;
	}
}

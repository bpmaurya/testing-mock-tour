import { NextFunction, Request, Response } from "express";
import jwt = require("jsonwebtoken");
import { config } from "../../config";
import { logger } from "../../utils/logger";

export const requireAuth = (req: Request, res: Response, next: NextFunction) => {
	const token =
		req.body.token || req.query.token || req.headers["x-access-token"];

	//check json web token exists & is verified
	if (token) {
		jwt.verify(token, config.jwt_secret, (err: any) => {
			if (err) {
				logger.info(err.message);
				return res.status(400).send(err);
			} else {
				next();
			}
		});
	} else {
		return res.status(400).send({ "message": "token not found!" })
	}
};
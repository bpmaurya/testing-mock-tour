import mongoose from "mongoose";
import { logger } from "./logger";

mongoose.set("strictQuery", false);
export default (database: string) => {
	const connect = () => {
		mongoose
			.connect(database)
			.then(() => logger.info(`Database connection successful.....`))
			.catch((error) => {
				logger.info("Unable to connect to the db: " + error.message);
				return process.exit(1);
			});
	};
	connect();
	mongoose.connection.on("disconnected", () => {
		logger.info(`Database disconnected`);
	});

	process.on("SIGINT", async () => {
		await mongoose.connection.close();
		process.exit(0);
	});
};

import { Document } from "mongoose";

export interface IUser extends Document {
	email: string;
	password: string;
	tempPassword: string;
	firstName: string;
	middleName: string;
	lastName: string;
	pastExperience: string;
	domain: string;
	department: string;
	role: string;
	position: string;
	project: string;
	phone: number
}
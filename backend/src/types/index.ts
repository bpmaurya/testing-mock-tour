import { Document } from "mongoose";
export interface Config {
	serviceName: string,
	port: number;
	db: DbConfig;
	jwt_secret: string;
	JWT_MAX_AGE: string;
	ORIGIN_ACCESS: string;
	loggerLevel: string,
}

export interface DbConfig {
	database: string;
}


export interface ILocation extends Document {
	name: string,
	status: boolean
}

export interface SeatType extends Document {
	name: string,
	row: string,
	location: string,
	floor: string,
	wing: string,
	bookedBy: string,
	bookedStatus: string,
	status: boolean,
	isSelectable: boolean,
	orderIndex: number
}

export interface FloorType extends Document {
	name: string,
	location: string,
	status: boolean,
}
export interface WingType extends Document {
	name: string,
	location: string,
	floor: string,
	status: boolean,
}

export interface RequestType extends Document {
	user: string,
	seat: string,
	reportingManager: string,
	project: string,
	description: string,
	priority: string,
	status: 'pending' | 'approved' | 'reject',
}

export interface IDepartment extends Document {
	name: string;
	status: boolean
}

export interface IProject extends Document {
	name: string;
	status: boolean
}

export interface IDesignation extends Document {
	name: string;
	status: boolean
}

export interface INotification extends Document {
	user: string,
	request: string,
	isRead: boolean
}


import dotenv from 'dotenv'
import { Config } from '../types';
dotenv.config();

export const config: Config = {
	serviceName: process.env.SERVICE_NAME || '',
	port: Number(process.env.PORT) || 5000,
	loggerLevel: 'debug',
	jwt_secret: process.env.JWT_SECRET || '',
	JWT_MAX_AGE: process.env.JWT_MAX_AGE || '',
	ORIGIN_ACCESS: process.env.ORIGIN_ACCESS || '*',
	db: {
		database: process.env.DATABASE || '',
	}

}

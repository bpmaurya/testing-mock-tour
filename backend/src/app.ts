import express from "express";
import { Request, Response, NextFunction } from "express";
import helmet from "helmet";
import bodyParser from "body-parser";
import morgan from "morgan";
import cors, { CorsOptions } from 'cors'
import connectorDb from './utils/db'
import { config } from "./config";
import { logger } from "./utils/logger";
import mainRoutes from './api/routes'
import http, { Server } from 'http'
import { socketIoServer } from "./api/controllers/socket.controller";
import swaggerUi from "swagger-ui-express";
import swaggerOutput from "./swagger_output.json";

export class Application {
	public app: express.Application;
	public httpServer: Server<any>;
	public corsOptions: CorsOptions = {
		origin: ['http://localhost:3000', "http://192.168.2.219:8100"],
		optionsSuccessStatus: 200,
	}

	constructor() {
		this.app = express();
		this.httpServer = http.createServer(this.app)
		this.middlewares();
		this.routes();
		this.dbConnect();
	}

	private dbConnect(): void {
		const dbConnectionString: string = config.db.database;
		connectorDb(dbConnectionString);
	}

	private middlewares(): void {
		this.app.use(express.urlencoded({ extended: false }));
		this.app.use(express.json());
		this.app.use(helmet({
			crossOriginResourcePolicy: { policy: "cross-origin" },
			contentSecurityPolicy: false,
		}));
		this.app.use(bodyParser.json());
		this.app.use(bodyParser.urlencoded({ extended: true }));
		this.app.use(morgan<Request, Response>("dev"));
		this.app.use(cors(this.corsOptions))
		let io = socketIoServer.getIo(this.httpServer)
		this.app.set('socketIO', io);
	}

	private routes(): void {
		this.app.use("/api", mainRoutes);
		this.app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerOutput));
		const router: express.Router = express.Router();
		this.app.use(router);
		this.app.use((error: any, res: Response, next: NextFunction) => {
			try {
				const status = error.status || 500;
				const message =
					error.message ||
					"There was an error while processing your request, please try again";
				return res.status(status).send({
					status,
					message,
				});
			} catch (error) {
				next(error);
			}
		});
	}

	public start(): void {
		const port = config.port;
		this.httpServer.listen(port, async () => {
			logger.info(
				`🤖 Server -Seat-Sync- is running on port ${port}, path http://localhost:${port}`
			);
		});
	}
}

async function main() {
	const app = new Application();

	app.start();
}

main();
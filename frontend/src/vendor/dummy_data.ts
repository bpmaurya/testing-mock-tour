import { Department, Designation, Desk, DeskLocation, DeskRequest, Floor, IUser, Project, Wing } from "MyModels";
import moment from "moment";

export namespace Dummy {
	export const DATE = moment('20/11/1990', 'DD-MM-YYYY').toDate();

	export const AllLocation: DeskLocation[] = [
		{
			_id: 'dummy_id_1',
			name: 'safal-profitaire',
			status: true,
			createdAt: DATE,
			updatedAt: DATE
		}
	]

	export const AllFloor: Floor.Type[] = [
		{
			_id: 'dummy_id_1',
			name: 'floor-1',
			location: AllLocation[0],
			status: true,
			createdAt: DATE,
			updatedAt: DATE
		},
		{
			_id: 'dummy_id_2',
			name: 'floor-2',
			location: AllLocation[0],
			status: true,
			createdAt: DATE,
			updatedAt: DATE
		}
	]

	export const AllWings: Wing.Type[] = [
		{
			_id: 'dummy_id_1',
			name: "wing-1",
			location: AllLocation[0],
			floor: AllFloor[0],
			status: true,
			createdAt: DATE,
			updatedAt: DATE
		}
	]

	export const AllProject: Project.Type[] = [
		{
			_id: 'dummy_id_11',
			name: "project_1",
			status: true,
			createdAt: DATE,
			updatedAt: DATE
		}
	]

	export const AllDesignation: Designation.Type[] = [
		{
			_id: 'dummy_id_1',
			name: 'SEE',
			status: true,
			createdAt: DATE,
			updatedAt: DATE
		}
	]

	export const AllDepartment: Department.Type[] = [
		{
			_id: 'dummy_id_1',
			name: 'cloud',
			status: true,
			createdAt: DATE,
			updatedAt: DATE
		}
	]

	export const AllUsers: IUser.Type[] = [
		{
			_id: 'dummy_id_1',
			email: 'test_user@email.com',
			firstName: 'firstN',
			middleName: 'midN',
			lastName: 'lastN',
			domain: 'cloud',
			pastExperience: '1 years',
			role: "user",
			phone: 9898987878,
			project: AllProject[0],
			department: AllDepartment[0],
			position: AllDesignation[0],
			createdAt: DATE,
			updatedAt: DATE
		}
	]

	export const AllDesk: Desk.Type[] = [
		{
			_id: 'dummy_id_1',
			name: 'A-1',
			row: 'A',
			location: AllLocation[0],
			floor: AllFloor[0],
			wing: AllWings[0],
			orderIndex: 0,
			bookedBy: AllUsers[0],
			bookedStatus: 'approved',
			isSelectable: false,
			status: true,
			createdAt: DATE,
			updatedAt: DATE
		},
		{
			_id: 'dummy_id_2',
			name: 'A-2',
			row: 'A',
			location: AllLocation[0],
			floor: AllFloor[0],
			wing: AllWings[0],
			orderIndex: 0,
			isSelectable: true,
			status: true,
			createdAt: DATE,
			updatedAt: DATE
		}
	]

	export const AllRequest: DeskRequest.Type[] = [
		{
			_id: "dummy_id_1",
			user: AllUsers[0],
			seat: AllDesk[0],
			reportingManager: AllUsers[0],
			project: AllProject[0],
			priority: "high",
			description: "this is dummy descriptions!",
			status: 'approved',
			createdAt: DATE,
			updatedAt: DATE
		}
	]

}
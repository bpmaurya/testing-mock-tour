export const EmailRegex: RegExp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
export const PhoneRegex: RegExp = /^[6-9]{1}[0-9]{9}$/
export const PasswordRegex: RegExp = /(?=[A-Za-z0-9@#$%^&+!=]+$)^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%^&+!=])(?=.{8,}).*$/

export const UserRoleType: { name: string, value: string }[] = [{ name: 'User', value: 'user' }, { name: 'Staff', value: 'staff' }, { name: 'Admin', value: "admin" }]

export const Messages = {
	ERROR_MESSAGE: 'Something went wrong!',
	USER_UPDATE_SUCCESS_MESSAGE: 'User updated!',
	USER_DELETE_SUCCESS_MESSAGE: 'User deleted!',
	USER_REGISTER_SUCCESS_MESSAGE: 'User Successfully Registered!',
}
export function delayedPromise(milli: number): Promise<void> {
	return new Promise((res) => {
		setTimeout(res, milli);
	});
}
import { Page } from 'puppeteer-core';
const ScreenshotTester = require('puppeteer-screenshot-tester');

const ImagePath = `../../goldens/`;

export async function screenshotTester(page: Page, name: string) {
	const TesterConfig = await ScreenshotTester(
		0.4,
		false,
		false,
		{
			transparency: 0.5,
		},
		{ compressionLevel: 8 }
	);
	return await TesterConfig(page, `${ImagePath + name}`, { fullPage: true, saveNewImageOnError: true });
}

export function visualTesting(testName: string, test: () => Promise<Page>, waitForNetworkIdle = true) {
	it(testName, async () => {
		const page = await test();
		if (waitForNetworkIdle === true) await page.waitForNetworkIdle();
		const result = await screenshotTester(page, testName);
		expect(result).toBe(true);
	});
}

export async function pageClick(page: Page, pageQuery: string, index: number = 0) {
	const firstSelector = await page.$x(pageQuery);
	await page.evaluate((selector: any) => selector.click(), firstSelector[index]);
}
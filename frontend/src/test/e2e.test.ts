import puppeteer, { Browser, Page, executablePath } from 'puppeteer-core';
import { visualTesting, pageClick } from './helper';

jest.setTimeout(60 * 1000);
describe('Testing landing page', () => {
	let browser: Browser;
	const BaseUrl = 'http://localhost:3000';
	let page: Page;

	beforeAll(async () => {
		browser = await puppeteer.launch({ args: ['--window-size=1920,1080'], defaultViewport: null, headless: true, executablePath: executablePath('chrome'), });
		page = await browser.newPage();
	});

	visualTesting('landing-page', async () => {
		await page.goto(BaseUrl, { waitUntil: ['networkidle0', 'load'] });
		return page;
	});

	visualTesting('home-page', async () => {
		await page.waitForSelector('input[type="email"]');
		await page.type('input[name="email"]', 'admin@admin.com');

		await page.waitForSelector('input[type="password"]');
		await page.type('input[type="password"]', 'Aarush2805@');

		await pageClick(page, "//button[contains(text(), 'Login')]", 0);
		await page.waitForNetworkIdle();
		return page;
	});

	visualTesting('profile-page-user', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//span[contains(text(), 'Home')]", 0);
		return page;
	});

	visualTesting('setting-page-admin', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//span[contains(text(), 'Setting')]", 0);
		return page;
	});

	visualTesting('request-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//span[contains(text(), 'Request')]", 0);
		return page;
	});

	visualTesting('users-management-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//span[contains(text(), 'Users')]", 0);
		return page;
	});

	visualTesting('add-users-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//button[contains(text(), 'Add User')]", 0);
		return page;
	});

	visualTesting('my-request-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//span[contains(text(), 'My Request')]", 0);
		return page;
	});

	visualTesting('desks-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//span[contains(text(), 'Desks')]", 0);
		return page;
	});


	afterAll(() => browser.close());
});

describe('Testing additional page', () => {
	let browser: Browser;
	const BaseUrl = 'http://localhost:3000';
	let page: Page;

	beforeAll(async () => {
		browser = await puppeteer.launch({ args: ['--window-size=1920,1080'], defaultViewport: null, headless: true, executablePath: executablePath('chrome'), });
		page = await browser.newPage();

		await page.goto(BaseUrl, { waitUntil: ['networkidle0', 'load'] });
		await page.waitForSelector('input[type="email"]');
		await page.type('input[name="email"]', 'admin@admin.com');

		await page.waitForSelector('input[type="password"]');
		await page.type('input[type="password"]', 'Aarush2805@');

		await pageClick(page, "//button[contains(text(), 'Login')]", 0);
		await page.waitForNetworkIdle();
	});

	visualTesting('Add-Location-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//span[contains(text(), 'Setting')]", 0);
		await pageClick(page, "//button[contains(text(), 'Add Location')]", 0);
		return page;
	});

	visualTesting('Floors-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//span[contains(text(), 'Floors')]", 0);
		return page;
	});

	visualTesting('Add-Floor-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//button[contains(text(), 'Add Floor')]", 0);
		return page;
	});

	visualTesting('Wings-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//span[contains(text(), 'Wings')]", 0);
		return page;
	});

	visualTesting('Add-Wings-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//button[contains(text(), 'Add Wing')]", 0);
		return page;
	});

	visualTesting('Projects-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//span[contains(text(), 'Projects')]", 0);
		return page;
	});

	visualTesting('Add-projects-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//button[contains(text(), 'Add Project')]", 0);
		return page;
	});

	visualTesting('Designations-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//span[contains(text(), 'Designations')]", 0);
		return page;
	});

	visualTesting('Add Designation-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//button[contains(text(), 'Add Designation')]", 0);
		return page;
	});

	visualTesting('Department-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//span[contains(text(), 'Department')]", 0);
		return page;
	});

	visualTesting('Add-Department-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//button[contains(text(), 'Add Department')]", 0);
		return page;
	});

	afterAll(() => browser.close());
});


describe('Testing new pages', () => {
	let browser: Browser;
	const BaseUrl = 'http://localhost:3000';
	let page: Page;

	beforeAll(async () => {
		browser = await puppeteer.launch({ args: ['--window-size=1920,1080'], defaultViewport: null, headless: true, executablePath: executablePath('chrome'), });
		page = await browser.newPage();

		await page.goto(BaseUrl, { waitUntil: ['networkidle0', 'load'] });
		await page.waitForSelector('input[type="email"]');
		await page.type('input[name="email"]', 'admin@admin.com');

		await page.waitForSelector('input[type="password"]');
		await page.type('input[type="password"]', 'Aarush2805@');

		await pageClick(page, "//button[contains(text(), 'Login')]", 0);
		await page.waitForNetworkIdle();
	});

	visualTesting('select-location-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//span[contains(text(), 'safal-profitaire')]", 0);
		return page;
	});

	visualTesting('select-floor-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//span[contains(text(), 'floor-1')]", 0);
		return page;
	});

	visualTesting('select-wing-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//span[contains(text(), 'wing-1')]", 0);
		return page;
	});

	visualTesting('add-row-for-desk-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//button[contains(text(), 'Add desk')]", 0);
		return page;
	});


	afterAll(() => browser.close());
});

describe('Testing action pages user', () => {
	let browser: Browser;
	const BaseUrl = 'http://localhost:3000';
	let page: Page;

	beforeAll(async () => {
		browser = await puppeteer.launch({ args: ['--window-size=1920,1080'], defaultViewport: null, headless: true, executablePath: executablePath('chrome'), });
		page = await browser.newPage();

		await page.goto(BaseUrl, { waitUntil: ['networkidle0', 'load'] });
		await page.waitForSelector('input[type="email"]');
		await page.type('input[name="email"]', 'admin@admin.com');

		await page.waitForSelector('input[type="password"]');
		await page.type('input[type="password"]', 'Aarush2805@');

		await pageClick(page, "//button[contains(text(), 'Login')]", 0);
		await page.waitForNetworkIdle();
	});

	visualTesting('action-pop-menu', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//span[contains(text(), 'Users')]", 0);
		await page.waitForNetworkIdle();
		await pageClick(page, "//button[contains(@class, 'action-pop-up')]", 0);
		return page;
	});

	visualTesting('edit-user-page', async () => {
		await page.waitForNetworkIdle();
		await pageClick(page, "//div[contains(@class, 'edit-action-pop')]", 0);
		await page.waitForNetworkIdle();
		return page;
	});

	afterAll(() => browser.close());
});

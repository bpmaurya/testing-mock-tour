import './App.css';
import './index.css';
import { HomePage } from './app/component/HomePage';
import { FlexDiv } from './app/constant';
import { Route, Routes } from 'react-router-dom';
import { LoginPage } from './app/component/common/LoginPage';
import React, { useEffect, useState } from 'react';
import { AppDep } from './app/services';
import { Navigation } from './app/services/navigate';
import { createBrowserHistory, History } from 'history';
import ProtectedRoute from './app/component/common/PrivateRoute';
import { useAppSelector } from './app/states/store/hooks';
import { Navigate } from 'react-router-dom';
import { selectAuth } from './app/states/store/feature/auth';
import { UserComponent } from './app/component/users/UserComponent';
import { DesksComponent } from './app/component/desk/DesksComponent';
import { NoPageFound } from './app/pages/NoPageFound';
import tokenService from './app/services/token.service';
import { SettingComponent } from './app/component/settings/SettingComponent';
import { RequestComponent } from './app/component/desk/RequestComponent';
import { UserRequestComp } from './app/component/desk/UserRequestComp';
import { io, Socket } from 'socket.io-client';
import { LocationComponent } from './app/component/desk/LocationComponent';
import { FloorComponent } from './app/component/desk/FloorComponent';
import { WingComponent } from './app/component/desk/WingComponent';
import { SeatArrangement } from './app/component/desk/SeatArrangement';
import { UserSettingComponent } from './app/component/users/UserSettingComponent';
let socket: Socket = io('http://localhost:5000');

export const AppContext = React.createContext({} as AppDep);

export function App() {
	let browserHistory: History = createBrowserHistory({});
	let navigationBloc: Navigation.Bloc = Navigation.Bloc.create(browserHistory);
	let appDep: AppDep;
	appDep = {
		navigation: navigationBloc,
		socketContext: socket,
	};
	const { isLoggedIn } = useAppSelector(selectAuth);
	const [admin, setIsAdmin] = useState<any>();

	useEffect(() => {
		window.addEventListener('storage', () => {
			window.location.reload();
		});
		setIsAdmin(tokenService.isAdmin());
	}, [isLoggedIn]);

	return (
		<AppContext.Provider value={appDep}>
			<FlexDiv style={{ height: '100vh', width: '100vw' }}>
				<Routes>
					<Route
						path={`${Navigation.Path.Root}`}
						element={!!isLoggedIn ? <Navigate to={Navigation.Path.User} /> : <LoginPage />}
					/>
					{/* <Route path={`${Navigation.Path.User}`} element={<ProtectedRoute comp={<Dashboard />} />} /> */}
					<Route path={`${Navigation.Path.User}/home`} element={<ProtectedRoute comp={<HomePage />} />} />
					<Route
						path={`${Navigation.Path.User}/desk`}
						element={<ProtectedRoute comp={<DesksComponent />} />}
					/>
					<Route
						path={`${Navigation.Path.User}/my_setting`}
						element={<ProtectedRoute comp={<UserSettingComponent />} />}
					/>
					<Route
						path={`${Navigation.Path.User}/my_request`}
						element={<ProtectedRoute comp={<UserRequestComp />} />}
					/>
					<Route path={`${Navigation.Path.User}`} element={<ProtectedRoute comp={<LocationComponent />} />} />
					<Route
						path={`${Navigation.Path.User}/:id/floor?`}
						element={<ProtectedRoute comp={<FloorComponent />} />}
					/>
					<Route
						path={`${Navigation.Path.User}/:id/wing?`}
						element={<ProtectedRoute comp={<WingComponent />} />}
					/>
					<Route
						path={`${Navigation.Path.User}/:id/seats?`}
						element={<ProtectedRoute comp={<SeatArrangement />} />}
					/>
					{!!admin && (
						<>
							<Route
								path={`${Navigation.Path.User}/users`}
								element={<ProtectedRoute comp={<UserComponent />} />}
							/>
							<Route
								path={`${Navigation.Path.User}/request`}
								element={<ProtectedRoute comp={<RequestComponent />} />}
							/>
							<Route
								path={`${Navigation.Path.User}/setting`}
								element={<ProtectedRoute comp={<SettingComponent />} />}
							/>
						</>
					)}

					<Route path={`*`} element={<NoPageFound />} />
				</Routes>
			</FlexDiv>
		</AppContext.Provider>
	);
}

import styled from '@emotion/styled'

type FlexDivProps = {
	gap?: string;
	justify?: 'end' | 'start' | 'center' | 'space-evenly' | 'space-between';
	align?: 'start' | 'center';
	direction?: 'row' | 'column';
	alignItems?: 'center' | 'start' | 'end' | 'last baseline';
	padding?: string;
	text?: 'start' | 'center';
	width?: 'full'
};

export const FlexDiv = styled.div`
	gap: ${(props: FlexDivProps) => props.gap ?? '0px'};
	align-items: ${(props: FlexDivProps) => props.alignItems ?? 'start'};
	justify-content: ${(props: FlexDivProps) => props.justify ?? 'start'};
	align-content: ${(props: FlexDivProps) => props.align ?? 'center'};
	text-align: ${(props: FlexDivProps) => props.text ?? 'start'};
	display: flex;
	flex-direction: ${(props: FlexDivProps) => props.direction ?? 'row'};
	padding: ${(props: FlexDivProps) => props.padding ?? '0px'};
	width: ${(props: FlexDivProps) => props.width ? '100%' : ''};
	box-sizing: border-box;
`;
import DashboardIcon from '@mui/icons-material/Dashboard';
import HomeIcon from '@mui/icons-material/Home';
import DesktopMacIcon from '@mui/icons-material/DesktopMac';
import RequestPageIcon from '@mui/icons-material/RequestPage';
import GroupIcon from '@mui/icons-material/Group';
import SettingsIcon from '@mui/icons-material/Settings';

interface MenuType {
	name: string;
	url: string;
	icon?: JSX.Element;
	adminOnly: boolean;
}

export const Menu: MenuType[] = [
	{
		name: 'Dashboard',
		url: '/',
		icon: <DashboardIcon />,
		adminOnly: false,
	},
	{
		name: 'Home',
		url: '/home',
		icon: <HomeIcon />,
		adminOnly: false,
	},
	{
		name: 'Desks',
		url: '/desk',
		icon: <DesktopMacIcon />,
		adminOnly: false,
	},
	{
		name: 'Request',
		url: '/request',
		icon: <RequestPageIcon />,
		adminOnly: true,
	},
	{
		name: 'My Request',
		url: '/my_request',
		icon: <RequestPageIcon />,
		adminOnly: false,
	},
	{
		name: 'Settings',
		url: '/setting',
		icon: <SettingsIcon />,
		adminOnly: true,
	},
	{
		name: 'Users',
		url: '/users',
		icon: <GroupIcon />,
		adminOnly: true,
	},
	{
		name: 'Setting',
		url: '/my_setting',
		icon: <GroupIcon />,
		adminOnly: false,
	},
];

export const Logo = () => <img height="55px" src="/Volansys_ACL_LOGO_White.png" alt="logo" />;

export const adminRoute: MenuType[] = Menu;
export const userRoute: MenuType[] = Menu.filter(e => !e.adminOnly);

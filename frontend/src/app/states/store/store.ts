import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import authReducer from './feature/auth'
import userReducer from './feature/user.slice'
import locationReducer from './feature/location.slice'
import floorReducer from './feature/floor.slice'
import wingReducer from './feature/wing.slice'
import deskReducer from './feature/desk.slice'
import requestReducer from './feature/request.slice'
import projectReducer from './feature/project.slice'
import designationReducer from './feature/designation.slice'
import departmentReducer from './feature/department.slice'
import notificationReducer from './feature/notification.slice'

export const store = configureStore({
	reducer: {
		auth: authReducer,
		user: userReducer,
		locations: locationReducer,
		floor: floorReducer,
		wing: wingReducer,
		desk: deskReducer,
		request: requestReducer,
		project: projectReducer,
		designation: designationReducer,
		department: departmentReducer,
		notification: notificationReducer
	},
	middleware: (getDefaultMiddleware) => getDefaultMiddleware({ serializableCheck: false }),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
	ReturnType,
	RootState,
	unknown,
	Action<string>
>;
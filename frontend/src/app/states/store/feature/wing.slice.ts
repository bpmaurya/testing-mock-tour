import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { Wing } from "MyModels"
import axios, { AxiosError } from "axios";
import Log from "../../../../vendor/utils";
import { Messages } from "../../../../vendor/constant";
import { RootState } from "../store";
import { appService } from "../../../services";

const TAG = 'inside-wing-slice'

type InitialState = {
	wingList: Wing.Type[],
	error: string,
	success: string,
	isWingAdded: boolean,
	isWingDeleted: boolean,
	isWingUpdated: boolean
}
const initialState: InitialState = { wingList: [] as Wing.Type[], error: '', success: '', isWingAdded: false, isWingDeleted: false, isWingUpdated: false }

export const getAllWingForLocationAndFloor = createAsyncThunk<InitialState, { location: string, floor: string }>(
	'wing/all-wings-for-location-and-floor',
	async (data: { location: string, floor: string }, thunkApi) => {
		try {
			const response = await appService().wingService.getWingsForLocationAndFloor(data.location, data.floor);
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			return error.message
		}
	}
);

export const getAllWingAsync = createAsyncThunk<InitialState>(
	'wing/all-wings',
	async (thunkApi) => {
		try {
			const response = await appService().wingService.getWings();
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			return error.message
		}
	}
);

export const createWingAsync = createAsyncThunk<InitialState, Wing.Type>(
	'wing/create-wing',
	async (wingData: Wing.Type, thunkApi) => {
		try {
			const response = await appService().wingService.createWing(
				wingData.name,
				wingData.location._id,
				wingData.floor._id,
				wingData.status
			);
			Log.d(TAG, response);
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const deleteWingAsync = createAsyncThunk<InitialState, Wing.Type>(
	'wing/delete-wing',
	async (wingData: Wing.Type, thunkApi) => {
		try {
			const response = await appService().wingService.deleteWing(
				wingData._id!
			);
			Log.d(TAG, response);
			return response;

		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const updateWingAsync = createAsyncThunk<InitialState, Wing.Type>(
	'wing/update-wing',
	async (wingData: Wing.Type, thunkApi) => {

		try {
			const response = await appService().wingService.updateWing(
				wingData
			);
			Log.d(TAG, response);
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const wingSlice = createSlice({
	name: 'wing',
	initialState,
	reducers: {
		setError: (state, action: PayloadAction<string>) => {
			state.error = action.payload;
		},
		setSuccess: (state, action: PayloadAction<string>) => {
			state.success = action.payload;
		},
		resetWingState: () => initialState
	},
	extraReducers: (builder) => {
		builder
			.addCase(getAllWingAsync.fulfilled, (state, { payload }) => {
				state.wingList = payload.wingList
			})
			.addCase(getAllWingAsync.rejected, (state) => {
				state.error = Messages.ERROR_MESSAGE;
			})
			.addCase(createWingAsync.fulfilled, (state, { payload }) => {
				state.wingList = payload.wingList
				state.error = ''
				state.isWingAdded = true
			})
			.addCase(createWingAsync.rejected, (state) => {
				state.isWingAdded = false
			})
			.addCase(deleteWingAsync.fulfilled, (state, { payload }) => {
				state.wingList = payload.wingList
				state.error = ''
				state.isWingDeleted = true
			})
			.addCase(deleteWingAsync.pending, (state, { payload }) => {
				state.error = ''
				state.isWingDeleted = true
			})
			.addCase(deleteWingAsync.rejected, (state) => {
				state.isWingDeleted = false
			})
			.addCase(updateWingAsync.fulfilled, (state, { payload }) => {
				state.wingList = payload.wingList
				state.error = ''
				state.isWingUpdated = true
			})
			.addCase(updateWingAsync.pending, (state, { payload }) => {
				state.error = ''
				state.isWingUpdated = true
			})
			.addCase(updateWingAsync.rejected, (state) => {
				state.isWingUpdated = false
			})
	}
});

export const { setError, setSuccess, resetWingState } = wingSlice.actions;
export const getWings = (state: RootState) => state.wing;

export default wingSlice.reducer;
import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios, { AxiosError } from "axios";
import Log from "../../../../vendor/utils";
import { Messages } from "../../../../vendor/constant";
import { RootState } from "../store";
import { DeskRowComponent } from "../../../component/desk/DesksComponent";
import { Desk } from "MyModels";
import { DeskArrangement } from "../../../component/forms/AddDeskForm";
import { DeleteDeskType } from "../../../component/desk/SeatArrangement";
import { appService } from "../../../services";

const TAG = 'inside-desk-slice'

type InitialState = {
	deskList: DeskArrangement[],
	error: string,
	success: string,
	isDeskAdded: boolean,
	isDeskDeleted: boolean,
	isDeskUpdated: boolean,
}
const initialState: InitialState = { deskList: [], error: '', success: '', isDeskAdded: false, isDeskDeleted: false, isDeskUpdated: false }


export const getAllSeatsAsync = createAsyncThunk<InitialState, { location: string, floor: string, wing: string, }>(
	'desk/all-desks',
	async ({ location, floor, wing }, thunkApi) => {
		try {
			const response = await appService().deskService.getDesks(location, floor, wing);
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			return error.message
		}
	}
);

export const createDeskAsync = createAsyncThunk<InitialState, DeskArrangement>(
	'desk/create-desk',
	async (deskData: DeskArrangement, thunkApi) => {
		try {
			const response = await appService().deskService.createDesk(
				deskData.row.toUpperCase(),
				deskData.start,
				deskData.end,
				deskData.location,
				deskData.floor!,
				deskData.wing!,
			);
			Log.d(TAG, response);
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const deleteDeskAsync = createAsyncThunk<InitialState, DeleteDeskType>(
	'desk/delete-desk',
	async (deskData: DeleteDeskType, thunkApi) => {
		try {
			const response = await appService().deskService.deleteDesk(
				deskData.location,
				deskData.wing!,
				deskData.floor!,
				deskData.row
			);
			Log.d(TAG, response);
			return response;

		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const updateDeskAsync = createAsyncThunk<InitialState, Desk.Type>(
	'desk/update-desk',
	async (deskData: Desk.Type, thunkApi) => {
		try {
			const response = await appService().deskService.updateDesk(
				deskData
			);
			Log.d(TAG, response);
			return response;

		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const updateDeskOrderAsync = createAsyncThunk<InitialState, DeskRowComponent[]>(
	'desk/update-desk-order',
	async (deskData: DeskRowComponent[], thunkApi) => {
		try {
			const response = await appService().deskService.updateDeskOrder(
				deskData
			);
			Log.d(TAG, response);
			return response;

		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);



export const deskSlice = createSlice({
	name: 'desk',
	initialState,
	reducers: {
		setError: (state, action: PayloadAction<string>) => {
			state.error = action.payload;
		},
		setSuccess: (state, action: PayloadAction<string>) => {
			state.success = action.payload;
		},
		resetDeskState: () => initialState
	},
	extraReducers: (builder) => {
		builder
			.addCase(getAllSeatsAsync.fulfilled, (state, { payload }) => {
				state.deskList = payload.deskList
			})
			.addCase(getAllSeatsAsync.rejected, (state) => {
				state.error = Messages.ERROR_MESSAGE;
			})
			.addCase(createDeskAsync.fulfilled, (state, { payload }) => {
				state.deskList = payload.deskList
				state.error = ''
				state.isDeskAdded = true
			})
			.addCase(createDeskAsync.rejected, (state) => {
				state.isDeskAdded = false
			})
			.addCase(deleteDeskAsync.fulfilled, (state, { payload }) => {
				state.isDeskDeleted = true
			})
			.addCase(deleteDeskAsync.rejected, (state) => {
				state.isDeskDeleted = false
			})
			.addCase(updateDeskAsync.fulfilled, (state, { payload }) => {
				state.isDeskUpdated = true
			})
			.addCase(updateDeskAsync.rejected, (state) => {
				state.isDeskUpdated = false
			})
	}
});

export const { setError, setSuccess, resetDeskState } = deskSlice.actions;
export const getDesks = (state: RootState) => state.desk;

export default deskSlice.reducer;
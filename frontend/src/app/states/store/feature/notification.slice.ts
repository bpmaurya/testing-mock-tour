import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { Notification } from "MyModels"
import { AxiosError } from "axios";
import { Messages } from "../../../../vendor/constant";
import { RootState } from "../store";
import Log from "../../../../vendor/utils";
import { appService } from "../../../services";

const TAG = 'inside-notification-slice'

type InitialState = {
	notification: Notification.Type[],
	error: string,
	success: string,
}

const initialState: InitialState = { notification: [] as Notification.Type[], error: '', success: '' }

export const getAllNotificationAsync = createAsyncThunk<InitialState, { id: string }>(
	'notification/all-notification',
	async (data: { id: string }, thunkApi) => {
		try {
			const response = await appService().notificationService.getAllNotification(data.id);
			Log.d(TAG, response);
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			return error.message
		}
	}
);

export const notificationSlice = createSlice({
	name: 'notification',
	initialState,
	reducers: {
		setError: (state, action: PayloadAction<string>) => {
			state.error = action.payload;
		},
		setSuccess: (state, action: PayloadAction<string>) => {
			state.success = action.payload;
		},
		resetNotificationState: () => initialState
	},
	extraReducers: (builder) => {
		builder
			.addCase(getAllNotificationAsync.fulfilled, (state, { payload }) => {
				// state.notification = payload.notification
			})
			.addCase(getAllNotificationAsync.rejected, (state) => {
				state.error = Messages.ERROR_MESSAGE;
			})
	}
});

export const { setError, setSuccess, resetNotificationState } = notificationSlice.actions;
export const getNotifications = (state: RootState) => state.notification;

export default notificationSlice.reducer;
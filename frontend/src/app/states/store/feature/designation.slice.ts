import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios, { AxiosError } from "axios";
import Log from "../../../../vendor/utils";
import { Messages } from "../../../../vendor/constant";
import { RootState } from "../store";
import { Designation } from "MyModels";
import { appService } from "../../../services";

const TAG = 'inside-designation-slice'

type InitialState = {
	designationList: Designation.Type[],
	error: string,
	success: string,
	isDesignationAdded: boolean,
	isDesignationDeleted: boolean,
	isDesignationUpdated: boolean,
	isDesignationCreated: boolean
}
const initialState: InitialState = { designationList: [], error: '', success: '', isDesignationAdded: false, isDesignationDeleted: false, isDesignationUpdated: false, isDesignationCreated: false }


export const getAllDesignationAsync = createAsyncThunk<InitialState>(
	'designation/all-designation',
	async (thunkApi) => {
		try {
			const response = await appService().designationService.getDesignation()
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			return error.message
		}
	}
);

export const createDesignationAsync = createAsyncThunk<InitialState, Designation.Type>(
	'designation/create-designation',
	async (data: Designation.Type, thunkApi) => {
		try {
			const response = await appService().designationService.createDesignation(
				data
			);
			Log.d(TAG, response);
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const deleteDesignationAsync = createAsyncThunk<InitialState, Designation.Type>(
	'designation/delete-designation',
	async (data: Designation.Type, thunkApi) => {
		try {
			const response = await appService().designationService.deleteDesignation(
				data._id!
			);
			Log.d(TAG, response);
			return response;

		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const updateDesignationAsync = createAsyncThunk<InitialState, Designation.Type>(
	'designation/update-designation',
	async (data: Designation.Type, thunkApi) => {
		try {
			const response = await appService().designationService.updateDesignation(
				data
			);
			Log.d(TAG, response);
			return response;

		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);



export const designationSlice = createSlice({
	name: 'designation',
	initialState,
	reducers: {
		setError: (state, action: PayloadAction<string>) => {
			state.error = action.payload;
		},
		setSuccess: (state, action: PayloadAction<string>) => {
			state.success = action.payload;
		},
		resetDesignationState: () => initialState
	},
	extraReducers: (builder) => {
		builder
			.addCase(getAllDesignationAsync.fulfilled, (state, { payload }) => {
				state.designationList = payload.designationList
			})
			.addCase(getAllDesignationAsync.rejected, (state) => {
				state.error = Messages.ERROR_MESSAGE;
			})
			.addCase(createDesignationAsync.fulfilled, (state, { payload }) => {
				state.designationList = payload.designationList
				state.error = ''
				state.isDesignationAdded = true
			})
			.addCase(createDesignationAsync.rejected, (state) => {
				state.isDesignationAdded = false
			})
			.addCase(deleteDesignationAsync.fulfilled, (state, { payload }) => {
				state.isDesignationDeleted = true
			})
			.addCase(deleteDesignationAsync.rejected, (state) => {
				state.isDesignationDeleted = false
			})
			.addCase(updateDesignationAsync.fulfilled, (state, { payload }) => {
				state.isDesignationUpdated = true
			})
			.addCase(updateDesignationAsync.rejected, (state) => {
				state.isDesignationUpdated = false
			})
	}
});

export const { setError, setSuccess, resetDesignationState } = designationSlice.actions;
export const getDesignation = (state: RootState) => state.designation;

export default designationSlice.reducer;
import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { Desk, DeskRequest } from "MyModels"
import axios, { AxiosError } from "axios";
import { Messages } from "../../../../vendor/constant";
import { RootState } from "../store";
import Log from "../../../../vendor/utils";
import { appService } from "../../../services";

const TAG = 'inside-request-slice'

type InitialState = {
	requests: Desk.Type[],
	error: string,
	success: string,
	isRequestCreated: boolean,
	isRequestUpdated: boolean,
}

const initialState: InitialState = { requests: [] as Desk.Type[], error: '', success: '', isRequestCreated: false, isRequestUpdated: false }

export const getAllRequestAsync = createAsyncThunk<InitialState>(
	'request/all-request',
	async (thunkApi) => {
		try {
			const response = await appService().requestService.getBookedRequests();
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			return error.message
		}
	}
);

export const getAllRequestForUserAsync = createAsyncThunk<InitialState, { id: string }>(
	'request/all-request-for-user',
	async (data: { id: string }, thunkApi) => {
		try {
			const response = await appService().requestService.getBookedRequestsForUser(data.id);
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			return error.message
		}
	}
);

export const createRequestAsync = createAsyncThunk<InitialState, DeskRequest.Type>(
	'request/create-request',
	async (data: DeskRequest.Type, thunkApi) => {
		try {
			const response = await appService().requestService.createDeskRequest(
				data
			);
			Log.d(TAG, response);
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const updateRequestAsync = createAsyncThunk<InitialState, DeskRequest.Type>(
	'request/update-request',
	async (data: DeskRequest.Type, thunkApi) => {
		try {
			const response = await appService().requestService.updateDeskRequest(
				data
			);
			Log.d(TAG, response);
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);



export const requestSlice = createSlice({
	name: 'request',
	initialState,
	reducers: {
		setError: (state, action: PayloadAction<string>) => {
			state.error = action.payload;
		},
		setSuccess: (state, action: PayloadAction<string>) => {
			state.success = action.payload;
		},
		resetRequestState: () => initialState
	},
	extraReducers: (builder) => {
		builder
			.addCase(getAllRequestAsync.fulfilled, (state, { payload }) => {
				state.requests = payload.requests
			})
			.addCase(getAllRequestAsync.rejected, (state) => {
				state.error = Messages.ERROR_MESSAGE;
			})
			.addCase(createRequestAsync.fulfilled, (state, { payload }) => {
				state.isRequestCreated = true
			})
			.addCase(createRequestAsync.rejected, (state) => {
				state.isRequestCreated = false
			})
			.addCase(updateRequestAsync.fulfilled, (state, { payload }) => {
				state.isRequestUpdated = true
			})
			.addCase(updateRequestAsync.rejected, (state) => {
				state.isRequestUpdated = false
			})

	}
});

export const { setError, setSuccess, resetRequestState } = requestSlice.actions;
export const getRequests = (state: RootState) => state.request;

export default requestSlice.reducer;
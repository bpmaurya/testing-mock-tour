import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios, { AxiosError } from "axios";
import Log from "../../../../vendor/utils";
import { Messages } from "../../../../vendor/constant";
import { RootState } from "../store";
import { Department } from "MyModels";
import { appService } from "../../../services";


const TAG = 'inside-department-slice'

type InitialState = {
	departmentList: Department.Type[],
	error: string,
	success: string,
	isDepartmentAdded: boolean,
	isDepartmentDeleted: boolean,
	isDepartmentUpdated: boolean,
	isDepartmentCreated: boolean
}
const initialState: InitialState = { departmentList: [], error: '', success: '', isDepartmentAdded: false, isDepartmentDeleted: false, isDepartmentUpdated: false, isDepartmentCreated: false }


export const getAllDepartmentAsync = createAsyncThunk<InitialState>(
	'department/all-department',
	async (thunkApi) => {
		try {
			const response = await appService().departmentService.getDepartments()
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			return error.message
		}
	}
);

export const createDepartmentAsync = createAsyncThunk<InitialState, Department.Type>(
	'department/create-department',
	async (data: Department.Type, thunkApi) => {
		try {
			const response = await appService().departmentService.createDepartment(
				data
			);
			Log.d(TAG, response);
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const deleteDepartmentAsync = createAsyncThunk<InitialState, Department.Type>(
	'department/delete-department',
	async (data: Department.Type, thunkApi) => {
		try {
			const response = await appService().departmentService.deleteDepartment(
				data._id!
			);
			Log.d(TAG, response);
			return response;

		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const updateDepartmentAsync = createAsyncThunk<InitialState, Department.Type>(
	'department/update-department',
	async (data: Department.Type, thunkApi) => {
		try {
			const response = await appService().departmentService.updateDepartment(
				data
			);
			Log.d(TAG, response);
			return response;

		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);



export const departmentSlice = createSlice({
	name: 'department',
	initialState,
	reducers: {
		setError: (state, action: PayloadAction<string>) => {
			state.error = action.payload;
		},
		setSuccess: (state, action: PayloadAction<string>) => {
			state.success = action.payload;
		},
		resetDepartmentState: () => initialState
	},
	extraReducers: (builder) => {
		builder
			.addCase(getAllDepartmentAsync.fulfilled, (state, { payload }) => {
				state.departmentList = payload.departmentList
			})
			.addCase(getAllDepartmentAsync.rejected, (state) => {
				state.error = Messages.ERROR_MESSAGE;
			})
			.addCase(createDepartmentAsync.fulfilled, (state, { payload }) => {
				state.departmentList = payload.departmentList
				state.error = ''
				state.isDepartmentAdded = true
			})
			.addCase(createDepartmentAsync.rejected, (state) => {
				state.isDepartmentAdded = false
			})
			.addCase(deleteDepartmentAsync.fulfilled, (state, { payload }) => {
				state.isDepartmentDeleted = true
			})
			.addCase(deleteDepartmentAsync.rejected, (state) => {
				state.isDepartmentDeleted = false
			})
			.addCase(updateDepartmentAsync.fulfilled, (state, { payload }) => {
				state.isDepartmentUpdated = true
			})
			.addCase(updateDepartmentAsync.rejected, (state) => {
				state.isDepartmentUpdated = false
			})
	}
});

export const { setError, setSuccess, resetDepartmentState } = departmentSlice.actions;
export const getDepartment = (state: RootState) => state.department;

export default departmentSlice.reducer;
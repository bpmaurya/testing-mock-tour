import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';

import axios, { AxiosError } from 'axios';
import { DeskLocation, User } from 'MyModels';
import tokenService from '../../../services/token.service';
import { RootState } from '../store';
import Log from '../../../../vendor/utils';
import { Messages } from '../../../../vendor/constant';
import { appService } from '../../../services';

const TAG = 'inside-location-feature'

type LocationList = {
	error: string,
	success: string,
	locationList: DeskLocation[];
	getLocationError: string,
	addLocationError: string,
	addLocationSuccess: string,
	isLocationAdded: boolean,
	deleteLocationError: string,
	deleteLocationSuccess: string,
	isLocationDeleted: boolean,
	updateLocationError: string,
	updateLocationSuccess: string,
	isLocationUpdated: boolean
};

const user: User = tokenService.getUser();
const initialState: LocationList = user.accessToken
	? {
		locationList: [] as DeskLocation[],
		addLocationError: '',
		addLocationSuccess: '',
		isLocationAdded: false,
		getLocationError: '',
		deleteLocationError: '',
		deleteLocationSuccess: '',
		isLocationDeleted: false,
		updateLocationError: '',
		updateLocationSuccess: '',
		isLocationUpdated: false,
		error: '',
		success: ''
	}
	: {
		locationList: [] as DeskLocation[],
		addLocationError: '',
		addLocationSuccess: '',
		isLocationAdded: false,
		getLocationError: '',
		deleteLocationError: '',
		deleteLocationSuccess: '',
		isLocationDeleted: false,
		updateLocationError: '',
		updateLocationSuccess: '',
		isLocationUpdated: false,
		error: '',
		success: ''
	};

export const getAllLocationAsync = createAsyncThunk<LocationList>(
	'get/all-location',
	async (thunkApi) => {
		try {
			const response = await appService().locationService.getLocation();
			Log.d(TAG, response)
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			return error.message
		}
	}
);

export const createLocationAsync = createAsyncThunk<LocationList, DeskLocation>(
	'location/create-location',
	async (locationData: DeskLocation, thunkApi) => {

		try {
			const response = await appService().locationService.createLocation(
				locationData.name,
				locationData.status
			);
			Log.d(TAG, response);
			if (response.status === 200) {
				return response;
			}
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const deleteLocationAsync = createAsyncThunk<LocationList, DeskLocation>(
	'location/delete-location',
	async (locationData: DeskLocation, thunkApi) => {

		try {
			const response = await appService().locationService.deleteLocation(
				locationData._id!
			);
			Log.d(TAG, response);
			if (response.status === 200) {
				return response;
			}
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const updateLocationAsync = createAsyncThunk<LocationList, DeskLocation>(
	'location/update-location',
	async (locationData: DeskLocation, thunkApi) => {

		try {
			const response = await appService().locationService.updateLocation(
				locationData
			);
			Log.d(TAG, response);
			if (response.status === 200) {
				return response;
			}
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const locationSlice = createSlice({
	name: 'locations',
	initialState,
	reducers: {
		setError: (state, action: PayloadAction<string>) => {
			state.error = action.payload;
			state.addLocationError = action.payload;
			state.updateLocationError = action.payload;
		},
		setSuccess: (state, action: PayloadAction<string>) => {
			state.success = action.payload;
		},
		setUpdateError: (state, action: PayloadAction<string>) => {
			state.updateLocationError = action.payload
		},
		resetLocationState: () => initialState
	},
	extraReducers: (builder) => {
		builder
			.addCase(getAllLocationAsync.fulfilled, (state, { payload }) => {
				state.locationList = payload.locationList
			})
			.addCase(getAllLocationAsync.rejected, (state) => {
				state.getLocationError = Messages.ERROR_MESSAGE;
			})
			.addCase(createLocationAsync.fulfilled, (state, { payload }) => {
				state.isLocationAdded = true
				state.addLocationSuccess = 'Location Added!'
				state.addLocationError = ''
			})
			.addCase(createLocationAsync.rejected, (state) => {
				state.isLocationAdded = false
			})
			.addCase(deleteLocationAsync.fulfilled, (state, { payload }) => {
				state.isLocationDeleted = true
				state.deleteLocationSuccess = 'Location Deleted!'
				state.deleteLocationError = ''
			})
			.addCase(deleteLocationAsync.rejected, (state) => {
				state.isLocationDeleted = false
			})
			.addCase(updateLocationAsync.fulfilled, (state, { payload }) => {
				state.isLocationUpdated = true
				state.updateLocationSuccess = 'Location Updated!'
				state.updateLocationError = ''
			})
			.addCase(updateLocationAsync.rejected, (state) => {
				state.isLocationUpdated = false
			})
	}
});

export const { setError, setSuccess, setUpdateError, resetLocationState } = locationSlice.actions;

export const getLocations = (state: RootState) => state.locations;

export default locationSlice.reducer;
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';

import axios, { AxiosError } from 'axios';
import { DeleteUser, IUser, User, UserRegister } from 'MyModels';
import tokenService from '../../../services/token.service';
import { RootState } from '../store';
import Log from '../../../../vendor/utils';
import { Messages } from '../../../../vendor/constant';
import { appService } from '../../../services';

const TAG = 'inside-user-feature'

type UserList = {
	allUser: UserRegister[];
	error: string,
	success: string,
	updateSuccess: string,
	updateError: string,
	userUpdate: boolean,
	userDeleted: boolean,
	userDeletedMessage: string,
	userDeletedError: string,
};

const user: User = tokenService.getUser();
const initialState: UserList = user.accessToken
	? {
		allUser: [] as UserRegister[],
		error: '',
		success: '',
		updateSuccess: '',
		updateError: '',
		userUpdate: false,
		userDeleted: false,
		userDeletedError: '',
		userDeletedMessage: ''
	}
	: {
		allUser: [] as UserRegister[],
		error: '',
		success: '',
		updateSuccess: '',
		updateError: '',
		userUpdate: false,
		userDeleted: false,
		userDeletedError: '',
		userDeletedMessage: ''
	};

export const getAllUserAsync = createAsyncThunk<UserList>(
	'get/all-user',
	async (thunkApi) => {
		try {
			const response = await appService().userService.getUser();
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			return error.message
		}
	}
);

export const updateUserAsync = createAsyncThunk<UserList, IUser.Type>(
	'update/user',
	async (userUpdate: IUser.Type, thunkApi) => {
		if (!userUpdate.phone.toString().match(/^[6-9]{1}[0-9]{9}$/)) {
			thunkApi.dispatch(setError(`Invalid Phone number!!`));
			return thunkApi.rejectWithValue(`Invalid Phone number!!`);
		}
		try {
			const response = await appService().userService.updateUser(
				userUpdate
			);
			Log.d(TAG, response);
			if (response.status === 200) {
				return response;
			}
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const deleteUserAsync = createAsyncThunk<UserList, DeleteUser>(
	'delete/user',
	async (data: DeleteUser, thunkApi) => {
		try {
			const response = await appService().userService.deleteUser(
				data.id
			);
			Log.d(TAG, response);
			if (response.status === 200) {
				return response;
			}
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const userSlice = createSlice({
	name: 'users',
	initialState,
	reducers: {
		setError: (state, action: PayloadAction<string>) => {
			state.updateError = action.payload;
			state.userDeletedError = action.payload;
		},
		setSuccess: (state, action: PayloadAction<string>) => {
			state.updateSuccess = action.payload;
		},
		reset: () => initialState
	},
	extraReducers: (builder) => {
		builder
			.addCase(getAllUserAsync.fulfilled, (state, { payload }) => {
				state.allUser = payload.allUser
			})
			.addCase(getAllUserAsync.rejected, (state) => {
				state.error = Messages.ERROR_MESSAGE;
			})
			.addCase(updateUserAsync.fulfilled, (state) => {
				state.error = '';
				state.updateSuccess = Messages.USER_UPDATE_SUCCESS_MESSAGE;
				state.userUpdate = true;
			})
			.addCase(updateUserAsync.rejected, (state) => {
				state.error = '';
				state.updateSuccess = '';
				state.userUpdate = false;

			})
			.addCase(deleteUserAsync.fulfilled, (state) => {
				state.userDeletedMessage = Messages.USER_DELETE_SUCCESS_MESSAGE;
				state.userDeleted = true;
			})
			.addCase(deleteUserAsync.rejected, (state) => {
				state.userDeleted = false;

			})
	}
});

export const { setError, setSuccess, reset } = userSlice.actions;

export const getUsers = (state: RootState) => state.user;

export default userSlice.reducer;
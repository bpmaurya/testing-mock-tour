import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { Floor } from "MyModels"
import axios, { AxiosError } from "axios";
import Log from "../../../../vendor/utils";
import { Messages } from "../../../../vendor/constant";
import { RootState } from "../store";
import { appService } from "../../../services";

const TAG = 'inside-floor-slice'

type InitialState = {
	floorList: Floor.Type[],
	error: string,
	success: string,
	isFloorAdded: boolean,
	isFloorDeleted: boolean,
	isFloorUpdated: boolean
}
const initialState: InitialState = { floorList: [] as Floor.Type[], error: '', success: '', isFloorAdded: false, isFloorDeleted: false, isFloorUpdated: false }


export const getAllFloorWithLocationAsync = createAsyncThunk<InitialState, { locationId?: string }>(
	'floor/all-floors-for-location',
	async (data: { locationId?: string }, thunkApi) => {
		try {
			const response = await appService().floorService.getFloorsWithLocation(data.locationId);
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			return error.message
		}
	}
);

export const getAllFloorAsync = createAsyncThunk<InitialState>(
	'floor/all-floors',
	async (thunkApi) => {
		try {
			const response = await appService().floorService.getFloors();
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			return error.message
		}
	}
);

export const createFloorsAsync = createAsyncThunk<InitialState, Floor.Type>(
	'floor/create-floor',
	async (floorData: Floor.Type, thunkApi) => {
		try {
			const response = await appService().floorService.createFloor(
				floorData.name,
				floorData.location._id,
				floorData.status
			);
			Log.d(TAG, response);
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const deleteFloorAsync = createAsyncThunk<InitialState, Floor.Type>(
	'floor/delete-floor',
	async (floorData: Floor.Type, thunkApi) => {
		try {
			const response = await appService().floorService.deleteFloor(
				floorData._id!
			);
			Log.d(TAG, response);
			return response;

		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const updateFloorAsync = createAsyncThunk<InitialState, Floor.Type>(
	'floor/update-floor',
	async (floorData: Floor.Type, thunkApi) => {

		try {
			const response = await appService().floorService.updateFloor(
				floorData
			);
			Log.d(TAG, response);
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const floorSlice = createSlice({
	name: 'floor',
	initialState,
	reducers: {
		setError: (state, action: PayloadAction<string>) => {
			state.error = action.payload;
		},
		setSuccess: (state, action: PayloadAction<string>) => {
			state.success = action.payload;
		},
		resetFloorState: () => initialState
	},
	extraReducers: (builder) => {
		builder
			.addCase(getAllFloorAsync.fulfilled, (state, { payload }) => {
				state.floorList = payload.floorList
			})
			.addCase(getAllFloorAsync.rejected, (state) => {
				state.error = Messages.ERROR_MESSAGE;
			})
			.addCase(createFloorsAsync.fulfilled, (state, { payload }) => {
				state.floorList = payload.floorList
				state.error = ''
				state.isFloorAdded = true
			})
			.addCase(createFloorsAsync.rejected, (state) => {
				state.isFloorAdded = false
			})
			.addCase(deleteFloorAsync.fulfilled, (state, { payload }) => {
				state.floorList = payload.floorList
				state.error = ''
				state.isFloorDeleted = true
			})
			.addCase(deleteFloorAsync.pending, (state, { payload }) => {
				state.error = ''
				state.isFloorDeleted = true
			})
			.addCase(deleteFloorAsync.rejected, (state) => {
				state.isFloorDeleted = false
			})
			.addCase(updateFloorAsync.fulfilled, (state, { payload }) => {
				state.floorList = payload.floorList
				state.error = ''
				state.isFloorUpdated = true
			})
			.addCase(updateFloorAsync.pending, (state, { payload }) => {
				state.error = ''
				state.isFloorUpdated = true
			})
			.addCase(updateFloorAsync.rejected, (state) => {
				state.isFloorUpdated = false
			})
	}
});

export const { setError, setSuccess, resetFloorState } = floorSlice.actions;
export const getFloors = (state: RootState) => state.floor;

export default floorSlice.reducer;
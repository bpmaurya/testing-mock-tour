import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios, { AxiosError } from "axios";
import Log from "../../../../vendor/utils";
import { Messages } from "../../../../vendor/constant";
import { RootState } from "../store";
import { Project } from "MyModels";
import { appService } from "../../../services";

const TAG = 'inside-project-slice'

type InitialState = {
	projectList: Project.Type[],
	error: string,
	success: string,
	isProjectAdded: boolean,
	isProjectDeleted: boolean,
	isProjectUpdated: boolean,
	isProjectCreated: boolean
}
const initialState: InitialState = { projectList: [], error: '', success: '', isProjectAdded: false, isProjectDeleted: false, isProjectUpdated: false, isProjectCreated: false }


export const getAllProjectAsync = createAsyncThunk<InitialState>(
	'project/all-projects',
	async (thunkApi) => {
		try {
			const response = await appService().projectService.getProjects()
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			return error.message
		}
	}
);

export const createProjectAsync = createAsyncThunk<InitialState, Project.Type>(
	'project/create-project',
	async (projectData: Project.Type, thunkApi) => {
		try {
			const response = await appService().projectService.createProjects(
				projectData
			);
			Log.d(TAG, response);
			return response;
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const deleteProjectAsync = createAsyncThunk<InitialState, Project.Type>(
	'project/delete-project',
	async (projectData: Project.Type, thunkApi) => {
		try {
			const response = await appService().projectService.deleteProject(
				projectData._id!
			);
			Log.d(TAG, response);
			return response;

		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const updateProjectAsync = createAsyncThunk<InitialState, Project.Type>(
	'project/update-project',
	async (deskData: Project.Type, thunkApi) => {
		try {
			const response = await appService().projectService.updateProject(
				deskData
			);
			Log.d(TAG, response);
			return response;

		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);



export const projectSlice = createSlice({
	name: 'project',
	initialState,
	reducers: {
		setError: (state, action: PayloadAction<string>) => {
			state.error = action.payload;
		},
		setSuccess: (state, action: PayloadAction<string>) => {
			state.success = action.payload;
		},
		resetProjectState: () => initialState
	},
	extraReducers: (builder) => {
		builder
			.addCase(getAllProjectAsync.fulfilled, (state, { payload }) => {
				state.projectList = payload.projectList
			})
			.addCase(getAllProjectAsync.rejected, (state) => {
				state.error = Messages.ERROR_MESSAGE;
			})
			.addCase(createProjectAsync.fulfilled, (state, { payload }) => {
				state.projectList = payload.projectList
				state.error = ''
				state.isProjectAdded = true
			})
			.addCase(createProjectAsync.rejected, (state) => {
				state.isProjectAdded = false
			})
			.addCase(deleteProjectAsync.fulfilled, (state, { payload }) => {
				state.isProjectDeleted = true
			})
			.addCase(deleteProjectAsync.rejected, (state) => {
				state.isProjectDeleted = false
			})
			.addCase(updateProjectAsync.fulfilled, (state, { payload }) => {
				state.isProjectUpdated = true
			})
			.addCase(updateProjectAsync.rejected, (state) => {
				state.isProjectUpdated = false
			})
	}
});

export const { setError, setSuccess, resetProjectState } = projectSlice.actions;
export const getProjects = (state: RootState) => state.project;

export default projectSlice.reducer;
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';

import axios, { AxiosError } from 'axios';
import { AuthState, User, UserCredentials, UserRegister } from 'MyModels';
import tokenService from '../../../services/token.service';
import { authService } from '../../../services/auth.service';
import { RootState } from '../store';
import Log from '../../../../vendor/utils';
import { Messages, PasswordRegex, PhoneRegex } from '../../../../vendor/constant';

const TAG = 'inside-auth-feature'

const user: User = tokenService.getUser();
const initialState: AuthState = user.accessToken
	? {
		isLoggedIn: true,
		user: user,
		error: '',
		success: '',
		userRegisterSuccess: false
	}
	: {
		isLoggedIn: false,
		user: { accessToken: '', refreshToken: '', name: '', email: '' },
		error: '',
		success: '',
		userRegisterSuccess: false
	};

export const registerAsync = createAsyncThunk<AuthState, UserRegister>(
	'auth/register',
	async (userRegister: UserRegister, thunkApi) => {
		if (userRegister.password !== userRegister.passwordConf) {
			thunkApi.dispatch(setError(`Your password doesn't match`));
			return thunkApi.rejectWithValue(`Your password doesn't match`);
		}
		if (!userRegister.password.match(PasswordRegex)) {
			thunkApi.dispatch(setError(`Password should be Minimum eight characters, at least one uppercase, one lowercase, one special charactor and  one number`));
			return thunkApi.rejectWithValue(`Password should be Minimum eight characters, at least one uppercase, one lowercase, one special charactor and  one number`);
		}
		if (!userRegister.phone.toString().match(PhoneRegex)) {
			thunkApi.dispatch(setError(`Invalid Phone number!!`));
			return thunkApi.rejectWithValue(`Invalid Phone number!!`);
		}
		try {
			const response = await authService.register(
				userRegister.email,
				userRegister.firstName,
				userRegister.middleName,
				userRegister.lastName,
				userRegister.pastExperience,
				userRegister.domain,
				userRegister.department._id!,
				userRegister.password,
				userRegister.role,
				userRegister.position._id!,
				userRegister.project._id!,
				Number(userRegister.phone)
			);
			Log.d(TAG, response);
			if (response.status === 200) {
				return response;
			}
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const loginAsync = createAsyncThunk<AuthState, UserCredentials>(
	'auth/login',
	async (userCredentials: UserCredentials, thunkApi) => {
		Log.d(TAG, userCredentials);
		try {
			const response = await authService.login(
				userCredentials.email,
				userCredentials.password
			);
			if (response) {
				return response;
			}
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const updatePasswordAsync = createAsyncThunk<AuthState, { newPass: string, _id: string, currentPass: string }>(
	'auth/change password',
	async (userData: { newPass: string, _id: string, currentPass: string }, thunkApi) => {
		Log.d(TAG, userData);
		if (!userData.newPass.match(PasswordRegex)) {
			thunkApi.dispatch(setError(`Password should be Minimum eight characters, at least one uppercase, one lowercase, one special charactor and  one number`));
			return thunkApi.rejectWithValue(`Password should be Minimum eight characters, at least one uppercase, one lowercase, one special charactor and  one number`);
		}
		try {
			const response = await authService.changePassword(
				userData.newPass,
				userData._id,
				userData.currentPass
			);
			if (response) {
				return response;
			}
		} catch (_error) {
			const error = _error as Error | AxiosError;
			Log.d(TAG, error);
			if (axios.isAxiosError(error)) {
				thunkApi.dispatch(setError(error.response?.data.message));
				return thunkApi.rejectWithValue(error.response?.data.message);
			}
			thunkApi.dispatch(setError(error.message));
			return thunkApi.rejectWithValue(error.message);
		}
	}
);

export const logoutAsync = createAsyncThunk('auth/logout', async () => {
	authService.logout();
});

export const authSlice = createSlice({
	name: 'auth',
	initialState,
	reducers: {
		setError: (state, action: PayloadAction<string>) => {
			state.error = action.payload;
		},
		setSuccess: (state, action: PayloadAction<string>) => {
			state.success = action.payload;
		},
		refreshToken: (state, { payload }) => {
			state.user.accessToken = payload.accessToken;
			state.user.refreshToken = payload.refreshToken;
		}
	},
	extraReducers: (builder) => {
		builder
			.addCase(loginAsync.fulfilled, (state, { payload }) => {
				state.isLoggedIn = true;
				state.user = payload.user;
				state.error = '';
			})
			.addCase(loginAsync.rejected, (state) => {
				state.isLoggedIn = false;
			})
			.addCase(updatePasswordAsync.fulfilled, (state, { payload }) => {
				state.isLoggedIn = true;
				state.user = payload.user;
			})
			.addCase(updatePasswordAsync.rejected, (state) => {
				state.isLoggedIn = false;
			})
			.addCase(registerAsync.fulfilled, (state) => {
				state.error = '';
				state.success = Messages.USER_REGISTER_SUCCESS_MESSAGE;
				state.userRegisterSuccess = true
			})
			.addCase(registerAsync.rejected, (state) => {
				state.userRegisterSuccess = false
			})
			.addCase(logoutAsync.fulfilled, (state) => {
				state.isLoggedIn = false;
				state.user = { accessToken: '', refreshToken: '', name: '', email: '' };
				state.error = '';
			});

	}
});

export const { setError, setSuccess, refreshToken } = authSlice.actions;

export const selectAuth = (state: RootState) => state.auth;

export default authSlice.reducer;
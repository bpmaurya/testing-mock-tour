import { Navigate } from 'react-router-dom';
import { history } from '../helpers/history';
import { useAppSelector } from '../states/store/hooks';
import { selectAuth } from '../states/store/feature/auth';

export function NoPageFound() {
	const { isLoggedIn } = useAppSelector(selectAuth);
	if (!isLoggedIn) {
		return <Navigate to="/" state={{ from: history.location }} />;
	}
	return <div>NoPageFound</div>;
}

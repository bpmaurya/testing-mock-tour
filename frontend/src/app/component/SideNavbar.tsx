import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import LogoutIcon from '@mui/icons-material/Logout';
import { Link } from 'react-router-dom';
import { Logo, adminRoute, userRoute } from '../constant/menu';
import { AppDep } from '../services';
import { Navigation } from '../services/navigate';
import { FlexDiv } from '../constant';
import tokenService from '../services/token.service';
import { useAppDispatch } from '../states/store/hooks';
import { logoutAsync } from '../states/store/feature/auth';
import NotificationsIcon from '@mui/icons-material/Notifications';
import { DivWithArrowIcon, NotificationDiv, NotificationMessage } from './common-component/NotificationDiv';
import Badge from '@mui/material/Badge';
import { getAllNotificationAsync } from '../states/store/feature/notification.slice';
import Log from '../../vendor/utils';
import { Notification } from 'MyModels';
import { AppContext } from '../../App';

const TAG = 'inside-navbar';

const drawerWidth = 240;

interface Props {
	window?: () => Window;
	children?: React.ReactNode;
	appDep?: AppDep;
	currentMenu?: string;
}

export function SideNavbar(props: Props) {
	const socket = React.useContext(AppContext);
	const { window } = props;
	const [mobileOpen, setMobileOpen] = React.useState(false);
	const dispatch = useAppDispatch();

	const handleDrawerToggle = () => {
		setMobileOpen(!mobileOpen);
	};

	const onLogoutClick = () => {
		dispatch(logoutAsync())
			.unwrap()
			.then(() => {});
	};

	const [open, setOpen] = React.useState(false);
	const [notifications, setNotification] = React.useState<Notification.Type[]>([]);

	React.useEffect(() => {
		const user = tokenService.getUser();
		if (!!user) {
			socket.socketContext.on(`message`, (notification: any) => {
				console.log('notification', notification);
				if (notification.user._id === user.userObject._id) {
					setNotification((prevData: any) => [...prevData, notification]);
				}
			});
		}
		return () => {
			socket.socketContext.off('message');
		};
	}, [socket.socketContext]);

	React.useEffect(() => {
		const user = tokenService.getUser();
		if (!!user.userObject) {
			dispatch(getAllNotificationAsync({ id: user.userObject._id }))
				.unwrap()
				.then((res: any) => {
					setNotification(res.allNotification);
				})
				.catch(err => {
					Log.d(TAG, err);
				});
		}
	}, [dispatch]);

	const drawer = (
		<div>
			{!!mobileOpen ? (
				<Link to="/">
					<FlexDiv justify="center" align="center">
						<Logo />
					</FlexDiv>
				</Link>
			) : (
				<Link to="/">
					<FlexDiv justify="center" align="center">
						<Logo />
					</FlexDiv>
				</Link>
			)}
			<Divider />
			<List>
				{tokenService.isAdmin() ? (
					<>
						{adminRoute.map((e, index) => (
							<ListItem key={index} disablePadding>
								<Link
									style={{ textDecoration: 'none', width: '100%', color: 'white' }}
									to={Navigation.Path.User + e.url}
								>
									<ListItemButton onClick={handleDrawerToggle}>
										<ListItemIcon>{e.icon}</ListItemIcon>
										<ListItemText primary={e.name} />
									</ListItemButton>
								</Link>
							</ListItem>
						))}
					</>
				) : (
					<>
						{userRoute.map((e, index) => (
							<ListItem key={index} disablePadding>
								<Link
									style={{ textDecoration: 'none', width: '100%', color: 'white' }}
									to={Navigation.Path.User + e.url}
								>
									<ListItemButton onClick={handleDrawerToggle}>
										<ListItemIcon>{e.icon}</ListItemIcon>
										<ListItemText primary={e.name} />
									</ListItemButton>
								</Link>
							</ListItem>
						))}
					</>
				)}

				<ListItemButton style={{ color: 'white' }} onClick={onLogoutClick}>
					<ListItemIcon>
						<LogoutIcon />
					</ListItemIcon>
					<ListItemText primary="Logout" />
				</ListItemButton>
			</List>
		</div>
	);
	const container = window !== undefined ? () => window().document.body : undefined;

	return (
		<Box sx={{ display: 'flex', width: '100%' }}>
			<CssBaseline />
			<AppBar
				position="fixed"
				sx={{
					width: { sm: `calc(100% - ${drawerWidth}px)` },
					ml: { sm: `${drawerWidth}px` },
				}}
			>
				<Toolbar>
					<IconButton
						color="inherit"
						aria-label="open drawer"
						edge="start"
						onClick={handleDrawerToggle}
						sx={{ mr: 2, display: { sm: 'none' } }}
					>
						<MenuIcon />
					</IconButton>

					<FlexDiv justify="space-between" align="center" alignItems="center" width="full">
						<h3> {props.currentMenu?.toUpperCase()}</h3>
						<FlexDiv justify="center" align="center" alignItems="center">
							<ListItemText primary="Notifications" />
							<NotificationDiv
								placement="bottom-end"
								content={
									<DivWithArrowIcon
										divContent={
											<FlexDiv
												style={{ padding: '12px', maxHeight: '500px', overflow: 'scroll' }}
												direction="column"
											>
												{!!notifications && notifications.length !== 0 ? (
													<Link
														style={{
															textDecoration: 'none',
															width: '100%',
															color: 'inherit',
														}}
														to={'/dashboard/my_request'}
													>
														<List
															sx={{
																width: '100%',
																maxWidth: 360,
																bgcolor: 'background.paper',
															}}
														>
															<NotificationMessage notification={notifications} />
														</List>
													</Link>
												) : (
													<FlexDiv>
														<p>No new notifications</p>
													</FlexDiv>
												)}
											</FlexDiv>
										}
									/>
								}
								open={open}
								onClose={() => setOpen(false)}
							>
								<IconButton color="inherit" onClick={() => setOpen(!open)}>
									<Badge badgeContent={!!notifications ? notifications.length : 0} color="error">
										<NotificationsIcon fontSize="large" />
									</Badge>
								</IconButton>
							</NotificationDiv>
						</FlexDiv>
					</FlexDiv>
				</Toolbar>
			</AppBar>
			<Box
				component="nav"
				sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
				aria-label="mailbox folders"
			>
				<Drawer
					container={container}
					variant="temporary"
					open={mobileOpen}
					onClose={handleDrawerToggle}
					ModalProps={{
						keepMounted: true,
					}}
					sx={{
						display: { xs: 'block', sm: 'none' },
						'& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
					}}
				>
					{drawer}
				</Drawer>
				<Drawer
					variant="permanent"
					sx={{
						display: { xs: 'none', sm: 'block' },
						'& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
					}}
					open
				>
					{drawer}
				</Drawer>
			</Box>
			<Box component="main" sx={{ flexGrow: 1, p: 3, width: { sm: `calc(100% - ${drawerWidth}px)` } }}>
				<Toolbar />
				{props.children}
			</Box>
		</Box>
	);
}

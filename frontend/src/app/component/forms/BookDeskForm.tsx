import { Desk, DeskRequest, IUser, Project } from 'MyModels';
import { useEffect, useState } from 'react';
import { useAppDispatch } from '../../states/store/hooks';
import { getAllProjectAsync } from '../../states/store/feature/project.slice';
import Log from '../../../vendor/utils';
import { getAllUserAsync } from '../../states/store/feature/user.slice';
import { FlexDiv } from '../../constant';
import { Autocomplete, Button, FormControl, MenuItem, Select, TextField, InputLabel } from '@mui/material';
import { LoadingButton } from '../common-component/LoaderComponent';
import { MakeItemList } from './UpdateRequest';

const TAG = 'inside book desk';
interface DeskFormProps {
	initialData?: DeskRequest.Type;
	onSubmit: (e: any) => Promise<any>;
	onCancel: (e: any) => void;
	user: IUser.Type;
	seat: Desk.Type;
}
export const BookDeskForm: React.FC<DeskFormProps> = ({ initialData, onSubmit, onCancel, user, seat }) => {
	const [formData, setFormData] = useState<DeskRequest.Type>(
		initialData || {
			user: {} as IUser.Type,
			seat: {} as Desk.Type,
			priority: '',
			description: '',
			reportingManager: {} as IUser.Type,
			project: {} as Project.Type,
			status: 'pending',
		}
	);

	const dispatch = useAppDispatch();
	const [allUser, setAllUser] = useState<IUser.Type[]>([]);
	const [allProject, setAllProject] = useState<Project.Type[]>([]);

	useEffect(() => {
		if (initialData) {
			setFormData(initialData);
		}
		dispatch(getAllProjectAsync())
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res);
				setAllProject(res.allProject);
			})
			.catch(error => {
				Log.d(TAG, error);
			});

		dispatch(getAllUserAsync())
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res.alluser);
				setAllUser(res.alluser);
			})
			.catch((error: string) => {
				Log.d(TAG, error);
			});
	}, [dispatch, initialData]);

	const [errors, setError] = useState<Partial<DeskRequest.Type>>({});

	const validateForm = (name: string, value: string) => {
		switch (name) {
			case 'priority':
				if (!value || value.trim() === '') {
					return 'priority is required!';
				} else return '';
			case 'description':
				if (!value || value.trim() === '') {
					return 'description is required!';
				} else return '';
			default:
				break;
		}
	};

	const handleChange = (e: React.ChangeEvent<HTMLInputElement>, v?: any) => {
		const { name, value } = e.target;
		setError(preData => ({
			...preData,
			[e.target.name]: validateForm(name, value),
		}));
		if (!!v && e.target.id.split('-')[0] === 'reportingManager') {
			setFormData((prevData: any) => ({
				...prevData,
				reportingManager: allUser.filter(e => e.email === v)[0],
			}));
		}
		setFormData((prevData: any) => ({
			...prevData,
			[name]: value,
		}));
		Log.d(TAG, formData);
	};

	const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		Log.d(TAG, formData);
		await onSubmit({ ...formData, user: user, seat: seat });
	};

	return (
		<>
			<FlexDiv gap="20px" direction="column">
				<MakeItemList
					data={[
						{
							itemName: 'Name',
							value: user.firstName + ' ' + user.middleName + ' ' + user.lastName,
						},
						{
							itemName: 'Email',
							value: user.email,
						},
						{
							itemName: 'New Seat Number',
							value: seat.name,
						},
						{
							itemName: 'New Location',
							value: `${seat.location?.name} ${seat.floor?.name} ${seat.wing?.name}`,
						},
					]}
				/>

				<FlexDiv gap="20px" direction="column" style={{ width: '100%' }}>
					<FormControl required fullWidth>
						<InputLabel id="reg_project_id">Project</InputLabel>
						<Select
							name="project"
							id="project_id"
							label="Project"
							value={formData.project?.name}
							onChange={(e: any) => handleChange(e)}
						>
							{!!allProject &&
								allProject.length > 0 &&
								allProject.map((e, index) => (
									<MenuItem value={e as any} key={e._id}>
										{e.name}
									</MenuItem>
								))}
						</Select>
					</FormControl>
					<FormControl required fullWidth>
						<Autocomplete
							id="reportingManager"
							options={allUser.map(e => e.email)}
							value={formData.reportingManager.firstName}
							onChange={(e: any, v) => handleChange(e, v)}
							sx={{ width: '100%' }}
							renderInput={params => (
								<TextField {...params} label="Reporting manager" value={params as any} />
							)}
						/>
					</FormControl>
					<FormControl fullWidth>
						<InputLabel id="reg_priority_id">Priority</InputLabel>
						<Select
							required
							type="text"
							name="priority"
							id="priority_id"
							label="Priority"
							value={formData.priority}
							onChange={(e: any) => handleChange(e)}
						>
							<MenuItem value="high">High</MenuItem>
							<MenuItem value="low">Low</MenuItem>
						</Select>
					</FormControl>
					<TextField
						fullWidth
						rows={2}
						maxRows={5}
						multiline
						name="description"
						label="Description"
						id="description_id"
						type="textarea"
						value={formData.description}
						onChange={handleChange}
					/>
				</FlexDiv>

				<FlexDiv gap="20px" justify="center" align="center" alignItems="center" style={{ width: '100%' }}>
					<LoadingButton
						disabled={!!errors.description || !!errors.priority}
						onClick={(e: any) => handleSubmit(e)}
					>
						<Button variant="contained" disabled={!!errors.description || !!errors.priority}>
							Submit
						</Button>
					</LoadingButton>
					<Button variant="contained" color="error" onClick={e => onCancel(e)}>
						Cancel
					</Button>
				</FlexDiv>
			</FlexDiv>
		</>
	);
};

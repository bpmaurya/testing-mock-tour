import React, { useEffect, useState } from 'react';
import { FlexDiv } from '../../constant';
import { Textfield } from '../common-component/Textfield';
import Log from '../../../vendor/utils';
import { Button, FormControl, InputLabel, MenuItem, Select } from '@mui/material';
import { DeskLocation, Floor, Wing } from 'MyModels';
import { LoadingButton } from '../common-component/LoaderComponent';
import { getAllLocationAsync } from '../../states/store/feature/location.slice';
import { useAppDispatch } from '../../states/store/hooks';
import { getAllFloorWithLocationAsync } from '../../states/store/feature/floor.slice';

const TAG = 'inside-wing-form';

interface FormProps {
	initialData?: Wing.Type;
	onSubmit: (data: Wing.Type) => Promise<any>;
	handleCancel: (data: any) => void;
	onEdit?: boolean;
}

export const WingForm: React.FC<FormProps> = ({ initialData, onSubmit, handleCancel, onEdit }) => {
	const dispatch = useAppDispatch();
	const [formData, setFormData] = useState<Wing.Type>(
		initialData || {
			_id: '',
			name: '',
			location: {} as DeskLocation,
			floor: {} as Floor.Type,
		}
	);
	const [errors, setError] = useState<Partial<Floor.Type>>({});

	const validateForm = (name: string, value: string) => {
		switch (name) {
			case 'name':
				if (!value || value.trim() === '') {
					return 'Name is required!';
				} else return '';
			default:
				break;
		}
	};

	useEffect(() => {
		if (initialData) {
			if (!!initialData.location) {
				dispatch(getAllFloorWithLocationAsync({ locationId: initialData.location._id }))
					.unwrap()
					.then((res: any) => {
						Log.d(TAG, res);
						setAllLFloor(res.allFloor);
					})
					.catch(error => {
						Log.d(TAG, error);
					});
			}
			setFormData(initialData);
		}
	}, [dispatch, initialData]);

	const [allLocation, setAllLocation] = useState<DeskLocation[]>();
	const [allFloor, setAllLFloor] = useState<Floor.Type[]>();

	const onLocationChange = async (data: DeskLocation) => {
		dispatch(getAllFloorWithLocationAsync({ locationId: data._id }))
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res);
				setAllLFloor(res.allFloor);
			})
			.catch(error => {
				Log.d(TAG, error);
			});
	};

	useEffect(() => {
		dispatch(getAllLocationAsync())
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res);
				setAllLocation(res.allLocation);
			})
			.catch(error => {
				Log.d(TAG, error);
			});
	}, [dispatch]);

	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const { name, value } = e.target;
		if (e.target.name === 'location') {
			onLocationChange(e.target.value as any);
			setFormData((prevData: any) => ({
				...prevData,
				floor: {},
			}));
		}
		setError(preData => ({
			...preData,
			[e.target.name]: validateForm(name, value),
		}));
		setFormData((prevData: any) => ({
			...prevData,
			[name]: value,
		}));
		Log.d(TAG, formData);
	};

	const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		await onSubmit(formData);
	};

	return (
		<>
			<FlexDiv direction="column" justify="center" gap="20px" alignItems="center">
				<FlexDiv style={{ width: '100%' }} gap="20px">
					{formData.location && !!onEdit && (
						<FlexDiv style={{ width: '100%' }} gap="20px">
							<FormControl required fullWidth>
								<InputLabel id="reg_role_id">Location</InputLabel>
								<Select
									required
									type="text"
									name="location"
									id="Location_id"
									label="Location"
									value={formData.location.name as any}
									onChange={(e: any) => handleChange(e)}
									renderValue={p => p}
								>
									{!!allLocation &&
										allLocation.length > 0 &&
										allLocation.map((e, index) => (
											<MenuItem value={e as any} key={e._id}>
												{e.name}
											</MenuItem>
										))}
								</Select>
							</FormControl>
							<FormControl required fullWidth>
								<InputLabel id="reg_role_id">Floor</InputLabel>
								<Select
									required
									type="text"
									name="floor"
									id="Floor_id"
									label="Floor"
									value={formData.floor.name as any}
									onChange={(e: any) => handleChange(e)}
									renderValue={p => p}
								>
									{!!allFloor &&
										allFloor.length > 0 &&
										allFloor.map((e, index) => (
											<MenuItem value={e as any} key={e._id}>
												{e.name}
											</MenuItem>
										))}
								</Select>
							</FormControl>
						</FlexDiv>
					)}
					{formData.location && !onEdit && (
						<FlexDiv style={{ width: '100%' }} gap="20px">
							<FormControl required fullWidth>
								<InputLabel id="reg_role_id">Location</InputLabel>
								<Select
									required
									type="text"
									name="location"
									id="Location_id"
									label="Location"
									value={formData.location.name as any}
									onChange={(e: any) => handleChange(e)}
								>
									{!!allLocation &&
										allLocation.length > 0 &&
										allLocation.map((e, index) => (
											<MenuItem value={e as any} key={e._id}>
												{e.name}
											</MenuItem>
										))}
								</Select>
							</FormControl>
							<FormControl required fullWidth>
								<InputLabel id="reg_role_id">Floor</InputLabel>
								<Select
									required
									type="text"
									name="floor"
									id="floor_id"
									label="Floor"
									disabled={!allFloor}
									value={formData.floor.name as any}
									onChange={(e: any) => handleChange(e)}
								>
									{!!allFloor &&
										allFloor.length > 0 &&
										allFloor.map((e, index) => (
											<MenuItem value={e as any} key={e._id}>
												{e.name}
											</MenuItem>
										))}
								</Select>
							</FormControl>
						</FlexDiv>
					)}

					<Textfield
						required
						type="text"
						label="Name"
						maxLength={50}
						id="Name_id"
						name="name"
						value={formData.name}
						onChange={e => handleChange(e)}
						error={!!errors.name}
						errorText={!!errors.name ? errors.name : ''}
					/>
				</FlexDiv>
				<FlexDiv gap="20px">
					<LoadingButton disabled={!!errors.location || !!errors.name} onClick={(e: any) => handleSubmit(e)}>
						<Button disabled={!!errors.location || !!errors.name} variant="contained">
							{!!onEdit ? 'Update' : 'Add'}
						</Button>
					</LoadingButton>
					<Button variant="contained" color="error" onClick={e => handleCancel(e)}>
						Cancel
					</Button>
				</FlexDiv>
			</FlexDiv>
		</>
	);
};

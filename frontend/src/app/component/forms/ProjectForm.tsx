import React, { useEffect, useState } from 'react';
import { FlexDiv } from '../../constant';
import { Textfield } from '../common-component/Textfield';
import Log from '../../../vendor/utils';
import { Button } from '@mui/material';
import { Project } from 'MyModels';
import { LoadingButton } from '../common-component/LoaderComponent';

const TAG = 'inside-location-form';

interface FormProps {
	initialData?: Project.Type;
	onSubmit: (data: Project.Type) => Promise<any>;
	handleCancel: (data: any) => void;
	onEdit?: boolean;
}

export const ProjectForm: React.FC<FormProps> = ({ initialData, onSubmit, handleCancel, onEdit }) => {
	const [formData, setFormData] = useState<Project.Type>(
		initialData || {
			_id: '',
			name: '',
			status: false,
		}
	);
	const [errors, setError] = useState<Partial<Project.Type>>({});

	const validateForm = (name: string, value: string) => {
		switch (name) {
			case 'name':
				if (!value || value.trim() === '') {
					return 'Name is required!';
				} else return '';
			default:
				break;
		}
	};

	useEffect(() => {
		if (initialData) {
			setFormData(initialData);
		}
	}, [initialData]);

	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const { name, value } = e.target;
		setError(preData => ({
			...preData,
			[e.target.name]: validateForm(name, value),
		}));
		setFormData((prevData: any) => ({
			...prevData,
			[name]: value,
		}));
		Log.d(TAG, formData);
	};

	const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		await onSubmit(formData);
	};

	return (
		<>
			<FlexDiv direction="column" justify="center" gap="20px" alignItems="center">
				<Textfield
					type="text"
					label="Name"
					id="location_id"
					name="name"
					value={formData.name}
					onChange={handleChange}
					error={!!errors.name}
					errorText={!!errors.name ? errors.name : ''}
				/>
			</FlexDiv>
			<FlexDiv gap="20px">
				<LoadingButton disabled={!!errors.name} onClick={(e: any) => handleSubmit(e)}>
					<Button disabled={!!errors.name} variant="contained">
						{!!onEdit ? 'Update' : 'Add'}
					</Button>
				</LoadingButton>
				<Button variant="contained" color="error" onClick={e => handleCancel(e)}>
					Cancel
				</Button>
			</FlexDiv>
		</>
	);
};

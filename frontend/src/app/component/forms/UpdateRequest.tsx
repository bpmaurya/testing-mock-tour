import React from 'react';
import { Desk, DeskRequest, IUser, Project } from 'MyModels';
import { useEffect, useState } from 'react';
import Log from '../../../vendor/utils';
import { FlexDiv } from '../../constant';
import { Button, FormControl, MenuItem, Select, InputLabel, Divider, List, ListItem } from '@mui/material';
import { LoadingButton } from '../common-component/LoaderComponent';

const TAG = 'inside update request';

export function MakeItemList(props: { data: { itemName: string; value?: string }[] }) {
	return (
		<List>
			{props.data.map(e => {
				return (
					<div key={e.itemName}>
						<ListItem>
							<FlexDiv gap="20px" width="full" justify="space-between">
								<h4>{e.itemName}: &nbsp; </h4>
								<p>{e.value}</p>
							</FlexDiv>
						</ListItem>
						<Divider />
					</div>
				);
			})}
		</List>
	);
}

interface DeskFormProps {
	initialData: DeskRequest.Type;
	onSubmit: (e: any) => Promise<any>;
	onCancel: (e: any) => void;
}

export const UpdateRequestForm: React.FC<DeskFormProps> = ({ initialData, onSubmit, onCancel }) => {
	const [formData, setFormData] = useState<DeskRequest.Type>(
		initialData || {
			user: {} as IUser.Type,
			seat: {} as Desk.Type,
			priority: '',
			description: '',
			reportingManager: {} as IUser.Type,
			project: {} as Project.Type,
			status: '',
		}
	);

	useEffect(() => {
		if (initialData) {
			setFormData(initialData);
		}
	}, [initialData]);

	const [errors, setError] = useState<Partial<DeskRequest.Type>>({});

	const validateForm = (name: string, value: string) => {
		switch (name) {
			case 'status':
				if (!value || value.trim() === '') {
					return 'status is required!';
				} else return '';
			default:
				break;
		}
	};

	const handleChange = (e: React.ChangeEvent<HTMLInputElement>, v?: any) => {
		const { name, value } = e.target;
		setError(preData => ({
			...preData,
			[e.target.name]: validateForm(name, value),
		}));
		setFormData((prevData: any) => ({
			...prevData,
			[name]: value,
		}));
		Log.d(TAG, formData);
	};

	const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		Log.d(TAG, formData);
		await onSubmit(formData);
	};

	return (
		<>
			<FlexDiv gap="20px" direction="column">
				<MakeItemList
					data={[
						{
							itemName: 'Name',
							value:
								formData.user.firstName + ' ' + formData.user.middleName + ' ' + formData.user.lastName,
						},
						{
							itemName: 'Email',
							value: formData.user.email,
						},
						{
							itemName: 'New Seat Number',
							value: formData.seat.name,
						},
						{
							itemName: 'New Location',
							value: `${formData.seat.location?.name} ${formData.seat.floor?.name} ${formData.seat.wing?.name}`,
						},
						{
							itemName: 'Reporting authority',
							value: `${formData.reportingManager?.firstName} ${formData.reportingManager.middleName} ${formData.reportingManager.lastName}`,
						},
						{
							itemName: 'Project',
							value: `${formData.project.name}`,
						},
						{
							itemName: 'Priority',
							value: `${formData.priority}`,
						},
					]}
				/>

				<FlexDiv gap="20px" direction="column" style={{ width: '100%' }}>
					<FormControl required fullWidth>
						<InputLabel id="reg_priority_id">Status</InputLabel>
						<Select
							required
							disabled={initialData.status === 'reject'}
							type="text"
							name="status"
							id="status_id"
							label="Status"
							value={formData.status}
							onChange={(e: any) => handleChange(e)}
						>
							<MenuItem value="approved">Approved</MenuItem>
							<MenuItem value="reject">Reject</MenuItem>
						</Select>
					</FormControl>
				</FlexDiv>

				<FlexDiv gap="20px" justify="center" align="center" alignItems="center" style={{ width: '100%' }}>
					{initialData.status !== 'reject' && (
						<LoadingButton
							disabled={!!errors.description || !!errors.priority}
							onClick={(e: any) => handleSubmit(e)}
						>
							<Button variant="contained" disabled={!!errors.description || !!errors.priority}>
								Submit
							</Button>
						</LoadingButton>
					)}
					<Button variant="contained" color="error" onClick={e => onCancel(e)}>
						Cancel
					</Button>
				</FlexDiv>
			</FlexDiv>
		</>
	);
};

import { Button, FormControl, InputLabel, MenuItem, Select } from '@mui/material';
import { Department, Designation, Project, UserRegister } from 'MyModels';
import React, { useState, useEffect } from 'react';
import { FlexDiv } from '../../constant';
import { Textfield } from '../common-component/Textfield';
import Log from '../../../vendor/utils';
import FormHelperText from '@mui/material/FormHelperText';
import { EmailRegex, PhoneRegex, UserRoleType } from '../../../vendor/constant';
import { LoadingButton } from '../common-component/LoaderComponent';
import { getAllDepartmentAsync } from '../../states/store/feature/department.slice';
import { useAppDispatch } from '../../states/store/hooks';
import { getAllDesignationAsync } from '../../states/store/feature/designation.slice';
import { getAllProjectAsync } from '../../states/store/feature/project.slice';

const TAG = 'inside-user-form';
interface FormProps {
	initialData?: UserRegister;
	onSubmit: (data: UserRegister) => Promise<any>;
	handleCancel: (data: any) => void;
	onEdit?: boolean;
}

export const UserFormComponent: React.FC<FormProps> = ({ initialData, onSubmit, handleCancel, onEdit }) => {
	const [formData, setFormData] = useState<UserRegister>(
		initialData || {
			email: '',
			firstName: '',
			middleName: '',
			lastName: '',
			pastExperience: '',
			domain: '',
			department: {} as Department.Type,
			password: '',
			passwordConf: '',
			role: '',
			position: {} as Designation.Type,
			project: {} as Project.Type,
			phone: '',
		}
	);

	const dispatch = useAppDispatch();
	const [errors, setError] = useState<Partial<UserRegister>>({});
	const [passwordError, setPasswordError] = useState<string>('');
	const [allDepartment, setAllDepartment] = useState<Department.Type[]>([]);
	const [allDesignation, setAllDesignation] = useState<Designation.Type[]>([]);
	const [allProject, setAllProject] = useState<Project.Type[]>([]);

	const validateForm = (name: any, value: any) => {
		switch (name) {
			case 'email':
				if (!value || value.trim() === '') {
					return 'Email is required!';
				} else if (!EmailRegex.test(value)) {
					return 'Email is invalid!';
				} else {
					return '';
				}
			case 'firstName':
				if (!value || value.trim() === '') {
					return 'first name is required!';
				} else return '';
			case 'lastName':
				if (!value || value.trim() === '') {
					return 'last name is required!';
				} else return '';
			case 'domain':
				if (!value || value.trim() === '') {
					return 'domain is required!';
				} else return '';
			case 'pastExperience':
				if (!value || value.trim() === '') {
					return 'past experience is required!';
				} else return '';
			case 'role':
				if (!value || value.trim() === '') {
					return 'Role is required!';
				} else return '';
			case 'phone':
				if (!value || value.trim() === '') {
					return 'Phone is required!';
				} else if (!PhoneRegex.test(value.toString())) {
					return 'Phone is invalid!';
				} else return '';
			case 'password':
				if (!onEdit) {
					if (!value || value.trim() === '') {
						return 'Password is required!';
					} else return '';
				} else return '';
			case 'passwordConf':
				if (!onEdit) {
					if (!value || value.trim() === '') {
						return 'Password confirmation is required!';
					} else return '';
				} else return '';
			default:
				break;
		}
	};

	useEffect(() => {
		if (initialData) {
			setFormData(initialData);
		}
		dispatch(getAllDepartmentAsync())
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res);
				setAllDepartment(res.allDepartment);
			})
			.catch((error: any) => {
				Log.d(TAG, error);
			});
		dispatch(getAllDesignationAsync())
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res);
				setAllDesignation(res.allDesignation);
			})
			.catch(error => {
				Log.d(TAG, error);
			});
		dispatch(getAllProjectAsync())
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res);
				setAllProject(res.allProject);
			})
			.catch(error => {
				Log.d(TAG, error);
			});
	}, [dispatch, initialData]);

	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const { name, value } = e.target;
		setError(preData => ({
			...preData,
			[e.target.name]: validateForm(name, value),
		}));
		if (name === 'passwordConf' || name === 'password') {
			if (!(formData.password === value)) {
				setPasswordError('Password not match!');
			} else setPasswordError('');
		} else setPasswordError('');
		setFormData(prevData => ({
			...prevData,
			[name]: value,
		}));
		Log.d(TAG, formData);
	};

	const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
		// e.preventDefault();
		await onSubmit(formData);
	};

	const disabled =
		!!errors.email ||
		!!errors.firstName ||
		!!errors.phone ||
		!!errors.position ||
		!!errors.role ||
		!!errors.lastName ||
		!!errors.department ||
		!!errors.domain ||
		!!errors.pastExperience ||
		!!errors.position ||
		(!onEdit && !!errors.password) ||
		(!onEdit && !!errors.passwordConf);

	return (
		<FlexDiv justify="center" alignItems="center" direction="column" gap="20px">
			<FlexDiv gap="20px">
				<Textfield
					required
					maxLength={50}
					name="firstName"
					label="First Name"
					id="reg_firstName_id"
					type="text"
					value={formData.firstName}
					onChange={e => handleChange(e)}
					error={!!errors.firstName}
					errorText={!!errors.firstName ? errors.firstName : ''}
				/>
				<Textfield
					maxLength={50}
					name="middleName"
					label="Middle Name"
					id="reg_middleName_id"
					type="text"
					value={formData.middleName}
					onChange={e => handleChange(e)}
					error={!!errors.middleName}
					errorText={!!errors.middleName ? errors.middleName : ''}
				/>
			</FlexDiv>
			<FlexDiv gap="20px">
				<Textfield
					required
					maxLength={50}
					name="lastName"
					label="Last Name"
					id="reg_lastName_id"
					type="text"
					value={formData.lastName}
					onChange={e => handleChange(e)}
					error={!!errors.lastName}
					errorText={!!errors.lastName ? errors.lastName : ''}
				/>
				<Textfield
					required
					maxLength={50}
					name="pastExperience"
					label="Past Experience"
					id="reg_pastExperience_id"
					type="text"
					value={formData.pastExperience}
					onChange={e => handleChange(e)}
					error={!!errors.pastExperience}
					errorText={!!errors.pastExperience ? errors.pastExperience.toString() : ''}
				/>
			</FlexDiv>
			<FlexDiv gap="20px" width="full">
				<FlexDiv width="full">
					<Textfield
						required
						maxLength={50}
						name="domain"
						label="Domain"
						id="reg_domain_id"
						type="text"
						value={formData.domain}
						onChange={e => handleChange(e)}
						error={!!errors.domain}
						errorText={!!errors.domain ? errors.domain : ''}
					/>
				</FlexDiv>
				<FlexDiv style={{ width: '100%' }}>
					<FormControl required fullWidth>
						<InputLabel id="reg_department_id">Department</InputLabel>
						{!!onEdit ? (
							<Select
								MenuProps={{
									style: {
										maxHeight: 400,
									},
								}}
								required
								name="department"
								id="department_id"
								label="Department"
								value={formData.department?.name as any}
								onChange={(e: any) => handleChange(e)}
								renderValue={p => p}
							>
								{!!allDepartment &&
									allDepartment.length > 0 &&
									allDepartment.map((e, index) => (
										<MenuItem value={e as any} key={e._id}>
											{e.name}
										</MenuItem>
									))}
							</Select>
						) : (
							<Select
								required
								name="department"
								id="department_id"
								label="Department"
								value={formData.department?.name as any}
								onChange={(e: any) => handleChange(e)}
							>
								{!!allDepartment &&
									allDepartment.length > 0 &&
									allDepartment.map((e, index) => (
										<MenuItem value={e as any} key={e._id}>
											{e.name}
										</MenuItem>
									))}
							</Select>
						)}
					</FormControl>
				</FlexDiv>
			</FlexDiv>
			<FlexDiv gap="20px" width="full">
				<FlexDiv width="full">
					<Textfield
						required
						maxLength={50}
						name="email"
						label="Email"
						id="reg_email_id"
						type="text"
						disable={!!onEdit}
						value={formData.email}
						onChange={handleChange}
						error={!!errors.email}
						errorText={!!errors.email ? errors.email : ''}
					/>
				</FlexDiv>
				<FormControl required fullWidth>
					<InputLabel id="reg_designation_id">Position</InputLabel>
					{!!onEdit ? (
						<Select
							required
							name="position"
							id="position_id"
							label="Position"
							value={formData.position?.name}
							onChange={(e: any) => handleChange(e)}
							renderValue={p => p}
						>
							{!!allDesignation &&
								allDesignation.length > 0 &&
								allDesignation.map((e, index) => (
									<MenuItem value={e as any} key={e._id}>
										{e.name}
									</MenuItem>
								))}
						</Select>
					) : (
						<Select
							required
							name="position"
							id="position_id"
							label="Position"
							value={formData.position?.name}
							onChange={(e: any) => handleChange(e)}
						>
							{!!allDesignation &&
								allDesignation.length > 0 &&
								allDesignation.map((e, index) => (
									<MenuItem value={e as any} key={e._id}>
										{e.name}
									</MenuItem>
								))}
						</Select>
					)}
				</FormControl>
			</FlexDiv>
			<FlexDiv gap="20px" width="full">
				<FlexDiv width="full">
					<Textfield
						maxLength={10}
						required
						name="phone"
						label="Phone"
						id="reg_phone_id"
						type="number"
						value={formData.phone}
						onChange={handleChange}
						error={!!errors.phone}
						errorText={!!errors.phone ? errors.phone.toString() : ''}
					/>
				</FlexDiv>

				<FormControl required fullWidth>
					<InputLabel id="reg_project_id">Project</InputLabel>
					{!!onEdit ? (
						<Select
							name="project"
							id="project_id"
							label="Project"
							value={formData.project?.name}
							onChange={(e: any) => handleChange(e)}
							renderValue={p => p}
						>
							{!!allProject &&
								allProject.length > 0 &&
								allProject.map((e, index) => (
									<MenuItem value={e as any} key={e._id}>
										{e.name}
									</MenuItem>
								))}
						</Select>
					) : (
						<Select
							name="project"
							id="project_id"
							label="Project"
							value={formData.project?.name}
							onChange={(e: any) => handleChange(e)}
						>
							{!!allProject &&
								allProject.length > 0 &&
								allProject.map((e, index) => (
									<MenuItem value={e as any} key={e._id}>
										{e.name}
									</MenuItem>
								))}
						</Select>
					)}
				</FormControl>
			</FlexDiv>

			{!onEdit && (
				<FlexDiv gap="20px" style={{ width: '100%' }}>
					<FlexDiv gap="20px">
						<Textfield
							maxLength={50}
							required
							name="password"
							label="Password"
							id="reg_password_id"
							type="password"
							value={formData.password}
							onChange={handleChange}
							error={!!errors.password}
							errorText={!!errors.password ? errors.password : ''}
						/>

						<FlexDiv direction="column">
							<Textfield
								required
								maxLength={50}
								name="passwordConf"
								label="Confirm Password"
								id="reg_password_cnf_id"
								type="password"
								value={formData.passwordConf}
								onChange={handleChange}
								error={!!errors.passwordConf || passwordError.length > 1}
								errorText={!!errors.passwordConf ? errors.passwordConf : ''}
							/>
							{passwordError.length > 1 && (
								<FormHelperText style={{ marginLeft: '14px', color: '#d32f2f' }}>
									{passwordError}
								</FormHelperText>
							)}
						</FlexDiv>
					</FlexDiv>
				</FlexDiv>
			)}

			<FlexDiv style={{ width: '100%' }}>
				<FormControl fullWidth required className="userForm-role">
					<InputLabel id="reg_role_id">Role</InputLabel>
					<Select
						required
						name="role"
						id="reg_role_id"
						value={formData.role}
						label="Role"
						onChange={(e: any) => handleChange(e)}
						error={!!errors.role}
					>
						{UserRoleType.map((e, index) => (
							<MenuItem value={e.value} key={index}>
								{e.name}
							</MenuItem>
						))}
					</Select>
					{!!errors.role && (
						<FormHelperText style={{ color: '#d32f2f' }}>{!!errors.role ? errors.role : ''}</FormHelperText>
					)}
				</FormControl>
			</FlexDiv>

			<FlexDiv gap="20px">
				<LoadingButton disabled={disabled} onClick={(e: any) => handleSubmit(e)}>
					<Button disabled={disabled} variant="contained">
						{!!onEdit ? 'Update' : 'Add'}
					</Button>
				</LoadingButton>
				<Button variant="contained" color="error" onClick={e => handleCancel(e)}>
					Cancel
				</Button>
			</FlexDiv>
		</FlexDiv>
	);
};

import { DeskLocation, Floor, Wing } from 'MyModels';
import { useEffect, useState } from 'react';
import Log from '../../../vendor/utils';
import { FlexDiv } from '../../constant';
import { Textfield } from '../common-component/Textfield';
import { Button } from '@mui/material';
import { LoadingButton } from '../common-component/LoaderComponent';

const TAG = 'inside add desk form';

export interface DeskArrangement {
	row: string;
	start: number;
	end: number;
	location: string;
	floor: string;
	wing: string;
}
interface FormProps {
	initialData?: DeskArrangement;
	onSubmit: (e: any) => Promise<any>;
	onCancel: (e: any) => void;
	floor?: string;
	wing?: string;
	location?: string;
}
export const AddDeskForm: React.FC<FormProps> = ({ initialData, onSubmit, onCancel, floor, wing, location }) => {
	const [formData, setFormData] = useState<DeskArrangement>(
		initialData || {
			row: '',
			start: 1,
			end: 1,
			location: '',
			floor: '',
			wing: '',
		}
	);

	useEffect(() => {
		if (initialData) {
			setFormData(initialData);
		}
		setFormData((prevData: any) => ({
			...prevData,
			floor: floor,
			location: location,
			wing: wing,
		}));
	}, [floor, initialData, location, wing]);

	const [errors, setError] = useState<Partial<DeskArrangement>>({});

	const validateForm = (name: string, value: string) => {
		switch (name) {
			case 'row':
				if (!value || value.trim() === '') {
					return 'row is required!';
				} else return '';
			case 'end':
				if (!value || value.trim() === '') {
					return 'End is required!';
				} else return '';
			default:
				break;
		}
	};

	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const { name, value } = e.target;
		setError(preData => ({
			...preData,
			[e.target.name]: validateForm(name, value),
		}));
		setFormData((prevData: any) => ({
			...prevData,
			[name]: value,
		}));
		Log.d(TAG, formData);
	};

	const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		Log.d(TAG, formData);
		await onSubmit(formData);
	};

	return (
		<>
			<FlexDiv gap="20px">
				<Textfield
					maxLength={1}
					name="row"
					label="Row"
					id="row_id"
					required
					type="text"
					value={formData.row}
					onChange={handleChange}
					error={!!errors.row}
					errorText={!!errors.row ? errors.row : ''}
				/>
				<Textfield
					maxLength={50}
					name="start"
					label="Start"
					id="start_id"
					required
					type="number"
					disable
					value={formData.start}
					onChange={handleChange}
				/>
				<Textfield
					name="end"
					label="End"
					maxLength={10}
					id="end_id"
					required
					type="number"
					value={formData.end}
					onChange={handleChange}
					error={!!errors.end}
					errorText={!!errors.end ? errors.end.toString() : ''}
				/>
			</FlexDiv>
			<FlexDiv gap="20px">
				<LoadingButton disabled={!!errors.row || !!errors.end} onClick={(e: any) => handleSubmit(e)}>
					<Button variant="contained" disabled={!!errors.row || !!errors.end}>
						Add
					</Button>
				</LoadingButton>
				<Button variant="contained" color="error" onClick={e => onCancel(e)}>
					Cancel
				</Button>
			</FlexDiv>
		</>
	);
};

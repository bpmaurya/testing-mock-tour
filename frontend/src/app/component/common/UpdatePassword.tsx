import { FlexDiv } from '../../constant';
import { Alert, Button, FormHelperText } from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../states/store/hooks';
import { useState } from 'react';
import { logoutAsync, selectAuth, updatePasswordAsync } from '../../states/store/feature/auth';
import * as React from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import { Textfield } from '../common-component/Textfield';
import Log from '../../../vendor/utils';
import { ModalStyle } from './styles';
import tokenService from '../../services/token.service';

const TAG = 'inside-login-page';

export function BasicModal(props: { children: JSX.Element }) {
	return (
		<div>
			<Modal open={true} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
				<Box sx={ModalStyle}>
					<FlexDiv direction="column" justify="center" gap="40px" alignItems="center">
						<h1>Change Your Password</h1>
						{props.children}
					</FlexDiv>
				</Box>
			</Modal>
		</div>
	);
}

interface UserCredentials {
	c_password: string;
	new_password: string;
	conf_password: string;
}

export function UpdatePasswordPage() {
	const dispatch = useAppDispatch();
	const navigate = useNavigate();
	const [loading, setLoading] = useState(false);
	const { error } = useAppSelector(selectAuth);
	const [formData, setFormData] = useState<UserCredentials>({
		c_password: '',
		new_password: '',
		conf_password: '',
	});

	const [message, setMessage] = useState('');

	const handleSubmit = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
		event.preventDefault();
		if (!!formData.c_password && !!formData.new_password && !!formData.conf_password) {
			setMessage('');
			const user_id = localStorage.getItem('_id');
			if (user_id)
				await dispatch(
					updatePasswordAsync({
						newPass: formData.new_password,
						_id: user_id,
						currentPass: formData.c_password,
					})
				)
					.unwrap()
					.then(() => {
						localStorage.removeItem('jwt');
						localStorage.removeItem('user');
						window.location.reload();
					})
					.catch(e => {
						setShow(true);
						Log.d(TAG, e);
					});
		} else setMessage('Please Enter Valid Input');
		setLoading(true);
	};

	const [errors, setError] = useState<Partial<UserCredentials>>({});

	const validateForm = (name: any, value: any) => {
		switch (name) {
			case 'c_password':
				if (!value || value.trim() === '') {
					return 'Current Password is required!';
				} else {
					return '';
				}
			case 'new_password':
				if (!value || value.trim() === '') {
					return 'New Password is required!';
				} else {
					return '';
				}
			case 'conf_password':
				if (!value || value.trim() === '') {
					return 'Confirm Password is required!!';
				} else return '';
			default:
				break;
		}
	};
	const [passwordError, setPasswordError] = useState<string>('');

	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const { name, value } = e.target;

		setError(preData => ({
			...preData,
			[e.target.name]: validateForm(name, value),
		}));
		if (name === 'new_password' || name === 'conf_password') {
			if (!(formData.new_password === value)) {
				setPasswordError('Password not match!');
			} else setPasswordError('');
		} else setPasswordError('');
		setFormData((prevData: any) => ({
			...prevData,
			[name]: value,
		}));
		Log.d(TAG, formData);
	};

	const [show, setShow] = useState<boolean>(false);
	React.useEffect(() => {
		const timeId = setTimeout(() => {
			setShow(false);
		}, 3000);

		return () => {
			clearTimeout(timeId);
		};
	}, [show]);

	return (
		<FlexDiv className="login_main_comp">
			<BasicModal
				children={
					<FlexDiv justify="center" gap="20px" align="center" alignItems="center">
						<FlexDiv
							direction="column"
							gap="20px"
							style={{ maxWidth: '300px' }}
							justify="center"
							alignItems="center"
						>
							{!!show ? (
								<Alert severity="error">{error}</Alert>
							) : (
								<>{message.length > 1 && <Alert severity="error">{message}</Alert>}</>
							)}
							<Textfield
								required
								name="c_password"
								id="c_password"
								label="Current Password"
								type="password"
								maxLength={50}
								value={formData.c_password}
								onChange={handleChange}
								error={!!errors.c_password}
								errorText={!!errors.c_password ? errors.c_password : ''}
							/>
							<Textfield
								required
								name="new_password"
								id="new_password"
								label="New Password"
								type="password"
								value={formData.new_password}
								onChange={handleChange}
								error={!!errors.new_password}
								errorText={!!errors.new_password ? errors.new_password : ''}
								maxLength={50}
							/>
							<FlexDiv direction="column">
								<Textfield
									required
									name="conf_password"
									id="conf_password"
									label="Confirm Password"
									type="password"
									value={formData.conf_password}
									onChange={handleChange}
									error={!!errors.conf_password || passwordError.length > 1}
									errorText={!!errors.conf_password ? errors.conf_password : ''}
									maxLength={50}
								/>
								{passwordError.length > 1 && (
									<FormHelperText style={{ marginLeft: '14px', color: '#d32f2f' }}>
										{passwordError}
									</FormHelperText>
								)}
							</FlexDiv>

							<Button
								disabled={
									!!errors.c_password ||
									!!errors.conf_password ||
									!!errors.new_password ||
									!!(passwordError.length > 1)
								}
								onClick={(e: any) => handleSubmit(e)}
								variant="contained"
							>
								Update Password
							</Button>
						</FlexDiv>
					</FlexDiv>
				}
			/>
		</FlexDiv>
	);
}

import * as React from 'react';
import { alpha } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Checkbox from '@mui/material/Checkbox';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import DeleteIcon from '@mui/icons-material/Delete';
import FilterListIcon from '@mui/icons-material/FilterList';
import { visuallyHidden } from '@mui/utils';
import { HeadCell } from '../../core/do';
import Log from '../../../vendor/utils';
import { FlexDiv } from '../../constant';
import { Button, Fade, InputAdornment, Popper, PopperPlacementType, TextField } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import SearchIcon from '@mui/icons-material/Search';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import GeneratePdf from './PdfGenerator';

type Order = 'asc' | 'desc';

function stableSort<T>(array: readonly T[], comparator: (a: T, b: T) => number) {
	const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
	stabilizedThis.sort((a, b) => {
		const order = comparator(a[0], b[0]);
		if (order !== 0) {
			return order;
		}
		return a[1] - b[1];
	});
	return stabilizedThis.map(el => el[0]);
}

function getComparator<Key extends keyof any>(
	order: Order,
	orderBy: Key
): (a: { [key in Key]: number | string }, b: { [key in Key]: number | string }) => number {
	return order === 'desc'
		? (a, b) => descendingComparator(a, b, orderBy)
		: (a, b) => -descendingComparator(a, b, orderBy);
}

interface EnhancedTableToolbarProps {
	numSelected: number;
	heading: string;
	selectedItem: any;
	report?: boolean;
}
function EnhancedTableToolbar(props: EnhancedTableToolbarProps) {
	const { numSelected } = props;

	return (
		<Toolbar
			sx={{
				pl: { sm: 2 },
				pr: { xs: 1, sm: 1 },
				...(numSelected > 0 && {
					bgcolor: theme => alpha(theme.palette.primary.main, theme.palette.action.activatedOpacity),
				}),
			}}
		>
			{numSelected > 0 ? (
				<Typography sx={{ flex: '1 1 100%' }} color="inherit" variant="subtitle1" component="div">
					{numSelected} selected
				</Typography>
			) : (
				<Typography sx={{ flex: '1 1 100%' }} variant="h6" id="tableTitle" component="div">
					{props.heading}
				</Typography>
			)}
			{numSelected > 0 ? (
				<FlexDiv width="full" justify="end" gap="20px">
					<Tooltip title="Delete">
						<IconButton>
							<DeleteIcon />
						</IconButton>
					</Tooltip>
					{props.report && numSelected > 0 && <GeneratePdf data={props.selectedItem} />}
				</FlexDiv>
			) : (
				<Tooltip title="Filter list">
					<IconButton>
						<FilterListIcon />
					</IconButton>
				</Tooltip>
			)}
		</Toolbar>
	);
}

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
	if (b[orderBy] < a[orderBy]) {
		return -1;
	}
	if (b[orderBy] > a[orderBy]) {
		return 1;
	}
	return 0;
}

interface EnhancedTableProps<T> {
	numSelected: number;
	onRequestSort: (event: React.MouseEvent<unknown>, property: keyof T) => void;
	onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
	order: Order;
	orderBy: keyof T;
	rowCount: number;
}

function EnhancedTableHead<T>(props: {
	enhanceTableHead: EnhancedTableProps<T>;
	headCell: HeadCell<T>[];
	showActionHead: boolean;
}) {
	const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props.enhanceTableHead;
	const createSortHandler = (property: keyof T) => (event: React.MouseEvent<unknown>) => {
		onRequestSort(event, property);
	};

	return (
		<TableHead>
			<TableRow>
				<TableCell padding="checkbox">
					<Checkbox
						color="primary"
						indeterminate={numSelected > 0 && numSelected < rowCount}
						checked={rowCount > 0 && numSelected === rowCount}
						onChange={onSelectAllClick}
						inputProps={{
							'aria-label': 'select all desserts',
						}}
					/>
				</TableCell>
				{props.headCell.map(headCell => (
					<TableCell
						key={headCell.label}
						align={headCell.numeric ? 'right' : 'left'}
						padding={headCell.disablePadding ? 'none' : 'normal'}
						sortDirection={orderBy === headCell.id ? order : false}
					>
						<TableSortLabel
							active={orderBy === headCell.id}
							direction={orderBy === headCell.id ? order : 'asc'}
							onClick={createSortHandler(headCell.id)}
						>
							{headCell.label}
							{orderBy === headCell.id ? (
								<Box component="span" sx={visuallyHidden}>
									{order === 'desc' ? 'sorted descending' : 'sorted ascending'}
								</Box>
							) : null}
						</TableSortLabel>
					</TableCell>
				))}
				{props.showActionHead && <TableCell>Action</TableCell>}
			</TableRow>
		</TableHead>
	);
}

export function TableComponent<T>(props: {
	data: T[];
	tableHead: HeadCell<T>[];
	heading: string;
	onEdit?: (e: any) => void;
	onDelete?: (e: any) => void;
	report?: boolean;
	additionalButton?: JSX.Element;
}) {
	const [order, setOrder] = React.useState<Order>('asc');
	const [orderBy, setOrderBy] = React.useState<keyof T>('id' as any);
	const [selected, setSelected] = React.useState<T[]>([]);
	const [page, setPage] = React.useState(0);
	const [rowsPerPage, setRowsPerPage] = React.useState(5);

	const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof T) => {
		const isAsc = orderBy === property && order === 'asc';
		setOrder(isAsc ? 'desc' : 'asc');
		setOrderBy(property);
	};

	const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
		if (event.target.checked) {
			const newSelected = props.data.map((n: any) => n);
			setSelected(newSelected);
			return;
		}
		setSelected([]);
	};

	const handleClick = (event: React.MouseEvent<unknown>, id: T) => {
		const selectedIndex = selected.indexOf(id);
		let newSelected: T[] = [];

		if (selectedIndex === -1) {
			newSelected = newSelected.concat(selected, id);
		} else if (selectedIndex === 0) {
			newSelected = newSelected.concat(selected.slice(1));
		} else if (selectedIndex === selected.length - 1) {
			newSelected = newSelected.concat(selected.slice(0, -1));
		} else if (selectedIndex > 0) {
			newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
		}
		setSelected(newSelected);
	};

	const handleChangePage = (event: unknown, newPage: number) => {
		setPage(newPage);
		setSearchText('');
	};

	const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
		setRowsPerPage(parseInt(event.target.value, 10));
		setPage(0);
	};

	const isSelected = (id: T) => {
		Log.d('isSelected', id);
		return selected.indexOf(id) !== -1;
	};

	// Avoid a layout jump when reaching the last page with empty rows.
	const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - props.data.length) : 0;

	const [searchText, setSearchText] = React.useState<string | undefined>('');

	const visibleRows = React.useMemo(() => {
		if (!!searchText) {
			setPage(0);
			return stableSort(
				props.data.filter((row: any) =>
					props.tableHead
						.map(e => row[e.id as any])
						.join('')
						.toString()
						.toLowerCase()
						.includes(searchText.toLowerCase())
				) as any,
				getComparator(order, orderBy as any)
			).slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);
		} else
			return stableSort(props.data as any, getComparator(order, orderBy as any)).slice(
				page * rowsPerPage,
				page * rowsPerPage + rowsPerPage
			);
	}, [props.data, props.tableHead, order, orderBy, page, rowsPerPage, searchText]);
	React.useEffect(() => {
		if (visibleRows.length < 1 && page > 0) setPage(page - 1);
	}, [page, visibleRows]);

	const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);
	const [open, setOpen] = React.useState(false);
	const [placement, setPlacement] = React.useState<PopperPlacementType>();
	const [actionData, setActionData] = React.useState<any>();

	const handleClickPopper =
		(newPlacement: PopperPlacementType, data: any) => (event: React.MouseEvent<HTMLButtonElement>) => {
			setAnchorEl(event.currentTarget);
			setOpen(prev => placement !== newPlacement || !prev);
			setPlacement(newPlacement);
			setActionData(data);
		};

	return (
		<Box sx={{ width: '100%' }}>
			<Popper sx={{ zIndex: 1200 }} open={open} anchorEl={anchorEl} placement={placement} transition>
				{({ TransitionProps }) => (
					<Fade {...TransitionProps} timeout={350}>
						<Paper>
							<FlexDiv gap="20px" direction="column" style={{ padding: '12px' }}>
								<FlexDiv gap="12px">
									<DeleteIcon
										sx={{
											color: '#d32f2f',
											cursor: 'pointer',
										}}
										onClick={() => {
											props.onDelete?.(actionData);
											setOpen(false);
										}}
									/>
									<span>Delete</span>
								</FlexDiv>
								<FlexDiv
									className="edit-action-pop"
									gap="12px"
									onClick={(e: any) => {
										props.onEdit?.(actionData);
										setOpen(false);
									}}
								>
									<EditIcon
										sx={{
											color: '#1976d2',
											cursor: 'pointer',
										}}
									/>
									<span>Edit</span>
								</FlexDiv>
							</FlexDiv>
						</Paper>
					</Fade>
				)}
			</Popper>
			<FlexDiv justify="space-between" style={{ marginBottom: '12px' }}>
				<TextField
					value={searchText}
					onChange={e => setSearchText(e.target.value)}
					label="Search and filter"
					variant="standard"
					InputProps={{
						endAdornment: (
							<InputAdornment position="start">
								<SearchIcon />
							</InputAdornment>
						),
					}}
				/>
				<FlexDiv style={{ marginTop: '12px' }}>{props.additionalButton}</FlexDiv>
			</FlexDiv>
			{props.data.length >= 1 ? (
				<Paper sx={{ width: '100%', mb: 2 }}>
					<EnhancedTableToolbar
						numSelected={selected.length}
						selectedItem={selected}
						heading={props.heading}
						report={props.report}
					/>
					<TableContainer>
						<Table sx={{ minWidth: 750 }} aria-labelledby="tableTitle">
							<EnhancedTableHead
								showActionHead={!!props.onDelete || !!props.onEdit}
								headCell={props.tableHead}
								enhanceTableHead={{
									numSelected: selected.length,
									order: order,
									orderBy: orderBy,
									onSelectAllClick: handleSelectAllClick,
									onRequestSort: handleRequestSort,
									rowCount: props.data.length,
								}}
							/>
							<TableBody>
								{visibleRows.map((row, index) => {
									const isItemSelected = isSelected(row as any);
									const labelId = `enhanced-table-checkbox-${index}`;

									return (
										<TableRow
											aria-checked={isItemSelected}
											tabIndex={-1}
											key={index}
											selected={isItemSelected}
										>
											<TableCell padding="checkbox">
												<Checkbox
													onClick={event => handleClick(event, row as any)}
													color="primary"
													checked={isItemSelected}
													inputProps={{
														'aria-labelledby': labelId,
													}}
												/>
											</TableCell>
											{props.tableHead.map((e, index) => (
												<TableCell
													style={{
														color:
															row[e.id as any] === 'reject'
																? 'red'
																: row[e.id as any] === 'pending'
																? 'red'
																: row[e.id as any] === 'approved'
																? 'green'
																: '',
													}}
													align="left"
													key={index}
												>
													{row[e.id as any]}
												</TableCell>
											))}
											{!!props.onEdit && !!props.onDelete && (
												<TableCell align="left">
													<Button
														className="action-pop-up"
														onClick={handleClickPopper('bottom-end', row)}
													>
														<MoreVertIcon />
													</Button>
												</TableCell>
											)}
										</TableRow>
									);
								})}
								{emptyRows > 0 && (
									<TableRow
										style={{
											height: 53 * emptyRows,
										}}
									>
										<TableCell colSpan={6} />
									</TableRow>
								)}
							</TableBody>
						</Table>
					</TableContainer>
					<TablePagination
						rowsPerPageOptions={[5, 10, 25]}
						component="div"
						count={props.data.length}
						rowsPerPage={rowsPerPage}
						page={page}
						onPageChange={handleChangePage}
						onRowsPerPageChange={handleChangeRowsPerPage}
					/>
				</Paper>
			) : (
				<FlexDiv justify="center" align="center" padding="20px">
					<h2>No Data Found</h2>
				</FlexDiv>
			)}
		</Box>
	);
}

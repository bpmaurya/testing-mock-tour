import styled from '@emotion/styled';

export const ModalStyle = {
	position: 'absolute' as 'absolute',
	top: '50%',
	left: '50%',
	transform: 'translate(-50%, -50%)',
	width: 400,
	bgcolor: 'background.paper',
	border: 'none',
	borderRadius: '12px',
	boxShadow: 30,
	outline: 'none',
	p: 4,
	'@media screen and (max-width: 450px)': {
		padding: '5px',
	},
};

export const Row = styled.div`
	&::after {
		content: '';
		clear: both;
		display: table;
	}
`;

type ColumnProps = {
	span?: string;
};

export const Column = styled.div`
	float: left;
	width: 100%;

	@media only screen and (min-width: 768px) {
		width: ${(props: ColumnProps) => (props.span ? (parseInt(props.span) / 12) * 100 : '8.33')}%;
	}
`;

import { Navigate } from 'react-router-dom';
import { selectAuth } from '../../states/store/feature/auth';
import { useAppDispatch, useAppSelector } from '../../states/store/hooks';
import { history } from '../../helpers/history';
import { SideNavbar } from '../SideNavbar';
import { useEffect, useState } from 'react';
import { reset } from '../../states/store/feature/user.slice';
import { resetLocationState } from '../../states/store/feature/location.slice';
import { UpdatePasswordPage } from './UpdatePassword';

const PrivateRoute = ({ children }: { children: JSX.Element }) => {
	const { isLoggedIn } = useAppSelector(selectAuth);
	if (!isLoggedIn) {
		return <Navigate to="/" state={{ from: history.location }} />;
	}
	return children;
};

function ProtectedRoute(props: { comp: JSX.Element }): JSX.Element {
	const currentMenu = window.location.pathname;
	const name = currentMenu.split('/').pop();
	const dispatch = useAppDispatch();
	const [isNeedToChangePass, setIsNeedToChangePass] = useState<any>();
	useEffect(() => {
		dispatch(reset());
		dispatch(resetLocationState());
		setIsNeedToChangePass(
			!!(localStorage.getItem('user')
				? localStorage.getItem('user')!.includes('jwt')
				: '' || localStorage.getItem('jwt'))
		);
	}, [dispatch, currentMenu]);
	return (
		<>
			{!!isNeedToChangePass ? (
				<UpdatePasswordPage />
			) : (
				<PrivateRoute>
					<SideNavbar currentMenu={name}>{props.comp}</SideNavbar>
				</PrivateRoute>
			)}
		</>
	);
}
export default ProtectedRoute;

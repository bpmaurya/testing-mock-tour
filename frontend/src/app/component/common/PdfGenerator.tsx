import { saveAs } from 'file-saver';
import { Page, Text, View, Document, StyleSheet, pdf } from '@react-pdf/renderer';
import { Button } from '@mui/material';
import moment from 'moment';
import { DeskRequest } from 'MyModels';

const styles = StyleSheet.create({
	table: {
		margin: '4',
		width: 'auto',
		borderStyle: 'solid',
		borderWidth: 1,
		borderRightWidth: 0,
		borderBottomWidth: 0,
		borderCollapse: 'collapse',
		// @ts-ignore
		display: 'table',
	},
	tableRow: {
		margin: 'auto',
		flexDirection: 'row',
	},
	tableCol: {
		width: '16.65%',
		borderStyle: 'solid',
		borderWidth: 1,
		borderLeftWidth: 0,
		borderTopWidth: 0,
		// @ts-ignore
		borderCollapse: 'collapse',
	},
	tableCell: {
		margin: 'auto',
		marginTop: 5,
		fontSize: 10,
	},
	status_average: {
		fontSize: 8,
		marginLeft: 4,
		fontWeight: 'bold',
	},
});
async function savePDF(data: any[]) {
	try {
		const doc = (
			<Document
				title="Request status report"
				author="volansys Tech"
				producer="volansys Tech"
				creator="volansys Tech"
				subject="request status"
				keywords="volansys seat sync"
			>
				<Page style={styles.body}>
					<View style={styles.table}>
						<View style={styles.tableRow}>
							<View style={styles.tableCol}>
								<Text style={styles.tableCell}>Name</Text>
							</View>
							<View style={styles.tableCol}>
								<Text style={styles.tableCell}>Email</Text>
							</View>
							<View style={styles.tableCol}>
								<Text style={styles.tableCell}>Project</Text>
							</View>
							<View style={styles.tableCol}>
								<Text style={styles.tableCell}>Seat</Text>
							</View>
							<View style={styles.tableCol}>
								<Text style={styles.tableCell}>Status</Text>
							</View>
							<View style={styles.tableCol}>
								<Text style={styles.tableCell}>CreatedAt</Text>
							</View>
						</View>

						{data
							? data.map((a: DeskRequest.Type, index) => {
									return (
										<View wrap={false} key={index} style={styles.tableRow}>
											<View style={styles.tableCol}>
												<Text style={styles.tableCell}>
													{a.user.firstName + ' ' + a.user.middleName + ' ' + a.user.lastName}
												</Text>
											</View>
											<View style={styles.tableCol}>
												<Text style={styles.tableCell}>{a.user.email}</Text>
											</View>
											<View style={styles.tableCol}>
												<Text style={styles.tableCell}>{a.project.name}</Text>
											</View>
											<View style={styles.tableCol}>
												<Text style={styles.tableCell}>
													{a.seat.location.name +
														' ' +
														a.seat.floor.name +
														' ' +
														a.seat.wing.name +
														' ' +
														a.seat.name}
												</Text>
											</View>
											<View
												style={{
													...styles.tableCol,
													backgroundColor: a.status === 'approved' ? '#61C74F' : 'red',
												}}
											>
												<Text style={styles.tableCell}>{a.status}</Text>
											</View>
											<View style={styles.tableCol}>
												<Text style={styles.tableCell}>
													{moment(a.createdAt, 'YYYY-MM-DD').format(' MMMM D Y')}
												</Text>
											</View>
										</View>
									);
							  })
							: ''}
					</View>
					<Text style={styles.status_average}>Volansys Technologies an ACL company @ 2024</Text>
				</Page>
			</Document>
		);

		const asPdf = pdf();
		asPdf.updateContainer(doc);
		const pdfBlob = await asPdf.toBlob();
		saveAs(pdfBlob, 'seat_sync_document.pdf');
	} catch (error) {
		console.error(error);
		alert('Error generating PDF');
	}
}

function GeneratePdf(props: { data: any[] }) {
	return (
		<div>
			<Button onClick={() => savePDF(props.data)}>Download</Button>
		</div>
	);
}

export default GeneratePdf;

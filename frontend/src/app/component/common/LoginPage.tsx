import { FlexDiv } from '../../constant';
import { Alert, Button } from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../states/store/hooks';
import { useState } from 'react';
import { loginAsync, selectAuth } from '../../states/store/feature/auth';
import * as React from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import { Textfield } from '../common-component/Textfield';
import Log from '../../../vendor/utils';
import { UserCredentials } from 'MyModels';
import { ModalStyle } from './styles';
import { EmailRegex } from '../../../vendor/constant';

const TAG = 'inside-login-page';

export function BasicModal(props: { children: JSX.Element }) {
	return (
		<div>
			<Modal open={true} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
				<Box sx={ModalStyle}>
					<FlexDiv direction="column" justify="center" gap="40px" alignItems="center">
						<h1>Login</h1>
						{props.children}
					</FlexDiv>
				</Box>
			</Modal>
		</div>
	);
}

export function LoginPage() {
	const dispatch = useAppDispatch();
	const navigate = useNavigate();
	const [loading, setLoading] = useState(false);
	const { error } = useAppSelector(selectAuth);
	const [formData, setFormData] = useState<UserCredentials>({
		email: '',
		password: '',
	});

	const handleSubmit = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
		event.preventDefault();
		setLoading(true);
		dispatch(loginAsync(formData))
			.unwrap()
			.then((res: any) => {
				setLoading(false);
				if (res.jwt) {
					localStorage.setItem('jwt', res.jwt);
					localStorage.setItem('_id', res._id);
				}
				navigate('/dashboard');
			})
			.catch(() => {
				setLoading(false);
				setShow(true);
			});
	};

	const [errors, setError] = useState<Partial<UserCredentials>>({});

	const validateForm = (name: any, value: any) => {
		switch (name) {
			case 'email':
				if (!value || value.trim() === '') {
					return 'Email is required!';
				} else if (!EmailRegex.test(value)) {
					return 'Email is invalid!';
				} else {
					return '';
				}
			case 'password':
				if (!value || value.trim() === '') {
					return 'Password is required!!';
				} else return '';
			default:
				break;
		}
	};

	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const { name, value } = e.target;

		setError(preData => ({
			...preData,
			[e.target.name]: validateForm(name, value),
		}));
		setFormData((prevData: any) => ({
			...prevData,
			[name]: value,
		}));
		Log.d(TAG, formData);
	};

	const onFocus = () => {};

	const [show, setShow] = useState<boolean>(false);
	React.useEffect(() => {
		const timeId = setTimeout(() => {
			setShow(false);
		}, 3000);

		return () => {
			clearTimeout(timeId);
		};
	}, [show]);

	return (
		<FlexDiv className="login_main_comp">
			<BasicModal
				children={
					<FlexDiv justify="center" gap="20px" align="center" alignItems="center">
						<FlexDiv
							direction="column"
							gap="20px"
							style={{ maxWidth: '300px' }}
							justify="center"
							alignItems="center"
						>
							{!!show ? <Alert severity="error">{error}</Alert> : <></>}
							<Textfield
								autoFocus
								shrink
								name="email"
								id="username_login_form"
								label="Email"
								type="email"
								onFocus={onFocus}
								maxLength={50}
								value={formData.email}
								onChange={handleChange}
								error={!!errors.email}
								errorText={!!errors.email ? errors.email : ''}
							/>
							<Textfield
								shrink
								name="password"
								id="password_login_form"
								label="Password"
								type="password"
								value={formData.password}
								onChange={handleChange}
								error={!!errors.password}
								errorText={!!errors.password ? errors.password : ''}
								maxLength={50}
							/>

							<Button
								disabled={!!errors.email || !!errors.password}
								onClick={(e: any) => handleSubmit(e)}
								variant="contained"
							>
								Login
							</Button>
							<Link style={{ textDecoration: 'none' }} to="/forget-password">
								Forget Password
							</Link>
						</FlexDiv>
					</FlexDiv>
				}
			/>
		</FlexDiv>
	);
}

import { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../states/store/hooks';
import Log from '../../../vendor/utils';
import tokenService from '../../services/token.service';
import { FlexDiv } from '../../constant';
import { Alert, Button, CircularProgress } from '@mui/material';
import { TableComponent } from '../common/TableComponent';
import { Designation } from 'MyModels';
import { HeadCell } from '../../core/do';
import { ModalComponent } from '../common-component/ModalComponent';
import { toast, ToastContainer } from 'react-toastify';
import { LoadingButton } from '../common-component/LoaderComponent';
import {
	createDesignationAsync,
	deleteDesignationAsync,
	getAllDesignationAsync,
	getDesignation,
	resetDesignationState,
	updateDesignationAsync,
} from '../../states/store/feature/designation.slice';
import { DesignationForm } from '../forms/DesignationForm';

const TAG = 'inside-designation-component';

const headCells: HeadCell<Designation.Type>[] = [
	{
		id: 'name',
		numeric: false,
		disablePadding: false,
		label: 'Name',
	},
	{
		id: 'status',
		numeric: false,
		disablePadding: false,
		label: 'Status',
	},
];

export const DesignationComp = () => {
	const dispatch = useAppDispatch();
	const [openAction, setOpenAction] = useState<boolean>(false);
	const [openEditAction, setOpenEditAction] = useState<Designation.Type | null>();
	const [openDeleteAction, setOpenDeleteAction] = useState<Designation.Type | undefined>();
	const [isAdmin, setIsAdmin] = useState<boolean>(false);
	const [allDesignation, setAllDesignation] = useState<any>();

	const { isDesignationAdded, isDesignationCreated, isDesignationDeleted, isDesignationUpdated, error } =
		useAppSelector(getDesignation);

	const onCreateDesignation = async (data: Designation.Type): Promise<any> => {
		return dispatch(createDesignationAsync(data))
			.unwrap()
			.then(res => {
				Log.d(TAG, res);
				setOpenAction(false);
				return toast.success('Designation added!!');
			})
			.catch(error => {
				Log.d(TAG, error);
				setShow(true);
			});
	};

	const onDeleteDesignation = async (data: Designation.Type): Promise<any> => {
		return dispatch(deleteDesignationAsync(data))
			.unwrap()
			.then(e => {
				setOpenAction(false);
				return toast.success('Designation deleted!!');
			})
			.catch(error => {
				Log.d(error);
			});
	};

	const onEditDesignation = async (data: Designation.Type): Promise<any> => {
		return dispatch(updateDesignationAsync(data))
			.unwrap()
			.then(e => {
				setOpenEditAction(null);
				return toast.success('Designation Updated!!');
			})
			.catch(error => {
				Log.d(error);
			});
	};

	useEffect(() => {
		setIsAdmin(tokenService.isAdmin());
		if (isDesignationAdded) setOpenAction(false);
		if (isDesignationDeleted) setOpenDeleteAction(undefined);
		if (isDesignationUpdated) setOpenEditAction(null);
		dispatch(resetDesignationState());
		dispatch(getAllDesignationAsync())
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res);
				setAllDesignation(res.allDesignation);
			})
			.catch(error => {
				Log.d(TAG, error);
			});
	}, [dispatch, isDesignationAdded, isDesignationDeleted, isDesignationUpdated]);

	const [Show, setShow] = useState<boolean>(false);
	useEffect(() => {
		const timeId1 = setTimeout(() => {
			setShow(false);
		}, 3000);

		return () => {
			clearTimeout(timeId1);
		};
	}, [Show]);

	return (
		<>
			{!!openDeleteAction && (
				<ModalComponent
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center">
								<LoadingButton onClick={() => onDeleteDesignation(openDeleteAction)}>
									<Button variant="contained" color="error">
										Delete
									</Button>
								</LoadingButton>
								<Button variant="contained" onClick={() => setOpenDeleteAction(undefined)}>
									Cancel
								</Button>
							</FlexDiv>
						</>
					}
					heading={`Delete This Designation? ${openDeleteAction.name}`}
					open={!!openDeleteAction}
				/>
			)}
			{openEditAction ? (
				<ModalComponent
					size="med"
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{!!Show && error.length > 1 ? (
									<Alert style={{ maxWidth: '400px' }} severity="error">
										{error}
									</Alert>
								) : (
									<></>
								)}
								<DesignationForm
									initialData={openEditAction}
									handleCancel={() => setOpenEditAction(null)}
									onSubmit={e => onEditDesignation(e)}
									onEdit
								/>
							</FlexDiv>
						</>
					}
					heading={`Edit Designation `}
					open={!!openEditAction}
				/>
			) : (
				<></>
			)}
			{openAction ? (
				<ModalComponent
					size="med"
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{!!Show && error.length > 1 ? (
									<Alert style={{ maxWidth: '400px' }} severity="error">
										{error}
									</Alert>
								) : (
									<></>
								)}
								<DesignationForm
									handleCancel={() => setOpenAction(false)}
									onSubmit={e => onCreateDesignation(e)}
								/>
							</FlexDiv>
						</>
					}
					heading={`Add Designation`}
					open={openAction}
				/>
			) : (
				<></>
			)}
			<FlexDiv>
				{!!allDesignation ? (
					<TableComponent
						data={allDesignation}
						tableHead={headCells}
						heading="Designation"
						onEdit={e => setOpenEditAction(e)}
						onDelete={e => setOpenDeleteAction(e)}
						additionalButton={
							isAdmin ? (
								<Button variant="contained" onClick={() => setOpenAction(true)}>
									Add Designation
								</Button>
							) : (
								<></>
							)
						}
					/>
				) : (
					<FlexDiv justify="center" align="center">
						<CircularProgress />
					</FlexDiv>
				)}
			</FlexDiv>
			<ToastContainer closeOnClick={true} />
		</>
	);
};

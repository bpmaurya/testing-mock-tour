import { Floor } from 'MyModels';
import { useEffect, useState } from 'react';
import { TableComponent } from '../common/TableComponent';
import { HeadCell } from '../../core/do';
import Button from '@mui/material/Button';
import { FloorForm } from '../forms/FloorForm';
import Alert from '@mui/material/Alert';
import { ModalComponent } from '../common-component/ModalComponent';
import { FlexDiv } from '../../constant';
import tokenService from '../../services/token.service';
import { LoadingButton } from '../common-component/LoaderComponent';
import {
	createFloorsAsync,
	deleteFloorAsync,
	getAllFloorAsync,
	getFloors,
	resetFloorState,
	updateFloorAsync,
} from '../../states/store/feature/floor.slice';
import Log from '../../../vendor/utils';
import { toast, ToastContainer } from 'react-toastify';
import { useAppDispatch, useAppSelector } from '../../states/store/hooks';

const TAG = 'inside-floor-component';

const headCells: HeadCell<ExtendedTypeForTable>[] = [
	{
		id: 'name',
		numeric: false,
		disablePadding: false,
		label: 'Name',
	},
	{
		id: 'location_name',
		numeric: false,
		disablePadding: false,
		label: 'Location',
	},
	{
		id: 'status',
		numeric: false,
		disablePadding: false,
		label: 'Status',
	},
];

type ExtendedTypeForTable = Floor.Type & {
	location_name: string;
};

export function FloorComponent() {
	const dispatch = useAppDispatch();
	const [floors, setFloors] = useState<ExtendedTypeForTable[]>([] as any);
	const [isAdmin, setIsAdmin] = useState<boolean>(false);
	const [open, setOpen] = useState<boolean>(false);
	const [openForEdit, setOpenForEdit] = useState<Floor.Type | undefined>();
	const [openForDelete, setOpenForDelete] = useState<Floor.Type | undefined>();
	const [show, setShow] = useState<boolean>(false);

	const { error, isFloorAdded, isFloorDeleted, isFloorUpdated } = useAppSelector(getFloors);

	const onCreateFloor = async (data: Floor.Type): Promise<any> => {
		dispatch(createFloorsAsync(data))
			.unwrap()
			.then(res => {
				Log.d(TAG, res);
				setOpen(false);
				return toast.success('Floor added!!');
			})
			.catch(error => {
				Log.d(TAG, error);
				setShow(true);
			});
	};

	const onDeleteFloor = async (data: Floor.Type): Promise<any> => {
		return dispatch(deleteFloorAsync(data))
			.unwrap()
			.then(res => {
				setOpenForDelete(undefined);
				return toast.success('Floor Deleted!');
			})
			.catch(err => {
				Log.d(TAG, err);
				setShow(true);
			});
	};

	const onUpdateFloor = async (data: Floor.Type): Promise<any> => {
		return dispatch(updateFloorAsync(data))
			.unwrap()
			.then(res => {
				setOpenForEdit(undefined);
				return toast.success('Floor Updated!');
			})
			.catch(err => {
				Log.d(TAG, err);
				setShow(true);
			});
	};

	useEffect(() => {
		const user = tokenService.getUser();
		if (!!user && user.role === 'admin') setIsAdmin(true);
	}, []);
	useEffect(() => {
		if (isFloorAdded) setOpen(false);
		if (isFloorDeleted) setOpenForDelete(undefined);
		if (isFloorUpdated) setOpenForEdit(undefined);
		dispatch(resetFloorState());
		dispatch(getAllFloorAsync())
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res);
				let data = res.allFloor;
				let allFloor: any = [];
				data.map((e: any) => {
					return allFloor.push({ ...e, location_name: e.location.name });
				});
				setFloors(allFloor);
			})
			.catch(error => {
				Log.d(TAG, error);
			});
	}, [dispatch, isFloorAdded, isFloorDeleted, isFloorUpdated]);

	useEffect(() => {
		const timeId1 = setTimeout(() => {
			setShow(false);
		}, 3000);

		return () => {
			clearTimeout(timeId1);
		};
	}, [show]);

	return (
		<>
			{openForEdit ? (
				<ModalComponent
					size="med"
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{!!show && error.length > 1 ? (
									<Alert style={{ maxWidth: '400px' }} severity="error">
										{error}
									</Alert>
								) : (
									<></>
								)}
								<FloorForm
									initialData={openForEdit}
									handleCancel={() => setOpenForEdit(undefined)}
									onSubmit={e => onUpdateFloor(e)}
									onEdit
								/>
							</FlexDiv>
						</>
					}
					heading={`Edit Floor `}
					open={!!openForEdit}
				/>
			) : (
				<></>
			)}
			{!!openForDelete && (
				<ModalComponent
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center">
								<LoadingButton onClick={() => new Promise(() => onDeleteFloor(openForDelete))}>
									<Button variant="contained" color="error">
										Delete
									</Button>
								</LoadingButton>
								<Button variant="contained" onClick={() => setOpenForDelete(undefined)}>
									Cancel
								</Button>
							</FlexDiv>
						</>
					}
					heading={`Delete This Floor? ${openForDelete.name}`}
					open={!!openForDelete}
				/>
			)}
			{open ? (
				<ModalComponent
					size="med"
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{!!show && error.length > 1 ? (
									<Alert style={{ maxWidth: '400px' }} severity="error">
										{error}
									</Alert>
								) : (
									<></>
								)}
								<FloorForm handleCancel={() => setOpen(false)} onSubmit={e => onCreateFloor(e)} />
							</FlexDiv>
						</>
					}
					heading={`Add Floor `}
					open={open}
				/>
			) : (
				<></>
			)}
			<TableComponent
				data={floors as any}
				tableHead={headCells}
				heading="Floors"
				onEdit={e => {
					setOpenForEdit(e);
					dispatch(resetFloorState());
				}}
				onDelete={e => setOpenForDelete(e)}
				additionalButton={
					isAdmin ? (
						<Button
							variant="contained"
							onClick={() => {
								setOpen(true);
								dispatch(resetFloorState());
							}}
						>
							Add Floor
						</Button>
					) : (
						<></>
					)
				}
			/>
			<ToastContainer closeOnClick={true} />
		</>
	);
}

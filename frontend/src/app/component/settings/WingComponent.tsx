import { Wing } from 'MyModels';
import { useEffect, useState } from 'react';
import { TableComponent } from '../common/TableComponent';
import { HeadCell } from '../../core/do';
import Button from '@mui/material/Button';
import Alert from '@mui/material/Alert';
import { ModalComponent } from '../common-component/ModalComponent';
import { FlexDiv } from '../../constant';
import tokenService from '../../services/token.service';
import { LoadingButton } from '../common-component/LoaderComponent';
import { resetFloorState } from '../../states/store/feature/floor.slice';
import Log from '../../../vendor/utils';
import { toast, ToastContainer } from 'react-toastify';
import { useAppDispatch, useAppSelector } from '../../states/store/hooks';
import {
	createWingAsync,
	deleteWingAsync,
	getAllWingAsync,
	getWings,
	resetWingState,
	updateWingAsync,
} from '../../states/store/feature/wing.slice';
import { WingForm } from '../forms/WingForm';

const TAG = 'inside-floor-component';

const headCells: HeadCell<ExtendedTypeForTable>[] = [
	{
		id: 'name',
		numeric: false,
		disablePadding: false,
		label: 'Name',
	},
	{
		id: 'location_name',
		numeric: false,
		disablePadding: false,
		label: 'Location',
	},
	{
		id: 'floor_name',
		numeric: false,
		disablePadding: false,
		label: 'Floor',
	},
	{
		id: 'status',
		numeric: false,
		disablePadding: false,
		label: 'Status',
	},
];

type ExtendedTypeForTable = Wing.Type & {
	location_name: string;
	floor_name: string;
};

export function WingComponent() {
	const dispatch = useAppDispatch();
	const [wings, setWings] = useState<ExtendedTypeForTable[]>([] as any);
	const [isAdmin, setIsAdmin] = useState<boolean>(false);
	const [open, setOpen] = useState<boolean>(false);
	const [openForEdit, setOpenForEdit] = useState<Wing.Type | undefined>();
	const [openForDelete, setOpenForDelete] = useState<Wing.Type | undefined>();
	const [show, setShow] = useState<boolean>(false);

	const { error, isWingAdded, isWingDeleted, isWingUpdated } = useAppSelector(getWings);

	const onCreateWing = async (data: Wing.Type): Promise<any> => {
		dispatch(createWingAsync(data))
			.unwrap()
			.then(res => {
				Log.d(TAG, res);
				setOpen(false);
				return toast.success('Wing added!!');
			})
			.catch(error => {
				Log.d(TAG, error);
				setShow(true);
			});
	};

	const onDeleteWing = async (data: Wing.Type): Promise<any> => {
		return dispatch(deleteWingAsync(data))
			.unwrap()
			.then(res => {
				setOpenForDelete(undefined);
				return toast.success('Wing Deleted!');
			})
			.catch(err => {
				Log.d(TAG, err);
				setShow(true);
			});
	};

	const onUpdateWing = async (data: Wing.Type): Promise<any> => {
		return dispatch(updateWingAsync(data))
			.unwrap()
			.then(res => {
				setOpenForEdit(undefined);
				return toast.success('Wing Updated!');
			})
			.catch(err => {
				Log.d(TAG, err);
				setShow(true);
			});
	};

	useEffect(() => {
		const user = tokenService.getUser();
		if (!!user && user.role === 'admin') setIsAdmin(true);
	}, []);
	useEffect(() => {
		if (isWingAdded) setOpen(false);
		if (isWingDeleted) setOpenForDelete(undefined);
		if (isWingUpdated) setOpenForEdit(undefined);
		dispatch(resetWingState());
		dispatch(getAllWingAsync())
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res);
				let data = res.allWing;
				let all_wing: any = [];
				data.map((e: any) => {
					return all_wing.push({ ...e, location_name: e.location.name, floor_name: e.floor.name });
				});
				setWings(all_wing);
			})
			.catch(error => {
				Log.d(TAG, error);
			});
	}, [dispatch, isWingAdded, isWingDeleted, isWingUpdated]);

	useEffect(() => {
		const timeId1 = setTimeout(() => {
			setShow(false);
		}, 3000);

		return () => {
			clearTimeout(timeId1);
		};
	}, [show]);

	return (
		<>
			{openForEdit ? (
				<ModalComponent
					size="med"
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{!!show && error.length > 1 ? (
									<Alert style={{ maxWidth: '400px' }} severity="error">
										{error}
									</Alert>
								) : (
									<></>
								)}
								<WingForm
									initialData={openForEdit}
									handleCancel={() => setOpenForEdit(undefined)}
									onSubmit={e => onUpdateWing(e)}
									onEdit
								/>
							</FlexDiv>
						</>
					}
					heading={`Edit Wing `}
					open={!!openForEdit}
				/>
			) : (
				<></>
			)}
			{!!openForDelete && (
				<ModalComponent
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center">
								<LoadingButton onClick={() => new Promise(() => onDeleteWing(openForDelete))}>
									<Button variant="contained" color="error">
										Delete
									</Button>
								</LoadingButton>
								<Button variant="contained" onClick={() => setOpenForDelete(undefined)}>
									Cancel
								</Button>
							</FlexDiv>
						</>
					}
					heading={`Delete This Wing? ${openForDelete.name}`}
					open={!!openForDelete}
				/>
			)}
			{open ? (
				<ModalComponent
					size="med"
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{!!show && error.length > 1 ? (
									<Alert style={{ maxWidth: '400px' }} severity="error">
										{error}
									</Alert>
								) : (
									<></>
								)}
								<WingForm handleCancel={() => setOpen(false)} onSubmit={e => onCreateWing(e)} />
							</FlexDiv>
						</>
					}
					heading={`Add Wing `}
					open={open}
				/>
			) : (
				<></>
			)}
			<TableComponent
				data={wings}
				tableHead={headCells}
				heading="Wings"
				onEdit={e => {
					setOpenForEdit(e);
					dispatch(resetFloorState());
				}}
				onDelete={e => setOpenForDelete(e)}
				additionalButton={
					isAdmin ? (
						<Button
							variant="contained"
							onClick={() => {
								setOpen(true);
								dispatch(resetFloorState());
							}}
						>
							Add Wing
						</Button>
					) : (
						<></>
					)
				}
			/>
			<ToastContainer closeOnClick={true} />
		</>
	);
}

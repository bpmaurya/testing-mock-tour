import { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../states/store/hooks';
import Log from '../../../vendor/utils';
import tokenService from '../../services/token.service';
import { FlexDiv } from '../../constant';
import { Alert, Button, CircularProgress } from '@mui/material';
import { TableComponent } from '../common/TableComponent';
import { Project } from 'MyModels';
import { HeadCell } from '../../core/do';
import { ModalComponent } from '../common-component/ModalComponent';
import { toast, ToastContainer } from 'react-toastify';
import { LoadingButton } from '../common-component/LoaderComponent';
import {
	createProjectAsync,
	deleteProjectAsync,
	getAllProjectAsync,
	getProjects,
	resetProjectState,
	updateProjectAsync,
} from '../../states/store/feature/project.slice';
import { ProjectForm } from '../forms/ProjectForm';

const TAG = 'inside-project-component';

const headCells: HeadCell<Project.Type>[] = [
	{
		id: 'name',
		numeric: false,
		disablePadding: false,
		label: 'Name',
	},
	{
		id: 'status',
		numeric: false,
		disablePadding: false,
		label: 'Status',
	},
];

export const ProjectsComponent = () => {
	const dispatch = useAppDispatch();
	const [openAction, setOpenAction] = useState<boolean>(false);
	const [openEditAction, setOpenEditAction] = useState<Project.Type | null>();
	const [openDeleteAction, setOpenDeleteAction] = useState<Project.Type | undefined>();
	const [isAdmin, setIsAdmin] = useState<boolean>(false);
	const [allProject, setAllProject] = useState<any>();

	const { isProjectAdded, isProjectCreated, isProjectDeleted, isProjectUpdated, error } = useAppSelector(getProjects);

	const onCreateProject = async (data: Project.Type): Promise<any> => {
		return dispatch(createProjectAsync(data))
			.unwrap()
			.then(res => {
				Log.d(TAG, res);
				setOpenAction(false);
				return toast.success('Project added!!');
			})
			.catch(error => {
				Log.d(TAG, error);
				setShow(true);
			});
	};

	const onDeleteProject = async (data: Project.Type): Promise<any> => {
		return dispatch(deleteProjectAsync(data))
			.unwrap()
			.then(e => {
				setOpenAction(false);
				return toast.success('Project deleted!!');
			})
			.catch(error => {
				Log.d(error);
			});
	};

	const onEditProject = async (data: Project.Type): Promise<any> => {
		return dispatch(updateProjectAsync(data))
			.unwrap()
			.then(e => {
				setOpenEditAction(null);
				return toast.success('Project Updated!!');
			})
			.catch(error => {
				Log.d(error);
			});
	};

	useEffect(() => {
		setIsAdmin(tokenService.isAdmin());
		if (isProjectAdded) setOpenAction(false);
		if (isProjectDeleted) setOpenDeleteAction(undefined);
		if (isProjectUpdated) setOpenEditAction(null);
		dispatch(resetProjectState());
		dispatch(getAllProjectAsync())
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res);
				setAllProject(res.allProject);
			})
			.catch(error => {
				Log.d(TAG, error);
			});
	}, [dispatch, isProjectAdded, isProjectDeleted, isProjectUpdated]);

	const [Show, setShow] = useState<boolean>(false);
	useEffect(() => {
		const timeId1 = setTimeout(() => {
			setShow(false);
		}, 3000);

		return () => {
			clearTimeout(timeId1);
		};
	}, [Show]);

	return (
		<>
			{!!openDeleteAction && (
				<ModalComponent
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center">
								<LoadingButton onClick={() => onDeleteProject(openDeleteAction)}>
									<Button variant="contained" color="error">
										Delete
									</Button>
								</LoadingButton>
								<Button variant="contained" onClick={() => setOpenDeleteAction(undefined)}>
									Cancel
								</Button>
							</FlexDiv>
						</>
					}
					heading={`Delete This Project? ${openDeleteAction.name}`}
					open={!!openDeleteAction}
				/>
			)}
			{openEditAction ? (
				<ModalComponent
					size="med"
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{!!Show && error.length > 1 ? (
									<Alert style={{ maxWidth: '400px' }} severity="error">
										{error}
									</Alert>
								) : (
									<></>
								)}
								<ProjectForm
									initialData={openEditAction}
									handleCancel={() => setOpenEditAction(null)}
									onSubmit={e => onEditProject(e)}
									onEdit
								/>
							</FlexDiv>
						</>
					}
					heading={`Edit Project `}
					open={!!openEditAction}
				/>
			) : (
				<></>
			)}
			{openAction ? (
				<ModalComponent
					size="med"
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{!!Show && error.length > 1 ? (
									<Alert style={{ maxWidth: '400px' }} severity="error">
										{error}
									</Alert>
								) : (
									<></>
								)}
								<ProjectForm
									handleCancel={() => setOpenAction(false)}
									onSubmit={e => onCreateProject(e)}
								/>
							</FlexDiv>
						</>
					}
					heading={`Add Project `}
					open={openAction}
				/>
			) : (
				<></>
			)}
			<FlexDiv>
				{!!allProject ? (
					<TableComponent
						data={allProject}
						tableHead={headCells}
						heading="Project"
						onEdit={e => setOpenEditAction(e)}
						onDelete={e => setOpenDeleteAction(e)}
						additionalButton={
							isAdmin ? (
								<Button variant="contained" onClick={() => setOpenAction(true)}>
									Add Project
								</Button>
							) : (
								<></>
							)
						}
					/>
				) : (
					<FlexDiv justify="center" align="center">
						<CircularProgress />
					</FlexDiv>
				)}
			</FlexDiv>
			<ToastContainer closeOnClick={true} />
		</>
	);
};

import * as React from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { LocationComponent } from './LocationComponent';
import { FloorComponent } from './FloorComponent';
import { WingComponent } from './WingComponent';
import PublicIcon from '@mui/icons-material/Public';
import ApartmentIcon from '@mui/icons-material/Apartment';
import MeetingRoomIcon from '@mui/icons-material/MeetingRoom';
import TaskIcon from '@mui/icons-material/Task';
import AccountBalanceIcon from '@mui/icons-material/AccountBalance';
import ComputerIcon from '@mui/icons-material/Computer';
import { FlexDiv } from '../../constant';
import { ProjectsComponent } from './ProjectsComponent';
import { DesignationComp } from './Designation';
import { DepartmentComp } from './Department';

interface TabPanelProps {
	children?: React.ReactNode;
	index: number;
	value: number;
}

function CustomTabPanel(props: TabPanelProps) {
	const { children, value, index, ...other } = props;

	return (
		<div
			role="tabpanel"
			hidden={value !== index}
			id={`simple-tabpanel-${index}`}
			aria-labelledby={`simple-tab-${index}`}
			{...other}
		>
			{value === index && (
				<Box sx={{ p: 3 }}>
					<Typography>{children}</Typography>
				</Box>
			)}
		</div>
	);
}

function a11yProps(index: number) {
	return {
		id: `simple-tab-${index}`,
		'aria-controls': `simple-tabpanel-${index}`,
	};
}

function IconWithLabel(props: { icon: JSX.Element; label: string; active?: boolean; onClick?: () => void }) {
	return (
		<FlexDiv
			onClick={props.onClick}
			padding="30px 50px"
			justify="center"
			align="center"
			alignItems="center"
			style={{ border: `2px solid ${!!props.active ? '#1976d2' : 'grey'}`, borderRadius: '5px' }}
		>
			<FlexDiv
				justify="center"
				align="center"
				alignItems="center"
				gap="20px"
				direction="column"
				style={{ width: '50px', height: '40px' }}
			>
				{props.icon}
				<span style={{ whiteSpace: 'nowrap' }}>{props.label}</span>
			</FlexDiv>
		</FlexDiv>
	);
}

export default SettingComponent;

export function SettingComponent() {
	const [value, setValue] = React.useState(0);

	const handleChange = (event: React.SyntheticEvent, newValue: number) => {
		setValue(newValue);
	};

	return (
		<Box sx={{ width: '100%' }}>
			<Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
				<Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
					<Tab
						icon={<IconWithLabel icon={<PublicIcon />} label="Location" active={value === 0} />}
						label=""
						{...a11yProps(0)}
					/>
					<Tab
						icon={<IconWithLabel icon={<ApartmentIcon />} label="Floors" active={value === 1} />}
						label=""
						{...a11yProps(1)}
					/>
					<Tab
						icon={<IconWithLabel icon={<MeetingRoomIcon />} label="Wings" active={value === 2} />}
						label=""
						{...a11yProps(2)}
					/>
					<Tab
						icon={<IconWithLabel icon={<TaskIcon />} label="Projects" active={value === 3} />}
						label=""
						{...a11yProps(2)}
					/>
					<Tab
						icon={<IconWithLabel icon={<ComputerIcon />} label="Designations" active={value === 4} />}
						label=""
						{...a11yProps(2)}
					/>
					<Tab
						icon={<IconWithLabel icon={<AccountBalanceIcon />} label="Department" active={value === 5} />}
						label=""
						{...a11yProps(2)}
					/>
				</Tabs>
			</Box>
			<CustomTabPanel value={value} index={0}>
				<LocationComponent />
			</CustomTabPanel>
			<CustomTabPanel value={value} index={1}>
				<FloorComponent />
			</CustomTabPanel>
			<CustomTabPanel value={value} index={2}>
				<WingComponent />
			</CustomTabPanel>
			<CustomTabPanel value={value} index={3}>
				<ProjectsComponent />
			</CustomTabPanel>
			<CustomTabPanel value={value} index={4}>
				<DesignationComp />
			</CustomTabPanel>
			<CustomTabPanel value={value} index={5}>
				<DepartmentComp />
			</CustomTabPanel>
		</Box>
	);
}

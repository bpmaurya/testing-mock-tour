import { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../states/store/hooks';
import Log from '../../../vendor/utils';
import tokenService from '../../services/token.service';
import { FlexDiv } from '../../constant';
import { Alert, Button, CircularProgress } from '@mui/material';
import { TableComponent } from '../common/TableComponent';
import { Department } from 'MyModels';
import { HeadCell } from '../../core/do';
import { ModalComponent } from '../common-component/ModalComponent';
import { toast, ToastContainer } from 'react-toastify';
import { LoadingButton } from '../common-component/LoaderComponent';
import {
	createDepartmentAsync,
	deleteDepartmentAsync,
	getAllDepartmentAsync,
	getDepartment,
	resetDepartmentState,
	updateDepartmentAsync,
} from '../../states/store/feature/department.slice';
import { DepartmentForm } from '../forms/DepartmentForm';

const TAG = 'inside-designation-component';

const headCells: HeadCell<Department.Type>[] = [
	{
		id: 'name',
		numeric: false,
		disablePadding: false,
		label: 'Name',
	},
	{
		id: 'status',
		numeric: false,
		disablePadding: false,
		label: 'Status',
	},
];

export const DepartmentComp = () => {
	const dispatch = useAppDispatch();
	const [openAction, setOpenAction] = useState<boolean>(false);
	const [openEditAction, setOpenEditAction] = useState<Department.Type | null>();
	const [openDeleteAction, setOpenDeleteAction] = useState<Department.Type | undefined>();
	const [isAdmin, setIsAdmin] = useState<boolean>(false);
	const [allDepartment, setAllDepartment] = useState<any>();

	const { isDepartmentAdded, isDepartmentCreated, isDepartmentDeleted, isDepartmentUpdated, error } =
		useAppSelector(getDepartment);

	const onCreateDepartment = async (data: Department.Type): Promise<any> => {
		return dispatch(createDepartmentAsync(data))
			.unwrap()
			.then(res => {
				Log.d(TAG, res);
				setOpenAction(false);
				return toast.success('Department added!!');
			})
			.catch(error => {
				Log.d(TAG, error);
				setShow(true);
			});
	};

	const onDeleteDepartment = async (data: Department.Type): Promise<any> => {
		return dispatch(deleteDepartmentAsync(data))
			.unwrap()
			.then(e => {
				setOpenAction(false);
				return toast.success('Department deleted!!');
			})
			.catch(error => {
				Log.d(error);
			});
	};

	const onEditDepartment = async (data: Department.Type): Promise<any> => {
		return dispatch(updateDepartmentAsync(data))
			.unwrap()
			.then(e => {
				setOpenEditAction(null);
				return toast.success('Department Updated!!');
			})
			.catch(error => {
				Log.d(error);
			});
	};

	useEffect(() => {
		setIsAdmin(tokenService.isAdmin());
		if (isDepartmentAdded) setOpenAction(false);
		if (isDepartmentDeleted) setOpenDeleteAction(undefined);
		if (isDepartmentUpdated) setOpenEditAction(null);
		dispatch(resetDepartmentState());
		dispatch(getAllDepartmentAsync())
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res);
				setAllDepartment(res.allDepartment);
			})
			.catch(error => {
				Log.d(TAG, error);
			});
	}, [dispatch, isDepartmentAdded, isDepartmentDeleted, isDepartmentUpdated]);

	const [Show, setShow] = useState<boolean>(false);
	useEffect(() => {
		const timeId1 = setTimeout(() => {
			setShow(false);
		}, 3000);

		return () => {
			clearTimeout(timeId1);
		};
	}, [Show]);

	return (
		<>
			{!!openDeleteAction && (
				<ModalComponent
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center">
								<LoadingButton onClick={() => onDeleteDepartment(openDeleteAction)}>
									<Button variant="contained" color="error">
										Delete
									</Button>
								</LoadingButton>
								<Button variant="contained" onClick={() => setOpenDeleteAction(undefined)}>
									Cancel
								</Button>
							</FlexDiv>
						</>
					}
					heading={`Delete This Department? ${openDeleteAction.name}`}
					open={!!openDeleteAction}
				/>
			)}
			{openEditAction ? (
				<ModalComponent
					size="med"
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{!!Show && error.length > 1 ? (
									<Alert style={{ maxWidth: '400px' }} severity="error">
										{error}
									</Alert>
								) : (
									<></>
								)}
								<DepartmentForm
									initialData={openEditAction}
									handleCancel={() => setOpenEditAction(null)}
									onSubmit={e => onEditDepartment(e)}
									onEdit
								/>
							</FlexDiv>
						</>
					}
					heading={`Edit Department`}
					open={!!openEditAction}
				/>
			) : (
				<></>
			)}
			{openAction ? (
				<ModalComponent
					size="med"
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{!!Show && error.length > 1 ? (
									<Alert style={{ maxWidth: '400px' }} severity="error">
										{error}
									</Alert>
								) : (
									<></>
								)}
								<DepartmentForm
									handleCancel={() => setOpenAction(false)}
									onSubmit={e => onCreateDepartment(e)}
								/>
							</FlexDiv>
						</>
					}
					heading={`Add Department`}
					open={openAction}
				/>
			) : (
				<></>
			)}
			<FlexDiv>
				{!!allDepartment ? (
					<TableComponent
						data={allDepartment}
						tableHead={headCells}
						heading="Department"
						onEdit={e => setOpenEditAction(e)}
						onDelete={e => setOpenDeleteAction(e)}
						additionalButton={
							isAdmin ? (
								<Button variant="contained" onClick={() => setOpenAction(true)}>
									Add Department
								</Button>
							) : (
								<></>
							)
						}
					/>
				) : (
					<FlexDiv justify="center" align="center">
						<CircularProgress />
					</FlexDiv>
				)}
			</FlexDiv>
			<ToastContainer closeOnClick={true} />
		</>
	);
};

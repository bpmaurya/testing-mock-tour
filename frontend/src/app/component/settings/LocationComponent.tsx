import { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../states/store/hooks';
import {
	createLocationAsync,
	deleteLocationAsync,
	getAllLocationAsync,
	getLocations,
	resetLocationState,
	updateLocationAsync,
} from '../../states/store/feature/location.slice';
import Log from '../../../vendor/utils';
import tokenService from '../../services/token.service';
import { FlexDiv } from '../../constant';
import { Alert, Button, CircularProgress } from '@mui/material';
import { TableComponent } from '../common/TableComponent';
import { DeskLocation } from 'MyModels';
import { HeadCell } from '../../core/do';
import { ModalComponent } from '../common-component/ModalComponent';
import { LocationForm } from '../forms/LocationForm';
import { toast, ToastContainer } from 'react-toastify';
import { LoadingButton } from '../common-component/LoaderComponent';

const TAG = 'inside-location-form';

const headCells: HeadCell<DeskLocation>[] = [
	{
		id: 'name',
		numeric: false,
		disablePadding: false,
		label: 'Name',
	},
	{
		id: 'status',
		numeric: false,
		disablePadding: false,
		label: 'Status',
	},
];

export const LocationComponent = () => {
	const dispatch = useAppDispatch();
	const [openAction, setOpenAction] = useState<boolean>(false);
	const [openEditAction, setOpenEditAction] = useState<DeskLocation | null>();
	const [openDeleteAction, setOpenDeleteAction] = useState<DeskLocation | undefined>();
	const [isAdmin, setIsAdmin] = useState<boolean>(false);
	const [allLocation, setAllLocation] = useState<any>();

	const {
		addLocationError,
		addLocationSuccess,
		isLocationAdded,
		deleteLocationError,
		deleteLocationSuccess,
		isLocationDeleted,
		isLocationUpdated,
		updateLocationError,
		error,
	} = useAppSelector(getLocations);

	const onCreateLocation = async (data: DeskLocation): Promise<any> => {
		return dispatch(createLocationAsync(data))
			.unwrap()
			.then(res => {
				Log.d(TAG, res);
				setOpenAction(false);
				return toast.success('Location added!!');
			})
			.catch(error => {
				Log.d(TAG, error);
				setShow(true);
			});
	};

	const onDeleteLocation = async (data: DeskLocation): Promise<any> => {
		return dispatch(deleteLocationAsync(data))
			.unwrap()
			.then(e => {
				setOpenAction(false);
				return toast.success('Location deleted!!');
			})
			.catch(error => {
				Log.d(error);
			});
	};

	const onEditLocation = async (data: DeskLocation): Promise<any> => {
		return dispatch(updateLocationAsync(data))
			.unwrap()
			.then(e => {
				setOpenEditAction(null);
				return toast.success('Location Updated!!');
			})
			.catch(error => {
				Log.d(error);
			});
	};

	useEffect(() => {
		setIsAdmin(tokenService.isAdmin());
		if (isLocationAdded) setOpenAction(false);
		if (isLocationDeleted) setOpenDeleteAction(undefined);
		if (isLocationUpdated) setOpenEditAction(null);
		dispatch(resetLocationState());
		dispatch(getAllLocationAsync())
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res);
				setAllLocation(res.allLocation);
			})
			.catch(error => {
				Log.d(TAG, error);
			});
	}, [dispatch, isLocationAdded, isLocationDeleted, isLocationUpdated]);

	const [Show, setShow] = useState<boolean>(false);
	useEffect(() => {
		const timeId1 = setTimeout(() => {
			setShow(false);
		}, 3000);

		return () => {
			clearTimeout(timeId1);
		};
	}, [Show]);

	return (
		<>
			{!!openDeleteAction && (
				<ModalComponent
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center">
								<LoadingButton onClick={() => onDeleteLocation(openDeleteAction)}>
									<Button variant="contained" color="error">
										Delete
									</Button>
								</LoadingButton>
								<Button variant="contained" onClick={() => setOpenDeleteAction(undefined)}>
									Cancel
								</Button>
							</FlexDiv>
						</>
					}
					heading={`Delete This Location? ${openDeleteAction.name}`}
					open={!!openDeleteAction}
				/>
			)}
			{openEditAction ? (
				<ModalComponent
					size="med"
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{updateLocationError.length > 1 ? (
									<Alert style={{ maxWidth: '400px' }} severity="error">
										{updateLocationError}
									</Alert>
								) : (
									<></>
								)}
								<LocationForm
									initialData={openEditAction}
									handleCancel={() => setOpenEditAction(null)}
									onSubmit={e => onEditLocation(e)}
									onEdit
								/>
							</FlexDiv>
						</>
					}
					heading={`Edit Location `}
					open={!!openEditAction}
				/>
			) : (
				<></>
			)}
			{openAction ? (
				<ModalComponent
					size="med"
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{!!Show && addLocationError.length > 1 ? (
									<Alert style={{ maxWidth: '400px' }} severity="error">
										{addLocationError}
									</Alert>
								) : (
									<></>
								)}
								<LocationForm
									handleCancel={() => setOpenAction(false)}
									onSubmit={e => onCreateLocation(e)}
								/>
							</FlexDiv>
						</>
					}
					heading={`Add Location `}
					open={openAction}
				/>
			) : (
				<></>
			)}
			<FlexDiv>
				{!!allLocation ? (
					<TableComponent
						data={allLocation}
						tableHead={headCells}
						heading="Location"
						onEdit={e => setOpenEditAction(e)}
						onDelete={e => setOpenDeleteAction(e)}
						additionalButton={
							isAdmin ? (
								<Button variant="contained" onClick={() => setOpenAction(true)}>
									Add Location
								</Button>
							) : (
								<></>
							)
						}
					/>
				) : (
					<FlexDiv justify="center" align="center">
						<CircularProgress />
					</FlexDiv>
				)}
			</FlexDiv>
			<ToastContainer closeOnClick={true} />
		</>
	);
};

import { useEffect, useState } from 'react';
import { useAppDispatch } from '../../states/store/hooks';
import { Floor } from 'MyModels';
import Log from '../../../vendor/utils';
import { Link } from 'react-router-dom';
import { getAllFloorWithLocationAsync, resetFloorState } from '../../states/store/feature/floor.slice';
import { FlexDiv } from '../../constant';
import ApartmentIcon from '@mui/icons-material/Apartment';
import { IconWithLabelAction } from './LocationComponent';
import { GoBackButton } from './WingComponent';

const TAG = 'inside-location-comp';

export function FloorComponent() {
	const dispatch = useAppDispatch();
	const [allFloor, setAllLFloor] = useState<Floor.Type[]>();
	const [location, setLocation] = useState<string>();

	useEffect(() => {
		dispatch(resetFloorState());
		const params: any = {};
		document.location.search.split('?').forEach(pair => {
			const [key, value] = pair.split('=');
			params[key] = value;
		});
		if (!!params['location_id']) {
			setLocation(params['location_id']);
			dispatch(getAllFloorWithLocationAsync({ locationId: params['location_id'] }))
				.unwrap()
				.then((res: any) => {
					Log.d(TAG, res);
					setAllLFloor(res.allFloor);
				})
				.catch(error => {
					Log.d(TAG, error);
				});
		}
	}, [dispatch]);

	return (
		<>
			<FlexDiv justify="center" alignItems="center" align="center" gap="20px">
				<h3 style={{ color: '#1976d2' }}>
					{allFloor?.filter(e => e.location._id === location)[0].location.name.toUpperCase()}{' '}
				</h3>
			</FlexDiv>
			<GoBackButton text="Location" />
			<div className="flex-box">
				{!!allFloor && allFloor.length > 0 ? (
					allFloor.map((e, index) => (
						<div className="templatecontent" key={e._id}>
							<Link
								style={{
									textDecoration: 'none',
									width: '100%',
									color: 'inherit',
								}}
								to={`/dashboard/${e._id}/wing?floor_id=${e._id}?location_id=${e.location._id}`}
							>
								<IconWithLabelAction icon={<ApartmentIcon />} label={e.name} />
							</Link>
						</div>
					))
				) : (
					<FlexDiv padding="0 0 20px  0">
						<h2>No Floors Found</h2>
					</FlexDiv>
				)}
			</div>
		</>
	);
}

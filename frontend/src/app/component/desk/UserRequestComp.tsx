import { useEffect, useState } from 'react';
import { useAppDispatch } from '../../states/store/hooks';
import { getAllRequestForUserAsync, resetRequestState } from '../../states/store/feature/request.slice';
import Log from '../../../vendor/utils';
import { FlexDiv } from '../../constant';
import { TableComponent } from '../common/TableComponent';
import { HeadCell } from '../../core/do';
import { DeskRequest } from 'MyModels';
import tokenService from '../../services/token.service';
import { CircularProgress } from '@mui/material';

const TAG = 'inside request component';
const headCells: HeadCell<ExtendedTypeForTable>[] = [
	{
		id: 'user_name',
		numeric: false,
		disablePadding: false,
		label: 'Name',
	},
	{
		id: 'location_name',
		numeric: false,
		disablePadding: false,
		label: 'Location',
	},
	{
		id: 'wing_name',
		numeric: false,
		disablePadding: false,
		label: 'Wing',
	},
	{
		id: 'project_name',
		numeric: false,
		disablePadding: false,
		label: 'Project',
	},
	{
		id: 'reporting_Manager',
		numeric: false,
		disablePadding: false,
		label: 'Reporting Manager',
	},
	{
		id: 'desk_name',
		numeric: false,
		disablePadding: false,
		label: 'Desk Name',
	},
	{
		id: 'status',
		numeric: false,
		disablePadding: false,
		label: 'Status',
	},
	{
		id: 'priority',
		numeric: false,
		disablePadding: false,
		label: 'Priority',
	},
	{
		id: 'request_date',
		numeric: false,
		disablePadding: false,
		label: 'Requested Date',
	},
];
type ExtendedTypeForTable = DeskRequest.Type & {
	user_name: string;
	location_name: string;
	wing_name: string;
	desk_name: string;
	project_name?: string;
	request_date?: string;
	reporting_Manager: string;
};
export function UserRequestComp() {
	const dispatch = useAppDispatch();
	const [isAdmin, setIsAdmin] = useState<boolean>(false);
	const [allRequest, setAllRequests] = useState<ExtendedTypeForTable[]>([]);

	useEffect(() => {
		setIsAdmin(tokenService.isAdmin());
		dispatch(resetRequestState());
		const user = tokenService.getUser();
		if (user.userId !== undefined) {
			dispatch(getAllRequestForUserAsync({ id: user.userId! }))
				.unwrap()
				.then((res: any) => {
					Log.d(TAG, res);
					return setDataInState(res.allRequest);
				})
				.catch(error => {
					Log.d(TAG, error);
				});
		}
	}, [dispatch]);

	const setDataInState = (data: any) => {
		let modifiedData: ExtendedTypeForTable[] = [];
		data.map((e: DeskRequest.Type) => {
			return modifiedData.push({
				...e,
				user_name: `${e.user?.firstName} ${e.user?.middleName} ${e.user?.lastName}`,
				location_name: e.seat.location.name + ' ' + e.seat.floor.name,
				desk_name: e.seat.name,
				wing_name: e.seat.wing.name,
				project_name: e.project?.name,
				request_date: e.updatedAt?.toLocaleString().split('T')[0],
				reporting_Manager: e.reportingManager.firstName,
			});
		});
		setAllRequests(modifiedData);
	};

	return (
		<FlexDiv>
			{!!allRequest ? (
				<TableComponent data={allRequest} tableHead={headCells} heading="My Requests" report />
			) : (
				<FlexDiv justify="center" align="center">
					<CircularProgress />
				</FlexDiv>
			)}
		</FlexDiv>
	);
}

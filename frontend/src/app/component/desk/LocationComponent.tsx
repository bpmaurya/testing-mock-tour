import { useEffect, useState } from 'react';
import { useAppDispatch } from '../../states/store/hooks';
import { resetDeskState } from '../../states/store/feature/desk.slice';
import { getAllLocationAsync } from '../../states/store/feature/location.slice';
import { DeskLocation } from 'MyModels';
import Log from '../../../vendor/utils';
import { Link } from 'react-router-dom';
import PublicIcon from '@mui/icons-material/Public';
import { FlexDiv } from '../../constant';
import Card from '@mui/material/Card';

const TAG = 'inside-location-comp';

export function LocationComponent() {
	const dispatch = useAppDispatch();
	const [allLocation, setAllLocation] = useState<DeskLocation[]>();
	useEffect(() => {
		dispatch(resetDeskState());
		dispatch(getAllLocationAsync())
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res);
				setAllLocation(res.allLocation);
			})
			.catch(error => {
				Log.d(TAG, error);
			});
	}, [dispatch]);

	return (
		<>
			<FlexDiv padding="0 0 20px  0">
				<h2>All Locations</h2>
			</FlexDiv>
			<div className="flex-box">
				{!!allLocation && allLocation.length > 0 ? (
					allLocation.map((e, index) => (
						<div className="templatecontent" key={e._id}>
							<Link
								style={{
									textDecoration: 'none',
									width: '100%',
									color: 'inherit',
								}}
								to={`/dashboard/${e._id}/floor?location_id=${e._id}`}
							>
								<IconWithLabelAction icon={<PublicIcon />} label={e.name} />
							</Link>
						</div>
					))
				) : (
					<FlexDiv padding="0 0 20px  0">
						<h2>No Locations Found</h2>
					</FlexDiv>
				)}
			</div>
		</>
	);
}

export function IconWithLabelAction(props: {
	icon: JSX.Element;
	label: string;
	active?: boolean;
	onClick?: () => void;
}) {
	return (
		<Card
			onClick={props.onClick}
			style={{
				padding: '30px 50px',
				justifyContent: 'center',
				alignContent: 'center',
				alignItems: 'center',
				display: 'flex',
				boxShadow: '0 3px 10px rgb(0 0 0 / 0.6)',
			}}
		>
			<FlexDiv
				justify="center"
				align="center"
				alignItems="center"
				gap="20px"
				direction="column"
				style={{ width: '50px', height: '40px' }}
			>
				{props.icon}
				<span style={{ whiteSpace: 'nowrap' }}>{props.label}</span>
			</FlexDiv>
		</Card>
	);
}

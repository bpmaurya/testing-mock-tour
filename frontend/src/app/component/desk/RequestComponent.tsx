import { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../states/store/hooks';
import {
	getAllRequestAsync,
	getRequests,
	resetRequestState,
	updateRequestAsync,
} from '../../states/store/feature/request.slice';
import Log from '../../../vendor/utils';
import { FlexDiv } from '../../constant';
import { TableComponent } from '../common/TableComponent';
import { HeadCell } from '../../core/do';
import { Desk, DeskRequest } from 'MyModels';
import { toast, ToastContainer } from 'react-toastify';
import tokenService from '../../services/token.service';
import { Alert, CircularProgress } from '@mui/material';
import { ModalComponent } from '../common-component/ModalComponent';
import { UpdateRequestForm } from '../forms/UpdateRequest';

const TAG = 'inside request component';
const headCells: HeadCell<ExtendedTypeForTable>[] = [
	{
		id: 'user_name',
		numeric: false,
		disablePadding: false,
		label: 'Name',
	},
	{
		id: 'location_name',
		numeric: false,
		disablePadding: false,
		label: 'Location',
	},
	{
		id: 'wing_name',
		numeric: false,
		disablePadding: false,
		label: 'Wing',
	},
	{
		id: 'project_name',
		numeric: false,
		disablePadding: false,
		label: 'Project',
	},
	{
		id: 'reporting_Manager',
		numeric: false,
		disablePadding: false,
		label: 'Reporting Manager',
	},
	{
		id: 'desk_name',
		numeric: false,
		disablePadding: false,
		label: 'Desk Name',
	},
	{
		id: 'status',
		numeric: false,
		disablePadding: false,
		label: 'Status',
	},
	{
		id: 'priority',
		numeric: false,
		disablePadding: false,
		label: 'Priority',
	},
	{
		id: 'request_date',
		numeric: false,
		disablePadding: false,
		label: 'Requested Date',
	},
];
type ExtendedTypeForTable = DeskRequest.Type & {
	user_name: string;
	location_name: string;
	wing_name: string;
	desk_name: string;
	project_name?: string;
	request_date?: string;
	reporting_Manager: string;
};
export function RequestComponent() {
	const dispatch = useAppDispatch();
	const [isAdmin, setIsAdmin] = useState<boolean>(false);
	const [allRequest, setAllRequests] = useState<ExtendedTypeForTable[]>([]);
	const [openEditAction, setOpenEditAction] = useState<DeskRequest.Type | null>();
	const [openDeleteAction, setOpenDeleteAction] = useState<Desk.Type | undefined>();
	const { error: error, success, isRequestUpdated } = useAppSelector(getRequests);

	useEffect(() => {
		setIsAdmin(tokenService.isAdmin());
		if (isRequestUpdated) setOpenEditAction(null);
		dispatch(resetRequestState());
		dispatch(getAllRequestAsync())
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res);
				return setDataInState(res.allRequest);
			})
			.catch(error => {
				Log.d(TAG, error);
			});
	}, [dispatch, isRequestUpdated]);

	const setDataInState = (data: any) => {
		let modifiedData: ExtendedTypeForTable[] = [];
		data.map((e: DeskRequest.Type) => {
			return modifiedData.push({
				...e,
				user_name: `${e.user?.firstName} ${e.user?.middleName} ${e.user?.lastName}`,
				location_name: e.seat.location.name + ' ' + e.seat.floor.name,
				desk_name: e.seat.name,
				wing_name: e.seat.wing.name,
				project_name: e.project?.name,
				request_date: e.updatedAt?.toLocaleString().split('T')[0],
				reporting_Manager: e.reportingManager.firstName,
			});
		});
		setAllRequests(modifiedData);
	};
	const [show, setShow] = useState<boolean>(false);

	useEffect(() => {
		const timeId1 = setTimeout(() => {
			setShow(false);
		}, 3000);

		return () => {
			clearTimeout(timeId1);
		};
	}, [show]);

	const onBookRequestUpdate = async (data: DeskRequest.Type): Promise<any> => {
		Log.d(TAG, data);
		return dispatch(updateRequestAsync(data))
			.unwrap()
			.then(res => {
				setOpenEditAction(null);
				return toast.success('Update successfully!!');
			})
			.catch(err => {
				Log.d(TAG, err);
				setShow(true);
			});
	};

	return (
		<>
			{!!openEditAction && (
				<ModalComponent
					size="med"
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{!!show && error.length > 1 ? (
									<Alert style={{ maxWidth: '400px' }} severity="error">
										{error}
									</Alert>
								) : (
									<></>
								)}
								<UpdateRequestForm
									initialData={openEditAction}
									onSubmit={e => onBookRequestUpdate(e)}
									onCancel={() => setOpenEditAction(null)}
								/>
							</FlexDiv>
						</>
					}
					heading={`Edit Request`}
					open={!!openEditAction}
				/>
			)}
			<FlexDiv>
				{!!allRequest ? (
					<TableComponent
						data={allRequest}
						tableHead={headCells}
						heading="Requests"
						onEdit={e => setOpenEditAction(e)}
						onDelete={e => setOpenDeleteAction(e)}
						report
					/>
				) : (
					<FlexDiv justify="center" align="center">
						<CircularProgress />
					</FlexDiv>
				)}
			</FlexDiv>
			<ToastContainer closeOnClick={true} />
		</>
	);
}

import React, { useEffect, useState } from 'react';
import './desk.css';
import { FlexDiv } from '../../constant';
import Log from '../../../vendor/utils';
import { Alert, Button, FormControl, InputLabel, MenuItem, Select } from '@mui/material';
import { ModalComponent } from '../common-component/ModalComponent';
import DeleteIcon from '@mui/icons-material/Delete';
import Checkbox, { checkboxClasses } from '@mui/material/Checkbox';
import { useAppDispatch, useAppSelector } from '../../states/store/hooks';
import { Desk, DeskLocation, DeskRequest, Floor, Wing } from 'MyModels';
import {
	createDeskAsync,
	deleteDeskAsync,
	getAllSeatsAsync,
	getDesks,
	resetDeskState,
	updateDeskAsync,
	updateDeskOrderAsync,
} from '../../states/store/feature/desk.slice';
import { toast, ToastContainer } from 'react-toastify';
import { LoadingButton } from '../common-component/LoaderComponent';
import tokenService from '../../services/token.service';
import { createRequestAsync, getRequests } from '../../states/store/feature/request.slice';
import { BookDeskForm } from '../forms/BookDeskForm';
import { AddDeskForm, DeskArrangement } from '../forms/AddDeskForm';
import { MakeItemList } from '../forms/UpdateRequest';
import { DragDropContext, Droppable, Draggable, DroppableProps } from '@hello-pangea/dnd';
import DragIndicatorIcon from '@mui/icons-material/DragIndicator';
import { AppContext } from '../../../App';
import { ChartComponent } from '../common/ChartComponent';
import { GoBackButton } from './WingComponent';

const TAG = 'Inside desk component';

const Seat = (props: {
	seat: Desk.Type;
	checked: any;
	onChange: (e: any) => void;
	floor?: Floor.Type;
	location?: DeskLocation;
	wing?: Wing.Type;
}): JSX.Element => {
	return (
		<div className="col-1 col-md-1">
			<Checkbox
				onChange={e => props.onChange(e.target.value)}
				color="primary"
				checked={!!props.checked}
				disableRipple
				inputProps={{
					'aria-labelledby': props.seat.name,
				}}
				// disabled={!props.seat.isSelectable}
				sx={{
					pl: 0,
					'& .MuiSvgIcon-root': { fontSize: 30 },
					[`&, &.${checkboxClasses.checked}`]: {
						// color: !props.seat.isSelectable ? 'red' : 'green',
					},
				}}
				icon={
					<CheckedIcon
						seatNo={props.seat.name}
						selectable={props.seat.isSelectable}
						color={!props.seat.isSelectable ? '#B2B2B2' : '#a6f6bb'}
					/>
				}
				checkedIcon={
					<CheckedIcon
						seatNo={props.seat.name}
						selectable={props.seat.isSelectable}
						color={!props.seat.isSelectable ? '#B2B2B2' : '#a6f6bb'}
					/>
				}
			/>
		</div>
	);
};

const CheckedIcon = (props: { seatNo: string; color?: string; selectable?: boolean }) => (
	<div
		className="checked_custom_icon"
		style={{
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center',
			width: '35px',
			height: '38px',
			background: props.color,
			borderRadius: '8px',
			boxShadow: props.selectable ? '0 3px 10px rgb(0 0 0 / 0.6)' : '0 3px 8px rgb(0 0 0 / 0.3)',
		}}
	>
		<p style={{ fontSize: 'small', fontWeight: 'bold', padding: '2px', color: '#161835' }}>{props.seatNo}</p>{' '}
	</div>
);

export interface DeleteDeskType {
	location: string;
	floor: string;
	wing: string;
	row: string;
}

export interface DeskRowComponent {
	orderIndex: number;
	seats: Desk.Type[];
	columName: string;
}

const getListStyle = (isDraggingOver: any) => ({
	width: '100%',
});

const reorder = (list: any, startIndex: any, endIndex: any) => {
	const result = Array.from(list);
	const [removed] = result.splice(startIndex, 1);
	result.splice(endIndex, 0, removed);

	return result;
};
const getItemStyle = (isDragging: any, draggableStyle: any) => ({
	userSelect: 'none',
	background: isDragging ? '#e6ffe6' : 'none',
	...draggableStyle,
});

const StrictModeDroppable = ({ children, ...props }: DroppableProps) => {
	const [enabled, setEnabled] = useState(false);
	useEffect(() => {
		const animation = requestAnimationFrame(() => setEnabled(true));
		return () => {
			cancelAnimationFrame(animation);
			setEnabled(false);
		};
	}, []);
	if (!enabled) {
		return null;
	}
	return <Droppable {...props}>{children}</Droppable>;
};

export const SeatArrangement = () => {
	const socket = React.useContext(AppContext);
	const dispatch = useAppDispatch();
	const [openAction, setOpenAction] = useState<boolean>(false);
	const [openDeleteAction, setOpenDeleteAction] = useState<DeleteDeskType | undefined>();
	const { error, isDeskAdded, isDeskDeleted, isDeskUpdated } = useAppSelector(getDesks);
	const { error: requestError, isRequestCreated } = useAppSelector(getRequests);
	const [Show, setShow] = useState<boolean>(false);
	useEffect(() => {
		const timeId1 = setTimeout(() => {
			setShow(false);
		}, 3000);

		return () => {
			clearTimeout(timeId1);
		};
	}, [Show]);

	const [deskArrangement, setDeskArrangement] = useState<DeskRowComponent[]>([]);

	const onSubmit = async (data: DeskArrangement) => {
		dispatch(createDeskAsync(data))
			.unwrap()
			.then(res => {
				Log.d(TAG, res);
				return toast.success('Desk added!!');
			})
			.catch(error => {
				Log.d(TAG, error);
				setShow(true);
			});
	};

	const onDeleteRow = async (data: DeleteDeskType): Promise<any> => {
		Log.d(TAG, data);
		return dispatch(deleteDeskAsync(data))
			.unwrap()
			.then(res => {
				return toast.success('Desk Deleted!');
			})
			.catch(err => {
				Log.d(TAG, err);
				setShow(true);
			});
	};

	const [selectedLocation, setSelectedLocation] = useState<string | undefined>();
	const [selectedFloor, setSelectedFloor] = useState<string | undefined>();
	const [selectedWing, setSelectedWing] = useState<string | undefined>();

	useEffect(() => {
		socket.socketContext.on(`update-message`, (notification: any) => {
			console.log('update-message', notification);
			if (!!notification) {
				setDeskArrangement(editCopy(deskArrangement, notification.seat, notification.selectable));
			}
		});

		return () => {
			socket.socketContext.off('update-message');
		};
	}, [deskArrangement, socket.socketContext]);

	const editCopy = (arr: DeskRowComponent[], id: string, val: any): DeskRowComponent[] => {
		return arr.map((item: DeskRowComponent) => ({
			...item,
			seats: item.seats.map(e => {
				if (e._id === id) {
					return { ...e, isSelectable: val };
				} else return e;
			}),
		}));
	};

	useEffect(() => {
		if (isDeskAdded) setOpenAction(false);
		if (isDeskDeleted) setOpenDeleteAction(undefined);
		if (isDeskUpdated) setOpenBookDeskForAdmin(null);
		if (isRequestCreated) setOpenBookDesk(null);
		if (!!selectedFloor && !!selectedLocation && selectedWing)
			if (selectedFloor !== undefined && selectedWing !== undefined && selectedLocation !== undefined) {
				dispatch(
					getAllSeatsAsync({
						location: selectedLocation,
						floor: selectedFloor,
						wing: selectedWing,
					})
				)
					.unwrap()
					.then((res: any) => {
						Log.d(TAG, res);
						return setDataInState(res.allSeat);
					})
					.catch(error => {
						Log.d(TAG, error);
					});
			}
		setDeskArrangement([]);
		setSelected(null);
	}, [
		dispatch,
		selectedWing,
		selectedFloor,
		selectedLocation,
		isDeskAdded,
		isDeskDeleted,
		isDeskUpdated,
		isRequestCreated,
	]);

	const setDataInState = (data: any) => {
		if (!!data) {
			const unique = [...new Set(data.map((item: any) => item.row))];
			let list: Desk.Type[][] = [];
			let newRowDesk: DeskRowComponent[] = [];
			for (let i = 0; i < unique.length; i++) {
				let listOfSeats: Desk.Type[] = [];
				data.map((e: any) => {
					if (unique[i] === e.row) {
						return listOfSeats.push(e);
					} else return null;
				});
				list.push([...listOfSeats.sort((a, b) => a.name.localeCompare(b.name, 'en', { numeric: true }))]);
			}
			Log.d(TAG, list);
			for (let i = 0; i < unique.length; i++) {
				for (let j = 0; j < list.length; j++) {
					if (Array.from(list[j][0].name)[0] === (unique[i] as any)) {
						newRowDesk.push({
							seats: list[j],
							columName: unique[i] as any,
							orderIndex: list[j][0].orderIndex,
						});
					}
				}
			}
			setDeskArrangement(newRowDesk.sort((a, b) => a.orderIndex - b.orderIndex));
		}
		if (data.length === 0) setDeskArrangement([]);
	};

	useEffect(() => {
		const params: any = {};
		document.location.search.split('?').forEach(pair => {
			const [key, value] = pair.split('=');
			params[key] = value;
		});

		setSelectedLocation(params['location_id']);
		setSelectedFloor(params['floor_id']);
		setSelectedWing(params['wing_id']);
	}, [dispatch]);

	const [user, setUser] = useState<any>();
	const [selected, setSelected] = useState(null);
	const [openBookDesk, setOpenBookDesk] = useState<Desk.Type | null>();
	const [openBookDeskForAdmin, setOpenBookDeskForAdmin] = useState<any>();
	const [availability, setAvailability] = useState<string>('');
	const [catchOnHover, setCatchOnHover] = useState<Desk.Type | null>();
	const [availableToDrag, setAvailableToDrag] = useState<boolean>(true);
	function onChange(i: any) {
		setSelected(prev => (i === prev ? null : i));
		if (i !== selected) {
			if (user.role !== 'admin') {
				if (!i.isSelectable && !!i.bookedBy) {
					setCatchOnHover(i);
				} else setOpenBookDesk(i);
			} else if (user.role === 'admin') {
				if (!i.isSelectable && i.bookedBy) {
					setCatchOnHover(i);
				} else {
					dispatch(resetDeskState());
					setOpenBookDeskForAdmin(i);
				}
			}
		}
		Log.d(TAG, i);
	}

	const onBookDeskRequest = async (data: DeskRequest.Type): Promise<any> => {
		Log.d(TAG, data);
		return dispatch(createRequestAsync(data))
			.unwrap()
			.then(res => {
				setOpenBookDesk(null);
				return toast.success('Request Created!');
			})
			.catch(err => {
				Log.d(TAG, err);
				setShow(true);
			});
	};

	const onChangeDeskProperty = async (data: Desk.Type): Promise<any> => {
		Log.d(TAG, data);
		return dispatch(updateDeskAsync(data))
			.unwrap()
			.then(res => {
				setOpenBookDeskForAdmin(null);
				return toast.success('Desk Updated!');
			})
			.catch(err => {
				Log.d(TAG, err);
				setShow(true);
			});
	};

	useEffect(() => {
		setUser(tokenService.getUser());
	}, []);

	const onDragEnd = async (result: any) => {
		// dropped outside the list

		if (!result.destination) {
			return;
		}

		const items = reorder(deskArrangement, result.source.index, result.destination.index);
		const modifiedItems: any[] = [];
		items.map((e: any, index) => modifiedItems.push({ ...e, orderIndex: index }));
		Log.d(TAG, modifiedItems);
		setDeskArrangement(modifiedItems as any);
		await setOrderIndexOfDesk(modifiedItems as any);
	};

	const setOrderIndexOfDesk = async (data: DeskRowComponent[]): Promise<any> => {
		setAvailableToDrag(false);
		dispatch(updateDeskOrderAsync(data))
			.unwrap()
			.then(res => {
				setAvailableToDrag(true);
				return toast.success('Row Order Updated!');
			})
			.catch(err => {
				Log.d(TAG, err);
			});
	};

	return (
		<>
			{!!catchOnHover && (
				<ModalComponent
					children={
						<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
							<MakeItemList
								data={[
									{
										itemName: 'Name',
										value:
											catchOnHover.bookedBy?.firstName +
											' ' +
											catchOnHover.bookedBy?.middleName +
											' ' +
											catchOnHover.bookedBy?.lastName,
									},
									{
										itemName: 'Email',
										value: catchOnHover.bookedBy?.email,
									},
									{
										itemName: 'Domain',
										value: catchOnHover.bookedBy?.domain,
									},
									{
										itemName: 'Designation',
										value: catchOnHover.bookedBy?.position?.name,
									},
									{
										itemName: 'Department',
										value: catchOnHover.bookedBy?.department?.name,
									},
									{
										itemName: 'Project',
										value: catchOnHover.bookedBy?.project?.name,
									},
									{
										itemName: 'Location',
										value: catchOnHover.location.name + ' ' + catchOnHover.floor.name,
									},
									{
										itemName: 'Seat',
										value: catchOnHover.wing.name + ' ' + catchOnHover.name,
									},
								]}
							/>

							<Button
								variant="contained"
								onClick={() => {
									setCatchOnHover(null);
									setSelected(null);
								}}
							>
								Close
							</Button>
						</FlexDiv>
					}
					heading={`User Data!`}
					open={!!catchOnHover}
				/>
			)}
			{!!openBookDeskForAdmin && (
				<ModalComponent
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								<FlexDiv gap="20px" align="center" direction="column" justify="center">
									<FormControl required fullWidth>
										<InputLabel id="reg_availability_id">Selectable</InputLabel>
										<Select
											required
											type="text"
											name="availability"
											id="availability_id"
											label="Selectable"
											value={availability}
											onChange={(e: any) => setAvailability(e.target.value)}
										>
											<MenuItem value={'true'}>True</MenuItem>
											<MenuItem value={'false'}>False</MenuItem>
										</Select>
									</FormControl>
									<FlexDiv gap="20px">
										<LoadingButton
											onClick={e =>
												onChangeDeskProperty({
													...openBookDeskForAdmin,
													isSelectable: availability === 'true',
												})
											}
										>
											<Button variant="contained" color="error">
												Change
											</Button>
										</LoadingButton>

										<Button
											variant="contained"
											onClick={() => {
												setOpenBookDeskForAdmin(null);
												setSelected(null);
											}}
										>
											Cancel
										</Button>
									</FlexDiv>
								</FlexDiv>
							</FlexDiv>
						</>
					}
					heading={`Change availability!`}
					open={!!openBookDeskForAdmin}
				/>
			)}
			{!!openBookDesk && (
				<ModalComponent
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								<FlexDiv gap="20px" align="center" justify="center" direction="column">
									{!!Show && requestError.length > 1 ? (
										<FlexDiv justify="center" align="center" alignItems="center" width="full">
											<Alert style={{ maxWidth: '400px' }} severity="error">
												{requestError}
											</Alert>
										</FlexDiv>
									) : (
										<></>
									)}
									{!openBookDesk.isSelectable ? (
										<FlexDiv
											direction="column"
											gap="20px"
											justify="center"
											align="center"
											alignItems="center"
										>
											<h3>This Desk is Disabled By Admin</h3>
											<Button
												onClick={() => {
													setOpenBookDesk(null);
													setSelected(null);
												}}
												variant="contained"
											>
												Close
											</Button>
										</FlexDiv>
									) : (
										<BookDeskForm
											user={user.userObject}
											seat={openBookDesk}
											onSubmit={e => onBookDeskRequest(e)}
											onCancel={() => {
												setSelected(null);
												setOpenBookDesk(null);
											}}
										/>
									)}
								</FlexDiv>
							</FlexDiv>
						</>
					}
					heading={!openBookDesk.isSelectable ? '' : 'Fill these details!'}
					open={!!openBookDesk}
				/>
			)}
			{!!openDeleteAction && (
				<ModalComponent
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{!!Show && error.length > 1 ? (
									<Alert style={{ maxWidth: '400px' }} severity="error">
										{error}
									</Alert>
								) : (
									<></>
								)}
								<FlexDiv gap="20px" align="center" justify="center">
									<LoadingButton onClick={(e: any) => onDeleteRow(openDeleteAction)}>
										<Button variant="contained" color="error">
											Delete
										</Button>
									</LoadingButton>

									<Button variant="contained" onClick={() => setOpenDeleteAction(undefined)}>
										Cancel
									</Button>
								</FlexDiv>
							</FlexDiv>
						</>
					}
					heading={`Delete This Row? ${openDeleteAction.row}`}
					open={!!openDeleteAction}
				/>
			)}
			{!!openAction && (
				<ModalComponent
					size="med"
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{!!Show && error.length > 1 ? (
									<Alert style={{ maxWidth: '400px' }} severity="error">
										{error}
									</Alert>
								) : (
									<></>
								)}
								<AddDeskForm
									floor={selectedFloor!}
									wing={selectedWing!}
									location={selectedLocation!}
									onCancel={() => setOpenAction(false)}
									onSubmit={(e: any) => onSubmit(e)}
								/>
							</FlexDiv>
						</>
					}
					heading={`Add Desks`}
					open={openAction}
				/>
			)}
			{!!deskArrangement && !!deskArrangement[0] && (
				<FlexDiv justify="center" alignItems="center" align="center" gap="20px">
					<h3 style={{ color: '#1976d2' }}>
						{deskArrangement[0].seats[0].location.name.toUpperCase()} &gt;{' '}
						{deskArrangement[0].seats[0].floor.name.toUpperCase()} &gt;{' '}
						{deskArrangement[0].seats[0].wing.name.toUpperCase()}
					</h3>
				</FlexDiv>
			)}
			<FlexDiv direction="column" gap="20px">
				<FlexDiv justify="space-between" alignItems="center" width="full">
					<GoBackButton />
				</FlexDiv>
				{selectedFloor && selectedLocation && selectedWing && (
					<FlexDiv justify="space-between" className="desk_main_div_content" width="full">
						<FlexDiv direction="column" gap="20px">
							<div className="desk-complex">
								<div className="container row movie-layout">
									<div className="desk-column-1">
										{deskArrangement.length <= 0 && <h3>No Desk available</h3>}
										{deskArrangement.length > 0 && (
											<DragDropContext onDragEnd={e => onDragEnd(e)}>
												<StrictModeDroppable droppableId="droppable-0">
													{(provided, snapshot) => (
														<div
															{...provided.droppableProps}
															ref={provided.innerRef}
															style={getListStyle(snapshot.isDraggingOver)}
														>
															{deskArrangement.map((e: DeskRowComponent, index) => (
																<Draggable
																	key={e.columName}
																	draggableId={`${
																		e.columName + '395dd91312589a8584aa0451'
																	}`}
																	index={index}
																	isDragDisabled={
																		!!user &&
																		user.role !== 'admin' &&
																		!!availableToDrag
																	}
																>
																	{(provided, snapshot) => (
																		<div
																			ref={provided.innerRef}
																			{...provided.draggableProps}
																			{...provided.dragHandleProps}
																			style={getItemStyle(
																				snapshot.isDragging,
																				provided.draggableProps.style
																			)}
																		>
																			<FlexDiv
																				ref={provided.innerRef}
																				gap="10px"
																				key={e.columName}
																			>
																				<FlexDiv
																					gap="20px"
																					justify="center"
																					align="center"
																					style={{ marginTop: '15px' }}
																				>
																					{!!user &&
																						user.role === 'admin' && (
																							<FlexDiv gap="10px">
																								<DeleteIcon
																									sx={{
																										color: '#d32f2f',
																										cursor: 'pointer',
																									}}
																									onClick={() => {
																										setOpenDeleteAction(
																											{
																												location:
																													selectedLocation,
																												floor: selectedFloor,
																												wing: selectedWing,
																												row: e.columName,
																											}
																										);
																										dispatch(
																											resetDeskState()
																										);
																									}}
																								/>
																								<DragIndicatorIcon
																									style={{
																										cursor: 'grab',
																									}}
																								/>
																							</FlexDiv>
																						)}
																					<h3 style={{ marginRight: '10px' }}>
																						{e.columName}
																					</h3>
																				</FlexDiv>

																				{e.seats.map((f, index) => {
																					return (
																						<FlexDiv key={index}>
																							<FlexDiv
																								direction="column"
																								justify="center"
																								alignItems="center"
																								style={{
																									marginBottom:
																										'12px',
																								}}
																							>
																								<Seat
																									key={index}
																									checked={
																										selected === f
																									}
																									onChange={() =>
																										onChange(f)
																									}
																									seat={f}
																								/>
																							</FlexDiv>
																						</FlexDiv>
																					);
																				})}
																			</FlexDiv>
																		</div>
																	)}
																</Draggable>
															))}
															{provided.placeholder}
														</div>
													)}
												</StrictModeDroppable>
											</DragDropContext>
										)}
									</div>
								</div>
							</div>

							{!!user && user.role === 'admin' && (
								<FlexDiv justify="start" style={{ marginTop: '20px' }}>
									<Button
										variant="contained"
										onClick={() => {
											setOpenAction(true);
											dispatch(resetDeskState());
										}}
									>
										Add desk
									</Button>
								</FlexDiv>
							)}
						</FlexDiv>
						{!!user && user.role === 'admin' && deskArrangement.length > 0 && (
							<FlexDiv direction="column" gap="20px" justify="end">
								<ChartComponent
									data={[
										{
											name: 'Available Seat',
											value: deskArrangement
												.map(e => {
													let count = 0;
													e.seats.map(f => {
														if (f.isSelectable === true) {
															return count++;
														}
														return count;
													});
													return count;
												})
												.reduce((a, b) => a + b, 0),
										},
										{
											name: 'Occupied Seat',
											value: deskArrangement
												.map(e => {
													let count = 0;
													e.seats.map(f => {
														if (f.isSelectable === false) {
															return count++;
														}
														return count;
													});
													return count;
												})
												.reduce((a, b) => a + b, 0),
										},
									]}
									width={300}
									height={300}
								/>
								<FlexDiv direction="column" gap="10px">
									<span style={{ width: '30px', height: '8px', backgroundColor: '#ffcb68' }}></span>{' '}
									<p>Available Seat</p>
									<span style={{ width: '30px', height: '8px', backgroundColor: '#ff5983' }}></span>
									<p>Occupied Seat</p>
								</FlexDiv>
							</FlexDiv>
						)}
					</FlexDiv>
				)}
			</FlexDiv>
			<ToastContainer closeOnClick={true} />
		</>
	);
};

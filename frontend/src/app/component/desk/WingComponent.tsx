import { useEffect, useState } from 'react';
import { useAppDispatch } from '../../states/store/hooks';
import { Wing } from 'MyModels';
import Log from '../../../vendor/utils';
import { Link, useNavigate } from 'react-router-dom';
import { resetFloorState } from '../../states/store/feature/floor.slice';
import { FlexDiv } from '../../constant';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { getAllWingForLocationAndFloor } from '../../states/store/feature/wing.slice';
import MeetingRoomIcon from '@mui/icons-material/MeetingRoom';
import { IconWithLabelAction } from './LocationComponent';

const TAG = 'inside-location-comp';

export function WingComponent() {
	const dispatch = useAppDispatch();
	const [wings, setWings] = useState<Wing.Type[] | undefined>();
	const [location, setLocation] = useState<string>();
	const [floor, setFloor] = useState<string>();

	useEffect(() => {
		dispatch(resetFloorState());
		const params: any = {};
		document.location.search.split('?').forEach(pair => {
			const [key, value] = pair.split('=');
			params[key] = value;
		});
		if (!!params['location_id'] && !!params['floor_id']) {
			setFloor(params['floor_id']);
			setLocation(params['location_id']);
			dispatch(getAllWingForLocationAndFloor({ location: params['location_id'], floor: params['floor_id']! }))
				.unwrap()
				.then((res: any) => {
					Log.d(TAG, res);
					setWings(res.allWing);
				})
				.catch(error => {
					Log.d(TAG, error);
				});
		}
	}, [dispatch]);

	return (
		<>
			{!!wings && (
				<FlexDiv justify="center" alignItems="center" align="center" gap="20px">
					<h3 style={{ color: '#1976d2' }}>
						{wings.filter(e => e.location._id === location)[0].location.name.toUpperCase()} &gt;{' '}
						{wings.filter(e => e.location._id === location)[0].floor.name.toUpperCase()}{' '}
					</h3>
				</FlexDiv>
			)}
			<GoBackButton text="Floor" />

			<div className="flex-box">
				{!!wings && wings.length > 0 ? (
					wings.map((e, index) => (
						<div className="templatecontent" key={e._id}>
							<Link
								style={{
									textDecoration: 'none',
									width: '100%',
									color: 'inherit',
								}}
								to={`/dashboard/${e._id}/seats?floor_id=${e.floor._id}?location_id=${e.location._id}?wing_id=${e._id}`}
							>
								<IconWithLabelAction icon={<MeetingRoomIcon />} label={e.name} />
							</Link>
						</div>
					))
				) : (
					<>
						<FlexDiv padding="0 0 20px  0">
							<h2>No Wings Found</h2>
						</FlexDiv>
					</>
				)}
			</div>
		</>
	);
}

export function GoBackButton(props: { text?: string }) {
	const navigate = useNavigate();
	return (
		<FlexDiv
			alignItems="center"
			gap="10px"
			padding="0 0 20px  0"
			style={{ cursor: 'pointer' }}
			onClick={() => navigate(-1)}
		>
			<div
				style={{
					padding: '10px',
					borderRadius: '50%',
					backgroundColor: '#1976d2',
					width: '40px',
					height: '40px',
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center',
				}}
			>
				<ArrowBackIcon style={{ color: 'white' }} />
			</div>
			<p style={{ fontWeight: '500' }}>{'Go Back'}</p>
		</FlexDiv>
	);
}

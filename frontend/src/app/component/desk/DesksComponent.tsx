import React, { useEffect, useState } from 'react';
import './desk.css';
import { FlexDiv } from '../../constant';
import Log from '../../../vendor/utils';
import { Alert, Button, FormControl, InputLabel, MenuItem, Select } from '@mui/material';
import { ModalComponent } from '../common-component/ModalComponent';
import DeleteIcon from '@mui/icons-material/Delete';
import Checkbox, { checkboxClasses } from '@mui/material/Checkbox';
import { useAppDispatch, useAppSelector } from '../../states/store/hooks';
import { getAllLocationAsync } from '../../states/store/feature/location.slice';
import { Desk, DeskLocation, DeskRequest, Floor, Wing } from 'MyModels';
import { getAllFloorWithLocationAsync } from '../../states/store/feature/floor.slice';
import { getAllWingForLocationAndFloor } from '../../states/store/feature/wing.slice';
import {
	createDeskAsync,
	deleteDeskAsync,
	getAllSeatsAsync,
	getDesks,
	resetDeskState,
	updateDeskAsync,
	updateDeskOrderAsync,
} from '../../states/store/feature/desk.slice';
import { toast, ToastContainer } from 'react-toastify';
import { LoadingButton } from '../common-component/LoaderComponent';
import tokenService from '../../services/token.service';
import { createRequestAsync, getRequests } from '../../states/store/feature/request.slice';
import { BookDeskForm } from '../forms/BookDeskForm';
import { AddDeskForm, DeskArrangement } from '../forms/AddDeskForm';
import { MakeItemList } from '../forms/UpdateRequest';
import { DragDropContext, Droppable, Draggable, DroppableProps } from '@hello-pangea/dnd';
import DragIndicatorIcon from '@mui/icons-material/DragIndicator';
import { AppContext } from '../../../App';
import { ChartComponent } from '../common/ChartComponent';
import { DeleteDeskType } from './SeatArrangement';

const TAG = 'Inside desk component';

const Seat = (props: {
	seat: Desk.Type;
	checked: any;
	onChange: (e: any) => void;
	floor?: Floor.Type;
	location?: DeskLocation;
	wing?: Wing.Type;
}): JSX.Element => {
	return (
		<div className="col-1 col-md-1">
			<Checkbox
				onChange={e => props.onChange(e.target.value)}
				color="primary"
				checked={!!props.checked}
				disableRipple
				inputProps={{
					'aria-labelledby': props.seat.name,
				}}
				// disabled={!props.seat.isSelectable}
				sx={{
					pl: 0,
					'& .MuiSvgIcon-root': { fontSize: 30 },
					[`&, &.${checkboxClasses.checked}`]: {
						// color: !props.seat.isSelectable ? 'red' : 'green',
					},
				}}
				icon={
					<CheckedIcon
						seatNo={props.seat.name}
						selectable={props.seat.isSelectable}
						color={!props.seat.isSelectable ? '#B2B2B2' : '#a6f6bb'}
					/>
				}
				checkedIcon={
					<CheckedIcon
						seatNo={props.seat.name}
						selectable={props.seat.isSelectable}
						color={!props.seat.isSelectable ? '#B2B2B2' : '#a6f6bb'}
					/>
				}
			/>
		</div>
	);
};

const CheckedIcon = (props: { seatNo: string; color?: string; selectable?: boolean }) => (
	<div
		className="checked_custom_icon"
		style={{
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center',
			width: '35px',
			height: '38px',
			background: props.color,
			borderRadius: '8px',
			boxShadow: props.selectable ? '0 3px 10px rgb(0 0 0 / 0.6)' : '0 3px 8px rgb(0 0 0 / 0.3)',
		}}
	>
		<p style={{ fontSize: 'small', fontWeight: 'bold', padding: '2px', color: '#161835' }}>{props.seatNo}</p>{' '}
	</div>
);

export interface DeskRowComponent {
	orderIndex: number;
	seats: Desk.Type[];
	columName: string;
}

const getListStyle = (isDraggingOver: any) => ({
	width: '100%',
});

const reorder = (list: any, startIndex: any, endIndex: any) => {
	const result = Array.from(list);
	const [removed] = result.splice(startIndex, 1);
	result.splice(endIndex, 0, removed);

	return result;
};
const getItemStyle = (isDragging: any, draggableStyle: any) => ({
	userSelect: 'none',
	background: isDragging ? '#e6ffe6' : 'none',
	...draggableStyle,
});

const StrictModeDroppable = ({ children, ...props }: DroppableProps) => {
	const [enabled, setEnabled] = useState(false);
	useEffect(() => {
		const animation = requestAnimationFrame(() => setEnabled(true));
		return () => {
			cancelAnimationFrame(animation);
			setEnabled(false);
		};
	}, []);
	if (!enabled) {
		return null;
	}
	return <Droppable {...props}>{children}</Droppable>;
};

export const DesksComponent = () => {
	const socket = React.useContext(AppContext);
	const dispatch = useAppDispatch();
	const [openAction, setOpenAction] = useState<boolean>(false);
	const [openDeleteAction, setOpenDeleteAction] = useState<DeleteDeskType | undefined>();
	const { error, isDeskAdded, isDeskDeleted, isDeskUpdated } = useAppSelector(getDesks);
	const { error: requestError, isRequestCreated } = useAppSelector(getRequests);
	const [Show, setShow] = useState<boolean>(false);
	useEffect(() => {
		const timeId1 = setTimeout(() => {
			setShow(false);
		}, 3000);

		return () => {
			clearTimeout(timeId1);
		};
	}, [Show]);

	const [deskArrangement, setDeskArrangement] = useState<DeskRowComponent[]>([]);

	const onSubmit = async (data: DeskArrangement) => {
		dispatch(createDeskAsync(data))
			.unwrap()
			.then(res => {
				Log.d(TAG, res);
				// setOpenAction(false);
				return toast.success('Desk added!!');
			})
			.catch(error => {
				Log.d(TAG, error);
				setShow(true);
			});
	};

	const onDeleteRow = async (data: DeleteDeskType): Promise<any> => {
		Log.d(TAG, data);
		return dispatch(deleteDeskAsync(data))
			.unwrap()
			.then(() => {
				return toast.success('Desk Deleted!');
			})
			.catch((err: any) => {
				Log.d(TAG, err);
				setShow(true);
			});
	};

	const [allLocation, setAllLocation] = useState<DeskLocation[]>();
	const [selectedLocation, setSelectedLocation] = useState<DeskLocation | undefined>();
	const [selectedFloor, setSelectedFloor] = useState<Floor.Type | undefined>();
	const [selectedWing, setSelectedWing] = useState<Wing.Type | undefined>();
	const [allFloor, setAllLFloor] = useState<Floor.Type[]>();
	const [wings, setWings] = useState<Wing.Type[] | undefined>();

	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		if (e.target.name === 'location') {
			onLocationChange(e.target.value as any);
			setSelectedLocation(e.target.value as any);
			setWings(undefined);
		}
		if (e.target.name === 'floor') {
			onFloorChange(selectedLocation!, e.target.value as any);
			setSelectedFloor(e.target.value as any);
		}
	};

	const onLocationChange = async (data: DeskLocation) => {
		setSelectedFloor(undefined);
		setSelectedWing(undefined);
		dispatch(getAllFloorWithLocationAsync({ locationId: data._id }))
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res);
				setAllLFloor(res.allFloor);
			})
			.catch(error => {
				Log.d(TAG, error);
			});
	};

	const onFloorChange = async (location: DeskLocation, data: Floor.Type) => {
		setSelectedWing(undefined);
		dispatch(getAllWingForLocationAndFloor({ location: location._id, floor: data._id! }))
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res);
				setWings(res.allWing);
			})
			.catch(error => {
				Log.d(TAG, error);
			});
	};

	useEffect(() => {
		socket.socketContext.on(`update-message`, (notification: any) => {
			console.log('update-message', notification);
			if (!!notification) {
				setDeskArrangement(editCopy(deskArrangement, notification.seat, notification.selectable));
			}
		});

		return () => {
			socket.socketContext.off('update-message');
		};
	}, [deskArrangement, socket.socketContext]);

	const editCopy = (arr: DeskRowComponent[], id: string, val: any): DeskRowComponent[] => {
		return arr.map((item: DeskRowComponent) => ({
			...item,
			seats: item.seats.map(e => {
				if (e._id === id) {
					return { ...e, isSelectable: val };
				} else return e;
			}),
		}));
	};

	useEffect(() => {
		if (isDeskAdded) setOpenAction(false);
		if (isDeskDeleted) setOpenDeleteAction(undefined);
		if (isDeskUpdated) setOpenBookDeskForAdmin(null);
		if (isRequestCreated) setOpenBookDesk(null);
		if (!!selectedFloor && !!selectedLocation && selectedWing)
			if (
				selectedFloor!._id !== undefined &&
				selectedWing!._id !== undefined &&
				selectedLocation!._id !== undefined
			) {
				dispatch(
					getAllSeatsAsync({
						location: selectedLocation!._id!,
						floor: selectedFloor!._id!,
						wing: selectedWing!._id!,
					})
				)
					.unwrap()
					.then((res: any) => {
						Log.d(TAG, res);
						return setDataInState(res.allSeat);
					})
					.catch(error => {
						Log.d(TAG, error);
					});
			}
		setDeskArrangement([]);
		setSelected(null);
	}, [
		dispatch,
		selectedWing,
		selectedFloor,
		selectedLocation,
		isDeskAdded,
		isDeskDeleted,
		isDeskUpdated,
		isRequestCreated,
	]);

	const setDataInState = (data: any) => {
		if (!!data) {
			const unique = [...new Set(data.map((item: any) => item.row))];
			let list: Desk.Type[][] = [];
			let newRowDesk: DeskRowComponent[] = [];
			for (let i = 0; i < unique.length; i++) {
				let listOfSeats: Desk.Type[] = [];
				data.map((e: any) => {
					if (unique[i] === e.row) {
						return listOfSeats.push(e);
					} else return null;
				});
				list.push([...listOfSeats.sort((a, b) => a.name.localeCompare(b.name, 'en', { numeric: true }))]);
			}
			Log.d(TAG, list);
			for (let i = 0; i < unique.length; i++) {
				for (let j = 0; j < list.length; j++) {
					if (Array.from(list[j][0].name)[0] === (unique[i] as any)) {
						newRowDesk.push({
							seats: list[j],
							columName: unique[i] as any,
							orderIndex: list[j][0].orderIndex,
						});
					}
				}
			}
			setDeskArrangement(newRowDesk.sort((a, b) => a.orderIndex - b.orderIndex));
		}
		if (data.length === 0) setDeskArrangement([]);
	};

	useEffect(() => {
		dispatch(resetDeskState());
		dispatch(getAllLocationAsync())
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res);
				setAllLocation(res.allLocation);
			})
			.catch(error => {
				Log.d(TAG, error);
			});
	}, [dispatch]);

	const [user, setUser] = useState<any>();
	const [selected, setSelected] = useState(null);
	const [openBookDesk, setOpenBookDesk] = useState<Desk.Type | null>();
	const [openBookDeskForAdmin, setOpenBookDeskForAdmin] = useState<any>();
	const [availability, setAvailability] = useState<string>('');
	const [catchOnHover, setCatchOnHover] = useState<Desk.Type | null>();
	const [availableToDrag, setAvailableToDrag] = useState<boolean>(true);
	function onChange(i: any) {
		setSelected(prev => (i === prev ? null : i));
		if (i !== selected) {
			if (user.role !== 'admin') {
				if (!i.isSelectable && !!i.bookedBy) {
					setCatchOnHover(i);
				} else setOpenBookDesk(i);
			} else if (user.role === 'admin') {
				if (!i.isSelectable && i.bookedBy) {
					setCatchOnHover(i);
				} else {
					dispatch(resetDeskState());
					setOpenBookDeskForAdmin(i);
				}
			}
		}
		Log.d(TAG, i);
	}

	const onBookDeskRequest = async (data: DeskRequest.Type): Promise<any> => {
		Log.d(TAG, data);
		return dispatch(createRequestAsync(data))
			.unwrap()
			.then(res => {
				setOpenBookDesk(null);
				return toast.success('Request Created!');
			})
			.catch(err => {
				Log.d(TAG, err);
				setShow(true);
			});
	};

	const onChangeDeskProperty = async (data: Desk.Type): Promise<any> => {
		Log.d(TAG, data);
		return dispatch(updateDeskAsync(data))
			.unwrap()
			.then(res => {
				setOpenBookDeskForAdmin(null);
				return toast.success('Desk Updated!');
			})
			.catch(err => {
				Log.d(TAG, err);
				setShow(true);
			});
	};

	useEffect(() => {
		setUser(tokenService.getUser());
	}, []);

	const onDragEnd = async (result: any) => {
		// dropped outside the list

		if (!result.destination) {
			return;
		}

		const items = reorder(deskArrangement, result.source.index, result.destination.index);
		const modifiedItems: any[] = [];
		items.map((e: any, index) => modifiedItems.push({ ...e, orderIndex: index }));
		Log.d(TAG, modifiedItems);
		setDeskArrangement(modifiedItems as any);
		await setOrderIndexOfDesk(modifiedItems as any);
	};

	const setOrderIndexOfDesk = async (data: DeskRowComponent[]): Promise<any> => {
		setAvailableToDrag(false);
		dispatch(updateDeskOrderAsync(data))
			.unwrap()
			.then(() => {
				setAvailableToDrag(true);
				return toast.success('Row Order Updated!');
			})
			.catch((err: any) => {
				Log.d(TAG, err);
			});
	};

	return (
		<>
			{!!catchOnHover && (
				<ModalComponent
					children={
						<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
							<MakeItemList
								data={[
									{
										itemName: 'Name',
										value:
											catchOnHover.bookedBy?.firstName +
											' ' +
											catchOnHover.bookedBy?.middleName +
											' ' +
											catchOnHover.bookedBy?.lastName,
									},
									{
										itemName: 'Email',
										value: catchOnHover.bookedBy?.email,
									},
									{
										itemName: 'Domain',
										value: catchOnHover.bookedBy?.domain,
									},
									{
										itemName: 'Designation',
										value: catchOnHover.bookedBy?.position?.name,
									},
									{
										itemName: 'Department',
										value: catchOnHover.bookedBy?.department?.name,
									},
									{
										itemName: 'Project',
										value: catchOnHover.bookedBy?.project?.name,
									},
									{
										itemName: 'Location',
										value: catchOnHover.location.name + ' ' + catchOnHover.floor.name,
									},
									{
										itemName: 'Seat',
										value: catchOnHover.wing.name + ' ' + catchOnHover.name,
									},
								]}
							/>

							<Button
								variant="contained"
								onClick={() => {
									setCatchOnHover(null);
									setSelected(null);
								}}
							>
								Close
							</Button>
						</FlexDiv>
					}
					heading={`User Data!`}
					open={!!catchOnHover}
				/>
			)}
			{!!openBookDeskForAdmin && (
				<ModalComponent
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								<FlexDiv gap="20px" align="center" direction="column" justify="center">
									<FormControl required fullWidth>
										<InputLabel id="reg_availability_id">Selectable</InputLabel>
										<Select
											required
											type="text"
											name="availability"
											id="availability_id"
											label="Selectable"
											value={availability}
											onChange={(e: any) => setAvailability(e.target.value)}
										>
											<MenuItem value={'true'}>True</MenuItem>
											<MenuItem value={'false'}>False</MenuItem>
										</Select>
									</FormControl>
									<FlexDiv gap="20px">
										<LoadingButton
											onClick={e =>
												onChangeDeskProperty({
													...openBookDeskForAdmin,
													isSelectable: availability === 'true',
												})
											}
										>
											<Button variant="contained" color="error">
												Change
											</Button>
										</LoadingButton>

										<Button
											variant="contained"
											onClick={() => {
												setOpenBookDeskForAdmin(null);
												setSelected(null);
											}}
										>
											Cancel
										</Button>
									</FlexDiv>
								</FlexDiv>
							</FlexDiv>
						</>
					}
					heading={`Change availability!`}
					open={!!openBookDeskForAdmin}
				/>
			)}
			{!!openBookDesk && (
				<ModalComponent
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								<FlexDiv gap="20px" align="center" justify="center" direction="column">
									{!!Show && requestError.length > 1 ? (
										<FlexDiv justify="center" align="center" alignItems="center" width="full">
											<Alert style={{ maxWidth: '400px' }} severity="error">
												{requestError}
											</Alert>
										</FlexDiv>
									) : (
										<></>
									)}
									{!openBookDesk.isSelectable ? (
										<FlexDiv
											direction="column"
											gap="20px"
											justify="center"
											align="center"
											alignItems="center"
										>
											<h3>This Desk is Disabled By Admin</h3>
											<Button
												onClick={() => {
													setOpenBookDesk(null);
													setSelected(null);
												}}
												variant="contained"
											>
												Close
											</Button>
										</FlexDiv>
									) : (
										<BookDeskForm
											user={user.userObject}
											seat={openBookDesk}
											onSubmit={e => onBookDeskRequest(e)}
											onCancel={() => {
												setSelected(null);
												setOpenBookDesk(null);
											}}
										/>
									)}
								</FlexDiv>
							</FlexDiv>
						</>
					}
					heading={!openBookDesk.isSelectable ? '' : 'Fill these details!'}
					open={!!openBookDesk}
				/>
			)}
			{!!openDeleteAction && (
				<ModalComponent
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{!!Show && error.length > 1 ? (
									<Alert style={{ maxWidth: '400px' }} severity="error">
										{error}
									</Alert>
								) : (
									<></>
								)}
								<FlexDiv gap="20px" align="center" justify="center">
									<LoadingButton onClick={(e: any) => onDeleteRow(openDeleteAction)}>
										<Button variant="contained" color="error">
											Delete
										</Button>
									</LoadingButton>

									<Button variant="contained" onClick={() => setOpenDeleteAction(undefined)}>
										Cancel
									</Button>
								</FlexDiv>
							</FlexDiv>
						</>
					}
					heading={`Delete This Row? ${openDeleteAction.row}`}
					open={!!openDeleteAction}
				/>
			)}
			{!!openAction && (
				<ModalComponent
					size="med"
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{!!Show && error.length > 1 ? (
									<Alert style={{ maxWidth: '400px' }} severity="error">
										{error}
									</Alert>
								) : (
									<></>
								)}
								<AddDeskForm
									floor={selectedFloor!._id!}
									wing={selectedWing!._id!}
									location={selectedLocation!._id!}
									onCancel={() => setOpenAction(false)}
									onSubmit={(e: any) => onSubmit(e)}
								/>
							</FlexDiv>
						</>
					}
					heading={`Add Desks`}
					open={openAction}
				/>
			)}
			<FlexDiv direction="column" gap="20px">
				<FlexDiv style={{ width: '100%' }} justify="center">
					<FlexDiv style={{ width: '100%' }}>
						<h3>Seating Arrangement</h3>
					</FlexDiv>
					<FlexDiv style={{ width: '100%' }} gap="20px" className="desk_input_forms">
						<FormControl required fullWidth>
							<InputLabel id="reg_location_id">Location</InputLabel>
							<Select
								required
								name="location"
								id="location_id"
								label="Location"
								value={selectedLocation?.name}
								onChange={(e: any) => handleChange(e)}
							>
								{!!allLocation &&
									allLocation.length > 0 &&
									allLocation.map((e, index) => (
										<MenuItem value={e as any} key={e._id}>
											{e.name}
										</MenuItem>
									))}
							</Select>
						</FormControl>
						<FormControl required fullWidth>
							<InputLabel id="reg_floor_id">Floor</InputLabel>
							<Select
								required
								name="floor"
								id="floor_id"
								label="Floor"
								disabled={!allFloor}
								value={selectedFloor?.name}
								onChange={(e: any) => handleChange(e)}
							>
								{!!allFloor &&
									allFloor.length > 0 &&
									allFloor.map((e, index) => (
										<MenuItem value={e as any} key={e._id}>
											{e.name}
										</MenuItem>
									))}
								{!!allFloor && allFloor.length < 1 && (
									<FlexDiv style={{ padding: '12px' }}>No Data </FlexDiv>
								)}
							</Select>
						</FormControl>
						<FormControl required fullWidth>
							<InputLabel id="reg_wing_id">Wing</InputLabel>
							<Select
								required
								name="wing"
								id="Wing_id"
								label="Wing"
								disabled={!wings || !allFloor}
								value={selectedWing?.name}
								onChange={e => setSelectedWing(e.target.value as any)}
							>
								{!!wings &&
									wings.length > 0 &&
									wings.map((e, index) => (
										<MenuItem value={e as any} key={e._id}>
											{e.name}
										</MenuItem>
									))}
								{!!wings && wings.length < 1 && <FlexDiv style={{ padding: '12px' }}>No Data </FlexDiv>}
							</Select>
						</FormControl>
					</FlexDiv>
				</FlexDiv>
				{selectedFloor && selectedLocation && selectedWing && (
					<FlexDiv justify="space-between" className="desk_main_div_content" width="full">
						<FlexDiv direction="column" gap="20px">
							<div className="desk-complex">
								<div className="container row movie-layout">
									<div className="desk-column-1">
										{deskArrangement.length <= 0 && <h3>No Desk available</h3>}
										{deskArrangement.length > 0 && (
											<DragDropContext onDragEnd={e => onDragEnd(e)}>
												<StrictModeDroppable droppableId="droppable-0">
													{(provided, snapshot) => (
														<div
															{...provided.droppableProps}
															ref={provided.innerRef}
															style={getListStyle(snapshot.isDraggingOver)}
														>
															{deskArrangement.map((e: DeskRowComponent, index) => (
																<Draggable
																	key={e.columName}
																	draggableId={`${
																		e.columName + '395dd91312589a8584aa0451'
																	}`}
																	index={index}
																	isDragDisabled={
																		!!user &&
																		user.role !== 'admin' &&
																		!!availableToDrag
																	}
																>
																	{(provided, snapshot) => (
																		<div
																			ref={provided.innerRef}
																			{...provided.draggableProps}
																			{...provided.dragHandleProps}
																			style={getItemStyle(
																				snapshot.isDragging,
																				provided.draggableProps.style
																			)}
																		>
																			<FlexDiv
																				ref={provided.innerRef}
																				gap="10px"
																				key={e.columName}
																			>
																				<FlexDiv
																					gap="20px"
																					justify="center"
																					align="center"
																					style={{ marginTop: '15px' }}
																				>
																					{!!user &&
																						user.role === 'admin' && (
																							<FlexDiv gap="10px">
																								<DeleteIcon
																									sx={{
																										color: '#d32f2f',
																										cursor: 'pointer',
																									}}
																									onClick={() => {
																										setOpenDeleteAction(
																											{
																												location:
																													selectedLocation._id!,
																												floor: selectedFloor._id!,
																												wing: selectedWing._id!,
																												row: e.columName,
																											}
																										);
																										dispatch(
																											resetDeskState()
																										);
																									}}
																								/>
																								<DragIndicatorIcon
																									style={{
																										cursor: 'grab',
																									}}
																								/>
																							</FlexDiv>
																						)}
																					<h3 style={{ marginRight: '10px' }}>
																						{e.columName}
																					</h3>
																				</FlexDiv>

																				{e.seats.map((f, index) => {
																					return (
																						<FlexDiv key={index}>
																							<FlexDiv
																								direction="column"
																								justify="center"
																								alignItems="center"
																								style={{
																									marginBottom:
																										'12px',
																								}}
																							>
																								<Seat
																									key={index}
																									checked={
																										selected === f
																									}
																									onChange={() =>
																										onChange(f)
																									}
																									seat={f}
																								/>
																							</FlexDiv>
																						</FlexDiv>
																					);
																				})}
																			</FlexDiv>
																		</div>
																	)}
																</Draggable>
															))}
															{provided.placeholder}
														</div>
													)}
												</StrictModeDroppable>
											</DragDropContext>
										)}
									</div>
								</div>
							</div>

							{!!user && user.role === 'admin' && (
								<FlexDiv justify="start" style={{ marginTop: '20px' }}>
									<Button
										variant="contained"
										onClick={() => {
											setOpenAction(true);
											dispatch(resetDeskState());
										}}
									>
										Add desk
									</Button>
								</FlexDiv>
							)}
						</FlexDiv>
						{!!user && user.role === 'admin' && (
							<FlexDiv direction="column" gap="20px" justify="start">
								<ChartComponent
									data={[
										{
											name: 'Available Seat',
											value: deskArrangement
												.map(e => {
													let count = 0;
													e.seats.map(f => {
														if (f.isSelectable === true) {
															return count++;
														}
														return count;
													});
													return count;
												})
												.reduce((a, b) => a + b, 0),
										},
										{
											name: 'Occupied Seat',
											value: deskArrangement
												.map(e => {
													let count = 0;
													e.seats.map(f => {
														if (f.isSelectable === false) {
															return count++;
														}
														return count;
													});
													return count;
												})
												.reduce((a, b) => a + b, 0),
										},
									]}
									width={400}
									height={400}
								/>
								<FlexDiv direction="column" gap="10px">
									<span style={{ width: '30px', height: '8px', backgroundColor: '#ffcb68' }}></span>{' '}
									<p>Available Seat</p>
									<span style={{ width: '30px', height: '8px', backgroundColor: '#ff5983' }}></span>
									<p>Occupied Seat</p>
								</FlexDiv>
							</FlexDiv>
						)}
					</FlexDiv>
				)}
			</FlexDiv>
			<ToastContainer closeOnClick={true} />
		</>
	);
};

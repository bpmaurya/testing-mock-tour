import { CircularProgress } from '@mui/material';
import { useState } from 'react';
import Log from '../../../vendor/utils';

export const LoadingButton = (props: {
	onClick: (e: any) => Promise<void>;
	children: JSX.Element;
	disabled?: boolean;
}) => {
	const [loading, setLoading] = useState<boolean>(false);

	const handleClick = async (e: any) => {
		setLoading(true);
		try {
			await props.onClick(e);
		} catch (error) {
			Log.d('error');
		} finally {
			setLoading(false);
		}
	};

	return (
		<>
			{!loading ? (
				<div onClick={e => !props.disabled && handleClick(e)}>{props.children}</div>
			) : (
				<CircularProgress />
			)}
		</>
	);
};

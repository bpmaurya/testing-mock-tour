import {
	Avatar,
	Box,
	ClickAwayListener,
	Fade,
	ListItem,
	ListItemAvatar,
	ListItemButton,
	ListItemText,
	Paper,
	Popper,
	PopperPlacementType,
} from '@mui/material';
import React, { ReactElement } from 'react';
import styled from '@emotion/styled';
import { Notification } from 'MyModels';
import NotificationsIcon from '@mui/icons-material/Notifications';

interface Props {
	content: ReactElement;
	children: ReactElement;
	open: boolean;
	onClose?: () => void;
	arrow?: boolean;
	placement?: PopperPlacementType;
}

const ArrowStyled = styled.span`
	z-index: 2000;
	overflow: hidden;
	position: absolute;
	width: 1em;
	height: 0.71em;
	box-sizing: border-box;
	'&::before': {
		content: '""';
		margin: auto;
		display: block;
		width: 100%;
		height: 100%;
		box-shadow: none;
		background-color: white;
		transform: rotate(45deg);
	}
`;

export const NotificationDiv = ({
	placement = 'top',
	arrow = true,
	open,
	onClose = () => {},
	content,
	children,
}: Props) => {
	const [arrowRef, setArrowRef] = React.useState<HTMLElement | null>(null);
	const [childNode, setChildNode] = React.useState<HTMLElement | null>(null);

	return (
		<div>
			{React.cloneElement(children, { ...children.props, ref: setChildNode })}
			<Popper
				sx={{
					zIndex: 2000,
				}}
				open={open}
				anchorEl={childNode}
				placement={placement}
				transition
				modifiers={[
					{
						name: 'arrow',
						enabled: true,
						options: {
							element: arrowRef,
						},
					},
					{
						name: 'preventOverflow',
						enabled: true,
						options: {
							boundariesElement: 'window',
						},
					},
				]}
			>
				{({ TransitionProps }) => (
					<Fade {...TransitionProps} timeout={350}>
						<Paper>
							<ClickAwayListener onClickAway={onClose}>
								<Paper sx={{ maxWidth: 1000 }}>
									{arrow ? <ArrowStyled ref={setArrowRef} /> : null}
									<Box sx={{ padding: 'none' }}>{content}</Box>
								</Paper>
							</ClickAwayListener>
						</Paper>
					</Fade>
				)}
			</Popper>
		</div>
	);
};

type MenuDivStyleProps = {
	minWidth?: string;
};
const MenuDivStyle = styled.div`
	z-index: 2000;
	overflow: visible;
	min-width: ${(props: MenuDivStyleProps) => props.minWidth ?? '250px'};
	width: auto;
	// border: 2px solid #1976d2;
	background: white;
	position: relative;
`;
const TriangleDivStyle = styled.div`
	z-index: 2000;
	border-left: 25px solid transparent;
	border-right: 3px solid transparent;
	position: absolute;
	top: 8px;
	right: 10px;
	display: block;
	width: 24px;
	height: 26px;
	background-color: white;
	transform: translateY(-50%) rotate(45deg);
`;
export const DivWithArrowIcon = (props: { divContent: JSX.Element; minWidth?: string }): JSX.Element => (
	<MenuDivStyle minWidth={props.minWidth}>
		<TriangleDivStyle />
		{props.divContent}
	</MenuDivStyle>
);

export const NotificationMessage = (props: { notification: Notification.Type[] }) => {
	return (
		<>
			{props.notification.length !== 0 ? (
				<>
					{props.notification.map((e, index) => (
						<ListItemButton style={{ margin: 0, padding: 0 }} key={index}>
							<ListItem style={{ margin: 0, padding: 0 }}>
								<ListItemAvatar>
									<Avatar>
										<NotificationsIcon />
									</Avatar>
								</ListItemAvatar>
								<ListItemText
									primary={`Request ${e.request.status} `}
									secondary={e.createdAt?.toLocaleString().split('T')[0]}
								/>
							</ListItem>
						</ListItemButton>
					))}
				</>
			) : (
				<></>
			)}
		</>
	);
};

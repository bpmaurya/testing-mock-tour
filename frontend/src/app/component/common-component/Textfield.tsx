import { TextField } from '@mui/material';

export function Textfield(props: {
	type: string;
	name: string;
	id: string;
	label: string;
	value?: string | number;
	onChange?: (e: any) => void;
	onFocus?: () => void;
	onBlur?: () => void;
	error?: boolean;
	errorText?: string;
	shrink?: boolean;
	autoFocus?: boolean;
	maxLength?: number;
	required?: boolean;
	disable?: boolean;
}) {
	return (
		<TextField
			required={props.required}
			autoFocus={!!props.autoFocus}
			InputLabelProps={{
				shrink: props.shrink,
			}}
			inputProps={{
				maxLength: props.maxLength,
			}}
			name={props.name}
			id={props.id}
			label={props.label}
			variant="outlined"
			type={props.type}
			value={props.value}
			onChange={props.onChange}
			error={!!props.error}
			helperText={`${!!props.errorText ? props.errorText : ''}`}
			onFocus={props.onFocus}
			onBlur={props.onBlur}
			autoComplete="off"
			disabled={props.disable}
		/>
	);
}

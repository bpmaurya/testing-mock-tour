import * as React from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import { FlexDiv } from '../../constant';
import { ModalStyle } from '../common/styles';

export function ModalComponent(props: {
	size?: 'small' | 'med';
	children: JSX.Element;
	heading: string;
	open: boolean;
}) {
	return (
		<Modal open={props.open}>
			<Box sx={{ ...ModalStyle, width: `${props.size === 'med' ? 800 : 400}` }} style={{ border: 'none' }}>
				<FlexDiv justify="center" align="center" gap="20px" direction="column" alignItems="center">
					<h3>{props.heading} </h3>
					{props.children}
				</FlexDiv>
			</Box>
		</Modal>
	);
}

import { Button } from '@mui/material';
import { useState } from 'react';
import { FlexDiv } from '../../constant';
import { Textfield } from '../common-component/Textfield';

export function UserSettingComponent() {
	const [open, setOpen] = useState<boolean>();
	return (
		<>
			{!open && (
				<Button variant="contained" onClick={() => setOpen(true)}>
					Change Password
				</Button>
			)}

			{!!open && (
				<FlexDiv direction="column" gap="20px">
					<Textfield type="password" name="password" id="current_password" label="Current Password" />
					<Textfield type="password" name="new password" id="new_password" label="New Password" />
					<Textfield type="password" name="confirm password" id="confirm_password" label="Confirm Password" />
					<FlexDiv gap="20px" justify="center" align="center">
						<Button variant="contained" color="primary" onClick={() => setOpen(false)}>
							Submit
						</Button>
						<Button variant="contained" color="error" onClick={() => setOpen(false)}>
							Cancel
						</Button>
					</FlexDiv>
				</FlexDiv>
			)}
		</>
	);
}

import { Alert, Box, Button, CircularProgress } from '@mui/material';
import { TableComponent } from '../common/TableComponent';
import { FlexDiv } from '../../constant';
import React, { useEffect, useState } from 'react';
import { ModalComponent } from '../common-component/ModalComponent';
import tokenService from '../../services/token.service';
import Log from '../../../vendor/utils';
import {
	deleteUserAsync,
	getAllUserAsync,
	getUsers,
	reset,
	updateUserAsync,
} from '../../states/store/feature/user.slice';
import { useAppDispatch, useAppSelector } from '../../states/store/hooks';
import { HeadCell } from '../../core/do';
import { UserFormComponent } from '../forms/UserForm';
import { IUser, UserRegister } from 'MyModels';
import { registerAsync, selectAuth } from '../../states/store/feature/auth';
import { toast, ToastContainer } from 'react-toastify';
import { LoadingButton } from '../common-component/LoaderComponent';

const TAG = 'inside-user-component';

const headCells: HeadCell<ExtendedTypeForTable>[] = [
	{
		id: 'full_name',
		numeric: false,
		disablePadding: false,
		label: 'Name',
	},
	{
		id: 'email',
		numeric: false,
		disablePadding: false,
		label: 'Email',
	},
	{
		id: 'department_name',
		numeric: false,
		disablePadding: false,
		label: 'Department',
	},
	{
		id: 'domain',
		numeric: false,
		disablePadding: false,
		label: 'Domain',
	},
	{
		id: 'position_name',
		numeric: false,
		disablePadding: false,
		label: 'Position',
	},
	{
		id: 'role',
		numeric: false,
		disablePadding: false,
		label: 'Role',
	},
];

type ExtendedTypeForTable = IUser.Type & {
	department_name: string;
	position_name: string;
	full_name: string;
};

export function UserComponent() {
	const dispatch = useAppDispatch();
	const [openAction, setOpenAction] = React.useState<boolean>(false);
	const [allUser, setAllUsers] = useState<any>([]);
	const { error, success, userRegisterSuccess } = useAppSelector(selectAuth);
	const { updateSuccess, updateError, userUpdate, userDeleted, userDeletedMessage } = useAppSelector(getUsers);
	const [openEditAction, setOpenEditAction] = React.useState<UserRegister | undefined>();
	const [openDeleteAction, setOpenDeleteAction] = React.useState<UserRegister | undefined>();
	useEffect(() => {
		if (!!userRegisterSuccess) setOpenAction(false);
		if (userUpdate) setOpenEditAction(undefined);
		if (userDeleted) setOpenDeleteAction(undefined);
		dispatch(reset());

		dispatch(getAllUserAsync())
			.unwrap()
			.then((res: any) => {
				Log.d(TAG, res.alluser);
				let data = res.alluser;
				let all_user: ExtendedTypeForTable[] = [];
				data.map((e: any) => {
					return all_user.push({
						...e,
						full_name: `${e.firstName} ${e.middleName} ${e.lastName}`,
						position_name: e.position.name,
						department_name: e.department.name,
					});
				});
				setAllUsers(all_user);
			})
			.catch((error: string) => {
				Log.d(TAG, error);
			});
	}, [dispatch, userUpdate, userRegisterSuccess, userDeleted]);

	const [user, setUser] = useState<any>();
	useEffect(() => {
		const user = tokenService.getUser();
		if (!!user) setUser(user);
	}, []);

	const [loading, setLoading] = useState(false);

	const handleRegister = async (data: UserRegister): Promise<any> => {
		setLoading(true);
		return dispatch(registerAsync(data))
			.unwrap()
			.then(res => {
				setOpenAction(false);
				return toast.success('User Registered!');
			})
			.catch((error: string) => {
				Log.d(TAG, error);
				setShow(true);
			});
	};

	const handleUserDelete = async (objectId: string): Promise<any> => {
		return dispatch(deleteUserAsync({ id: objectId }))
			.unwrap()
			.then(res => {
				Log.d(TAG, res);
				return toast.success('User Deleted!');
			})
			.catch(err => {
				Log.d(TAG, err);
			});
	};

	const handleUpdateUser = async (data: IUser.Type): Promise<any> => {
		return dispatch(updateUserAsync(data))
			.unwrap()
			.then(res => {
				Log.d(TAG, res);
				setOpenEditAction(undefined);
				return toast.success('User update!');
			})
			.catch(error => {
				Log.d(TAG, error);
			});
	};

	const [show, setShow] = useState<boolean>(false);
	useEffect(() => {
		const timeId = setTimeout(() => {
			setShow(false);
		}, 3000);

		return () => {
			clearTimeout(timeId);
		};
	}, [show]);

	return (
		<>
			{!!openAction && (
				<ModalComponent
					size="med"
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{!!show && error.length > 1 ? (
									<Box sx={{ display: 'flex', justifyContent: 'center' }}>
										<Alert style={{ maxWidth: '400px' }} severity="error">
											{error}
										</Alert>
									</Box>
								) : (
									<></>
								)}
								<UserFormComponent
									onSubmit={e => handleRegister(e)}
									handleCancel={() => setOpenAction(false)}
								/>
							</FlexDiv>
						</>
					}
					heading={`Add User `}
					open={openAction}
				/>
			)}
			{!!openEditAction && (
				<ModalComponent
					size="med"
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center" direction="column" alignItems="center">
								{updateError.length > 1 ? (
									<Box sx={{ display: 'flex', justifyContent: 'center' }}>
										<Alert style={{ maxWidth: '400px' }} severity="error">
											{updateError}
										</Alert>
									</Box>
								) : (
									<></>
								)}
								<UserFormComponent
									initialData={openEditAction}
									onSubmit={e => handleUpdateUser(e)}
									handleCancel={() => setOpenEditAction(undefined)}
									onEdit
								/>
							</FlexDiv>
						</>
					}
					heading={`Edit User `}
					open={!!openEditAction}
				/>
			)}
			{!!openDeleteAction && (
				<ModalComponent
					children={
						<>
							<FlexDiv gap="20px" align="center" justify="center">
								<LoadingButton onClick={() => handleUserDelete(openDeleteAction._id!)}>
									<Button variant="contained" color="error">
										Delete
									</Button>
								</LoadingButton>
								<Button variant="contained" onClick={() => setOpenDeleteAction(undefined)}>
									Cancel
								</Button>
							</FlexDiv>
						</>
					}
					heading={`Delete this User? ${openDeleteAction.firstName}`}
					open={!!openDeleteAction}
				/>
			)}

			<TableComponent
				data={allUser}
				tableHead={headCells}
				heading="User"
				onEdit={e => {
					setOpenEditAction(e);
					dispatch(reset());
				}}
				onDelete={e => {
					setOpenDeleteAction(e);
					dispatch(reset());
				}}
				additionalButton={
					!!user && user.role === 'admin' ? (
						<FlexDiv justify="end">
							<Button
								variant="contained"
								onClick={() => {
									setOpenAction(true);
									dispatch(reset());
								}}
							>
								Add User
							</Button>
						</FlexDiv>
					) : (
						<></>
					)
				}
			/>

			<ToastContainer closeOnClick={true} />
		</>
	);
}

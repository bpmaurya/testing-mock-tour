import { useEffect, useState } from 'react';
import { useAppDispatch } from '../states/store/hooks';
import { IUser } from 'MyModels';
import tokenService from '../services/token.service';
import { FlexDiv } from '../constant';
import ComputerIcon from '@mui/icons-material/Computer';
import AccountBalanceIcon from '@mui/icons-material/AccountBalance';
import AlternateEmailIcon from '@mui/icons-material/AlternateEmail';
import WorkHistoryIcon from '@mui/icons-material/WorkHistory';
import DomainIcon from '@mui/icons-material/Domain';
import GradeIcon from '@mui/icons-material/Grade';
import { Buffer } from 'buffer';

export function HomePage() {
	const dispatch = useAppDispatch();
	const [currentUser, setCurrentUser] = useState<IUser.Type | undefined>();
	const [image, setImage] = useState<any>();

	useEffect(() => {
		const user = tokenService.getUser();
		if (!!user) {
			setCurrentUser(user.userObject);
		}
		if (!!user.userObject.avatar) {
			const buffer = Buffer.from(user.userObject.avatar);
			const blob = new Blob([buffer]);
			const imageUrl = URL.createObjectURL(blob);
			setImage(imageUrl);
		}
	}, [dispatch]);

	return (
		<>
			{!!currentUser && (
				<FlexDiv style={{ padding: '30px' }} gap="50px" className="homepage_main_content">
					<FlexDiv direction="column" gap="20px" style={{ flexGrow: '1' }} width="full">
						<img width="150px" src={image ? image : '/user_dummy.png'} alt="imag_logo" />
						<FlexDiv direction="column">
							<h3>{currentUser.firstName + ' ' + currentUser.middleName + ' ' + currentUser.lastName}</h3>
							<p>{currentUser.position.name}</p>
						</FlexDiv>
						<FlexDiv direction="column" gap="10px">
							<FlexDiv gap="10px">
								<ComputerIcon color="primary" />
								<h4>
									Project :<span style={{ fontWeight: '400' }}>{currentUser.project.name}</span>{' '}
								</h4>
							</FlexDiv>
							<FlexDiv gap="10px">
								<AccountBalanceIcon color="primary" />
								<h4>
									Department :{' '}
									<span style={{ fontWeight: '400' }}>{currentUser.department.name}</span>{' '}
								</h4>
							</FlexDiv>
						</FlexDiv>
					</FlexDiv>
					<FlexDiv direction="column" style={{ flexGrow: '2' }}>
						<FlexDiv direction="column" gap="20px">
							<FlexDiv direction="column" gap="10px">
								<FlexDiv gap="10px">
									<AlternateEmailIcon color="primary" />
									<h4>
										Email : <span style={{ fontWeight: '400' }}>{currentUser.email}</span>{' '}
									</h4>
								</FlexDiv>
								<FlexDiv gap="10px">
									<WorkHistoryIcon color="primary" />
									<h4>
										Past Experience :{' '}
										<span style={{ fontWeight: '400' }}>{currentUser.pastExperience}</span>{' '}
									</h4>
								</FlexDiv>
							</FlexDiv>
							<FlexDiv direction="column" gap="10px">
								<FlexDiv gap="10px">
									<DomainIcon color="primary" />
									<h4>
										Domain : <span style={{ fontWeight: '400' }}>{currentUser.domain}</span>{' '}
									</h4>
								</FlexDiv>
								<FlexDiv gap="10px">
									<GradeIcon color="primary" />
									<h4>
										Authority : <span style={{ fontWeight: '400' }}>{currentUser.role}</span>{' '}
									</h4>
								</FlexDiv>
								<FlexDiv direction="column">
									<h4>About </h4>
									<p style={{ textAlign: 'justify' }}>
										Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse asperiores
										deserunt pariatur, ratione nisi autem. Libero velit, enim in molestiae ratione
										accusamus nobis tempora ab beatae reiciendis aperiam doloremque. Dolorum, nobis
										inventore! Nisi laborum magni optio dicta ducimus ipsa obcaecati incidunt eius
										error quaerat, adipisci enim dolores, unde delectus illo qui molestias
										necessitatibus eaque repellat temporibus minus ab id? Nostrum obcaecati possimus
										atque omnis et vero laudantium dolores corrupti cumque, commodi, voluptate sunt
										tenetur incidunt , voluptates modi odit veniam doloribus deserunt? Ea,
										praesentium accusantium?{' '}
									</p>
								</FlexDiv>
							</FlexDiv>
						</FlexDiv>
					</FlexDiv>
				</FlexDiv>
			)}
		</>
	);
}

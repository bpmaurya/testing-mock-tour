import { AuthState } from 'MyModels';
import Log from '../../vendor/utils';

const TAG = 'inside token service'

class TokenService {
	getLocalRefreshToken() {
		const user = this.getUser();
		return user?.refreshToken;
	}

	getLocalAccessToken() {
		const user = this.getUser();
		return user?.accessToken;
	}

	updateLocalRefreshToken(token: string) {
		const user = this.getUser();
		user.refreshToken = token;
		this.setUser(user);
	}

	updateLocalAccessToken(token: string) {
		const user = this.getUser();
		user.accessToken = token;
		this.setUser(user);
	}

	getUser() {
		const userJson = localStorage.getItem('user');
		const user = userJson !== null ? JSON.parse(userJson) : [];
		return user;
	}

	setUser(user: AuthState) {
		Log.d(TAG, JSON.stringify(user));
		localStorage.setItem('user', JSON.stringify(user));
	}

	isAdmin() {
		Log.d(TAG)
		const user = this.getUser()
		if (!!user && user.role === 'admin')
			return true
		return false
	}

	removeUser() {
		localStorage.removeItem('user');
	}
}
const tokenService = new TokenService()
export default tokenService;
import { IUser } from 'MyModels';
import Log from '../../vendor/utils';
import http from './http';
import tokenService from './token.service';

const TAG = 'inside-user-service'

export class UserService {
	async getUser() {
		return http
			.get('/api/admin/all-user', { headers: { "x-access-token": tokenService.getLocalAccessToken() } },)
			.then((response: any) => {
				Log.d(TAG, response.data)
				return response.data;
			});
	}

	async updateUser(data: IUser.Type) {
		return http
			.post('/api/user/update', {
				email: data.email,
				firstName: data.firstName,
				middleName: data.middleName,
				lastName: data.lastName,
				pastExperience: data.pastExperience,
				domain: data.domain,
				department: data.department._id!,
				position: data.position._id!,
				project: data.project._id!,
				role: data.role,
				phone: data.phone
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data;
			});
	}

	async deleteUser(objectId: string) {
		return http
			.post('/api/admin/delete-user', {
				id: objectId
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data;
			});
	}


}
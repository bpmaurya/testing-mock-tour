import { Project } from 'MyModels';
import Log from '../../vendor/utils';
import http from './http';
import tokenService from './token.service';

const TAG = 'inside-project-service'

export class ProjectService {
	async getProjects() {
		return http
			.get('/api/admin/all-projects', { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				return response.data;
			});
	}

	async createProjects(data: Project.Type) {
		return http
			.post('/api/admin/create-projects', {
				name: data.name,
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				return response.data;
			});
	}

	async deleteProject(id: string) {
		return http
			.post('/api/admin/delete-project', {
				id
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data
			})
	}

	async updateProject(data: Project.Type) {
		return http
			.post('/api/admin/update-project', {
				id: data._id,
				name: data.name,
				status: data.status
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data
			})
	}


}
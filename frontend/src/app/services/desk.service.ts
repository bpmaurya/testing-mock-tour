import { Desk } from "MyModels";
import Log from "../../vendor/utils";
import http from "./http";
import tokenService from "./token.service";
import { DeskRowComponent } from "../component/desk/DesksComponent";

const TAG = 'inside-desk-service'

export class DeskService {


	async createDesk(row: string, start: number, end: number, location: string, floor: string, wing: string, status?: boolean) {
		return http
			.post('/api/admin/create-desk', {
				row,
				start,
				end,
				location,
				floor,
				wing,
				status
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				return response.data;
			});
	}

	async getDesks(location: string, floor: string, wing: string) {
		return http
			.get(`/api/admin/all-desks/${location}/${floor}/${wing}`, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response.data)
				return response.data;
			});
	}

	async deleteDesk(location: string, wing: string, floor: string, row: string) {
		return http
			.post('/api/admin/delete-desk', {
				row,
				location,
				floor,
				wing,
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data
			})
	}

	async updateDesk(data: Desk.Type) {
		return http
			.post('/api/admin/update-desk', {
				id: data._id,
				name: data.name,
				row: data.row,
				location: data.location._id,
				floor: data.floor._id,
				wing: data.wing._id,
				isSelectable: data.isSelectable
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data
			})
	}

	async updateDeskOrder(data: DeskRowComponent[]) {
		return http
			.post('/api/admin/update-desk-index', {
				data: data
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data
			})
	}

	async createDeskBookRequest(data: Desk.Type) {
		return http
			.post('/api/admin/desk-book-request', {
				id: data._id,
				name: data.name,
				row: data.row,
				location: data.location._id,
				floor: data.floor._id,
				wing: data.wing._id,
				bookedBy: data.bookedBy,
				isSelectable: data.isSelectable
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data
			})
	}


}

import { Department } from 'MyModels';
import Log from '../../vendor/utils';
import http from './http';
import tokenService from './token.service';

const TAG = 'inside-department-service'

export class DepartmentService {
	async getDepartments() {
		return http
			.get('/api/admin/all-department', { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				return response.data;
			});
	}

	async createDepartment(data: Department.Type) {
		return http
			.post('/api/admin/create-department', {
				name: data.name,
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				return response.data;
			});
	}

	async deleteDepartment(id: string) {
		return http
			.post('/api/admin/delete-department', {
				id
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data
			})
	}

	async updateDepartment(data: Department.Type) {
		return http
			.post('/api/admin/update-department', {
				id: data._id,
				name: data.name,
				status: data.status
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data
			})
	}

}
declare module 'MyModels' {
	export interface User {
		accessToken: string;
		refreshToken: string;
		name: string;
		email: string;
		role?: 'admin' | 'staff' | 'user'
		jwt?: any;
	}

	export namespace IUser {
		export type Type = {
			_id?: string;
			email: string;
			firstName: string;
			middleName: string;
			lastName: string;
			pastExperience: number | string;
			domain: string;
			department: Department.Type,
			position: Designation.Type,
			project: Project.Type,
			role: string;
			phone: number | string;
			avatar?: { type: Buffer, data: any };
			createdAt?: Date;
			updatedAt?: Date;
		}
	}

	export type AuthState = {
		isLoggedIn: boolean;
		user: User;
		error: string;
		success: string;
		userRegisterSuccess: boolean;
	};

	export interface UserCredentials {
		email: string;
		password: string;
	};

	export interface UserRegister extends IUser.Type {
		password: string;
		passwordConf: string;
	};

	export interface DeleteUser {
		id: string
	}

	export interface DeskLocation {
		_id: string;
		name: string;
		status?: boolean;
		createdAt?: Date;
		updatedAt?: Date;
	}

	export namespace Floor {
		export type Type = {
			_id?: string;
			name: string;
			location: DeskLocation;
			status?: boolean;
			createdAt?: Date;
			updatedAt?: Date;
		}
	}

	export namespace Wing {
		export type Type = {
			_id?: string;
			name: string;
			location: DeskLocation;
			floor: Floor;
			status?: boolean;
			createdAt?: Date;
			updatedAt?: Date;
		}
	}

	export namespace Desk {
		export type Type = {
			_id?: string;
			name: string;
			row: string;
			location: DeskLocation,
			floor: Floor.Type,
			wing: Wing.Type,
			orderIndex: number,
			bookedBy?: IUser.Type,
			bookedStatus?: string,
			isSelectable?: boolean,
			status?: boolean,
			createdAt?: Date;
			updatedAt?: Date;
		}
	}

	export namespace Department {
		export type Type = {
			_id?: string;
			name: string;
			status?: boolean;
			createdAt?: Date;
			updatedAt?: Date;
		}
	}

	export namespace Project {
		export type Type = {
			_id?: string,
			name: string,
			status: boolean,
			createdAt?: Date,
			updatedAt?: Date,
		}
	}

	export namespace Designation {
		export type Type = {
			_id?: string,
			name: string,
			status: boolean,
			createdAt?: Date,
			updatedAt?: Date,
		}
	}

	export namespace DeskRequest {
		export type Type = {
			_id?: string,
			user: IUser.Type,
			seat: Desk.Type,
			reportingManager: IUser.Type,
			project: Project.Type,
			priority: string,
			description: string,
			status?: 'pending' | 'approved' | 'reject',
			createdAt?: Date,
			updatedAt?: Date,
		}
	}


	export namespace Notification {
		export type Type = {
			_id?: string,
			user: IUser.Type,
			request: DeskRequest.Type,
			status: boolean,
			createdAt?: Date,
			updatedAt?: Date,
		}
	}

}
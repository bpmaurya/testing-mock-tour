import { Floor } from "MyModels";
import Log from "../../vendor/utils";
import http from "./http";
import tokenService from "./token.service";

const TAG = 'inside-floor-service'

export class FloorService {
	async getFloorsWithLocation(location?: string) {
		return http
			.get(`/api/admin/all-floor/${location}`, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response.data)
				return response.data;
			});
	}
	async getFloors() {
		return http
			.get(`/api/admin/all-floor`, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response.data)
				return response.data;
			});
	}

	async createFloor(name: string, location: string, status?: boolean) {
		return http
			.post('/api/admin/create-floor', {
				name,
				location,
				status
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				return response.data;
			});
	}

	async deleteFloor(id: string) {
		return http
			.post('/api/admin/delete-floor', {
				id
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data
			})
	}
	async updateFloor(data: Floor.Type) {
		return http.post('/api/admin/update-floor', {
			id: data._id,
			name: data.name,
			location: data.location._id,
			status: data.status
		}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data
			})
	}
}

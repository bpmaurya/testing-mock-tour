import { DeskLocation } from 'MyModels';
import Log from '../../vendor/utils';
import http from './http';
import tokenService from './token.service';

const TAG = 'inside-location-service'

export class LocationService {
	async getLocation() {
		return http
			.get('/api/admin/all-location', { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				return response.data;
			});
	}

	async createLocation(name: string, status?: boolean) {
		return http
			.post('/api/admin/create-location', {
				name,
				status
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				return response.data;
			});
	}

	async deleteLocation(id: string) {
		return http
			.post('/api/admin/delete-location', {
				id
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data
			})
	}

	async updateLocation(data: DeskLocation) {
		return http
			.post('/api/admin/location-update', {
				id: data._id,
				name: data.name,
				status: data.status
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data
			})
	}


}

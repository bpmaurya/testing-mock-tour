import { Wing } from "MyModels";
import Log from "../../vendor/utils";
import http from "./http";
import tokenService from "./token.service";

const TAG = 'inside-wing-service'

export class WingService {
	async getWings() {
		return http
			.get('/api/admin/all-wings', { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response.data)
				return response.data;
			});
	}

	async getWingsForLocationAndFloor(location: string, floor: string) {
		return http
			.get(`/api/admin/all-wings/${location}/${floor}`, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response.data)
				return response.data;
			});
	}

	async createWing(name: string, location: string, floor: string, status?: boolean) {
		return http
			.post('/api/admin/create-wing', {
				name,
				location,
				floor,
				status
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				return response.data;
			});
	}

	async deleteWing(id: string) {
		return http
			.post('/api/admin/delete-wing', {
				id
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data
			})
	}
	async updateWing(data: Wing.Type) {
		return http.post('/api/admin/update-wing', {
			id: data._id,
			name: data.name,
			location: data.location._id,
			floor: data.floor._id,
			status: data.status
		}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data
			})
	}
}

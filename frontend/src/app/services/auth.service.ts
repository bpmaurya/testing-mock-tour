import http from './http';
import TokenService from './token.service';

export class AuthService {
	async login(email: string, password: string) {
		return http
			.post('/api/user/login', {
				email,
				password
			})
			.then((response: any) => {
				if (response.data.accessToken) {
					TokenService.setUser({ ...response.data });
				}

				return response.data;
			});
	}

	async changePassword(password: string, _id: string, currentPass: string) {
		return http
			.post(`/api/user/update-password/${_id}`, {
				password,
				currentPass
			})
			.then((response: any) => {
				if (response.data.accessToken) {
					TokenService.setUser({ ...response.data });
				}
				return response.data;
			});
	}

	logout() {
		TokenService.removeUser();
		window.location.reload()
	}


	async register(email: string, firstName: string,
		middleName: string,
		lastName: string,
		pastExperience: string | number,
		domain: string,
		department: string, password: string, role: string, position: string, project: string, phone: number) {
		return http
			.post('/api/user/register', {
				email,
				firstName,
				middleName,
				lastName,
				pastExperience,
				domain,
				department,
				password,
				role,
				position,
				project,
				phone

			})
			.then((response: any) => {
				return response.data;
			});
	}
}
export const authService = new AuthService()
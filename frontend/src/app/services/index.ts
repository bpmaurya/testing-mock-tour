import { DesignationService } from './designation.service';
import { DepartmentService } from './department.service';
import { AuthService } from "./auth.service";
import { LocationService } from "./location.service";
import { Navigation } from "./navigate";
import { DeskService } from './desk.service';
import { FloorService } from './floor.service';
import { NotificationService } from './notification.service';
import { ProjectService } from './project.service';
import { UserService } from './user.service';
import { RequestService } from './request.service';
import { WingService } from './wing.service';
import { DummyAPI } from './_dummy.service';


export type AppDep = {
	navigation: Navigation.Bloc
	socketContext: any
}

export type AppService = {
	locationService: LocationService,
	authService: AuthService,
	departmentService: DepartmentService,
	designationService: DesignationService,
	deskService: DeskService,
	floorService: FloorService,
	notificationService: NotificationService,
	projectService: ProjectService,
	requestService: RequestService,
	userService: UserService,
	wingService: WingService
}


export const appService = (): AppService => {
	if (process.env.REACT_APP_TEST === 'true') {
		let dummyApi = new DummyAPI();
		return {
			locationService: dummyApi,
			authService: dummyApi,
			departmentService: dummyApi,
			designationService: dummyApi,
			deskService: dummyApi,
			floorService: dummyApi,
			notificationService: dummyApi,
			projectService: dummyApi,
			requestService: dummyApi,
			userService: dummyApi,
			wingService: dummyApi
		}
	}
	else return {
		locationService: new LocationService(),
		authService: new AuthService(),
		departmentService: new DepartmentService(),
		designationService: new DesignationService(),
		deskService: new DeskService(),
		floorService: new FloorService(),
		notificationService: new NotificationService(),
		projectService: new ProjectService(),
		requestService: new RequestService(),
		userService: new UserService(),
		wingService: new WingService()
	}

};


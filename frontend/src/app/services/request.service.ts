import { DeskRequest } from "MyModels";
import Log from "../../vendor/utils";
import http from "./http";
import tokenService from "./token.service";

const TAG = 'inside-request-service'

export class RequestService {
	async getBookedRequests() {
		return http
			.get('/api/admin/all-deskrequest', { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response.data)
				return response.data;
			});
	}

	async getBookedRequestsForUser(_id: string) {
		return http
			.get(`/api/admin/all-desk-request/${_id}`, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response.data)
				return response.data;
			});
	}

	async createDeskRequest(data: DeskRequest.Type) {
		return http.post('/api/admin/create-request', {
			user: data.user._id,
			seat: data.seat._id,
			reportingManager: data.reportingManager._id!,
			project: data.project._id,
			priority: data.priority,
			description: data.description,
			status: data.status
		}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response.data)
				return response.data
			})
	}

	async updateDeskRequest(data: DeskRequest.Type) {
		return http.post('/api/admin/update-request', {
			_id: data._id,
			user: data.user._id,
			seat: data.seat._id,
			reportingManager: data.reportingManager._id,
			project: data.project._id,
			priority: data.priority,
			description: data.description,
			status: data.status
		}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response
			})
	}

}
import { BehaviorSubject } from 'rxjs'
import Log from '../../vendor/utils';
import { History } from 'history'
import autoBind from 'auto-bind';
const TAG = 'inside navigation'

export namespace Navigation {
	export const Path = {
		Root: '/',
		User: '/dashboard'
	}

	export const PREFIX = {
		ViewStaff: '/dashboard/view/staff/',
		ViewAdmin: '/dashboard/view/admin/',
	};

	export const NoAuthRequiredPathList = [
		Path.Root,
	];

	export function getBaseUrl(): string {
		return `${window.location.origin.toString()}`;
	}

	export class Bloc {
		private _currentPath: BehaviorSubject<string> = new BehaviorSubject('/');

		static create(history: History): Bloc {
			Log.d(TAG, 'inside create');
			let bloc = new Bloc(history);
			return bloc;
		}

		private constructor(private _history: History) {
			Log.d(`${TAG} ${_history}`);
			autoBind(this);
			this._history.listen((e: any) => this._currentPath.next(e.pathname));
		}

		get currentPath(): BehaviorSubject<string> {
			return this._currentPath;
		}

		public goToPath(path: string): void {
			Log.d(TAG, 'inside goToPath', path);
			this._history.push(path);
		}

		public goBack(): void {
			Log.d(TAG, 'inside goBack');
			this._history.back()
		}

	}

}
import { Designation } from 'MyModels';
import Log from '../../vendor/utils';
import http from './http';
import tokenService from './token.service';

const TAG = 'inside-designation-service'

export class DesignationService {
	async getDesignation() {
		return http
			.get('/api/admin/all-designation', { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				return response.data;
			});
	}

	async createDesignation(data: Designation.Type) {
		return http
			.post('/api/admin/create-designation', {
				name: data.name,
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				return response.data;
			});
	}

	async deleteDesignation(id: string) {
		return http
			.post('/api/admin/delete-designation', {
				id
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data
			})
	}

	async updateDesignation(data: Designation.Type) {
		return http
			.post('/api/admin/update-designation', {
				id: data._id,
				name: data.name,
				status: data.status
			}, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response)
				return response.data
			})
	}

}
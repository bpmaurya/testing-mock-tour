import Log from "../../vendor/utils";
import http from "./http";
import tokenService from "./token.service";

const TAG = 'inside-notification-service'

export class NotificationService {
	async getAllNotification(id: string) {
		return http
			.get(`/api/admin/get-all-notification/${id}`, { headers: { "x-access-token": tokenService.getLocalAccessToken() } })
			.then((response: any) => {
				Log.d(TAG, response.data)
				return response.data;
			});
	}
}

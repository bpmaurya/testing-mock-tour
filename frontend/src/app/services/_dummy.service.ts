import { Department, Designation, Desk, DeskLocation, DeskRequest, Floor, IUser, Project, Wing } from "MyModels";
import { AuthService } from "./auth.service";
import { DepartmentService } from "./department.service";
import { DesignationService } from "./designation.service";
import { DeskService } from "./desk.service";
import { DeskRowComponent } from "../component/desk/DesksComponent";
import { FloorService } from "./floor.service";
import { LocationService } from "./location.service";
import { NotificationService } from "./notification.service";
import { ProjectService } from "./project.service";
import { RequestService } from "./request.service";
import { UserService } from "./user.service";
import { WingService } from "./wing.service";
import { Dummy } from "../../vendor/dummy_data";

export class DummyAPI implements
	AuthService,
	DepartmentService,
	DesignationService,
	DeskService,
	FloorService,
	LocationService,
	NotificationService,
	ProjectService,
	RequestService,
	UserService,
	WingService {
	changePassword(password: string): Promise<any> {
		throw new Error("Method not implemented.");
	}

	//dummy wingService
	async getWings(): Promise<any> {
		return { "allWing": Dummy.AllWings }
	}
	async getWingsForLocationAndFloor(location: string, floor: string): Promise<any> {
		return { "allWing": Dummy.AllWings }
	}
	async createWing(name: string, location: string, floor: string, status?: boolean | undefined): Promise<any> {
		return true;
	}
	async deleteWing(id: string): Promise<any> {
		return true;
	}
	async updateWing(data: Wing.Type): Promise<any> {
		return true;
	}

	//dummy userService
	async getUser(): Promise<any> {
		return { 'alluser': Dummy.AllUsers };
	}
	async updateUser(data: IUser.Type): Promise<any> {
		return true;
	}
	async deleteUser(objectId: string): Promise<any> {
		return true;
	}


	//dummy requestService
	async getBookedRequests(): Promise<any> {
		return { 'allRequest': Dummy.AllRequest };
	}
	async getBookedRequestsForUser(_id: string): Promise<any> {
		return { 'allRequest': Dummy.AllRequest };
	}
	async createDeskRequest(data: DeskRequest.Type): Promise<any> {
		return true;
	}
	async updateDeskRequest(data: DeskRequest.Type): Promise<any> {
		return true;
	}

	//dummy projectService
	async getProjects(): Promise<any> {
		return { 'allProject': Dummy.AllProject }
	}
	async createProjects(data: Project.Type): Promise<any> {
		return true;
	}
	async deleteProject(id: string): Promise<any> {
		return true;
	}
	async updateProject(data: Project.Type): Promise<any> {
		return true;
	}

	//dummy notificationService
	getAllNotification(id: string): Promise<any> {
		throw new Error("Method not implemented.");
	}

	//dummy locationService
	async getLocation(): Promise<any> {
		return { "allLocation": Dummy.AllLocation }
	}

	async createLocation(name: string, status?: boolean | undefined): Promise<any> {
		return true;
	}
	async deleteLocation(id: string): Promise<any> {
		return true;
	}
	async updateLocation(data: DeskLocation): Promise<any> {
		return true;
	}

	//dummy floorService
	async getFloorsWithLocation(location?: string | undefined): Promise<any> {
		return { 'allFloor': Dummy.AllFloor }
	}
	async getFloors(): Promise<any> {
		return { "allFloor": Dummy.AllFloor }
	}
	async createFloor(name: string, location: string, status?: boolean | undefined): Promise<any> {
		return true;
	}
	async deleteFloor(id: string): Promise<any> {
		return true;
	}
	async updateFloor(data: Floor.Type): Promise<any> {
		return true;
	}

	//dummy deskService
	async createDesk(row: string, start: number, end: number, location: string, floor: string, wing: string, status?: boolean | undefined): Promise<any> {
		return true;
	}
	async getDesks(location: string, floor: string, wing: string): Promise<any> {
		return { 'allSeat': Dummy.AllDesk };
	}
	async deleteDesk(location: string, wing: string, floor: string, row: string): Promise<any> {
		return true;
	}
	async updateDesk(data: Desk.Type): Promise<any> {
		return true;
	}
	async updateDeskOrder(data: DeskRowComponent[]): Promise<any> {
		return true;
	}
	async createDeskBookRequest(data: Desk.Type): Promise<any> {
		return true;
	}

	//dummy designationService
	async getDesignation(): Promise<any> {
		return { "allDesignation": Dummy.AllDesignation };
	}
	async createDesignation(data: Designation.Type): Promise<any> {
		return true;
	}
	async deleteDesignation(id: string): Promise<any> {
		return true;
	}
	async updateDesignation(data: Designation.Type): Promise<any> {
		return true;
	}

	//dummy departmentService
	async getDepartments(): Promise<any> {
		return { 'allDepartment': Dummy.AllDepartment }
	}
	async createDepartment(data: Department.Type): Promise<any> {
		return true;
	}
	async deleteDepartment(id: string): Promise<any> {
		return true;
	}
	async updateDepartment(data: Department.Type): Promise<any> {
		return true;
	}


	//dummy auth service 
	login(email: string, password: string): Promise<any> {
		throw new Error("Method not implemented.");
	}
	logout(): void {
		throw new Error("Method not implemented.");
	}
	register(email: string, firstName: string, middleName: string, lastName: string, pastExperience: string | number, domain: string, department: string, password: string, role: string, position: string, project: string, phone: number): Promise<any> {
		throw new Error("Method not implemented.");
	}

}